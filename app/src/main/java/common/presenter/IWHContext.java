package common.presenter;

import android.content.Context;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public interface IWHContext {
    Context getActivityContext();
    void showLoading();
    void hideLoading();
}
