package common.presenter;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.StringRes;

import quangph.com.mvp.mvp.IView;
import quangph.com.mvp.mvp.mvpcomponent.MVPActivity;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/8/2019.
 */
public class WHActivity<V extends IView> extends MVPActivity<V> implements IWHContext {

    private LoadingDialogFragment mLoadingDlg;

    @Override
    protected void onViewDidCreated() {
        super.onViewDidCreated();

        View back = findViewById(R.id.action_bar_ic_back);
        if (back != null) {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onActionBarBackPressed();
                }
            });
        }
    }

    @Override
    protected Thread.UncaughtExceptionHandler getUncaughtExceptionHandler() {
        return null;
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void showLoading() {
        if (mLoadingDlg == null) {
            mLoadingDlg = new LoadingDialogFragment();
        }
        mLoadingDlg.show(getSupportFragmentManager(), getClass().getCanonicalName());
    }

    @Override
    public void hideLoading() {
        if (mLoadingDlg != null && !mLoadingDlg.isRemoving()) {
            mLoadingDlg.dismiss();
        }
    }

    protected void onActionBarBackPressed() {
        onBackPressed();
    }

    public void ToastLong(@StringRes int msgResId) {
        Toast.makeText(this, getString(msgResId), Toast.LENGTH_LONG).show();
    }
}
