package common.presenter;

import android.content.Context;

import java.lang.ref.WeakReference;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import vcc.com.wowholiday.repository.api.APIException;

/**
 * Created by Pham Hai Quang on 10/08/2019.
 */
public class WHApiActionCallback<T> extends Action.SimpleActionCallback<T> {

    private WeakReference<IWHContext> mWeakContext;

    public WHApiActionCallback(IWHContext context) {
        mWeakContext = new WeakReference<>(context);
    }

    @Override
    public void onSuccess(T responseValue) {
        super.onSuccess(responseValue);
    }

    @Override
    public void onError(ActionException e) {
        IWHContext context = mWeakContext.get();
        if (context == null) {
            return;
        }
        context.hideLoading();
        Throwable cause = e.getCause();
        if (cause instanceof APIException) {
            if (((APIException) cause).getCode() == APIException.AUTHEN_ERROR) {
                // TODO authen failed error
            } else if (((APIException) cause).getCode() == APIException.NETWORK_ERROR) {
                // TODO handle network failed error
            } else {
                onException(context.getActivityContext(), (APIException) cause);
            }
        } else {
            onUnhandledException(cause);
        }
    }

    protected void onException(Context context, APIException e) {
        /*if (e.getMessage().isEmpty()) {
        } else {
        }*/
    }

    protected void onUnhandledException(Throwable cause) {
    }
}
