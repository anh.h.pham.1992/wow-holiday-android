package common.presenter;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.StringRes;

import quangph.com.mvp.mvp.IView;
import quangph.com.mvp.mvp.mvpcomponent.MVPFragment;

/**
 * Created by Pham Hai Quang on 10/8/2019.
 */
public class WHFragment<V extends IView> extends MVPFragment<V> implements IWHContext {

    private LoadingDialogFragment mLoadingDlg;

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showLoading() {
        if (mLoadingDlg == null) {
            mLoadingDlg = new LoadingDialogFragment();
        }

        if (getFragmentManager() != null) {
            mLoadingDlg.show(getFragmentManager(), getClass().getCanonicalName());
        }
    }

    @Override
    public void hideLoading() {
        if (mLoadingDlg != null && !mLoadingDlg.isRemoving()) {
            mLoadingDlg.dismiss();
        }
    }

    public void ToastLong(@StringRes int msgResId) {
        Toast.makeText(getContext(), getString(msgResId), Toast.LENGTH_LONG).show();
    }
}
