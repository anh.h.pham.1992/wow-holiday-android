package common.presenter;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import quangph.com.mvp.mvp.IView;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class WHTabFragment<V extends IView> extends WHFragment<V> {

    private Config mConfig;

    @Override
    protected void onShow(boolean isShowing, int flag) {
        super.onShow(isShowing, flag);
        OnShowFragmentCmd cmd = new OnShowFragmentCmd();
        cmd.isShow = isShowing;
        cmd.fragment = this;
        if (getParentPresenter() != null) {
            getParentPresenter().executeCommand(cmd);
        }
    }

    @Override
    public IPresenter getParentPresenter() {
        return (IPresenter) getActivity();
    }

    public final Config getConfig() {
        if (mConfig == null) {
            mConfig = onCreateConfig();
        }
        return mConfig;
    }

    public Config onCreateConfig() {
        return null;
    }

    public static class OnShowFragmentCmd implements ICommand{
        public boolean isShow;
        public WHTabFragment fragment;
    }

    public static class Config {}
}
