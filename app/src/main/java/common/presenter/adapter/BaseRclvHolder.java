package common.presenter.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Pham Hai Quang on 1/11/2019.
 */
public class BaseRclvHolder<T> extends RecyclerView.ViewHolder {
    public BaseRclvHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void onBind(T vhData) {}
}
