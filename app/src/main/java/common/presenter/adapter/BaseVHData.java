package common.presenter.adapter;

/**
 * Created by Pham Hai Quang on 1/24/2019.
 */
public class BaseVHData<T> {
    public int type;
    public T realData;
    public BaseVHData(T data) {
        realData = data;
    }

    public BaseVHData() {
    }
}
