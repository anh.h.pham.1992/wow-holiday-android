package common.presenter.adapter.decor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Pham Hai QUANG on 9/21/2016.
 */
public class DividerDecorator extends RecyclerView.ItemDecoration {

    private Drawable mDivider;
    private int mLeft, mTop, mRight, mBottom;

    public DividerDecorator() {
    }

    public DividerDecorator(Context context, int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
    }

    public DividerDecorator(Drawable drawable) {
        mDivider = drawable;
    }

    public void setPadding(int left, int top, int right, int bottom) {
        mLeft = left;
        mTop = top;
        mRight = right;
        mBottom = bottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        if (parent.getChildAdapterPosition(view) == 0) {
            return;
        }

        outRect.top = mDivider.getIntrinsicHeight();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int dividerLeft = parent.getPaddingLeft() + mLeft;
        int dividerRight = parent.getWidth() - parent.getPaddingRight() - mRight;
        int count = parent.getChildCount();
        for (int i = 0; i < count - 1; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int dividerTop = child.getBottom() + params.bottomMargin + mTop;
            int dividerBottom = dividerTop + mDivider.getIntrinsicHeight() - mBottom;
            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            mDivider.draw(c);
        }
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        int dividerLeft = parent.getPaddingLeft() + mLeft;
        int dividerRight = parent.getWidth() - parent.getPaddingRight() - mRight;
        int count = parent.getChildCount();
        for (int i = 0; i < count - 1; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int dividerTop = child.getBottom() + params.bottomMargin + mTop;
            int dividerBottom = dividerTop + mDivider.getIntrinsicHeight() - mBottom;
            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            mDivider.draw(c);
        }
    }

    public Drawable getDrawable() {
        return mDivider;
    }
}
