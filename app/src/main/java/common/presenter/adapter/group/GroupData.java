package common.presenter.adapter.group;

import android.view.View;

import common.presenter.adapter.BaseRclvHolder;


/**
 * Created by Pham Hai Quang on 7/27/2019.
 */
public abstract class GroupData<T> {
    public static final int INVALID_RESOURCE = -1;
    public T data;
    public int adapterPosition = -1;
    protected GroupManager mManager;

    public GroupData(GroupManager manager, T data) {
        mManager = manager;
        this.data = data;
    }

    public int getLayoutResource(int viewType) {
        return INVALID_RESOURCE;
    }

    public int getItemViewType(int positionInGroup) {
        return 0;
    }

    public void attach() {
        adapterPosition = mManager.findAdapterPositionForGroup(this);
    }

    public void detach() {
        adapterPosition = -1;
    }

    public boolean isAttached() {
        return adapterPosition > -1;
    }

    public void notifyRemove(int groupPosition) {
        if (isAttached()) {
            mManager.notifyRemove(this,adapterPosition + groupPosition);
        }
    }

    public void notifyRemove(int groupPosition, int count) {
        if (isAttached()) {
            mManager.notifyRemove(this, adapterPosition + groupPosition, count);
        }
    }

    public void notifyInserted(int groupPosition, int count) {
        if (count <= 0) {
            return;
        }

        if (!isAttached()) {
            attach();
        }
        mManager.notifyInserted(this, adapterPosition + groupPosition, count);
    }

    public void notifyChanged(int groupPosition) {
        if (!isAttached()) {
            attach();
        }
        mManager.notifyChanged(this, adapterPosition + groupPosition);
    }

    public abstract Object getDataInGroup(int positionInGroup);
    public abstract int getCount();
    public abstract BaseRclvHolder onCreateVH(View itemView, int viewType);

}
