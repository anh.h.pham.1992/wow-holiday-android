package common.presenter.adapter.group;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import common.presenter.adapter.BaseRclvHolder;


/**
 * Created by Pham Hai Quang on 7/27/2019.
 */
public class GroupManager {

    private RecyclerView.Adapter mAdapter;
    private List<GroupData> mDataSet = new ArrayList<>();

    public GroupManager(RecyclerView.Adapter adapter) {
        mAdapter = adapter;
    }

    public void addGroupData(GroupData data) {
        mDataSet.add(data);
        if (data.getCount() == 0) {
            data.detach();
        } else if (!data.isAttached()){
            data.attach();
        }
    }

    public void addGroupDataAtIndex(int index, GroupData data) {
        mDataSet.add(index, data);
        if (data.getCount() == 0) {
            data.detach();
        } else if (!data.isAttached()){
            data.attach();
        }
    }

    public List<GroupData> getDataSet() {
        return mDataSet;
    }

    public int getItemCount() {
        int total = 0;
        for (GroupData data : mDataSet) {
            total += data.getCount();
        }
        return total;
    }

    public int getLayoutResource(int viewType) {
        for (GroupData data : mDataSet) {
            int layoutResource = data.getLayoutResource(viewType);
            if (layoutResource != GroupData.INVALID_RESOURCE) {
                return layoutResource;
            }
        }
        throw new IllegalArgumentException("Can not find layout for type: " + viewType);
    }

    public int getItemViewType(int adapterPosition) {
        GroupData data = findGroupDataByAdapterPosition(adapterPosition);
        if (data != null) {
            return data.getItemViewType(adapterPosition - data.adapterPosition);
        } else {
            throw new IllegalArgumentException("Can not find data for position: " + adapterPosition);
        }
    }

    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        for (GroupData data : mDataSet) {
            BaseRclvHolder vh = data.onCreateVH(itemView, viewType);
            if (vh != null) {
                return vh;
            }
        }
        throw new IllegalArgumentException("Can not find ViewHolder for type: " + viewType);
    }

    public void onBindViewHolder(@NonNull GroupRclvVH vh, int position) {
        vh.groupData = findGroupDataByAdapterPosition(position);
        vh.onBind(getItemDataAtAdapterPosition(position));
    }

    public Object getItemDataAtAdapterPosition(int adapterPosition) {
        GroupData data = findGroupDataByAdapterPosition(adapterPosition);
        if (data != null) {
            return data.getDataInGroup(adapterPosition - data.adapterPosition);
        } else {
            throw new IllegalArgumentException("Can not find data for position: " + adapterPosition);
        }
    }

    public void notifyRemove(GroupData group, int adapterPosition) {
        shiftAdapterPosition(group, -1);
        mAdapter.notifyItemRemoved(adapterPosition);
        checkToDetach(group);
    }

    public void notifyRemove(GroupData group, int adapterPosition, int count) {
        shiftAdapterPosition(group, -count);
        mAdapter.notifyItemRangeRemoved(adapterPosition, count);
        checkToDetach(group);
    }

    public void notifyInserted(GroupData group, int adapterPosition, int count) {
        shiftAdapterPosition(group, count);
        mAdapter.notifyItemRangeInserted(adapterPosition, count);
    }

    public void notifyChanged(GroupData group, int adapterPosition) {
        mAdapter.notifyItemChanged(adapterPosition);
    }

    public void notifyDataSetChanged() {
        for (GroupData group : mDataSet) {
            if (group.isAttached() && group.getCount() <= 0) {
                group.detach();
            } else {
                if (group.getCount() > 0) {
                    group.attach();
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    public GroupData findGroupDataByAdapterPosition(int adapterPosition) {
        for (GroupData data : mDataSet) {
            if (data.adapterPosition <= adapterPosition && adapterPosition < data.adapterPosition + data.getCount()) {
                return data;
            }
        }
        return null;
    }

    public int findAdapterPositionForGroup(GroupData group) {
        int index = mDataSet.indexOf(group);
        if (index > 0) {
            for (int i = index - 1; i >= 0; i--) {
                GroupData prev = mDataSet.get(i);
                if (prev.isAttached()) {
                    return prev.adapterPosition + prev.getCount();
                }
            }
            return 0;
        } else if (index == 0){
            return 0;
        } else {
            throw new IllegalArgumentException("The GroupData is not added");
        }
    }

    private void shiftAdapterPosition(GroupData startGroup, int count) {
        int startIndex = mDataSet.indexOf(startGroup);
        for (int i = startIndex + 1; i < mDataSet.size(); i++) {
            GroupData next = mDataSet.get(i);
            if (next.isAttached()) {
                next.adapterPosition += count;
            }
        }
    }

    private void checkToDetach(GroupData group) {
        if (group.getCount() == 0) {
            group.detach();
        }
    }
}
