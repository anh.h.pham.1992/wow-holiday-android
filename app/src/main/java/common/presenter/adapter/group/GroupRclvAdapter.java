package common.presenter.adapter.group;

import android.view.View;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;

/**
 * Created by Pham Hai Quang on 7/27/2019.
 */
public class GroupRclvAdapter extends BaseRclvAdapter {
    protected GroupManager mGroupManager;

    public GroupRclvAdapter() {
        mGroupManager = new GroupManager(this);
    }

    @Override
    public int getItemCount() {
        return mGroupManager.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return mGroupManager.getItemViewType(position);
    }

    @Override
    public int getLayoutResource(int viewType) {
        return mGroupManager.getLayoutResource(viewType);
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return mGroupManager.onCreateVH(itemView, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseRclvHolder baseRclvHolder, int position) {
        if (baseRclvHolder instanceof GroupRclvVH) {
            mGroupManager.onBindViewHolder((GroupRclvVH) baseRclvHolder, position);
        } else {
            super.onBindViewHolder(baseRclvHolder, position);
        }
    }

    @Override
    public Object getItemDataAtPosition(int position) {
        return mGroupManager.getItemDataAtAdapterPosition(position);
    }

    public void addGroup(GroupData data) {
        mGroupManager.addGroupData(data);
    }

    public void addGroupDataAtIndex(int index, GroupData data) {
        mGroupManager.addGroupDataAtIndex(index, data);
    }
}
