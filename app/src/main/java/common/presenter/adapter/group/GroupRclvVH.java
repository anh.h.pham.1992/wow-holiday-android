package common.presenter.adapter.group;

import android.view.View;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvHolder;

public class GroupRclvVH<T, GD extends GroupData> extends BaseRclvHolder<T> {

    public GD groupData;

    public GroupRclvVH(@NonNull View itemView) {
        super(itemView);
    }
}
