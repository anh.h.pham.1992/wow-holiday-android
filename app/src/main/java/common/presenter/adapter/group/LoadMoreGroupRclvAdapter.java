package common.presenter.adapter.group;

import android.view.View;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 9/7/2019.
 */
public class LoadMoreGroupRclvAdapter extends GroupRclvAdapter {

    public static final int LOADING_TYPE = -1;
    private LoadMoreGroup mLoadMoreGroup;

    public LoadMoreGroupRclvAdapter(boolean enableLoadMore) {
        mLoadMoreGroup = new LoadMoreGroup(mGroupManager, null);
        mLoadMoreGroup.enableLoadMore = enableLoadMore;
        addGroup(mLoadMoreGroup);
    }

    public void enableLoadMore(boolean enableLoadMore) {
        if (enableLoadMore) {
            if (!mLoadMoreGroup.enableLoadMore) {
                mLoadMoreGroup.enableLoadMore = true;
                mLoadMoreGroup.notifyInserted(0, 1);
            }
        } else {
            if (mLoadMoreGroup.enableLoadMore) {
                mLoadMoreGroup.enableLoadMore = false;
                mLoadMoreGroup.notifyRemove(0);
            }
        }
    }

    @Override
    public void addGroup(GroupData data) {
        if (!(data instanceof LoadMoreGroup)) {
            addGroupDataAtIndex(mGroupManager.getDataSet().size() - 1, data);
        } else {
            addGroupDataAtIndex(0, data);
        }
    }

    private class LoadMoreGroup extends GroupData<Void> {
        private boolean enableLoadMore;

        public LoadMoreGroup(GroupManager manager, Void data) {
            super(manager, data);
        }

        @Override
        public Object getDataInGroup(int positionInGroup) {
            return null;
        }

        @Override
        public int getCount() {
            return enableLoadMore ? 1 : 0;
        }

        @Override
        public int getItemViewType(int positionInGroup) {
            return LOADING_TYPE;
        }

        @Override
        public int getLayoutResource(int viewType) {
            return R.layout.item_loading;
        }

        @Override
        public BaseRclvHolder onCreateVH(View itemView, int viewType) {
            return new LoadingVH(itemView);
        }
    }

    private class LoadingVH extends BaseRclvHolder<Void> {
        public LoadingVH(@NonNull View itemView) {
            super(itemView);
        }
    }
}
