package vcc.com.wowholiday;

import android.app.Application;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */
public class WHApplication extends Application {
    private static WHApplication sInstances;

    public static WHApplication getInstance() {
        return sInstances;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstances = this;
    }
}
