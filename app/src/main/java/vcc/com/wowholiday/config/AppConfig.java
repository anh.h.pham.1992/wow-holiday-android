package vcc.com.wowholiday.config;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */
public class AppConfig {
    public static final int PASSWORD_LENGTH_MIN = 6;

    public interface APIConfig {
        String OS = "android";
        String API_SECRET_KEY = "acbec103badcfa561fdf9eb71280a7a4";
        String API_KEY = "20daa253eae0f32b04ba883d42c4056b";
        //String BASE_URL = "http://preprod-coreapi.travelcarma.com";
        String BASE_URL = "http://preprod-coreapi.wowholiday.vn";

        String REGISTER = "/api/v1/user/signup";
        String LOGIN = "/api/v1/user/login";
        String UPDATE_PROFILE = "/api/v1/user/update";
        String AIR_LOCATION_SEARCH = "/api/v1/air/search/location";
        String AIR_SEARCH = "/api/v1/air/search/air_search_mock";
        String ACTIVITIES_LOCATION_SEARCH = "/api/v1/activity/search/location";
        String ACTIVITIES_SEARCH = "/api/v1/activity/search/activity_search";
        String ACTIVITIES_DETAIL = "/api/v1/activity/details/act_detail";
        String HOTEL_LOCATION_SEARCH = "/api/v1/hotel/search/location";
        String HOTEL_SEARCH = "/api/v1/hotel/search";
        String HOTEL_DETAILS = "/api/v1/hotel/details";
    }

    public interface AirFilterName {
        String STOP = "STOP";
        String START_TIME = "START_TIME";
        String END_TIME = "END_TIME";
        String AIR = "AIR";
        String PRICE = "PRICE";
    }

    public interface AirSortName {
        String START_TIME = "START_TIME";
        String END_TIME = "END_TIME";
        String DURATION = "DURATION";
        String PRICE = "PRICE";
    }

    public interface HotelFilterName {
        String NAME = "NAME";
        String CURRENCY = "CURRENCY";
        String PRICE_RANGE = "PRICE_RANGE";
        String STAR_RATING = "STAR_RATING";
        String CATEGORY = "CATEGORY";
    }

    public interface HotelSortName {
        String NAME = "NAME";
        String RATE = "RATE";
        String STAR_RATING = "STAR_RATING";
        String PROMOTIONS = "PROMOTIONS";
    }
}
