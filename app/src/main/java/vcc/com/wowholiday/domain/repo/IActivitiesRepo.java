package vcc.com.wowholiday.domain.repo;

import java.util.List;

import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.model.dto.ActivitiesLocationDto;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */
public interface IActivitiesRepo {
    void searchLocation(String authen, String locationName, ICallback<ActivitiesLocationDto> callback);
    void searchActivities(ICallback<List<ActivitiesInfo>> callback);
    void getActivitiesDetail(String authen, String token, String activitiesID, ICallback<ActivitiesDetail> callback);
}
