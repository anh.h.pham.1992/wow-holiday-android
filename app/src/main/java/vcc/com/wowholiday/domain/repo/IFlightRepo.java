package vcc.com.wowholiday.domain.repo;

import java.util.List;

import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightDataDTO;
import vcc.com.wowholiday.model.air.FlightTrip;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public interface IFlightRepo {
    void getLocationSuggestion(String authen, String city, ICallback<List<Location>> callback);
    void searchAir(String authen, FlightBookingProperty property, ICallback<FlightDataDTO> callback);
}
