package vcc.com.wowholiday.domain.repo;

import java.util.List;

import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.model.hotel.HotelSearchDTO;
import vcc.com.wowholiday.model.hotel.HotelSuggestionList;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;

public interface IHotelRepo {
    void getHotelSuggestion(String authen, String keyword, ICallback<List<HotelSuggestionList>> callback);
    void searchHotel(String authen, HotelSearchProperty property, ICallback<HotelSearchDTO> callback);
    void getHotelDetail(String authen, String token, String id, ICallback<HotelDetail> callback);
}
