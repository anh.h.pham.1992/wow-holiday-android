package vcc.com.wowholiday.domain.repo;

import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.model.User;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public interface IUserRepo {
    void registerEmail(String authen, String email, String firstName, String lastName, String password, String gender,
                       ICallback<User> callback);
    void registerPhone(String authen, String phone, String firstName, String lastName, String password, String gender,
                       ICallback<User> callback);
    void loginEmail(String authen, String email, String password, ICallback<User> callback);
    void loginPhone(String authen, String phone, String countryCode, String password, ICallback<User> callback);
    void update(String authen, User user, ICallback<Boolean> callback);
    void logout();
}
