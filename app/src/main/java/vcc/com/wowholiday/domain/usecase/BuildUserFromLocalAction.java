package vcc.com.wowholiday.domain.usecase;

import com.google.gson.Gson;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.User;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class BuildUserFromLocalAction extends Action<Action.VoidRequest, User> {

    @Override
    protected User onExecute(VoidRequest voidRequest) throws ActionException {
        String userJson = WHSharedPreference.getUser();
        if (userJson == null || userJson.length() == 0) {
            return null;
        } else {
            Gson gson = new Gson();
            return gson.fromJson(userJson, User.class);
        }
    }
}
