package vcc.com.wowholiday.domain.usecase;

import java.util.List;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IFlightRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightDataDTO;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.repository.FlightRepo;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class FlightSearchAction extends ThirdPartyAction<FlightSearchAction.FlightSearchRv, FlightDataDTO> {


    @Override
    protected void onCall(FlightSearchRv flightSearchRv, ICallback<FlightDataDTO> callback) {
        IFlightRepo repo = new FlightRepo();
        repo.searchAir(WHSharedPreference.getBasicToken(), flightSearchRv.property, callback);
    }

    public static class FlightSearchRv implements Action.RequestValue {
        public FlightBookingProperty property;
    }
}
