package vcc.com.wowholiday.domain.usecase;

import java.util.List;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IFlightRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.repository.FlightRepo;

import static vcc.com.wowholiday.domain.usecase.GetFlightLocationAction.RV;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class GetFlightLocationAction extends ThirdPartyAction<RV, List<Location>> {

    @Override
    protected void onCall(RV rv, ICallback<List<Location>> callback) {
        IFlightRepo repo = new FlightRepo();
        repo.getLocationSuggestion(WHSharedPreference.getBasicToken(), rv.city, callback);
    }

    @Override
    protected String createID() {
        return getRequestVal().city;
    }

    public static class RV implements Action.RequestValue {
        public String city;
    }
}
