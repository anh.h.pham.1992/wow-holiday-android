package vcc.com.wowholiday.domain.usecase;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IUserRepo;
import vcc.com.wowholiday.domain.validate.PhoneNumberValidate;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.repository.UserRepo;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class LoginEmailOrPhoneAction extends ThirdPartyAction<LoginEmailOrPhoneAction.LoginEmailRV, User> {

    @Override
    protected void onCall(LoginEmailRV loginEmailRV, ICallback<User> callback) {
        IUserRepo repo = new UserRepo();
        if (new PhoneNumberValidate().validate(loginEmailRV.emailOrPhone)) {
            repo.loginPhone(WHSharedPreference.getBasicToken(), loginEmailRV.emailOrPhone,
                    "+84", loginEmailRV.password, callback);
        } else {
            repo.loginEmail(WHSharedPreference.getBasicToken(), loginEmailRV.emailOrPhone,
                    loginEmailRV.password, callback);
        }
    }

    public static class LoginEmailRV implements Action.RequestValue {
        public String emailOrPhone;
        public String password;
    }
}
