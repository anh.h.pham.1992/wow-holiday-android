package vcc.com.wowholiday.domain.usecase;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import vcc.com.wowholiday.infras.WHSharedPreference;

/**
 * Created by QuangPH on 2020-02-18.
 */
public class LogoutAction extends Action<Action.VoidRequest, Boolean> {
    @Override
    protected Boolean onExecute(VoidRequest voidRequest) throws ActionException {
        WHSharedPreference.clearUser();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }
}
