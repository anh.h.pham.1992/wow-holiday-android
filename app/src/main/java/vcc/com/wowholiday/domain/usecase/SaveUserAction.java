package vcc.com.wowholiday.domain.usecase;

import com.google.gson.Gson;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.User;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class SaveUserAction extends Action<SaveUserAction.SaveUserRv, Void> {

    @Override
    protected Void onExecute(SaveUserRv saveUserRv) throws ActionException {
        Gson gson = new Gson();
        String jsonInString = gson.toJson(saveUserRv.user);
        WHSharedPreference.saveUser(jsonInString);
        return null;
    }

    public static class SaveUserRv implements Action.RequestValue {
        public User user;
    }
}
