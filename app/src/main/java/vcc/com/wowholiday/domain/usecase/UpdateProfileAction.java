package vcc.com.wowholiday.domain.usecase;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IUserRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.repository.UserRepo;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class UpdateProfileAction extends ThirdPartyAction<UpdateProfileAction.UpdateProfileRV, Boolean> {

    @Override
    protected void onCall(UpdateProfileRV updateProfileRV, ICallback<Boolean> callback) {
        IUserRepo repo = new UserRepo();
        repo.update(WHSharedPreference.getBasicToken(), updateProfileRV.user, callback);
    }

    public static class UpdateProfileRV implements Action.RequestValue {
        public User user;
    }
}
