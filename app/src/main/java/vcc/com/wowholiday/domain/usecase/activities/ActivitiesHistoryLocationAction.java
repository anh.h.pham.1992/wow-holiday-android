package vcc.com.wowholiday.domain.usecase.activities;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IActivitiesRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.dto.ActivitiesLocationDto;
import vcc.com.wowholiday.repository.ActivitiesRepo;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */
public class ActivitiesHistoryLocationAction extends ThirdPartyAction<Action.VoidRequest, ActivitiesLocationDto> {
    @Override
    protected void onCall(VoidRequest voidRequest, ICallback<ActivitiesLocationDto> callback) {
        IActivitiesRepo repo = new ActivitiesRepo();
        repo.searchLocation(WHSharedPreference.getBasicToken(), "", callback);
    }
}
