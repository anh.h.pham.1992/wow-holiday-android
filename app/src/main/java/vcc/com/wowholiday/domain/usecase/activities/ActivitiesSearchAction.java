package vcc.com.wowholiday.domain.usecase.activities;

import java.util.List;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IActivitiesRepo;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.repository.ActivitiesRepo;

/**
 * Created by Pham Hai Quang on 12/2/2019.
 */
public class ActivitiesSearchAction extends ThirdPartyAction<Action.VoidRequest, List<ActivitiesInfo>> {
    @Override
    protected void onCall(VoidRequest voidRequest, ICallback<List<ActivitiesInfo>> callback) {
        IActivitiesRepo repo = new ActivitiesRepo();
        repo.searchActivities(callback);
    }
}
