package vcc.com.wowholiday.domain.usecase.activities;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IActivitiesRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.repository.ActivitiesRepo;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class GetActivitiesDetailAction extends ThirdPartyAction<GetActivitiesDetailAction.ActivitiesDetailRq, ActivitiesDetail> {

    @Override
    protected void onCall(ActivitiesDetailRq activitiesDetailRq, ICallback<ActivitiesDetail> callback) {
        IActivitiesRepo repo = new ActivitiesRepo();
        repo.getActivitiesDetail(WHSharedPreference.getBasicToken(),
                activitiesDetailRq.token, activitiesDetailRq.id, callback);
    }

    public static class ActivitiesDetailRq implements Action.RequestValue {
        public String token;
        public String id;
    }
}
