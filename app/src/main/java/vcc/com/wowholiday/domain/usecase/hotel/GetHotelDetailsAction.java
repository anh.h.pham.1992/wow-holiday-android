package vcc.com.wowholiday.domain.usecase.hotel;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IHotelRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.repository.HotelRepo;

import static vcc.com.wowholiday.domain.usecase.hotel.GetHotelDetailsAction.RV;

public class GetHotelDetailsAction extends ThirdPartyAction<RV, HotelDetail> {

    @Override
    protected void onCall(RV rv, ICallback<HotelDetail> callback) {
        IHotelRepo repo = new HotelRepo();
        repo.getHotelDetail(WHSharedPreference.getBasicToken(), rv.token, rv.data, callback);
    }

    @Override
    protected String createID() {
        return getRequestVal().token;
    }

    public static class RV implements Action.RequestValue {
        public String token;
        public String data;
    }
}
