package vcc.com.wowholiday.domain.usecase.hotel;

import java.util.List;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IHotelRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.hotel.HotelSuggestionList;
import vcc.com.wowholiday.repository.HotelRepo;

import static vcc.com.wowholiday.domain.usecase.hotel.GetHotelSuggestionAction.RV;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class GetHotelSuggestionAction extends ThirdPartyAction<RV, List<HotelSuggestionList>> {

    @Override
    protected void onCall(RV rv, ICallback<List<HotelSuggestionList>> callback) {
        IHotelRepo repo = new HotelRepo();
        repo.getHotelSuggestion(WHSharedPreference.getBasicToken(), rv.keyword, callback);
    }

    @Override
    protected String createID() {
        return getRequestVal().keyword;
    }

    public static class RV implements Action.RequestValue {
        public String keyword;
    }
}
