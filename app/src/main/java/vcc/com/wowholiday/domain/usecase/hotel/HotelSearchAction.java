package vcc.com.wowholiday.domain.usecase.hotel;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IHotelRepo;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.hotel.HotelSearchDTO;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.repository.HotelRepo;

public class HotelSearchAction extends ThirdPartyAction<HotelSearchAction.HotelSearchRV, HotelSearchDTO> {
    @Override
    protected void onCall(HotelSearchRV rv, ICallback<HotelSearchDTO> callback) {
        IHotelRepo repo = new HotelRepo();
        repo.searchHotel(WHSharedPreference.getBasicToken(), rv.property, callback);
    }

    @Override
    protected String createID() {
        return getRequestVal().property.toString();
    }

    public static class HotelSearchRV implements Action.RequestValue {
        public HotelSearchProperty property;
    }
}
