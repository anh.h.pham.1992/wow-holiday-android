package vcc.com.wowholiday.domain.usecase.user;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyAction;
import quangph.com.mvp.mvp.domain.ICallback;
import vcc.com.wowholiday.domain.repo.IUserRepo;
import vcc.com.wowholiday.domain.validate.EmailValidate;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.repository.UserRepo;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class RegisterAction extends ThirdPartyAction<RegisterAction.RegisterReq, User> {

    @Override
    protected void onCall(RegisterReq registerReq, ICallback<User> callback) {
        IUserRepo repo = new UserRepo();
        if (new EmailValidate().validate(registerReq.emailOrPhone)) {
            repo.registerEmail(WHSharedPreference.getBasicToken(),
                    registerReq.emailOrPhone,
                    registerReq.firstName,
                    registerReq.lastName,
                    registerReq.password,
                    registerReq.gender,
                    callback);
        } else {
            repo.registerPhone(WHSharedPreference.getBasicToken(),
                    registerReq.emailOrPhone,
                    registerReq.firstName,
                    registerReq.lastName,
                    registerReq.password,
                    registerReq.gender,
                    callback);
        }
    }

    public static class RegisterReq implements Action.RequestValue {
        public String emailOrPhone;
        public String password;
        public String firstName;
        public String lastName;
        public String gender;
    }
}
