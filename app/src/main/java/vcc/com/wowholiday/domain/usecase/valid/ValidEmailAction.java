package vcc.com.wowholiday.domain.usecase.valid;

import android.text.TextUtils;

import quangph.com.mvp.mvp.action.Action;
import vcc.com.wowholiday.domain.validate.EmailValidate;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class ValidEmailAction extends Action<ValidEmailAction.ValidEmailReq, Boolean> {

    @Override
    protected Boolean onExecute(ValidEmailReq requestValue) throws ValidTextException {
        if (TextUtils.isEmpty(requestValue.email)) {
            throw new ValidTextException(ValidTextType.EMPTY);
        }

        if (new EmailValidate().validate(requestValue.email)) {
            return true;
        }

        throw new ValidTextException(ValidTextType.FORMAT);
    }

    public static class ValidEmailReq implements Action.RequestValue {
        public String email;
    }

}