package vcc.com.wowholiday.domain.usecase.valid;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import vcc.com.wowholiday.domain.validate.EmailValidate;
import vcc.com.wowholiday.domain.validate.PhoneNumberValidate;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */
public class ValidEmailOrPhoneNumberAction
        extends Action<ValidEmailOrPhoneNumberAction.ValidEmailOrPhoneReq, Boolean> {

    @Override
    protected Boolean onExecute(ValidEmailOrPhoneReq requestValue) throws ActionException {
        if (new PhoneNumberValidate().validate(requestValue.emailOrPhone)
                || new EmailValidate().validate(requestValue.emailOrPhone)) {
            return true;
        }
        throw new ValidTextException(ValidTextType.FORMAT);
    }

    public static class ValidEmailOrPhoneReq implements Action.RequestValue {
        public String emailOrPhone;
    }
}
