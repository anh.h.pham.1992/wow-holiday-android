package vcc.com.wowholiday.domain.usecase.valid;

import android.text.TextUtils;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import vcc.com.wowholiday.domain.validate.UserNameValidate;

/**
 * Created by QuangPH on 2020-02-17.
 */
public class ValidFullNameAction extends Action<ValidFullNameAction.ValidFullNameReq, Boolean> {

    @Override
    protected Boolean onExecute(ValidFullNameAction.ValidFullNameReq validUserNameReq) throws ActionException {
        if (TextUtils.isEmpty(validUserNameReq.name)) {
            throw new ValidTextException(ValidTextType.EMPTY);
        }

        if (new UserNameValidate().validate((validUserNameReq.name))) {
            return true;
        }

        throw new ValidTextException(ValidTextType.FORMAT);
    }


    public static class ValidFullNameReq implements Action.RequestValue {
        public String name;
    }
}
