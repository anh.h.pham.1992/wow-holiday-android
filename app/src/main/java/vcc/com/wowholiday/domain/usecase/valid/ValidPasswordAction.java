package vcc.com.wowholiday.domain.usecase.valid;

import android.text.TextUtils;

import quangph.com.mvp.mvp.action.Action;
import vcc.com.wowholiday.domain.validate.PasswordValidate;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class ValidPasswordAction extends Action<ValidPasswordAction.ValidPasswordReq, Boolean> {

    @Override
    protected Boolean onExecute(ValidPasswordReq requestValue) throws ValidTextException {
        if (TextUtils.isEmpty(requestValue.password)) {
            throw new ValidTextException(ValidTextType.EMPTY);
        }

        if (new PasswordValidate().validate(requestValue.password)) {
            return true;
        }

        throw new ValidTextException(ValidTextType.FORMAT);
    }

    public static class ValidPasswordReq implements Action.RequestValue {
        public String password;
    }

}