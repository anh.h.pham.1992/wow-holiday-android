package vcc.com.wowholiday.domain.usecase.valid;

import android.text.TextUtils;

import quangph.com.mvp.mvp.action.Action;
import vcc.com.wowholiday.domain.validate.PhoneNumberValidate;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class ValidPhoneNumberAction extends Action<ValidPhoneNumberAction.ValidPhoneNumberReq, Boolean> {

    @Override
    protected Boolean onExecute(ValidPhoneNumberReq requestValue) throws ValidTextException {
        if (TextUtils.isEmpty(requestValue.phoneNumber)) {
            throw new ValidTextException(ValidTextType.EMPTY);
        }
        if (new PhoneNumberValidate().validate(requestValue.phoneNumber)) {
            return true;
        }
        throw new ValidTextException(ValidTextType.FORMAT);
    }

    public static class ValidPhoneNumberReq implements Action.RequestValue {
        public String phoneNumber;
    }

}

