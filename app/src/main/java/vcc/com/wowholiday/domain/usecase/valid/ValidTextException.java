package vcc.com.wowholiday.domain.usecase.valid;

import quangph.com.mvp.mvp.action.ActionException;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class ValidTextException extends ActionException {

    private ValidTextType mValidTextType;

    public ValidTextException(String message) {
        super(message);
    }

    public ValidTextException(ValidTextType validTextType) {
        super("");
        mValidTextType = validTextType;
    }


    public ValidTextException(String message, ValidTextType validTextType) {
        super(message);
        mValidTextType = validTextType;
    }

    public ValidTextException(String message, Throwable t) {
        super(message, t);
    }

    public ValidTextType getValidTextType() {
        return mValidTextType;
    }
}

