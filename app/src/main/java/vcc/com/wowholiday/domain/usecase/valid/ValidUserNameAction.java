package vcc.com.wowholiday.domain.usecase.valid;

import android.text.TextUtils;

import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.ActionException;
import vcc.com.wowholiday.domain.validate.UserNameValidate;

/**
 * Created by Pham Hai Quang on 10/14/2019.
 */
public class ValidUserNameAction extends Action<ValidUserNameAction.ValidUserNameReq, Boolean> {

    @Override
    protected Boolean onExecute(ValidUserNameReq validUserNameReq) throws ActionException {
        if (TextUtils.isEmpty(validUserNameReq.lastName) || TextUtils.isEmpty(validUserNameReq.firstName)) {
            throw new ValidTextException(ValidTextType.EMPTY);
        }

        /*if (new UserNameValidate().validate((validUserNameReq.lastName + " " + validUserNameReq.firstName))) {
            return true;
        }*/

        //throw new ValidTextException(ValidTextType.FORMAT);

        return true;
    }


    public static class ValidUserNameReq implements Action.RequestValue {
        public String lastName;
        public String firstName;
    }
}
