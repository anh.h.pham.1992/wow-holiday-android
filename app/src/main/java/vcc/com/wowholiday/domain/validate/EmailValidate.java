package vcc.com.wowholiday.domain.validate;

import android.text.TextUtils;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class EmailValidate implements IValidate<String> {
    @Override
    public boolean validate(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
