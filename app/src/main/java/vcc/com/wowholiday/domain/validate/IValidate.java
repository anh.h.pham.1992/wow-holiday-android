package vcc.com.wowholiday.domain.validate;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public interface IValidate<T> {
    boolean validate(T data);
}
