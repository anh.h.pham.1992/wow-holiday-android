package vcc.com.wowholiday.domain.validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import vcc.com.wowholiday.config.AppConfig;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class PasswordValidate implements IValidate<String> {
    @Override
    public boolean validate(String password) {
        /*Pattern pattern = Pattern.compile("(?=.*[a-z])(?=.*[0-9])(?=.{6,})(^\\S*$)");
        Matcher m = pattern.matcher(password);
        return m.matches();*/

        return password != null && password.length() >= AppConfig.PASSWORD_LENGTH_MIN;
    }
}