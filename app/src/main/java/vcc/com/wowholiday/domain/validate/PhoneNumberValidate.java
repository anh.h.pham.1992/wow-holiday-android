package vcc.com.wowholiday.domain.validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class PhoneNumberValidate implements IValidate<String> {
    @Override
    public boolean validate(String phoneNumber) {
        Pattern pattern = Pattern.compile("(0[1-9])+([0-9]{8})\\b");
        Matcher m = pattern.matcher(phoneNumber);
        return m.matches();
    }
}
