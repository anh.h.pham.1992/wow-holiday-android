package vcc.com.wowholiday.domain.validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Pham Hai Quang on 10/14/2019.
 */
public class UserNameValidate implements IValidate<String> {
    @Override
    public boolean validate(String username) {
        //Pattern pattern = Pattern.compile("(?<![\\S&&[^,]])[a-zA-Z]+(?![\\S&&[^,]])");
        Pattern pattern = Pattern.compile("^[a-zA-z\\p{L}+]+([\\s][a-zA-Z\\p{L}+]+)*$");
        Matcher m = pattern.matcher(username);
        return m.matches();
    }
}
