package vcc.com.wowholiday.infras;

import android.content.SharedPreferences;
import android.text.TextUtils;

import vcc.com.wowholiday.WHApplication;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Pham Hai Quang on 1/12/2019.
 */
public class WHSharedPreference {

    private static final String SHARED_PREFERENCE_NAME = "edumall_shared_preference";
    private static final String AUTH_TOKEN = "AUTH_TOKEN";
    private static final String USER = "USER";

    public static void setAuthToken(String authToken) {
        SharedPreferences sharedPreferences = WHApplication.getInstance()
                .getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        sharedPreferences.edit().putString(AUTH_TOKEN, authToken).apply();
    }

    public static String getAuthToken() {
        SharedPreferences sharedPreferences = WHApplication.getInstance()
                .getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AUTH_TOKEN, "");
    }

    public static String getBasicToken() {
        /*String token = getAuthToken();
        if (TextUtils.isEmpty(token)) {
            return "";
        }*/
        //return "Basic MDg0Yzk0MmYyZmQwYjllYjYyZTkxMmQ4NjhkOGY3Y2E5NGJmNTZlYzEyOWEyM2U5YjEyMWI3Y2E5NjIyYWQ3MTplNDk1MGJmZDFjMTIwZjgwN2FlMTk5ODUyZWEyZDk0YWY3YzBmMzJhZDczNjhmZjNkNTQyN2U3MTAwMzYxMWQw";
        return "Basic MmVlMTNjZjg5ZWY4YWMwMjE4YWU5ZTY1ZDJmZWZhZjZlNjI0ZWMzYzExN2E1MDU0YTgwMjQ2YTY5YjJmMGIyNzo2ZGZjOTNlMDMxZjVkZmEzNTA2YmVmODljNzNmOTc1NzkyYzFmYzNmZWNiNTI5ZTM2MDYzZWMxNDBiYzAxM2I3";
    }

    public static void saveUser(String user) {
        SharedPreferences sharedPreferences = WHApplication.getInstance()
                .getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        sharedPreferences.edit().putString(USER, user).apply();
    }

    public static String getUser() {
        SharedPreferences sharedPreferences = WHApplication.getInstance()
                .getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(USER, "");
    }

    public static void clearUser() {
        SharedPreferences sharedPreferences = WHApplication.getInstance()
                .getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        sharedPreferences.edit().remove(USER).commit();
    }
}
