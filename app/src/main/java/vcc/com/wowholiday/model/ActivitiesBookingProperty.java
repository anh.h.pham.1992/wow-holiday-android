package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class ActivitiesBookingProperty implements Parcelable {
    private Calendar mStartDate;
    private int mAdultCount;
    private int mChildrenCount;
    private int mNewbornCount;
    private Location mStartLoc;

    public ActivitiesBookingProperty() {
        mStartDate = Calendar.getInstance();
        mStartDate.add(Calendar.DAY_OF_YEAR, 1);
        mStartDate.set(Calendar.HOUR_OF_DAY, 0);
        mStartDate.set(Calendar.MINUTE, 0);
        mStartDate.set(Calendar.SECOND, 0);
        mStartDate.set(Calendar.MILLISECOND, 0);
    }

    protected ActivitiesBookingProperty(Parcel in) {
        mAdultCount = in.readInt();
        mChildrenCount = in.readInt();
        mNewbornCount = in.readInt();
        mStartLoc = in.readParcelable(Location.class.getClassLoader());
    }

    public static final Creator<ActivitiesBookingProperty> CREATOR = new Creator<ActivitiesBookingProperty>() {
        @Override
        public ActivitiesBookingProperty createFromParcel(Parcel in) {
            return new ActivitiesBookingProperty(in);
        }

        @Override
        public ActivitiesBookingProperty[] newArray(int size) {
            return new ActivitiesBookingProperty[size];
        }
    };

    public Calendar getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Calendar startDate) {
        this.mStartDate = startDate;
    }

    public int getAdultCount() {
        return mAdultCount;
    }

    public void setAdultCount(int adultCount) {
        this.mAdultCount = adultCount;
    }

    public int getChildrenCount() {
        return mChildrenCount;
    }

    public void setChildrenCount(int childrenCount) {
        this.mChildrenCount = childrenCount;
    }

    public int getNewbornCount() {
        return mNewbornCount;
    }

    public void setNewbornCount(int newbornCount) {
        this.mNewbornCount = newbornCount;
    }

    public Location getStartLoc() {
        return mStartLoc;
    }

    public void setStartLoc(Location startLoc) {
        this.mStartLoc = startLoc;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mAdultCount);
        dest.writeInt(mChildrenCount);
        dest.writeInt(mNewbornCount);
        dest.writeParcelable(mStartLoc, flags);
    }
}
