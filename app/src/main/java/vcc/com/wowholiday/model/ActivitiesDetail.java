package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import vcc.com.wowholiday.model.air.PaxInfo;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class ActivitiesDetail implements Parcelable {
    private String mId;
    private String mName;
    private float mRating;
    private int mRaterCount;
    private DateInfo mDateInfo;
    private String mDescription;
    private Location mLocation;
    private List<String> mImageList;
    private String mPrice;
    private String mOriginPrice;
    private String mCurrencyUnit;
    private Vendor mVendor;
    private List<Policy> mPolicyList;
    private List<Amenity> mAmenityList;
    private List<Facility> mFacilityList;
    private List<PaxInfo> mPaxInfoList;

    public ActivitiesDetail() {}

    protected ActivitiesDetail(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mRating = in.readFloat();
        mRaterCount = in.readInt();
        mDateInfo = in.readParcelable(DateInfo.class.getClassLoader());
        mDescription = in.readString();
        mLocation = in.readParcelable(Location.class.getClassLoader());
        mImageList = in.createStringArrayList();
        mPrice = in.readString();
        mOriginPrice = in.readString();
        mCurrencyUnit = in.readString();
        mVendor = in.readParcelable(Vendor.class.getClassLoader());
        mPolicyList = in.createTypedArrayList(Policy.CREATOR);
        mAmenityList = in.createTypedArrayList(Amenity.CREATOR);
        mFacilityList = in.createTypedArrayList(Facility.CREATOR);
        mPaxInfoList = in.createTypedArrayList(PaxInfo.CREATOR);
    }

    public static final Creator<ActivitiesDetail> CREATOR = new Creator<ActivitiesDetail>() {
        @Override
        public ActivitiesDetail createFromParcel(Parcel in) {
            return new ActivitiesDetail(in);
        }

        @Override
        public ActivitiesDetail[] newArray(int size) {
            return new ActivitiesDetail[size];
        }
    };

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public float getRating() {
        return mRating;
    }

    public void setRating(float rating) {
        this.mRating = rating;
    }

    public int getRaterCount() {
        return mRaterCount;
    }

    public void setRaterCount(int raterCount) {
        this.mRaterCount = raterCount;
    }

    public DateInfo getDateInfo() {
        return mDateInfo;
    }

    public void setDateInfo(DateInfo dateInfo) {
        this.mDateInfo = dateInfo;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public List<String> getImageList() {
        return mImageList;
    }

    public void setImageList(List<String> imageList) {
        this.mImageList = imageList;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        this.mPrice = price;
    }

    public String getOriginPrice() {
        return mOriginPrice;
    }

    public void setOriginPrice(String originPrice) {
        this.mOriginPrice = originPrice;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.mCurrencyUnit = currencyUnit;
    }

    public Vendor getVendor() {
        return mVendor;
    }

    public void setVendor(Vendor vendor) {
        this.mVendor = vendor;
    }

    public List<Policy> getPolicyList() {
        return mPolicyList;
    }

    public void setPolicyList(List<Policy> policyList) {
        this.mPolicyList = policyList;
    }

    public List<Amenity> getAmenityList() {
        return mAmenityList;
    }

    public void setAmenityList(List<Amenity> amenityList) {
        this.mAmenityList = amenityList;
    }

    public List<Facility> getFacilityList() {
        return mFacilityList;
    }

    public void setFacilityList(List<Facility> facilityList) {
        this.mFacilityList = facilityList;
    }

    public List<PaxInfo> getPaxInfoList() {
        return mPaxInfoList;
    }

    public void setPaxInfoList(List<PaxInfo> paxInfoList) {
        this.mPaxInfoList = paxInfoList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeFloat(mRating);
        dest.writeInt(mRaterCount);
        dest.writeParcelable(mDateInfo, flags);
        dest.writeString(mDescription);
        dest.writeParcelable(mLocation, flags);
        dest.writeStringList(mImageList);
        dest.writeString(mPrice);
        dest.writeString(mOriginPrice);
        dest.writeString(mCurrencyUnit);
        dest.writeParcelable(mVendor, flags);
        dest.writeTypedList(mPolicyList);
        dest.writeTypedList(mAmenityList);
        dest.writeTypedList(mFacilityList);
        dest.writeTypedList(mPaxInfoList);
    }
}
