package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 10/19/2019.
 */
public class ActivitiesInfo implements Parcelable {
    private String mID;
    private String mName;
    private String mAvatarUrl;
    private float mRating;
    private int mRateCount;
    private Location mLocation;
    private String mPrice;
    private String mOriginalPrice;
    private String mCurrencyUnit;
    private Vendor mVendor;
    private String mToken;

    public ActivitiesInfo() {}

    protected ActivitiesInfo(Parcel in) {
        mID = in.readString();
        mName = in.readString();
        mAvatarUrl = in.readString();
        mRating = in.readFloat();
        mRateCount = in.readInt();
        mLocation = in.readParcelable(Location.class.getClassLoader());
        mPrice = in.readString();
        mOriginalPrice = in.readString();
        mCurrencyUnit = in.readString();
        mVendor = in.readParcelable(Vendor.class.getClassLoader());
        mToken = in.readString();
    }

    public static final Creator<ActivitiesInfo> CREATOR = new Creator<ActivitiesInfo>() {
        @Override
        public ActivitiesInfo createFromParcel(Parcel in) {
            return new ActivitiesInfo(in);
        }

        @Override
        public ActivitiesInfo[] newArray(int size) {
            return new ActivitiesInfo[size];
        }
    };

    public String getID() {
        return mID;
    }

    public void setID(String ID) {
        this.mID = ID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }

    public float getRating() {
        return mRating;
    }

    public void setRating(float rating) {
        this.mRating = rating;
    }

    public int getRateCount() {
        return mRateCount;
    }

    public void setRateCount(int rateCount) {
        this.mRateCount = rateCount;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        this.mPrice = price;
    }

    public String getOriginalPrice() {
        return mOriginalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.mOriginalPrice = originalPrice;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.mCurrencyUnit = currencyUnit;
    }

    public Vendor getVendor() {
        return mVendor;
    }

    public void setVendor(Vendor vendor) {
        this.mVendor = vendor;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        this.mToken = token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mID);
        dest.writeString(mName);
        dest.writeString(mAvatarUrl);
        dest.writeFloat(mRating);
        dest.writeInt(mRateCount);
        dest.writeParcelable(mLocation, flags);
        dest.writeString(mPrice);
        dest.writeString(mOriginalPrice);
        dest.writeString(mCurrencyUnit);
        dest.writeParcelable(mVendor, flags);
        dest.writeString(mToken);
    }
}
