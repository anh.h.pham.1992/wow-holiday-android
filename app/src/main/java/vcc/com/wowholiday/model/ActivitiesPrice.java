package vcc.com.wowholiday.model;

/**
 * Created by Pham Hai Quang on 10/24/2019.
 */
public class ActivitiesPrice {
    private String mTitle;
    private String mPrice;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        this.mPrice = price;
    }
}
