package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import vcc.com.wowholiday.model.air.PaxInfo;

/**
 * Created by Pham Hai Quang on 10/28/2019.
 */
public class ActivitiesTicket implements Parcelable {
    private String mCode;
    private String mName;
    private String mDate;
    private String mStartTime;
    private String mEndTime;
    private int mAdultCount;
    private int mChildCount;
    private BOOKING_STATUS mStatus;
    private float mPrice;
    private TicketContactInfo mContactInfo;
    private String mActiviesID;
    private String mActivitiesToken;
    private List<PaxInfo> mPaxInfoList;

    public ActivitiesTicket() {}


    protected ActivitiesTicket(Parcel in) {
        mCode = in.readString();
        mName = in.readString();
        mDate = in.readString();
        mStartTime = in.readString();
        mEndTime = in.readString();
        mAdultCount = in.readInt();
        mChildCount = in.readInt();
        mPrice = in.readFloat();
        mContactInfo = in.readParcelable(TicketContactInfo.class.getClassLoader());
        mStatus = (BOOKING_STATUS) in.readSerializable();
        mActiviesID = in.readString();
        mActivitiesToken = in.readString();
        mPaxInfoList = in.createTypedArrayList(PaxInfo.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCode);
        dest.writeString(mName);
        dest.writeString(mDate);
        dest.writeString(mStartTime);
        dest.writeString(mEndTime);
        dest.writeInt(mAdultCount);
        dest.writeInt(mChildCount);
        dest.writeFloat(mPrice);
        dest.writeParcelable(mContactInfo, flags);
        dest.writeSerializable(mStatus);
        dest.writeString(mActiviesID);
        dest.writeString(mActivitiesToken);
        dest.writeTypedList(mPaxInfoList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ActivitiesTicket> CREATOR = new Creator<ActivitiesTicket>() {
        @Override
        public ActivitiesTicket createFromParcel(Parcel in) {
            return new ActivitiesTicket(in);
        }

        @Override
        public ActivitiesTicket[] newArray(int size) {
            return new ActivitiesTicket[size];
        }
    };

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        this.mCode = code;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        this.mDate = date;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public void setStartTime(String time) {
        this.mStartTime = time;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public void setEndTime(String endTime) {
        this.mEndTime = endTime;
    }

    public int getAdultCount() {
        return mAdultCount;
    }

    public void setAdultCount(int adultCount) {
        this.mAdultCount = adultCount;
    }

    public int getChildCount() {
        return mChildCount;
    }

    public void setChildCount(int childCount) {
        this.mChildCount = childCount;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public BOOKING_STATUS getStatus() {
        return mStatus;
    }

    public void setStatus(BOOKING_STATUS status) {
        this.mStatus = status;
    }

    public TicketContactInfo getContact() {
        return mContactInfo;
    }

    public void setContact(TicketContactInfo ticketContactInfo) {
        this.mContactInfo = ticketContactInfo;
    }

    public String getActiviesID() {
        return mActiviesID;
    }

    public void setActiviesID(String activiesID) {
        this.mActiviesID = activiesID;
    }

    public String getActivitiesToken() {
        return mActivitiesToken;
    }

    public void setActivitiesToken(String activitiesToken) {
        this.mActivitiesToken = activitiesToken;
    }

    public List<PaxInfo> getPaxInfoList() {
        return mPaxInfoList;
    }

    public void setPaxInfoList(List<PaxInfo> paxInfoList) {
        this.mPaxInfoList = paxInfoList;
    }
}
