package vcc.com.wowholiday.model;

/**
 * Created by Pham Hai Quang on 10/19/2019.
 */
public class ActivityLocation {
    private String mName;
    private String mAvatarUrl;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }
}
