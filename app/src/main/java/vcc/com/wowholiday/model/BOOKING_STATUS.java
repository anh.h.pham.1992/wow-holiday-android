package vcc.com.wowholiday.model;

/**
 * Created by QuangPH on 2020-02-10.
 */
public enum BOOKING_STATUS {
    WAITING_PAY, CONFIRMED, USED, CANCELED
}
