package vcc.com.wowholiday.model;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */
public class Category {
    private String mIconUrl;
    private String mName;

    public String getIconUrl() {
        return mIconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.mIconUrl = iconUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }
}
