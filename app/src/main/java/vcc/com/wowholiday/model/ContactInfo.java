package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/12/2019.
 */
public class ContactInfo implements Parcelable {
    private String mPhoneNumber;
    private String mActlFormatPhoneNumber;
    private String mHomePhoneNumber;
    private String mPhoneNumberCountryCode;
    private String mHomePhoneNumberCountryCode;
    private String mActlFormatHomePhoneNumber;
    private String mFax;
    private String mEmail;

    public ContactInfo() {}

    protected ContactInfo(Parcel in) {
        mPhoneNumber = in.readString();
        mActlFormatPhoneNumber = in.readString();
        mHomePhoneNumber = in.readString();
        mPhoneNumberCountryCode = in.readString();
        mHomePhoneNumberCountryCode = in.readString();
        mActlFormatHomePhoneNumber = in.readString();
        mFax = in.readString();
        mEmail = in.readString();
    }

    public static final Creator<ContactInfo> CREATOR = new Creator<ContactInfo>() {
        @Override
        public ContactInfo createFromParcel(Parcel in) {
            return new ContactInfo(in);
        }

        @Override
        public ContactInfo[] newArray(int size) {
            return new ContactInfo[size];
        }
    };

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.mPhoneNumber = phoneNumber;
    }

    public String getActlFormatPhoneNumber() {
        return mActlFormatPhoneNumber;
    }

    public void setActlFormatPhoneNumber(String actlFormatPhoneNumber) {
        this.mActlFormatPhoneNumber = actlFormatPhoneNumber;
    }

    public String getHomePhoneNumber() {
        return mHomePhoneNumber;
    }

    public void setHomePhoneNumber(String homePhoneNumber) {
        this.mHomePhoneNumber = homePhoneNumber;
    }

    public String getPhoneNumberCountryCode() {
        return mPhoneNumberCountryCode;
    }

    public void setPhoneNumberCountryCode(String phoneNumberCountryCode) {
        this.mPhoneNumberCountryCode = phoneNumberCountryCode;
    }

    public String getHomePhoneNumberCountryCode() {
        return mHomePhoneNumberCountryCode;
    }

    public void setHomePhoneNumberCountryCode(String homePhoneNumberCountryCode) {
        this.mHomePhoneNumberCountryCode = homePhoneNumberCountryCode;
    }

    public String getActlFormatHomePhoneNumber() {
        return mActlFormatHomePhoneNumber;
    }

    public void setActlFormatHomePhoneNumber(String actlFormatHomePhoneNumber) {
        this.mActlFormatHomePhoneNumber = actlFormatHomePhoneNumber;
    }

    public String getFax() {
        return mFax;
    }

    public void setFax(String fax) {
        this.mFax = fax;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPhoneNumber);
        dest.writeString(mActlFormatPhoneNumber);
        dest.writeString(mHomePhoneNumber);
        dest.writeString(mPhoneNumberCountryCode);
        dest.writeString(mHomePhoneNumberCountryCode);
        dest.writeString(mActlFormatHomePhoneNumber);
        dest.writeString(mFax);
        dest.writeString(mEmail);
    }
}
