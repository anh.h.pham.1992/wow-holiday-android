package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class Criteria implements Parcelable {
    private String mKey;
    private String mValue;

    public Criteria() {}

    protected Criteria(Parcel in) {
        mKey = in.readString();
        mValue = in.readString();
    }

    public static final Creator<Criteria> CREATOR = new Creator<Criteria>() {
        @Override
        public Criteria createFromParcel(Parcel in) {
            return new Criteria(in);
        }

        @Override
        public Criteria[] newArray(int size) {
            return new Criteria[size];
        }
    };

    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        this.mKey = key;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        this.mValue = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKey);
        dest.writeString(mValue);
    }
}
