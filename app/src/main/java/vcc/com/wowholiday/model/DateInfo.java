package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class DateInfo implements Parcelable {
    private String mStartDate;
    private String mEndDate;
    private String mType;
    private String mStartTime;
    private String mEndTime;

    public DateInfo() {}

    protected DateInfo(Parcel in) {
        mStartDate = in.readString();
        mEndDate = in.readString();
        mType = in.readString();
        mStartTime = in.readString();
        mEndTime = in.readString();
    }

    public static final Creator<DateInfo> CREATOR = new Creator<DateInfo>() {
        @Override
        public DateInfo createFromParcel(Parcel in) {
            return new DateInfo(in);
        }

        @Override
        public DateInfo[] newArray(int size) {
            return new DateInfo[size];
        }
    };

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String startDate) {
        this.mStartDate = startDate;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String endDate) {
        this.mEndDate = endDate;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public void setStartTime(String startTime) {
        this.mStartTime = startTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public void setEndTime(String endTime) {
        this.mEndTime = endTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mStartDate);
        dest.writeString(mEndDate);
        dest.writeString(mType);
        dest.writeString(mStartTime);
        dest.writeString(mEndTime);
    }
}
