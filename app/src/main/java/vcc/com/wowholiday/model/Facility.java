package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by QuangPH on 2/8/2020.
 */
public class Facility implements Parcelable {
    private String mId;
    private String mDescription;

    public Facility() {}

    protected Facility(Parcel in) {
        mId = in.readString();
        mDescription = in.readString();
    }

    public static final Creator<Facility> CREATOR = new Creator<Facility>() {
        @Override
        public Facility createFromParcel(Parcel in) {
            return new Facility(in);
        }

        @Override
        public Facility[] newArray(int size) {
            return new Facility[size];
        }
    };

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mDescription);
    }
}
