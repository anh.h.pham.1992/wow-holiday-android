package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Image implements Parcelable {
    private String mId;
    private String mTitle;
    private String mUrl;
    private String mCategory;
    private String mDescription;
    private String mUpdatedDate;
    private String mType;
    private boolean mIsDefault;

    public Image() {
    }

    public Image(Parcel in) {
        mId = in.readString();
        mTitle = in.readString();
        mUrl = in.readString();
        mCategory = in.readString();
        mDescription = in.readString();
        mUpdatedDate = in.readString();
        mType = in.readString();
        mIsDefault = in.readInt() == 1;
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mTitle);
        dest.writeString(mUrl);
        dest.writeString(mCategory);
        dest.writeString(mDescription);
        dest.writeString(mUpdatedDate);
        dest.writeString(mType);
        dest.writeInt(mIsDefault ? 1 : 0);
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String mUpdatedDate) {
        this.mUpdatedDate = mUpdatedDate;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public boolean isDefault() {
        return mIsDefault;
    }

    public void setIsDefault(boolean mIsDefault) {
        this.mIsDefault = mIsDefault;
    }
}
