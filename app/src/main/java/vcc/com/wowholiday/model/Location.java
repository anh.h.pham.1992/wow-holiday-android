package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class Location implements Parcelable {
    private String mID;
    private String mZipCode;
    private String mName;
    private String mCultureIrrelevantName;
    private String mCountryID;
    private String mCountryName;
    private String mType;
    private float mLat;
    private float mLong;
    private String mAddress;
    private String mCity;
    private String mState;
    private String mDistrict;

    public Location(){}

    protected Location(Parcel in) {
        mID = in.readString();
        mZipCode = in.readString();
        mName = in.readString();
        mCultureIrrelevantName = in.readString();
        mCountryID = in.readString();
        mCountryName = in.readString();
        mType = in.readString();
        mLat = in.readFloat();
        mLong = in.readFloat();
        mAddress = in.readString();
        mCity = in.readString();
        mState = in.readString();
        mDistrict = in.readString();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    public String getID() {
        return mID;
    }

    public void setID(String ID) {
        this.mID = ID;
    }

    public String getZipCode() {
        return mZipCode;
    }

    public void setZipCode(String zipCode) {
        this.mZipCode = zipCode;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getCultureIrrelevantName() {
        return mCultureIrrelevantName;
    }

    public void setCultureIrrelevantName(String cultureIrrelevantName) {
        this.mCultureIrrelevantName = cultureIrrelevantName;
    }

    public String getCountryID() {
        return mCountryID;
    }

    public void setCountryID(String countryID) {
        this.mCountryID = countryID;
    }

    public String getCountryName() {
        return mCountryName;
    }

    public void setCountryName(String countryName) {
        this.mCountryName = countryName;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public float getLat() {
        return mLat;
    }

    public void setLat(float lat) {
        this.mLat = lat;
    }

    public float getLong() {
        return mLong;
    }

    public void setLong(float longtitude) {
        this.mLong = longtitude;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        this.mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        this.mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        this.mState = state;
    }

    public String getDistrict() {
        return mDistrict;
    }

    public void setDistrict(String district) {
        this.mDistrict = district;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mID);
        dest.writeString(mZipCode);
        dest.writeString(mName);
        dest.writeString(mCultureIrrelevantName);
        dest.writeString(mCountryID);
        dest.writeString(mCountryName);
        dest.writeString(mType);
        dest.writeFloat(mLat);
        dest.writeFloat(mLong);
        dest.writeString(mAddress);
        dest.writeString(mCity);
        dest.writeString(mState);
        dest.writeString(mDistrict);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Location) {
            return mID.equals(((Location) obj).mID);
        }

        return super.equals(obj);
    }
}
