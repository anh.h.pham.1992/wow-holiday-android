package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by QuangPH on 2/10/2020.
 */
public class MyCoupon implements Parcelable {

    private String mName;
    private String mDescription;
    private String mStartDate;
    private String mEndDate;
    private String mCode;
    private String mUrl;
    private List<String> mPolicyList;

    public MyCoupon() {}

    protected MyCoupon(Parcel in) {
        mName = in.readString();
        mDescription = in.readString();
        mStartDate = in.readString();
        mEndDate = in.readString();
        mCode = in.readString();
        mUrl = in.readString();
        mPolicyList = in.createStringArrayList();
    }

    public static final Creator<MyCoupon> CREATOR = new Creator<MyCoupon>() {
        @Override
        public MyCoupon createFromParcel(Parcel in) {
            return new MyCoupon(in);
        }

        @Override
        public MyCoupon[] newArray(int size) {
            return new MyCoupon[size];
        }
    };

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String startDate) {
        this.mStartDate = startDate;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String endDate) {
        this.mEndDate = endDate;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        this.mCode = code;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public List<String> getPolicyList() {
        return mPolicyList;
    }

    public void setPolicyList(List<String> policyList) {
        this.mPolicyList = policyList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeString(mStartDate);
        dest.writeString(mEndDate);
        dest.writeString(mCode);
        dest.writeString(mUrl);
        dest.writeStringList(mPolicyList);
    }
}
