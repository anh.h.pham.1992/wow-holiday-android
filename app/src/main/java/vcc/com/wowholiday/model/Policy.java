package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class Policy implements Parcelable {
    private String mType;
    private String mDescription;
    private DateInfo mDateInfo;
    private boolean isPortalPolicy;
    private List<Criteria> mCriteriaList;

    public Policy() {}


    protected Policy(Parcel in) {
        mType = in.readString();
        mDescription = in.readString();
        mDateInfo = in.readParcelable(DateInfo.class.getClassLoader());
        isPortalPolicy = in.readByte() != 0;
        mCriteriaList = in.createTypedArrayList(Criteria.CREATOR);
    }

    public static final Creator<Policy> CREATOR = new Creator<Policy>() {
        @Override
        public Policy createFromParcel(Parcel in) {
            return new Policy(in);
        }

        @Override
        public Policy[] newArray(int size) {
            return new Policy[size];
        }
    };

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public DateInfo getDateInfo() {
        return mDateInfo;
    }

    public void setDateInfo(DateInfo dateInfo) {
        this.mDateInfo = dateInfo;
    }

    public boolean isPortalPolicy() {
        return isPortalPolicy;
    }

    public void setPortalPolicy(boolean portalPolicy) {
        isPortalPolicy = portalPolicy;
    }

    public List<Criteria> getCriteriaList() {
        return mCriteriaList;
    }

    public void setCriteriaList(List<Criteria> criteriaList) {
        this.mCriteriaList = criteriaList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeString(mDescription);
        dest.writeParcelable(mDateInfo, flags);
        dest.writeByte((byte) (isPortalPolicy ? 1 : 0));
        dest.writeTypedList(mCriteriaList);
    }
}
