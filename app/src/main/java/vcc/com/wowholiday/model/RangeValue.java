package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/15/2019.
 */
public class RangeValue implements Parcelable {

    private String mMinValue;
    private String mMaxValue;

    public RangeValue() {}

    protected RangeValue(Parcel in) {
        mMinValue = in.readString();
        mMaxValue = in.readString();
    }

    public static final Creator<RangeValue> CREATOR = new Creator<RangeValue>() {
        @Override
        public RangeValue createFromParcel(Parcel in) {
            return new RangeValue(in);
        }

        @Override
        public RangeValue[] newArray(int size) {
            return new RangeValue[size];
        }
    };

    public String getMinValue() {
        return mMinValue;
    }

    public void setMinValue(String minValue) {
        this.mMinValue = minValue;
    }

    public String getMaxValue() {
        return mMaxValue;
    }

    public void setMaxValue(String maxValue) {
        this.mMaxValue = maxValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMinValue);
        dest.writeString(mMaxValue);
    }
}
