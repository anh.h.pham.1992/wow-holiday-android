package vcc.com.wowholiday.model;

public class Room {

    private String mAvatarUrl;
    private String mName;
    private String[] mFacilities;
    private String[] mBathroomFacilities;
    private float mPrice;
    private float mPriceAfterDiscount;
    private String mCurrency;

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String[] getFacilities() {
        return mFacilities;
    }

    public void setBathroomFacilities(String[] bathroomFacilities) {
        this.mBathroomFacilities = bathroomFacilities;
    }

    public String[] getBathroomFacilities() {
        return mBathroomFacilities;
    }

    public void setFacilities(String[] facilities) {
        this.mFacilities = facilities;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public float getPriceAfterDiscount() {
        return mPriceAfterDiscount;
    }

    public void setPriceAfterDiscount(float priceAfterDiscount) {
        this.mPriceAfterDiscount = priceAfterDiscount;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        this.mCurrency = currency;
    }
}
