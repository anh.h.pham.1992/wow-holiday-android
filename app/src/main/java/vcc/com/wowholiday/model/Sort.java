package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/15/2019.
 */
public class Sort implements Parcelable {

    public static final int ASC = 1;
    public static final int DESC = -1;

    private int mOrder;
    private String mName;

    public Sort() {}

    protected Sort(Parcel in) {
        mOrder = in.readInt();
        mName = in.readString();
    }

    public static final Creator<Sort> CREATOR = new Creator<Sort>() {
        @Override
        public Sort createFromParcel(Parcel in) {
            return new Sort(in);
        }

        @Override
        public Sort[] newArray(int size) {
            return new Sort[size];
        }
    };

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int order) {
        this.mOrder = order;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mOrder);
        dest.writeString(mName);
    }
}
