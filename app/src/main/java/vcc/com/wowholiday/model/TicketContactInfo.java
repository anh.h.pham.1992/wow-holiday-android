package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/21/2019.
 */
public class TicketContactInfo implements Parcelable {

    private String mName;
    private String mPhone;
    private String mEmail;

    protected TicketContactInfo(Parcel in) {
        mName = in.readString();
        mPhone = in.readString();
        mEmail = in.readString();
    }

    public static final Creator<TicketContactInfo> CREATOR = new Creator<TicketContactInfo>() {
        @Override
        public TicketContactInfo createFromParcel(Parcel in) {
            return new TicketContactInfo(in);
        }

        @Override
        public TicketContactInfo[] newArray(int size) {
            return new TicketContactInfo[size];
        }
    };

    public TicketContactInfo(){}

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mPhone);
        dest.writeString(mEmail);
    }
}
