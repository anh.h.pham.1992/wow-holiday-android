package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public class User implements Parcelable {

    public static final String MALE = "male";
    public static final String FEMALE = "female";

    private String mUserID;
    private String mEntityID;
    private String mAgentID;
    private String mCustomerID;
    private String mFirstName;
    private String mLastName;
    private Location mLocation;
    private ContactInfo mContactInfo;
    private String mGender;
    private String mBirthday;
    private String mPassportExpirationDate;
    private String mAvatarUrl;

    public User(){}


    protected User(Parcel in) {
        mUserID = in.readString();
        mEntityID = in.readString();
        mAgentID = in.readString();
        mCustomerID = in.readString();
        mFirstName = in.readString();
        mLastName = in.readString();
        mLocation = in.readParcelable(Location.class.getClassLoader());
        mContactInfo = in.readParcelable(ContactInfo.class.getClassLoader());
        mGender = in.readString();
        mBirthday = in.readString();
        mPassportExpirationDate = in.readString();
        mAvatarUrl = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getID() {
        return mUserID;
    }

    public void setID(String ID) {
        this.mUserID = ID;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        this.mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public ContactInfo getContactInfo() {
        return mContactInfo;
    }

    public void setContactInfo(ContactInfo contactInfo) {
        this.mContactInfo = contactInfo;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        this.mGender = gender;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String birthday) {
        this.mBirthday = birthday;
    }

    public String getPassportExpirationDate() {
        return mPassportExpirationDate;
    }

    public void setPassportExpirationDate(String passportExpirationDate) {
        this.mPassportExpirationDate = passportExpirationDate;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }

    public String getEntityID() {
        return mEntityID;
    }

    public void setEntityID(String entityID) {
        this.mEntityID = entityID;
    }

    public String getAgentID() {
        return mAgentID;
    }

    public void setAgentID(String agentID) {
        this.mAgentID = agentID;
    }

    public String getCustomerID() {
        return mCustomerID;
    }

    public void setCustomerID(String customerID) {
        this.mCustomerID = customerID;
    }

    public String getFullName() {
        if (TextUtils.isEmpty(mLastName)) {
            return mFirstName;
        } else {
            return mLastName + " " + mFirstName;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUserID);
        dest.writeString(mEntityID);
        dest.writeString(mAgentID);
        dest.writeString(mCustomerID);
        dest.writeString(mFirstName);
        dest.writeString(mLastName);
        dest.writeParcelable(mLocation, flags);
        dest.writeParcelable(mContactInfo, flags);
        dest.writeString(mGender);
        dest.writeString(mBirthday);
        dest.writeString(mPassportExpirationDate);
        dest.writeString(mAvatarUrl);
    }

    public User copy() {
        User user = new User();
        user.mUserID = mUserID;
        user.mEntityID = mEntityID;
        user.mAgentID = mAgentID;
        user.mCustomerID = mCustomerID;
        user.mFirstName = mFirstName;
        user.mLastName = mLastName;
        user.mLocation = mLocation;
        user.mContactInfo = mContactInfo;
        user.mGender = mGender;
        user.mBirthday = mBirthday;
        user.mPassportExpirationDate = mPassportExpirationDate;
        user.mAvatarUrl = mAvatarUrl;
        return user;
    }
}
