package vcc.com.wowholiday.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class Vendor implements Parcelable {
    private String mCode;
    private String mName;

    public Vendor() {}

    protected Vendor(Parcel in) {
        mCode = in.readString();
        mName = in.readString();
    }

    public static final Creator<Vendor> CREATOR = new Creator<Vendor>() {
        @Override
        public Vendor createFromParcel(Parcel in) {
            return new Vendor(in);
        }

        @Override
        public Vendor[] newArray(int size) {
            return new Vendor[size];
        }
    };

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        this.mCode = code;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCode);
        dest.writeString(mName);
    }
}
