package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by QuangPH on 2020-01-11.
 */
public class AirSeat implements Parcelable {
    private int mType;
    private String mName;
    private String mDescription;

    public AirSeat() {}

    protected AirSeat(Parcel in) {
        mType = in.readInt();
        mName = in.readString();
        mDescription = in.readString();
    }

    public static final Creator<AirSeat> CREATOR = new Creator<AirSeat>() {
        @Override
        public AirSeat createFromParcel(Parcel in) {
            return new AirSeat(in);
        }

        @Override
        public AirSeat[] newArray(int size) {
            return new AirSeat[size];
        }
    };

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mType);
        dest.writeString(mName);
        dest.writeString(mDescription);
    }
}
