package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class Baggage implements Parcelable {
    private float mWeight;
    private double mPrice;

    public Baggage() {}

    protected Baggage(Parcel in) {
        mWeight = in.readFloat();
        mPrice = in.readDouble();
    }

    public static final Creator<Baggage> CREATOR = new Creator<Baggage>() {
        @Override
        public Baggage createFromParcel(Parcel in) {
            return new Baggage(in);
        }

        @Override
        public Baggage[] newArray(int size) {
            return new Baggage[size];
        }
    };

    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float weight) {
        this.mWeight = weight;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        this.mPrice = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(mWeight);
        dest.writeDouble(mPrice);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Baggage)) return false;
        Baggage other = (Baggage) obj;
        return mPrice == other.mPrice && mWeight == other.mWeight;
    }
}
