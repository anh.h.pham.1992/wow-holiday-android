package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */

public class Flight implements Parcelable {

    private String mID;
    private String mFlightType;
    private int mCode;
    private String mToken;

    private DateInfo mDateInfo;
    private long mLayOverDuration;
    private long mJourneyDuration;
    private Location mStartPoint;
    private Location mEndPoint;
    private float mPrice;
    private String mCurrencyUnit;
    private Vendor mVendors;

    public Flight() {}

    protected Flight(Parcel in) {
        mID = in.readString();
        mFlightType = in.readString();
        mCode = in.readInt();
        mToken = in.readString();
        mDateInfo = in.readParcelable(DateInfo.class.getClassLoader());
        mLayOverDuration = in.readLong();
        mJourneyDuration = in.readLong();
        mStartPoint = in.readParcelable(Location.class.getClassLoader());
        mEndPoint = in.readParcelable(Location.class.getClassLoader());
        mPrice = in.readFloat();
        mCurrencyUnit = in.readString();
        mVendors = in.readParcelable(Vendor.class.getClassLoader());
    }

    public static final Creator<Flight> CREATOR = new Creator<Flight>() {
        @Override
        public Flight createFromParcel(Parcel in) {
            return new Flight(in);
        }

        @Override
        public Flight[] newArray(int size) {
            return new Flight[size];
        }
    };

    public String getID() {
        return mID;
    }

    public void setID(String ID) {
        this.mID = ID;
    }

    public String getFlightType() {
        return mFlightType;
    }

    public void setFlightType(String flightType) {
        this.mFlightType = flightType;
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        this.mCode = code;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        this.mToken = token;
    }

    public DateInfo getDateInfo() {
        return mDateInfo;
    }

    public void setDateInfo(DateInfo dateInfo) {
        this.mDateInfo = dateInfo;
    }

    public long getLayOverDuration() {
        return mLayOverDuration;
    }

    public void setLayOverDuration(long layOverDuration) {
        this.mLayOverDuration = layOverDuration;
    }

    public long getJourneyDuration() {
        return mJourneyDuration;
    }

    public void setJourneyDuration(long journeyDuration) {
        this.mJourneyDuration = journeyDuration;
    }

    public Location getStartPoint() {
        return mStartPoint;
    }

    public void setStartPoint(Location startPoint) {
        this.mStartPoint = startPoint;
    }

    public Location getEndPoint() {
        return mEndPoint;
    }

    public void setEndPoint(Location endPoint) {
        this.mEndPoint = endPoint;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.mCurrencyUnit = currencyUnit;
    }

    public Vendor getVendors() {
        return mVendors;
    }

    public void setVendors(Vendor vendors) {
        this.mVendors = vendors;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mID);
        dest.writeString(mFlightType);
        dest.writeInt(mCode);
        dest.writeString(mToken);
        dest.writeParcelable(mDateInfo, flags);
        dest.writeLong(mLayOverDuration);
        dest.writeLong(mJourneyDuration);
        dest.writeParcelable(mStartPoint, flags);
        dest.writeParcelable(mEndPoint, flags);
        dest.writeFloat(mPrice);
        dest.writeString(mCurrencyUnit);
        dest.writeParcelable(mVendors, flags);
    }
}
