package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public class FlightBookingProperty implements Parcelable {
    private Calendar mStartDate;
    private Calendar mEndDate;

    private int mAdultCount;
    private int mChildrenCount;
    private int mNewbornCount;
    private AirSeat mSeatType;
    private Location mStartLoc;
    private Location mEndLoc;
    private boolean isRoundTrip;

    public FlightBookingProperty() { }

    protected FlightBookingProperty(Parcel in) {
        mAdultCount = in.readInt();
        mChildrenCount = in.readInt();
        mNewbornCount = in.readInt();
        mSeatType = in.readParcelable(AirSeat.class.getClassLoader());
        mStartLoc = in.readParcelable(Location.class.getClassLoader());
        mEndLoc = in.readParcelable(Location.class.getClassLoader());
        mStartDate = (Calendar) in.readSerializable();
        mEndDate = (Calendar) in.readSerializable();
        isRoundTrip = in.readInt() == 1;
    }

    public static final Creator<FlightBookingProperty> CREATOR = new Creator<FlightBookingProperty>() {
        @Override
        public FlightBookingProperty createFromParcel(Parcel in) {
            return new FlightBookingProperty(in);
        }

        @Override
        public FlightBookingProperty[] newArray(int size) {
            return new FlightBookingProperty[size];
        }
    };

    public void setStartDate(Calendar date) {
        mStartDate = date;
        calculateEndDate();
    }

    public Calendar getStartDate() {
        return mStartDate;
    }

    public Calendar getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Calendar date) {
        mEndDate = date;
    }

    public int getAdultCount() {
        return mAdultCount;
    }

    public void setAdultCount(int adultCount) {
        this.mAdultCount = adultCount;
    }

    public int getChildrenCount() {
        return mChildrenCount;
    }

    public void setChildrenCount(int childrenCount) {
        this.mChildrenCount = childrenCount;
    }

    public int getNewbornCount() {
        return mNewbornCount;
    }

    public void setNewbornCount(int newbornCount) {
        this.mNewbornCount = newbornCount;
    }

    public AirSeat getSeatType() {
        return mSeatType;
    }

    public void setSeatType(AirSeat type) {
        mSeatType = type;
    }

    public int getDuration() {
        if (mStartDate == null || mEndDate == null) {
            return 0;
        }

        long diff = mEndDate.getTimeInMillis() - mStartDate.getTimeInMillis();
        return (int) (diff / (24 * 60 * 60 * 1000));
    }

    public Location getStartLoc() {
        return mStartLoc;
    }

    public void setStartLoc(Location startLoc) {
        this.mStartLoc = startLoc;
    }

    public Location getEndLoc() {
        return mEndLoc;
    }

    public void setEndLoc(Location endLoc) {
        this.mEndLoc = endLoc;
    }

    public void setRoundTrip(boolean isRoundTrip) {
        this.isRoundTrip = isRoundTrip;
    }

    public boolean isRoundTrip() {
        return isRoundTrip;
    }

    public int getTotalCount() {
        return mAdultCount + mChildrenCount + mNewbornCount;
    }

    public Calendar getDefaultDate() {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, 1);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        return date;
    }

    private void calculateEndDate() {
        mEndDate = (Calendar) mStartDate.clone();
        mEndDate.add(Calendar.DAY_OF_YEAR, 2);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mAdultCount);
        dest.writeInt(mChildrenCount);
        dest.writeInt(mNewbornCount);
        dest.writeParcelable(mSeatType, flags);
        dest.writeParcelable(mStartLoc, flags);
        dest.writeParcelable(mEndLoc, flags);
        dest.writeSerializable(mStartDate);
        dest.writeSerializable(mEndDate);
        dest.writeInt(isRoundTrip ? 1 : 0);
    }
}
