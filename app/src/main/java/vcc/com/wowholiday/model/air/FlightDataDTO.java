package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.model.Sort;

/**
 * Created by Pham Hai Quang on 11/15/2019.
 */
public class FlightDataDTO implements Parcelable {
    public List<FlightTrip> flightTripList = new ArrayList<>();
    public List<FlightFilter> flightFilterList = new ArrayList<>();
    public List<Sort> flightSortList = new ArrayList<>();

    public FlightDataDTO() {}

    protected FlightDataDTO(Parcel in) {
        flightTripList = in.createTypedArrayList(FlightTrip.CREATOR);
        flightFilterList = in.createTypedArrayList(FlightFilter.CREATOR);
        flightSortList = in.createTypedArrayList(Sort.CREATOR);
    }

    public static final Creator<FlightDataDTO> CREATOR = new Creator<FlightDataDTO>() {
        @Override
        public FlightDataDTO createFromParcel(Parcel in) {
            return new FlightDataDTO(in);
        }

        @Override
        public FlightDataDTO[] newArray(int size) {
            return new FlightDataDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(flightTripList);
        dest.writeTypedList(flightFilterList);
        dest.writeTypedList(flightSortList);
    }
}
