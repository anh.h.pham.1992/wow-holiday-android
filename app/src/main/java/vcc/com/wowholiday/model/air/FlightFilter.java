package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

import vcc.com.wowholiday.model.RangeValue;

/**
 * Created by Pham Hai Quang on 11/15/2019.
 */
public class FlightFilter implements Parcelable {
    public static final String RADIO_TYPE = "RADIO_TYPE";
    public static final String RANGE_TYPE = "RANGE_TYPE";
    public static final String RANGE_LIST_TYPE = "RANGE_TYPE_LIST";
    public static final String CHECKBOX_TYPE = "CHECKBOX_TYPE";

    private String mType;
    private String mName;
    private String[] mValues;
    private RangeValue[] mRangeValues;

    public FlightFilter() {}

    protected FlightFilter(Parcel in) {
        mType = in.readString();
        mName = in.readString();
        mValues = in.createStringArray();
        mRangeValues = in.createTypedArray(RangeValue.CREATOR);
    }

    public static final Creator<FlightFilter> CREATOR = new Creator<FlightFilter>() {
        @Override
        public FlightFilter createFromParcel(Parcel in) {
            return new FlightFilter(in);
        }

        @Override
        public FlightFilter[] newArray(int size) {
            return new FlightFilter[size];
        }
    };

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String[] getValues() {
        return mValues;
    }

    public void setValues(String[] values) {
        this.mValues = values;
    }

    public RangeValue[] getRangeValues() {
        return mRangeValues;
    }

    public void setRangeValues(RangeValue[] rangeValues) {
        this.mRangeValues = rangeValues;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeString(mName);
        dest.writeStringArray(mValues);
        dest.writeTypedArray(mRangeValues, flags);
    }
}
