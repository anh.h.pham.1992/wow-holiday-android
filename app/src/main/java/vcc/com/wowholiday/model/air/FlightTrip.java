package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.DateInfo;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class FlightTrip implements Parcelable {
    public static final String TYPE_ONE_WAY = "oneway";
    public static final String TYPE_ROUND_TRIP = "roundtrip";

    private String mType;
    private FlightWay mDepartureWay;
    private FlightWay mArrivalWay;
    private long mTotalDur;
    private long mJourneyDuration;
    private DateInfo mDateInfo;
    private String mPrice;
    private String mCurrencyUnit;
    private Vendor mVendor;
    private PaxInfo[] mPaxInfos;
    private boolean isDomestic;

    public FlightTrip() {}

    protected FlightTrip(Parcel in) {
        mType = in.readString();
        mDepartureWay = in.readParcelable(FlightWay.class.getClassLoader());
        mArrivalWay = in.readParcelable(FlightWay.class.getClassLoader());
        mTotalDur = in.readLong();
        mJourneyDuration = in.readLong();
        mDateInfo = in.readParcelable(DateInfo.class.getClassLoader());
        mPrice = in.readString();
        mCurrencyUnit = in.readString();
        mVendor = in.readParcelable(Vendor.class.getClassLoader());
        mPaxInfos = in.createTypedArray(PaxInfo.CREATOR);
        isDomestic = in.readInt() == 1;
    }

    public static final Creator<FlightTrip> CREATOR = new Creator<FlightTrip>() {
        @Override
        public FlightTrip createFromParcel(Parcel in) {
            return new FlightTrip(in);
        }

        @Override
        public FlightTrip[] newArray(int size) {
            return new FlightTrip[size];
        }
    };

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public FlightWay getDepartureWay() {
        return mDepartureWay;
    }

    public void setDepartureWay(FlightWay departureFlight) {
        this.mDepartureWay = departureFlight;
    }

    public FlightWay getArrivalWay() {
        return mArrivalWay;
    }

    public void setArrivalWay(FlightWay arrivalFlights) {
        this.mArrivalWay = arrivalFlights;
    }

    public long getTotalDur() {
        return mTotalDur;
    }

    public void setTotalDur(long dur) {
        this.mTotalDur = dur;
    }

    public long getJourneyDuration() {
        return mJourneyDuration;
    }

    public void setJourneyDuration(long journeyDuration) {
        this.mJourneyDuration = journeyDuration;
    }

    public DateInfo getDateInfo() {
        return mDateInfo;
    }

    public void setDateInfo(DateInfo dateInfo) {
        this.mDateInfo = dateInfo;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        this.mPrice = price;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.mCurrencyUnit = currencyUnit;
    }

    public Vendor getVendor() {
        return mVendor;
    }

    public void setVendor(Vendor vendor) {
        this.mVendor = vendor;
    }

    public PaxInfo[] getPaxInfos() {
        return mPaxInfos;
    }

    public void setPaxInfos(PaxInfo[] paxInfos) {
        this.mPaxInfos = paxInfos;
    }

    public boolean isDomestic() {
        return isDomestic;
    }

    public void setDomestic(boolean domestic) {
        isDomestic = domestic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeParcelable(mDepartureWay, flags);
        dest.writeParcelable(mArrivalWay, flags);
        dest.writeLong(mTotalDur);
        dest.writeLong(mJourneyDuration);
        dest.writeParcelable(mDateInfo, flags);
        dest.writeString(mPrice);
        dest.writeString(mCurrencyUnit);
        dest.writeParcelable(mVendor, flags);
        dest.writeTypedArray(mPaxInfos, flags);
        dest.writeInt(isDomestic ? 1 : 0);
    }
}
