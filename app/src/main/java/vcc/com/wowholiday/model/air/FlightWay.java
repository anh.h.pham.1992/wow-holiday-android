package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 11/18/2019.
 */
public class FlightWay implements Parcelable {
    public static final int DEPARTURE = 1;
    public static final int ARRIVAL = 2;

    private Flight[] mTransits;
    private Location mStartLocation;
    private Location mEndLocation;
    private DateInfo mDateInfo;

    //Hour and minute
    private int[] mDuration;
    private int mType;
    private float mPrice;
    private String mCurrencyUnit;

    // format hh:mm
    private String mStartTime;
    private String mEndTime;
    //format dd/MM
    private String mEndDay;

    public FlightWay(){}


    protected FlightWay(Parcel in) {
        mTransits = in.createTypedArray(Flight.CREATOR);
        mStartLocation = in.readParcelable(Location.class.getClassLoader());
        mEndLocation = in.readParcelable(Location.class.getClassLoader());
        mDateInfo = in.readParcelable(DateInfo.class.getClassLoader());
        mType = in.readInt();
        mPrice = in.readFloat();
        mCurrencyUnit = in.readString();
        mDuration = new int[2];
        in.readIntArray(mDuration);
    }

    public static final Creator<FlightWay> CREATOR = new Creator<FlightWay>() {
        @Override
        public FlightWay createFromParcel(Parcel in) {
            return new FlightWay(in);
        }

        @Override
        public FlightWay[] newArray(int size) {
            return new FlightWay[size];
        }
    };

    public Flight[] getTransits() {
        return mTransits;
    }

    public void setTransits(Flight[] transits) {
        this.mTransits = transits;
    }

    public Location getStartLocation() {
        return mStartLocation;
    }

    public void setStartLocation(Location startLocation) {
        this.mStartLocation = startLocation;
    }

    public Location getEndLocation() {
        return mEndLocation;
    }

    public void setEndLocation(Location endLocation) {
        this.mEndLocation = endLocation;
    }

    public DateInfo getDateInfo() {
        return mDateInfo;
    }

    public void setDateInfo(DateInfo dateInfo) {
        this.mDateInfo = dateInfo;
    }

    public int[] getDuration() {
        return mDuration;
    }

    public void setDuration(int[] duration) {
        this.mDuration = duration;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.mCurrencyUnit = currencyUnit;
    }

    public String getTransit() {
        if (mTransits == null || mTransits.length < 2) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < mTransits.length; i++) {
            builder.append(mTransits[i].getStartPoint().getCity());
            if (i != mTransits.length - 1) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

    public String getTransitIDs() {
        if (mTransits == null || mTransits.length < 2) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < mTransits.length; i++) {
            builder.append(mTransits[i].getStartPoint().getID());
            if (i != mTransits.length - 1) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

    public String getVendors() {
        String[] vendorIDs = new String[mTransits.length];
        StringBuilder builder = new StringBuilder();
        int index = 0;
        for (Flight flight : mTransits) {
            if (!contain(flight.getVendors().getCode(), vendorIDs)) {
                vendorIDs[index] = flight.getVendors().getCode();
                index++;
                builder.append(flight.getVendors().getName()).append(", ");
            }
        }
        builder.replace(builder.length() - 2, builder.length(), "");
        return builder.toString();
    }

    public String getEndDay() {
        if (mEndDay == null) {
            mEndDay = TimeFormatUtil.getDefaultDayFormatFromISO8601UTC(mDateInfo.getEndDate());
        }
        return mEndDay;
    }

    public String getStartTime() {
        if (mStartTime == null) {
            mStartTime = TimeFormatUtil.getDefaultTimeFormatFromISO8601UTC(mDateInfo.getStartDate());
        }
        return mStartTime;
    }

    public String getEndTime() {
        if (mEndTime == null) {
            mEndTime = TimeFormatUtil.getDefaultTimeFormatFromISO8601UTC(mDateInfo.getEndDate());
        }
        return mEndTime;
    }

    /*public long getDuration() {
        long start = TimeFormatUtil.fromISO8601UTCToMillis(mDateInfo.getStartDate());
        long end = TimeFormatUtil.fromISO8601UTCToMillis(mDateInfo.getEndDate());
        return end - start;
    }*/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(mTransits, flags);
        dest.writeParcelable(mStartLocation, flags);
        dest.writeParcelable(mEndLocation, flags);
        dest.writeParcelable(mDateInfo, flags);
        dest.writeInt(mType);
        dest.writeFloat(mPrice);
        dest.writeString(mCurrencyUnit);
        dest.writeIntArray(mDuration);
    }

    private boolean contain(String elm, String[] array) {
        for (String itm : array) {
            if (elm.equals(itm)) {
                return true;
            }
        }
        return false;
    }
}
