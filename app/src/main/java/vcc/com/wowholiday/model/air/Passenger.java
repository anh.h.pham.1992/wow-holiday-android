package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/22/2019.
 */
public class Passenger implements Parcelable {
    public static final String MR = "MR";
    public static final String MRS = "MRS";

    private String mFistName;
    private String mLastName;
    private String mBirthday;
    private String mNation;
    private String mPassport;
    private String mPassportOfNation;
    private String mPassprortExpired;
    private String mGender = MR;
    private Baggage mBaggageDepart;
    private Baggage mBaggageArrival;
    private PaxInfo mPaxInfo;

    public Passenger(){}

    protected Passenger(Parcel in) {
        mFistName = in.readString();
        mLastName = in.readString();
        mBirthday = in.readString();
        mNation = in.readString();
        mPassport = in.readString();
        mPassportOfNation = in.readString();
        mPassprortExpired = in.readString();
        mGender = in.readString();
        mBaggageDepart = in.readParcelable(Baggage.class.getClassLoader());
        mBaggageArrival = in.readParcelable(Baggage.class.getClassLoader());
        mPaxInfo = in.readParcelable(PaxInfo.class.getClassLoader());
    }

    public static final Creator<Passenger> CREATOR = new Creator<Passenger>() {
        @Override
        public Passenger createFromParcel(Parcel in) {
            return new Passenger(in);
        }

        @Override
        public Passenger[] newArray(int size) {
            return new Passenger[size];
        }
    };

    public String getFistName() {
        return mFistName;
    }

    public void setFistName(String fistName) {
        this.mFistName = fistName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String birthday) {
        this.mBirthday = birthday;
    }

    public String getNation() {
        return mNation;
    }

    public void setNation(String nation) {
        this.mNation = nation;
    }

    public String getPassport() {
        return mPassport;
    }

    public void setPassport(String passport) {
        this.mPassport = passport;
    }

    public String getPassportOfNation() {
        return mPassportOfNation;
    }

    public void setPassportOfNation(String passportOfNation) {
        this.mPassportOfNation = passportOfNation;
    }

    public String getPassprortExpired() {
        return mPassprortExpired;
    }

    public void setPassprortExpired(String passprortExpired) {
        this.mPassprortExpired = passprortExpired;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        this.mGender = gender;
    }

    public Baggage getBaggageDepart() {
        return mBaggageDepart;
    }

    public void setBaggageDepart(Baggage baggageDepart) {
        this.mBaggageDepart = baggageDepart;
    }

    public Baggage getBaggageArrival() {
        return mBaggageArrival;
    }

    public void setBaggageArrival(Baggage baggageArrival) {
        this.mBaggageArrival = baggageArrival;
    }

    public PaxInfo getPaxInfo() {
        return mPaxInfo;
    }

    public void setPaxInfo(PaxInfo paxInfo) {
        this.mPaxInfo = paxInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFistName);
        dest.writeString(mLastName);
        dest.writeString(mBirthday);
        dest.writeString(mNation);
        dest.writeString(mPassport);
        dest.writeString(mPassportOfNation);
        dest.writeString(mPassprortExpired);
        dest.writeString(mGender);
        dest.writeParcelable(mBaggageDepart, flags);
        dest.writeParcelable(mBaggageArrival, flags);
        dest.writeParcelable(mPaxInfo, flags);
    }
}
