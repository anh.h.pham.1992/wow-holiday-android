package vcc.com.wowholiday.model.air;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pham Hai Quang on 11/17/2019.
 */
public class PaxInfo implements Parcelable {

    public static final String ADULT = "ADULT";
    public static final String CHILD = "CHILD";
    public static final String NEW_BORN = "NEW_BORN";

    private int mQuantity;
    private String mType;
    private String mAmount;
    private String mCurrencyCode;

    public PaxInfo() {}

    protected PaxInfo(Parcel in) {
        mQuantity = in.readInt();
        mType = in.readString();
        mAmount = in.readString();
        mCurrencyCode = in.readString();
    }

    public static final Creator<PaxInfo> CREATOR = new Creator<PaxInfo>() {
        @Override
        public PaxInfo createFromParcel(Parcel in) {
            return new PaxInfo(in);
        }

        @Override
        public PaxInfo[] newArray(int size) {
            return new PaxInfo[size];
        }
    };

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        this.mQuantity = quantity;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String amount) {
        this.mAmount = amount;
    }

    public String getCurrencyCode() {
        return mCurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.mCurrencyCode = currencyCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mQuantity);
        dest.writeString(mType);
        dest.writeString(mAmount);
        dest.writeString(mCurrencyCode);
    }
}
