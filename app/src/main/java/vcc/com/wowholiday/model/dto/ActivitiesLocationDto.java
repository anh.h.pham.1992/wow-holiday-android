package vcc.com.wowholiday.model.dto;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */
public class ActivitiesLocationDto {
    public List<Location> searchData = new ArrayList<>();
    public List<Location> recentData = new ArrayList<>();
}
