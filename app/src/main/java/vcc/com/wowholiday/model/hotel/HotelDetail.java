package vcc.com.wowholiday.model.hotel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.ContactInfo;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Image;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Vendor;

public class HotelDetail implements Parcelable {
    private String mID;
    private String mName;
    private int mRating;
    private int mRatingCount;
    private String mCategory;
    private List<RoomInfo> mRooms;
    private String mBusiness;
    private String mDescription;
    private DateInfo mDateInfo;
    private Location mLocation;
    private List<Image> mImages;
    private ContactInfo mContactInfo;
    private Vendor mVendor;
    private List<Amenity> mAmenities;
    private String mObjectIdentifier;
    private String mStatus;
    private float mPrice;
    private float mOriginalPrice;
    private String mCurrencyUnit;

    public HotelDetail() {}

    protected HotelDetail(Parcel in) {
        mID = in.readString();
        mName = in.readString();
        mRating = in.readInt();
        mRatingCount = in.readInt();
        mCategory = in.readString();
        mRooms = in.createTypedArrayList(RoomInfo.CREATOR);
        mBusiness = in.readString();
        mDescription = in.readString();
        mDateInfo = in.readParcelable(DateInfo.class.getClassLoader());
        mLocation = in.readParcelable(Location.class.getClassLoader());
        mImages = in.createTypedArrayList(Image.CREATOR);
        mContactInfo = in.readParcelable(ContactInfo.class.getClassLoader());
        mVendor = in.readParcelable(Location.class.getClassLoader());
        mAmenities = in.createTypedArrayList(Amenity.CREATOR);
        mObjectIdentifier = in.readString();
        mStatus = in.readString();
        mPrice = in.readFloat();
        mOriginalPrice = in.readFloat();
        mCurrencyUnit = in.readString();
    }

    public static final Creator<HotelDetail> CREATOR = new Creator<HotelDetail>() {
        @Override
        public HotelDetail createFromParcel(Parcel in) {
            return new HotelDetail(in);
        }

        @Override
        public HotelDetail[] newArray(int size) {
            return new HotelDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mID);
        dest.writeString(mName);
        dest.writeInt(mRating);
        dest.writeInt(mRatingCount);
        dest.writeString(mCategory);
        dest.writeTypedList(mRooms);
        dest.writeString(mBusiness);
        dest.writeString(mDescription);
        dest.writeParcelable(mDateInfo,flags);
        dest.writeParcelable(mLocation, flags);
        dest.writeTypedList(mImages);
        dest.writeParcelable(mContactInfo, flags);
        dest.writeParcelable(mVendor, flags);
        dest.writeTypedList(mAmenities);
        dest.writeString(mObjectIdentifier);
        dest.writeString(mStatus);
        dest.writeFloat(mPrice);
        dest.writeFloat(mOriginalPrice);
        dest.writeString(mCurrencyUnit);
    }

    public String getID() {
        return mID;
    }

    public void setID(String mID) {
        this.mID = mID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getRating() {
        return mRating;
    }

    public void setRating(int mRating) {
        this.mRating = mRating;
    }

    public int getRatingCount() {
        return mRatingCount;
    }

    public void setRatingCount(int mRatingCount) {
        this.mRatingCount = mRatingCount;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public List<RoomInfo> getRooms() {
        return mRooms;
    }

    public void setRooms(List<RoomInfo> mRooms) {
        this.mRooms = mRooms;
    }

    public String getBusiness() {
        return mBusiness;
    }

    public void setBusiness(String mBusiness) {
        this.mBusiness = mBusiness;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public DateInfo getDateInfo() {
        return mDateInfo;
    }

    public void setDateInfo(DateInfo mDateInfo) {
        this.mDateInfo = mDateInfo;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location mLocation) {
        this.mLocation = mLocation;
    }

    public List<Image> getImages() {
        return mImages;
    }

    public void setImages(List<Image> mImages) {
        this.mImages = mImages;
    }

    public ContactInfo getContactInfo() {
        return mContactInfo;
    }

    public void setContactInfo(ContactInfo mContactInfo) {
        this.mContactInfo = mContactInfo;
    }

    public Vendor getVendor() {
        return mVendor;
    }

    public void setVendor(Vendor mVendor) {
        this.mVendor = mVendor;
    }

    public List<Amenity> getAmenities() {
        return mAmenities;
    }

    public void setAmenities(List<Amenity> mAmenities) {
        this.mAmenities = mAmenities;
    }

    public String getObjectIdentifier() {
        return mObjectIdentifier;
    }

    public void setObjectIdentifier(String mObjectIdentifier) {
        this.mObjectIdentifier = mObjectIdentifier;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float mPrice) {
        this.mPrice = mPrice;
    }

    public float getOriginalPrice() {
        return mOriginalPrice;
    }

    public void setOriginalPrice(float mOriginalPrice) {
        this.mOriginalPrice = mOriginalPrice;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String mCurrencyUnit) {
        this.mCurrencyUnit = mCurrencyUnit;
    }
}
