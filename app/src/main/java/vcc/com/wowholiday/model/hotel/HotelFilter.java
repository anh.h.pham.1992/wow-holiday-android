package vcc.com.wowholiday.model.hotel;

import android.os.Parcel;
import android.os.Parcelable;

import vcc.com.wowholiday.model.RangeValue;

public class HotelFilter implements Parcelable {
    public static final String INPUT_TYPE = "INPUT_TYPE";
    public static final String RANGE_TYPE = "RANGE_TYPE";
    public static final String CHECKBOX_TYPE = "CHECKBOX_TYPE";

    private String mType;
    private String mName;
    private String defaultValue;
    private String[] mValues;
    private RangeValue[] mRangeValues;


    public HotelFilter() {}

    protected HotelFilter(Parcel in) {
        mType = in.readString();
        mName = in.readString();
        mValues = in.createStringArray();
        mRangeValues = in.createTypedArray(RangeValue.CREATOR);
    }

    public static final Creator<HotelFilter> CREATOR = new Creator<HotelFilter>() {
        @Override
        public HotelFilter createFromParcel(Parcel in) {
            return new HotelFilter(in);
        }

        @Override
        public HotelFilter[] newArray(int size) {
            return new HotelFilter[size];
        }
    };

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String[] getValues() {
        return mValues;
    }

    public void setValues(String[] values) {
        this.mValues = values;
    }

    public RangeValue[] getRangeValues() {
        return mRangeValues;
    }

    public void setRangeValues(RangeValue[] rangeValues) {
        this.mRangeValues = rangeValues;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeString(mName);
        dest.writeStringArray(mValues);
        dest.writeTypedArray(mRangeValues, flags);
    }
}
