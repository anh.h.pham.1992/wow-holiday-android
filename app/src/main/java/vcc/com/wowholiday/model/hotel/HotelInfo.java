package vcc.com.wowholiday.model.hotel;

import android.os.Parcel;
import android.os.Parcelable;

import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Vendor;

public class HotelInfo implements Parcelable {
    private String mID;
    private String mName;
    private String mAvatarUrl;
    private int mRating;
    private int mRatingCount;
    private String mBusiness;
    private String mObjectIdentifier;
    private String mToken;
    private String mStatus;
    private DateInfo mDateInfo;
    private String mDescription;
    private Location mLocation;
    private float mPrice;
    private float mOriginalPrice;
    private String mCurrencyUnit;
    private Vendor mVendor;
    private String mSearchToken;

    public HotelInfo() {}

    protected HotelInfo(Parcel in) {
        mID = in.readString();
        mName = in.readString();
        mAvatarUrl = in.readString();
        mRating = in.readInt();
        mRatingCount = in.readInt();
        mBusiness = in.readString();
        mObjectIdentifier = in.readString();
        mToken = in.readString();
        mStatus = in.readString();
        mDateInfo = in.readParcelable(DateInfo.class.getClassLoader());
        mDescription = in.readString();
        mLocation = in.readParcelable(Location.class.getClassLoader());
        mPrice = in.readFloat();
        mOriginalPrice = in.readFloat();
        mCurrencyUnit = in.readString();
        mVendor = in.readParcelable(Location.class.getClassLoader());
        mSearchToken = in.readString();
    }

    public static final Creator<HotelInfo> CREATOR = new Creator<HotelInfo>() {
        @Override
        public HotelInfo createFromParcel(Parcel in) {
            return new HotelInfo(in);
        }

        @Override
        public HotelInfo[] newArray(int size) {
            return new HotelInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mID);
        dest.writeString(mName);
        dest.writeString(mAvatarUrl);
        dest.writeInt(mRating);
        dest.writeInt(mRatingCount);
        dest.writeString(mBusiness);
        dest.writeString(mObjectIdentifier);
        dest.writeString(mToken);
        dest.writeString(mStatus);
        dest.writeParcelable(mDateInfo,flags);
        dest.writeString(mDescription);
        dest.writeParcelable(mLocation, flags);
        dest.writeFloat(mPrice);
        dest.writeFloat(mOriginalPrice);
        dest.writeString(mCurrencyUnit);
        dest.writeParcelable(mVendor, flags);
        dest.writeString(mSearchToken);
    }

    public String getID() {
        return mID;
    }

    public void setID(String mID) {
        this.mID = mID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String mAvatarUrl) {
        this.mAvatarUrl = mAvatarUrl;
    }

    public int getRating() {
        return mRating;
    }

    public void setRating(int mRating) {
        this.mRating = mRating;
    }

    public int getRatingCount() {
        return mRatingCount;
    }

    public void setRatingCount(int mRatingCount) {
        this.mRatingCount = mRatingCount;
    }

    public String getBusiness() {
        return mBusiness;
    }

    public void setBusiness(String mBusiness) {
        this.mBusiness = mBusiness;
    }

    public String getObjectIdentifier() {
        return mObjectIdentifier;
    }

    public void setObjectIdentifier(String mObjectIdentifier) {
        this.mObjectIdentifier = mObjectIdentifier;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String mToken) {
        this.mToken = mToken;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public DateInfo getDateInfo() {
        return mDateInfo;
    }

    public void setDateInfo(DateInfo mDateInfo) {
        this.mDateInfo = mDateInfo;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location mLocation) {
        this.mLocation = mLocation;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float mPrice) {
        this.mPrice = mPrice;
    }

    public float getOriginalPrice() {
        return mOriginalPrice;
    }

    public void setOriginalPrice(float mOriginalPrice) {
        this.mOriginalPrice = mOriginalPrice;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String mCurrencyUnit) {
        this.mCurrencyUnit = mCurrencyUnit;
    }

    public Vendor getVendor() {
        return mVendor;
    }

    public void setVendor(Vendor mVendor) {
        this.mVendor = mVendor;
    }

    public String getSearchToken() {
        return mSearchToken;
    }

    public void setSearchToken(String mSearchToken) {
        this.mSearchToken = mSearchToken;
    }
}
