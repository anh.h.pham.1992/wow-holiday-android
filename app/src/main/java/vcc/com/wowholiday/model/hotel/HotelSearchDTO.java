package vcc.com.wowholiday.model.hotel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.model.Sort;

public class HotelSearchDTO implements Parcelable {
    public List<HotelInfo> hotelInfoList = new ArrayList<>();
    public List<HotelFilter> hotelFilterList = new ArrayList<>();
    public List<Sort> hotelSortList = new ArrayList<>();
    public String token;

    public HotelSearchDTO() {}

    protected HotelSearchDTO(Parcel in) {
        hotelInfoList = in.createTypedArrayList(HotelInfo.CREATOR);
        hotelFilterList = in.createTypedArrayList(HotelFilter.CREATOR);
        hotelSortList = in.createTypedArrayList(Sort.CREATOR);
        token = in.readString();
    }

    public static final Creator<HotelSearchDTO> CREATOR = new Creator<HotelSearchDTO>() {
        @Override
        public HotelSearchDTO createFromParcel(Parcel in) {
            return new HotelSearchDTO(in);
        }

        @Override
        public HotelSearchDTO[] newArray(int size) {
            return new HotelSearchDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(hotelInfoList);
        dest.writeTypedList(hotelFilterList);
        dest.writeTypedList(hotelSortList);
        dest.writeString(token);
    }
}
