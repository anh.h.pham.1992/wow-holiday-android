package vcc.com.wowholiday.model.hotel;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class HotelSuggestion implements Parcelable {
    private String mID;
    private String mName;
    private String mCountryID;
    private String mType;
    private int mPriority;

    public HotelSuggestion() {
    }

    protected HotelSuggestion(Parcel in) {
        mID = in.readString();
        mName = in.readString();
        mCountryID = in.readString();
        mType = in.readString();
        mPriority = in.readInt();
    }

    public static final Creator<HotelSuggestion> CREATOR = new Creator<HotelSuggestion>() {
        @Override
        public HotelSuggestion createFromParcel(Parcel in) {
            return new HotelSuggestion(in);
        }

        @Override
        public HotelSuggestion[] newArray(int size) {
            return new HotelSuggestion[size];
        }
    };

    public String getID() {
        return mID;
    }

    public void setID(String ID) {
        this.mID = ID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }


    public String getCountryID() {
        return mCountryID;
    }

    public void setCountryID(String countryID) {
        this.mCountryID = countryID;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public int getPriority() {
        return mPriority;
    }

    public void setPriority(int priority) {
        this.mPriority = priority;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mID);
        dest.writeString(mName);
        dest.writeString(mCountryID);
        dest.writeString(mType);
        dest.writeInt(mPriority);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof HotelSuggestion) {
            return mID.equals(((HotelSuggestion) obj).mID);
        }

        return super.equals(obj);
    }

    @NonNull
    @Override
    public String toString() {
        return "{ID:" + mID + ", name:" + mName + ", countryID:" + mCountryID + ", type:" + mType + ", priority:" + mPriority + "}";
    }
}
