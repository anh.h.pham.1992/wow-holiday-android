package vcc.com.wowholiday.model.hotel;

import java.util.List;

public class HotelSuggestionList  {
    public static String TYPE_LOCATION = "location";
    public static String TYPE_HOTEL= "hotel";
    public static String TYPE_RECENT_SEARCH= "recentlocations";
    private String mType;
    private List<HotelSuggestion> mList;

    public HotelSuggestionList() {
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public List<HotelSuggestion> getList() {
        return mList;
    }

    public void setList(List<HotelSuggestion> list) {
        this.mList = list;
    }
}
