package vcc.com.wowholiday.model.hotel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.Image;

public class RoomInfo implements Parcelable {
    private String mName;
    private String mBusiness;
    private String mObjectIdentifier;
    private String mToken;
    private String mCode;
    private String mStatus;
    private boolean mBoardingStatus;
    private List<Image> mImages;
    private List<Amenity> mAmenities;
    private float mPrice;
    private float mStrikeThroughPrice;
    private float mOriginalPrice;
    private String mCurrencyUnit;
    private Image mAvatar;

    public RoomInfo() {

    }

    protected RoomInfo(Parcel in) {
        mName = in.readString();
        mBusiness = in.readString();
        mObjectIdentifier = in.readString();
        mToken = in.readString();
        mCode = in.readString();
        mStatus = in.readString();
        mBoardingStatus = in.readInt() == 1;
        mImages = in.createTypedArrayList(Image.CREATOR);
        mAmenities = in.createTypedArrayList(Amenity.CREATOR);
        mPrice = in.readFloat();
        mStrikeThroughPrice = in.readFloat();
        mOriginalPrice = in.readFloat();
        mCurrencyUnit = in.readString();
        mAvatar = in.readParcelable(Image.class.getClassLoader());

    }

    public static final Creator<RoomInfo> CREATOR = new Creator<RoomInfo>() {
        @Override
        public RoomInfo createFromParcel(Parcel in) {
            return new RoomInfo(in);
        }

        @Override
        public RoomInfo[] newArray(int size) {
            return new RoomInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mBusiness);
        dest.writeString(mObjectIdentifier);
        dest.writeString(mToken);
        dest.writeString(mCode);
        dest.writeString(mStatus);
        dest.writeInt(mBoardingStatus ? 1 : 0);
        dest.writeTypedList(mImages);
        dest.writeTypedList(mAmenities);
        dest.writeFloat(mPrice);
        dest.writeFloat(mStrikeThroughPrice);
        dest.writeFloat(mOriginalPrice);
        dest.writeString(mCurrencyUnit);
        dest.writeParcelable(mAvatar, flags);
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getBusiness() {
        return mBusiness;
    }

    public void setBusiness(String mBusiness) {
        this.mBusiness = mBusiness;
    }

    public String getObjectIdentifier() {
        return mObjectIdentifier;
    }

    public void setObjectIdentifier(String mObjectIdentifier) {
        this.mObjectIdentifier = mObjectIdentifier;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String mToken) {
        this.mToken = mToken;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public boolean getBoardingStatus() {
        return mBoardingStatus;
    }

    public void setBoardingStatus(boolean mBoardingStatus) {
        this.mBoardingStatus = mBoardingStatus;
    }

    public List<Image> getImages() {
        return mImages;
    }

    public void setImages(List<Image> mImages) {
        this.mImages = mImages;
    }

    public List<Amenity> getAmenities() {
        return mAmenities;
    }

    public void setAmenities(List<Amenity> mAmenities) {
        this.mAmenities = mAmenities;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float mPrice) {
        this.mPrice = mPrice;
    }

    public float getStrikeThroughPrice() {
        return mStrikeThroughPrice;
    }

    public void setStrikeThroughPrice(float mStrikeThroughPrice) {
        this.mStrikeThroughPrice = mStrikeThroughPrice;
    }

    public float getOriginalPrice() {
        return mOriginalPrice;
    }

    public void setOriginalPrice(float mOriginalPrice) {
        this.mOriginalPrice = mOriginalPrice;
    }

    public String getCurrencyUnit() {
        return mCurrencyUnit;
    }

    public void setCurrencyUnit(String mCurrencyUnit) {
        this.mCurrencyUnit = mCurrencyUnit;
    }

    public Image getAvatar() {
        return mAvatar;
    }

    public void setAvatar(Image mAvatar) {
        this.mAvatar = mAvatar;
    }
}
