package vcc.com.wowholiday.presenter;

/**
 * Created by QuangPH on 2020-02-19.
 */
public class RegisterType {
    public static final int PHONE = 1;
    public static final int EMAIL = 2;
}
