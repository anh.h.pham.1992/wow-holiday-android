package vcc.com.wowholiday.presenter.activitiesscreen;

import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.presenter.activitiesscreen.activitydetail.ActivitiesDetailActivity;
import vcc.com.wowholiday.presenter.activitiesscreen.locationsearch.ActivitiesLocationSearchActivity;
import vcc.com.wowholiday.presenter.activitiesscreen.searchresult.SearchResultActivity;

/**
 * Created by Pham Hai Quang on 10/19/2019.
 */

@Layout(R.layout.activity_activities)
public class ActivitiesActivity extends WHActivity<ActivitiesView> {

    private static final int LOC_SEARCH_REQ = 2345;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOC_SEARCH_REQ && resultCode == RESULT_OK) {
            mView.showLocation((Location) data.getParcelableExtra(ActivitiesLocationSearchActivity.LOCATION_KEY));
        }
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ActivitiesView.SearchLocCmd) {
            gotoSearchLocation();
        } else if (command instanceof ActivitiesView.SearchCmd) {
            gotoSearchResult();
        } else if (command instanceof ActivitiesItemAdapter.AcitvitiesSelectedCmd) {
            Intent itn = new Intent(this, ActivitiesDetailActivity.class);
            ActivitiesDetailActivity.createParams(itn, ((ActivitiesItemAdapter.AcitvitiesSelectedCmd) command).info);
            startActivity(itn);
        }
    }

    private void gotoSearchLocation() {
        Intent itn = new Intent(this, ActivitiesLocationSearchActivity.class);
        startActivityForResult(itn, LOC_SEARCH_REQ);
    }

    private void gotoSearchResult() {
        Intent itn = new Intent(this, SearchResultActivity.class);
        startActivity(itn);
    }
}
