package vcc.com.wowholiday.presenter.activitiesscreen;

import android.graphics.Outline;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;

/**
 * Created by Pham Hai Quang on 10/19/2019.
 */
public class ActivitiesItemAdapter extends BaseRclvAdapter {

    private IImageLoader mImageLoader = GlideImageLoader.getInstance();

    private IPresenter mPresenter;

    public ActivitiesItemAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_activities_item;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new ActivitiesVH(itemView);
    }

    private class ActivitiesVH extends BaseRclvHolder<ActivitiesInfo> {
        ImageView ivAvatar;
        TextView tvName;
        RatingBar ratingBar;
        TextView tvRatingCount;
        TextViewWithIcon tvicLocation;
        TextView tvOriginPrice;
        TextView tvDiscountPrice;

        public ActivitiesVH(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.item_activities_itm_imv_avatar);
            tvName = itemView.findViewById(R.id.item_activities_itm_tv_name);
            ratingBar = itemView.findViewById(R.id.item_activities_itm_rating);
            tvRatingCount = itemView.findViewById(R.id.item_activities_itm_tv_rate_count);
            tvicLocation = itemView.findViewById(R.id.item_activities_itm_tvic_loc);
            tvOriginPrice = itemView.findViewById(R.id.item_activities_itm_tv_origin_price);
            tvDiscountPrice = itemView.findViewById(R.id.item_activities_itm_tv_discount_price);

            tvOriginPrice.setPaintFlags(tvOriginPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            ivAvatar.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    int corner = (int) view.getResources().getDimension(R.dimen.corner_8);
                    outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), corner);
                }
            });
            ivAvatar.setClipToOutline(true);
            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    AcitvitiesSelectedCmd cmd = new AcitvitiesSelectedCmd();
                    cmd.info = (ActivitiesInfo) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(ActivitiesInfo vhData) {
            super.onBind(vhData);
            mImageLoader.loadImage(itemView, vhData.getAvatarUrl(), ivAvatar);
            tvName.setText(vhData.getName());
            ratingBar.setRating(vhData.getRating());
            tvRatingCount.setText("(" + String.valueOf(vhData.getRateCount()) + ")");
            tvicLocation.setText(vhData.getLocation().getName());

            if (!vhData.getPrice().equals(vhData.getOriginalPrice())) {
                tvOriginPrice.setVisibility(View.VISIBLE);
                tvOriginPrice.setText(MoneyFormatter.defaultFormat(String.valueOf(vhData.getOriginalPrice())) + " VNĐ");
            } else {
                tvOriginPrice.setVisibility(View.INVISIBLE);
            }
            tvDiscountPrice.setText(MoneyFormatter.defaultFormat(String.valueOf(vhData.getPrice())) + " VNĐ");
        }
    }

    public static class AcitvitiesSelectedCmd implements ICommand {
        public ActivitiesInfo info;
    }
}
