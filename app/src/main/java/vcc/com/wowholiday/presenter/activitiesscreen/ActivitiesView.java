package vcc.com.wowholiday.presenter.activitiesscreen;

import android.content.Context;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import common.presenter.SafeClicked;
import common.presenter.WHBottomSheetDialog;
import common.presenter.adapter.decor.GridPaddingDecorator;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesBookingProperty;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.presenter.dialog.DateSelectBottomSheet;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorBottomSheet;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorItem;
import vcc.com.wowholiday.presenter.util.SpannableBuilder;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/19/2019.
 */
public class ActivitiesView extends BaseRelativeView {

    private TextViewWithIcon mTvicStartDate;
    private TextViewWithIcon mTvicPersonCount;
    private TextViewWithIcon mTvicLocSearch;

    private GroupActivitiesAdapter mAdapter;

    private ActivitiesBookingProperty mProperty = new ActivitiesBookingProperty();

    public ActivitiesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        RecyclerView rclv = findViewById(R.id.activities_act_rclv_content);
        //int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        //rclv.addItemDecoration(new GridPaddingDecorator(padding));
        mAdapter = new GroupActivitiesAdapter(mPresenter);
        rclv.setAdapter(mAdapter);

        /*GridLayoutManager layoutManager = new GridLayoutManager(getContext(),1 );
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int itemType = mAdapter.getItemViewType(position);
                if (itemType == GroupActivitiesAdapter.ACTIVITIES_HOT
                        || itemType == GroupActivitiesAdapter.HOT_DEAL) {
                    return 1;
                }

                return 2;
            }
        });*/
        rclv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter.setHotDeal(dummy());
        mAdapter.setActivitiesHot(dummy());

        mTvicLocSearch = findViewById(R.id.activities_act_tvic_loc_search);
        mTvicLocSearch.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new SearchLocCmd());
            }
        });

        mTvicStartDate = findViewById(R.id.activities_act_tvic_start_date);
        mTvicStartDate.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                final DateSelectBottomSheet sheet = new DateSelectBottomSheet();
                sheet.initDate(mProperty.getStartDate());
                sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Calendar cal = sheet.getSelectedDate();
                        mProperty.setStartDate(cal);
                        mTvicStartDate.setText(TimeFormatUtil.getDefaultDateFormat(cal));
                    }
                });
                sheet.show((AppCompatActivity) getContext(), "start_date");
            }
        });

        mTvicPersonCount = findViewById(R.id.activities_act_tvic_person_count);
        mTvicPersonCount.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                CountSelectorBottomSheet sheet = new CountSelectorBottomSheet();
                sheet.addItems(createSelectorPersonCountItem());
                sheet.setOnCountSelectedListener(new CountSelectorBottomSheet.IOnCountSelectListener() {
                    @Override
                    public void onCountChange(TextView tvTitle, int rowIndex, CountSelectorItem item) {
                        showPersonCountLabel(tvTitle, rowIndex, item.count);
                        updatePeopleCount(rowIndex, item.count);
                    }
                });
                sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mTvicPersonCount.setText(String.valueOf(mProperty.getAdultCount()
                                + mProperty.getChildrenCount()
                                + mProperty.getNewbornCount()));
                    }
                });
                sheet.setTitle(R.string.person_count);
                sheet.show((AppCompatActivity) getContext(), "person_count");
            }
        });

        findViewById(R.id.activities_act_btn_search).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                SearchCmd cmd = new SearchCmd();
                cmd.property = mProperty;
                mPresenter.executeCommand(cmd);
            }
        });
    }

    public void showLocation(Location location) {
        mTvicLocSearch.setText(location.getName());
    }

    private CountSelectorItem[] createSelectorPersonCountItem() {
        CountSelectorItem[] selectors = new CountSelectorItem[3];
        CountSelectorItem adult = new CountSelectorItem();
        adult.min = 1;
        adult.max = 7;
        adult.count = 1;
        selectors[0] = adult;

        CountSelectorItem child = new CountSelectorItem();
        child.min = 0;
        child.max = 6;
        child.count = 0;
        selectors[1] = child;

        CountSelectorItem newBorn = new CountSelectorItem();
        newBorn.min = 0;
        newBorn.max = 4;
        newBorn.count = 0;
        selectors[2] = newBorn;

        return selectors;
    }

    private void showPersonCountLabel(TextView tvLabel, int rowIndex, int count) {
        SpannableBuilder builder = new SpannableBuilder();
        String mainTitle = "";
        if (rowIndex == 0) {
            mainTitle = getResources().getString(R.string.adult_count_format, count);
        } else if (rowIndex == 1){
            mainTitle = getResources().getString(R.string.child_count_format, count);
        } else if (rowIndex == 2) {
            mainTitle = getResources().getString(R.string.new_born_count_format, count);
        }
        builder.appendText(mainTitle).withSpan(new ForegroundColorSpan(getResources().getColor(R.color.dark)));
        builder.appendText("\n");
        String subTitle = "";
        if (rowIndex == 0) {
            subTitle = getResources().getString(R.string.flight_adult_year_old);
        } else if (rowIndex == 1){
            subTitle = getResources().getString(R.string.flight_child_year_old);
        } else if (rowIndex == 2) {
            subTitle = getResources().getString(R.string.flight_new_born_year_old);
        }
        builder.appendText(subTitle).withSpan(new ForegroundColorSpan(getResources().getColor(R.color.light_periwinkle)))
                .withSpan(new TextAppearanceSpan(getContext(), R.style.FlightPersonCountSubTitle));
        tvLabel.setText(builder.getSpannedText());
    }

    private void updatePeopleCount(int rowIndex, int count) {
        if (rowIndex == 0) {
            mProperty.setAdultCount(count);
        } else if (rowIndex == 1) {
            mProperty.setChildrenCount(count);
        } else if (rowIndex == 2) {
            mProperty.setNewbornCount(count);
        }
    }

    List<ActivitiesInfo> dummy() {
        List<ActivitiesInfo> result = new ArrayList<>();
        for (int i =0; i < 8; i++) {
            ActivitiesInfo info = new ActivitiesInfo();
            info.setAvatarUrl("https://media.activitiesbank.com/46102/ENG/L/PHOTO3.jpg");
            info.setName("Ngâm bùn khoáng nóng-Suối thần tài");
            info.setRating(5);
            info.setRateCount(100);
            Location loc = new Location();
            loc.setName("Đà Nẵng");
            info.setLocation(loc);
            info.setOriginalPrice("450000");
            info.setPrice("150000");
            info.setID("VIE-DADU1");
            info.setToken("54e997a5-8b68-47e7-a056-cdc1e1297414");
            result.add(info);
        }
        return result;
    }

    public static class SearchLocCmd implements ICommand{}
    public static class SearchCmd implements ICommand{
        public ActivitiesBookingProperty property;
    }
}
