package vcc.com.wowholiday.presenter.activitiesscreen;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.BaseVHData;
import common.presenter.adapter.decor.EndPaddingDecorator;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.StartPaddingDecorator;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.model.ActivityLocation;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;

/**
 * Created by Pham Hai Quang on 10/19/2019.
 */
public class GroupActivitiesAdapter extends BaseRclvAdapter {

    public static final int HOT_DEAL = 1;
    public static final int ACTIVITIES_HOT = 2;
    public static final int LOCATION_HOT = 3;

    private IPresenter mPresenter;

    public GroupActivitiesAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        BaseVHData data = (BaseVHData) mDataSet.get(position);
        return data.type;
    }

    @Override
    public int getLayoutResource(int viewType) {
        switch (viewType) {
            case HOT_DEAL:
            case ACTIVITIES_HOT:
                return R.layout.item_group_activities;
            case LOCATION_HOT:
                return R.layout.item_hot_location;
        }
        return 0;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        switch (viewType) {
            case HOT_DEAL:
            case ACTIVITIES_HOT:
                return new ActivitiesGroupVH(itemView);
            case LOCATION_HOT:
                return new LocationVH(itemView);
        }
        return null;
    }

    public void setHotDeal(List<ActivitiesInfo> activitiesInfoList) {
        HotDealVHData data = new HotDealVHData(activitiesInfoList);
        addItemAndNotify(data);
    }

    public void setActivitiesHot(List<ActivitiesInfo> activitiesInfoList) {
        ActivitiesDealVHData data = new ActivitiesDealVHData(activitiesInfoList);
        addItemAndNotify(data);
    }


    private class ActivitiesGroupVH extends BaseRclvHolder<ActivitiesGroupVHData> {

        TextView tvTitle;
        RecyclerView rclvContent;
        ActivitiesItemAdapter adapter;

        public ActivitiesGroupVH(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.group_activities_itm_tv_title);
            rclvContent = itemView.findViewById(R.id.group_activities_itm_rclv_content);
            rclvContent.setLayoutManager(new LinearLayoutManager(itemView.getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            int padding = itemView.getResources().getDimensionPixelSize(R.dimen.dimen_16);
            rclvContent.addItemDecoration(new StartPaddingDecorator(padding));
            rclvContent.addItemDecoration(new EndPaddingDecorator(padding));
            int padding8 = itemView.getResources().getDimensionPixelSize(R.dimen.dimen_8);
            rclvContent.addItemDecoration(new SpaceItemDecoration(padding8, LinearLayoutManager.HORIZONTAL));
            adapter = new ActivitiesItemAdapter(mPresenter);
            rclvContent.setAdapter(adapter);
        }

        @Override
        public void onBind(ActivitiesGroupVHData vhData) {
            super.onBind(vhData);
            if (vhData.type == HOT_DEAL) {
                tvTitle.setText(R.string.hot_deal);
            } else if (vhData.type == ACTIVITIES_HOT) {
                tvTitle.setText("Hoạt động nổi bật");
            }
            adapter.reset(vhData.realData);
        }
    }

    private class LocationVH extends BaseRclvHolder<ActivityLocation> {
        ImageView ivAvatar;
        TextView tvName;

        public LocationVH(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.hot_location_item_iv_avatar);
            tvName = itemView.findViewById(R.id.hot_location_item_tv_location_name);
        }

        @Override
        public void onBind(ActivityLocation vhData) {
            super.onBind(vhData);
            GlideImageLoader.getInstance().loadImage(itemView, vhData.getAvatarUrl(), ivAvatar);
            tvName.setText(vhData.getName());
        }
    }

    private class ActivitiesGroupVHData extends BaseVHData<List<ActivitiesInfo>> {
        ActivitiesGroupVHData(List<ActivitiesInfo> activitiesInfoList) {
            super(activitiesInfoList);
        }
    }

    private class HotDealVHData extends ActivitiesGroupVHData {
        HotDealVHData(List<ActivitiesInfo> activitiesInfoList) {
            super(activitiesInfoList);
            type = HOT_DEAL;
        }
    }

    private class ActivitiesDealVHData extends ActivitiesGroupVHData {
        ActivitiesDealVHData(List<ActivitiesInfo> activitiesInfoList) {
            super(activitiesInfoList);
            type = ACTIVITIES_HOT;
        }
    }

    private class LocationHotVHData extends BaseVHData<ActivityLocation> {
        LocationHotVHData(ActivityLocation loc) {
            super(loc);
            type = LOCATION_HOT;
        }
    }
}
