package vcc.com.wowholiday.presenter.activitiesscreen.activitydetail;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.activities.GetActivitiesDetailAction;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.presenter.activitiesscreen.booking.ActivitiesBookingActivity;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */

@Layout(R.layout.activity_activities_detail)
public class ActivitiesDetailActivity extends WHActivity<ActivitiesDetailView> {

    private static final String ACTIVITIES_KEY = "ACTIVITIES_KEY";
    private ActivitiesInfo mActivitiesInfo;
    private ActivitiesDetail mActivitiesDetail;

    public static void createParams(Intent itn, ActivitiesInfo activitiesInfo) {
        itn.putExtra(ACTIVITIES_KEY, activitiesInfo);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView.onInitMap(savedInstanceState);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mActivitiesInfo = getIntent().getParcelableExtra(ACTIVITIES_KEY);
        getDetail();
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ActivitiesDetailView.ContinueCmd) {
            Intent itn = new Intent(this, ActivitiesBookingActivity.class);
            ActivitiesBookingActivity.createParams(itn, mActivitiesDetail);
            startActivity(itn);
        }
    }

    private void getDetail() {
        GetActivitiesDetailAction.ActivitiesDetailRq rq = new GetActivitiesDetailAction.ActivitiesDetailRq();
        rq.id = mActivitiesInfo.getID();
        rq.token = mActivitiesInfo.getToken();
        Log.e("Detail", mActivitiesInfo.getID() + "   " + mActivitiesInfo.getToken());
        showLoading();
        mActionManager.executeAction(new GetActivitiesDetailAction(), rq, new WHApiActionCallback<ActivitiesDetail>(this) {
            @Override
            public void onSuccess(ActivitiesDetail responseValue) {
                super.onSuccess(responseValue);
                hideLoading();
                mActivitiesDetail = responseValue;
                mView.show(responseValue);
            }
        }, new ThirdPartyScheduler());
    }
}
