package vcc.com.wowholiday.presenter.activitiesscreen.activitydetail;

import android.content.Context;
import android.graphics.Outline;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.Facility;
import vcc.com.wowholiday.model.Policy;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.ActionBarView;
import vcc.com.wowholiday.presenter.widget.GoogleMapView;
import vcc.com.wowholiday.presenter.widget.ImageThumbLayout;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */
public class ActivitiesDetailView extends BaseRelativeView {
    private static final int DESCRIPTION_MAX_COUNT = 4;

    private GoogleMapView mGmvMap;
    private TextView mTvDes;
    private TextView mTvReadMore;
    private ImageThumbLayout mImThumbLayout;
    private ImageView mIvPreview;

    private IImageLoader mImageLoader = GlideImageLoader.getInstance();
    private boolean isDescriptionExpanded;

    public ActivitiesDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mGmvMap = findViewById(R.id.activities_detail_act_gmv_map);
        mTvDes = findViewById(R.id.activities_detail_act_tv_des);
        mTvReadMore = findViewById(R.id.activities_detail_act_tv_readmore);
        mIvPreview = findViewById(R.id.activities_detail_act_ratio_iv_preview);
        mTvReadMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowMoreDescription();
            }
        });
        mImThumbLayout = findViewById(R.id.activities_detail_act_thumblayout);
        mImThumbLayout.setMaxDisplayItem(4);

        mIvPreview.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                int corner = (int) view.getResources().getDimension(R.dimen.corner_8);
                outline.setRoundRect(0, 0,
                        view.getWidth(),
                        view.getHeight(), corner);
            }
        });
        mIvPreview.setClipToOutline(true);

        mTvReadMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = mTvReadMore.getText().toString();
                if (getResources().getString(R.string.read_more).equals(text)) {
                    mTvDes.setMaxLines(Integer.MAX_VALUE);
                    mTvReadMore.setText(getResources().getString(R.string.read_less));
                } else {
                    mTvDes.setMaxLines(DESCRIPTION_MAX_COUNT);
                    mTvReadMore.setText(getResources().getString(R.string.read_more));
                }
            }
        });

        findViewById(R.id.activities_detail_act_btn_find_ticket).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new ContinueCmd());
            }
        });
    }

    public void onInitMap(Bundle savedInstanceState) {
        mGmvMap.onInitView(savedInstanceState);

    }

    public void show(final ActivitiesDetail detail) {
        ActionBarView actionBarView = findViewById(R.id.activities_detail_act_actionbar);
        actionBarView.setLeftLabel(detail.getName());

        if (!detail.getImageList().isEmpty()) {
            mImageLoader.loadImage(this, detail.getImageList().get(0), mIvPreview);
        }
        if (detail.getImageList().size() < 2) {
            mImThumbLayout.setVisibility(View.GONE);
        } else {
            mImThumbLayout.showImage(detail.getImageList().subList(1, detail.getImageList().size()));
        }

        TextView tvType = findViewById(R.id.activities_detail_act_tv_type);
        List<Amenity> cats = detail.getAmenityList();
        if (cats != null && !cats.isEmpty()) {
            tvType.setText(cats.get(0).getName());
        } else {
            tvType.setVisibility(View.GONE);
        }

        TextView tvName = findViewById(R.id.activities_detail_act_tv_name);
        tvName.setText(detail.getName());

        TextView tvRateCount = findViewById(R.id.activities_detail_act_tv_rate_count);
        tvRateCount.setText(getResources().getString(R.string.rating_format, detail.getRaterCount()));

        RatingBar ratingBar = findViewById(R.id.activities_detail_act_ratingbar);
        ratingBar.setRating(detail.getRating());

        TextViewWithIcon tvicLoc = findViewById(R.id.activities_detail_act_tvic_loc);
        tvicLoc.setText(detail.getLocation().getCity());

        mGmvMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                final LatLng loc = new LatLng(detail.getLocation().getLat(), detail.getLocation().getLong());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15));
            }
        });

        mTvDes.setText(detail.getDescription());
        post(new Runnable() {
            @Override
            public void run() {
                int count = mTvDes.getLineCount();
                mTvReadMore.setVisibility(count <= DESCRIPTION_MAX_COUNT ? GONE : VISIBLE);
                mTvDes.setMaxLines(DESCRIPTION_MAX_COUNT);
            }
        });

        LinearLayout facilityContainer = findViewById(R.id.activities_detail_act_ll_facility_container);
        addFacilities(facilityContainer, detail.getFacilityList());
        showPolicy(detail.getPolicyList());
        showPrice(detail);
    }

    private void showPrice(ActivitiesDetail detail) {
        TextView tvOrigin = findViewById(R.id.activities_detail_act_tv_origin_price);
        TextView tvPrice = findViewById(R.id.activities_detail_act_tv_discount_price);
        if (!detail.getOriginPrice().equals(detail.getPrice())) {
            tvOrigin.setPaintFlags(tvOrigin.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvOrigin.setText(MoneyFormatter.defaultFormat(detail.getOriginPrice()) + " VNĐ");
        } else {
            tvOrigin.setVisibility(GONE);
        }
        tvPrice.setText(MoneyFormatter.defaultFormat(detail.getPrice()) + " VNĐ");
    }

    private void onShowMoreDescription() {
        if (!isDescriptionExpanded) {
            mTvDes.setMaxLines(Integer.MAX_VALUE);
            mTvReadMore.setText(getResources().getString(R.string.collapse));
        } else {
            mTvDes.setMaxLines(4);
            mTvReadMore.setText(getResources().getString(R.string.read_more));
        }
        isDescriptionExpanded = !isDescriptionExpanded;
    }

    private void addFacilities(LinearLayout container, List<Facility> facilityList) {
        for (int i = 0; i < facilityList.size(); i++) {
            TextView tv = new TextView(getContext());
            tv.setTextColor(ContextCompat.getColor(getContext(), R.color.slate_grey));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_size_14));
            tv.setText(facilityList.get(i).getDescription());
            tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_cir_turquoise_blue_bg_size6, 0, 0, 0);
            if (i > 0) {
                int padding = getResources().getDimensionPixelOffset(R.dimen.dimen_4);
                tv.setPadding(0, padding, 0, 0);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            container.addView(tv, params);
        }
    }

    private void showPolicy(List<Policy> policyList) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < policyList.size(); i++) {
            Policy p = policyList.get(i);
            builder.append("-").append(" ").append(p.getDescription().trim());
            if (i < policyList.size() - 1) {
                builder.append("\n");
            }
        }
        TextView tvPolicy = findViewById(R.id.activities_detail_act_tv_license);
        tvPolicy.setText(builder.toString());
    }


    public static class ContinueCmd implements ICommand{}
}
