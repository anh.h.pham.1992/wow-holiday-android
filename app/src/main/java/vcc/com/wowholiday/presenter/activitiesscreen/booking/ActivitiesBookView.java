package vcc.com.wowholiday.presenter.activitiesscreen.booking;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.Calendar;
import java.util.List;

import common.presenter.SafeClicked;
import common.presenter.WHBottomSheetDialog;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.model.ActivitiesPrice;
import vcc.com.wowholiday.model.Facility;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.presenter.dialog.DateSelectBottomSheet;
import vcc.com.wowholiday.presenter.dialog.sort.SelectorBottomSheet;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.ActionBarView;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;
import vcc.com.wowholiday.util.DateUtil;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/24/2019.
 */
public class ActivitiesBookView extends BaseRelativeView {

    private LinearLayout mLlTicketSelect;
    private TextView mTvDateLabel;
    private TextView mTvDateSelected;
    private TextViewWithIcon mTvicTime;
    private TextView[] mFacilityTvs;

    private LayoutInflater mInflater = LayoutInflater.from(getContext());
    private Calendar mSelectedDate = DateUtil.getCalendar();

    public ActivitiesBookView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mLlTicketSelect = findViewById(R.id.activities_booking_act_ll_ticket_select);
        mTvDateLabel = findViewById(R.id.activities_booking_act_tv_date_label);
        /*mTvDateSelected = findViewById(R.id.activities_booking_act_tv_date);
        mTvicTime = findViewById(R.id.activities_booking_act_tvic_time);

        findViewById(R.id.activities_booking_act_btn_select_date).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final DateSelectBottomSheet sheet = new DateSelectBottomSheet();
                sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mSelectedDate = sheet.getSelectedDate();
                        updateDateSelect(mSelectedDate);
                    }
                });
                sheet.initDate(mSelectedDate);
                sheet.show((AppCompatActivity) getContext(), "select_date");
            }
        });*/

        /*mTvicTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectorBottomSheet sheet = new SelectorBottomSheet();
                sheet.showSelectList(TIME_SELECTOR.MORNING, TIME_SELECTOR.AFTERNOON, TIME_SELECTOR.EVENING);
                sheet.showTitle(R.string.time_range);
                sheet.show((AppCompatActivity) getContext(), "select_time");
            }
        });*/

        //updateDateSelect(DateUtil.getCalendar());
        findViewById(R.id.activities_booking_act_btn_continue).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new ContinueCmd());
            }
        });
    }

    public void showActivitiesInfo(ActivitiesDetail info) {
        ActionBarView actionBarView = findViewById(R.id.activities_booking_act_actionbar);
        actionBarView.setLeftLabel(info.getName());

        LinearLayout facilityContainer = findViewById(R.id.activities_booking_act_ll_facility_container);
        addFacilities(info.getFacilityList(), facilityContainer);
        showPrice(info);
    }

    private void addFacilities(List<Facility> facilityList, final LinearLayout llContainer) {
        mFacilityTvs = new TextView[facilityList.size()];
        for (int i = 0; i < facilityList.size(); i++) {
            TextView tv = new TextView(getContext());
            tv.setTextColor(ContextCompat.getColor(getContext(), R.color.slate_grey));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_size_14));
            tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_cir_turquoise_blue_bg_size6, 0, 0, 0);
            if (i > 0) {
                int padding = getResources().getDimensionPixelOffset(R.dimen.dimen_4);
                tv.setPadding(0, padding, 0, 0);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            tv.setLayoutParams(params);
            tv.setText(facilityList.get(i).getDescription());
            mFacilityTvs[i] = tv;
        }

        int initCount = Math.min(2, mFacilityTvs.length);
        for (int i = 0; i < initCount; i++) {
            llContainer.addView(mFacilityTvs[i]);
        }

        final TextView readMoreTv = findViewById(R.id.activities_booking_act_tv_readmore);

        if (mFacilityTvs.length > 2) {
            readMoreTv.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    if (llContainer.getChildCount() <= 2) {
                        for (int i = 2; i < mFacilityTvs.length; i++) {
                            llContainer.addView(mFacilityTvs[i]);
                            readMoreTv.setText("Thu gọn");
                        }
                    } else {
                        for (int i = 2; i < mFacilityTvs.length; i++) {
                            llContainer.removeView(mFacilityTvs[i]);
                            readMoreTv.setText(R.string.read_more);
                        }
                    }
                }
            });
        } else {
            readMoreTv.setVisibility(GONE);
        }
    }

    private void showPrice(ActivitiesDetail detail) {
        TextView originalTv = findViewById(R.id.activities_booking_act_tv_origin_price);
        TextView disccountTv = findViewById(R.id.activities_booking_act_tv_discount_price);
        if (!detail.getOriginPrice().equals(detail.getPrice())) {
            originalTv.setPaintFlags(originalTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            originalTv.setText(MoneyFormatter.defaultFormat(detail.getOriginPrice()) + " VNĐ");
        } else {
            originalTv.setVisibility(GONE);
        }
        disccountTv.setText(MoneyFormatter.defaultFormat(detail.getPrice()) + " VNĐ");

        TextView tvTitle = findViewById(R.id.activities_booking_act_tv_price_total_label);
        int count = 0;
        for (PaxInfo p : detail.getPaxInfoList()) {
            count += p.getQuantity();
        }
        tvTitle.setText("Tổng: " + count + " vé");
    }

    private void updateDateSelect(Calendar selecDate) {
        mTvDateSelected.setText(TimeFormatUtil.getDayOfWeekDateFormat(selecDate));
        if (DateUtil.isSameDay(DateUtil.getCalendar(), selecDate)) {
            mTvDateLabel.setText(R.string.today);
        } else {
            mTvDateLabel.setText(R.string.selected_date);
        }
    }

    private void addPrice(List<ActivitiesPrice> priceList) {
        for (int i = 0; i < priceList.size(); i++) {
            ActivitiesPrice price = priceList.get(i);
            View view = mInflater.inflate(R.layout.layout_activities_ticket_selector,
                    mLlTicketSelect, false);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT);
            mLlTicketSelect.addView(view, params);
            TextView tvTitle = view.findViewById(R.id.activities_ticket_selector_layout_tv_title);
            TextView tvPrice = view.findViewById(R.id.activities_ticket_selector_layout_tv_price);
            tvTitle.setText(price.getTitle());
            tvPrice.setText(MoneyFormatter.defaultFormat(price.getPrice()));

            final TextView tvCount = view.findViewById(R.id.activities_ticket_selector_layout_tv_count);
            view.findViewById(R.id.activities_ticket_selector_layout_iv_minus).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String curr = tvCount.getText().toString();
                    int currCount = 0;
                    if (!TextUtils.isEmpty(curr)) {
                        currCount = Integer.valueOf(curr);
                    }
                    if (currCount > 0) {
                        currCount--;
                        tvCount.setText(currCount == 0 ? "" : String.valueOf(currCount - 1));
                    }
                }
            });
            view.findViewById(R.id.activities_ticket_selector_layout_iv_plus).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String curr = tvCount.getText().toString();
                    int currCount = 0;
                    if (!TextUtils.isEmpty(curr)) {
                        currCount = Integer.valueOf(curr);
                    }
                    tvCount.setText(String.valueOf(currCount + 1));
                }
            });

            if (i != priceList.size() - 1) {
                View divider = new View(getContext());
                divider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.light_grey));
                LinearLayout.LayoutParams dividerParam = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                        getResources().getDimensionPixelSize(R.dimen.divider_size));
                mLlTicketSelect.addView(divider, dividerParam);
            }
        }
    }

    public static class ContinueCmd implements ICommand {
    }
}
