package vcc.com.wowholiday.presenter.activitiesscreen.booking;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.presenter.activitiesscreen.ticketinfo.TicketInfoActivity;

/**
 * Created by Pham Hai Quang on 10/24/2019.
 */

@Layout(R.layout.activity_activities_booking)
public class ActivitiesBookingActivity extends WHActivity<ActivitiesBookView> {

    private static final String ACTIVITY_INFO = "ACTIVITY_INFO";
    private ActivitiesDetail mActivitiesInfo;

    public static void createParams(Intent itn, ActivitiesDetail detail) {
        itn.putExtra(ACTIVITY_INFO, detail);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mActivitiesInfo = getIntent().getParcelableExtra(ACTIVITY_INFO);
        mView.showActivitiesInfo(mActivitiesInfo);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ActivitiesBookView.ContinueCmd) {
            Intent itn = new Intent(this, TicketInfoActivity.class);
            TicketInfoActivity.createParams(itn, mActivitiesInfo);
            startActivity(itn);
        }
    }
}
