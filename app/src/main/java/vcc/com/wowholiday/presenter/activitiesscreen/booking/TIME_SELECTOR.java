package vcc.com.wowholiday.presenter.activitiesscreen.booking;

import vcc.com.wowholiday.presenter.dialog.sort.ISelectorType;

/**
 * Created by Pham Hai Quang on 10/25/2019.
 */
public enum TIME_SELECTOR implements ISelectorType {

    MORNING {
        @Override
        public String getTitle() {
            return "08:00 - 12:00";
        }

        @Override
        public int getCode() {
            return 1;
        }
    },

    AFTERNOON {
        @Override
        public String getTitle() {
            return "12:00 - 18:00";
        }

        @Override
        public int getCode() {
            return 2;
        }
    },

    EVENING {
        @Override
        public String getTitle() {
            return "18:00 - 22:00";
        }

        @Override
        public int getCode() {
            return 3;
        }
    };
}
