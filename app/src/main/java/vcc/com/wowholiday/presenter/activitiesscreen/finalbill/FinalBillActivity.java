package vcc.com.wowholiday.presenter.activitiesscreen.finalbill;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.presenter.activitiesscreen.ticket.ActivitiesTicketActivity;

/**
 * Created by Pham Hai Quang on 10/26/2019.
 */

@Layout(R.layout.activity_final_bill)
public class FinalBillActivity extends WHActivity<FinalBillView> {

    public static final String TICKET_KEY = "TICKET_KEY";
    private ActivitiesTicket mActivitiesTicket;

    public static void createParams(Intent itn, ActivitiesTicket ticket) {
        itn.putExtra(TICKET_KEY, ticket);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mActivitiesTicket = getIntent().getParcelableExtra(TICKET_KEY);
        mView.showTicket(mActivitiesTicket);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FinalBillView.PaymentCmd) {
            Intent itn = new Intent(this, ActivitiesTicketActivity.class);
            ActivitiesTicketActivity.createParams(itn, mActivitiesTicket);
            startActivity(itn);
        }
    }
}