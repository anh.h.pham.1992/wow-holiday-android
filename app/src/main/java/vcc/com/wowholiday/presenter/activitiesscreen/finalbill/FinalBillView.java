package vcc.com.wowholiday.presenter.activitiesscreen.finalbill;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;

/**
 * Created by Pham Hai Quang on 10/26/2019.
 */
public class FinalBillView extends BaseRelativeView {

    public FinalBillView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.final_bill_act_tv_payment).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new PaymentCmd());
            }
        });
    }

    public void showTicket(ActivitiesTicket ticket) {
        TextView tvDate = findViewById(R.id.final_bill_act_tv_date);
        tvDate.setText(ticket.getDate());
        TextView tvTime = findViewById(R.id.final_bill_act_tv_time);
        tvTime.setText(ticket.getStartTime() + " - " + ticket.getEndTime());

        TextView tvAdultCount = findViewById(R.id.final_bill_act_tv_adult_count);
        tvAdultCount.setText(String.valueOf(ticket.getAdultCount()));
        TextView tvChildCount = findViewById(R.id.final_bill_act_tv_child_count);
        tvChildCount.setText(String.valueOf(ticket.getChildCount()));

        TextView tvUserName = findViewById(R.id.final_bill_act_tv_user_name);
        tvUserName.setText(ticket.getContact().getName());

        for (PaxInfo p : ticket.getPaxInfoList()) {
            if (p.getType().equals(PaxInfo.ADULT)) {
                TextView tvAdultLabel = findViewById(R.id.final_bill_act_tv_ticket_adult_type);
                tvAdultLabel.setText("Vé người lớn (" + p.getQuantity() + ")");
                TextView tvAdultPrice = findViewById(R.id.final_bill_act_tv_ticket_adult_price);
                tvAdultPrice.setText(MoneyFormatter.defaultFormat(p.getAmount()) + " VNĐ");
            } else if (p.getType().equals(PaxInfo.CHILD)) {
                TextView tvChildLabel = findViewById(R.id.final_bill_act_tv_ticket_child_type);
                tvChildLabel.setText("Vé trẻ em (" + p.getQuantity() + ")");
                TextView tvChildPrice = findViewById(R.id.final_bill_act_tv_ticket_child_price);
                tvChildPrice.setText(MoneyFormatter.defaultFormat(p.getAmount()) + " VNĐ");
            }
        }

        TextView tvTotal = findViewById(R.id.final_bill_act_tv_total);
        tvTotal.setText(MoneyFormatter.defaultFormat(String.valueOf(ticket.getPrice())) + " VNĐ");
    }

    public static class PaymentCmd implements ICommand{}
}
