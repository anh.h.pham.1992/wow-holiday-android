package vcc.com.wowholiday.presenter.activitiesscreen.locationsearch;

import android.content.Context;
import android.content.Intent;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.activities.ActivitiesHistoryLocationAction;
import vcc.com.wowholiday.domain.usecase.activities.ActivitiesSearchLocationAction;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.dto.ActivitiesLocationDto;
import vcc.com.wowholiday.presenter.activitiesscreen.searchresult.SearchResultActivity;
import vcc.com.wowholiday.repository.api.APIException;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */

@Layout(R.layout.activity_activities_location_search)
public class ActivitiesLocationSearchActivity extends WHActivity<ActivitiesLocationSearchView> {

    public static final String LOCATION_KEY = "LOCATION_KEY";

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        getHistory();
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ActivitiesLocationSearchView.SearchCmd) {
            mActionManager.stopAction(ActivitiesSearchLocationAction.class);
            getSearchLocation(((ActivitiesLocationSearchView.SearchCmd) command).keyword);
        } else if (command instanceof ActivitiesLocationSearchView.CancelCmd) {
            mActionManager.stopAction(ActivitiesSearchLocationAction.class);
            getHistory();
        } else if (command instanceof ActivitiesLocationSearchAdapter.LocationItemSelectedCmd) {
            Intent itn = new Intent();
            itn.putExtra(LOCATION_KEY, ((ActivitiesLocationSearchAdapter.LocationItemSelectedCmd) command).location);
            setResult(RESULT_OK, itn);
            finish();
        }
    }

    private void getHistory() {
        ActivitiesHistoryLocationAction action = new ActivitiesHistoryLocationAction();
        mActionManager.executeAction(action, null, new WHApiActionCallback<ActivitiesLocationDto>(this) {
            @Override
            public void onSuccess(ActivitiesLocationDto responseValue) {
                super.onSuccess(responseValue);
                mView.showHistory(responseValue.recentData);
            }
        }, new ThirdPartyScheduler());
    }

    private void getSearchLocation(String keyword) {
        ActivitiesSearchLocationAction.SearchRq req = new ActivitiesSearchLocationAction.SearchRq();
        req.keyword = keyword;
        mActionManager.executeAction(new ActivitiesSearchLocationAction(), req, new WHApiActionCallback<ActivitiesLocationDto>(this) {
            @Override
            public void onSuccess(ActivitiesLocationDto responseValue) {
                super.onSuccess(responseValue);
                mView.showSearchResult(responseValue.searchData);
            }

            @Override
            protected void onException(Context context, APIException e) {
                super.onException(context, e);
                mView.showSearchResult(null);
            }
        }, new ThirdPartyScheduler());
    }
}
