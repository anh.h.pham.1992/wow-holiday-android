package vcc.com.wowholiday.presenter.activitiesscreen.locationsearch;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.WHApplication;
import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 11/27/2019.
 */
public class ActivitiesLocationSearchAdapter extends BaseRclvAdapter {

    private static final int TITLE_TYPE = 1;
    private static final int LOC_TYPE = 2;

    private IPresenter mPresenter;

    public ActivitiesLocationSearchAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TITLE_TYPE;
        } else {
            return LOC_TYPE;
        }
    }

    @Override
    public int getLayoutResource(int viewType) {
        return viewType == TITLE_TYPE ? R.layout.item_activities_loc_search_title
                : R.layout.item_activities_location_search_loc;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return viewType == TITLE_TYPE ? new TitleVH(itemView) : new LocationVH(itemView);
    }

    public void setHistory(List<Location> locationList) {
        mDataSet.clear();
        mDataSet.add(WHApplication.getInstance().getString(R.string.location_search_history));
        mDataSet.addAll(locationList);
        notifyDataSetChanged();
    }

    public void setResults(List<Location> locationList) {
        mDataSet.clear();
        if (locationList != null && !locationList.isEmpty()) {
            mDataSet.add(WHApplication.getInstance().getString(R.string.location_search_result_title));
            mDataSet.addAll(locationList);
        }

        notifyDataSetChanged();
    }


    private class LocationVH extends BaseRclvHolder<Location> {
        TextView tvLoc;

        public LocationVH(@NonNull View itemView) {
            super(itemView);
            tvLoc = (TextView) itemView;
            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    LocationItemSelectedCmd cmd = new LocationItemSelectedCmd();
                    cmd.location = (Location) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(Location vhData) {
            super.onBind(vhData);
            tvLoc.setText(vhData.getName());
        }
    }

    private class TitleVH extends BaseRclvHolder<String> {
        TextView tvTitle;
        public TitleVH(@NonNull View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView;
        }

        @Override
        public void onBind(String vhData) {
            super.onBind(vhData);
            tvTitle.setText(vhData);
        }
    }

    public static class LocationItemSelectedCmd implements ICommand {
        Location location;
    }
}
