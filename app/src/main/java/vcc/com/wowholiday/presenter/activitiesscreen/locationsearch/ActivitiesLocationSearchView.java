package vcc.com.wowholiday.presenter.activitiesscreen.locationsearch;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */
public class ActivitiesLocationSearchView extends BaseLinearView {

    private final int TIME_DELAY_HANDLER_USER_TYPING = 50;

    private RecyclerView mRclvContent;
    private EditText mEdtSearch;
    private ImageView mIvCancel;
    private LinearLayout mLlNoResult;

    private Runnable mSearchRunnable = new Runnable() {
        @Override
        public void run() {
            if (mLlNoResult.getVisibility() == VISIBLE) {
                mLlNoResult.setVisibility(GONE);
            }

            SearchCmd cmd = new SearchCmd();
            cmd.keyword =  mEdtSearch.getText().toString().trim();
            mPresenter.executeCommand(cmd);
        }
    };

    private ActivitiesLocationSearchAdapter mAdapter;

    public ActivitiesLocationSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mRclvContent = findViewById(R.id.activities_location_search_act_rclv_content);
        mRclvContent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new ActivitiesLocationSearchAdapter(mPresenter);
        mRclvContent.setAdapter(mAdapter);
        LocationRclvDecor decor = new LocationRclvDecor(getContext(),
                ContextCompat.getDrawable(getContext(), R.drawable.shape_divider_vert));
        mRclvContent.addItemDecoration(decor);

        mEdtSearch = findViewById(R.id.activities_location_search_act_edt_search);
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                removeCallbacks(mSearchRunnable);
                String key = mEdtSearch.getText().toString().trim();
                enableCancel(!TextUtils.isEmpty(key));
                if (TextUtils.isEmpty(key) || key.length() < 3) {
                    return;
                }
                postDelayed(mSearchRunnable, TIME_DELAY_HANDLER_USER_TYPING);
            }
        });

        mIvCancel = findViewById(R.id.activities_location_search_act_iv_cancel);
        mIvCancel.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mEdtSearch.setText("");
                mPresenter.executeCommand(new CancelCmd());
            }
        });

        mLlNoResult = findViewById(R.id.activities_location_search_act_ll_no_result);
    }

    public void showHistory(List<Location> locationList) {
        if (mLlNoResult.getVisibility() == VISIBLE) {
            mLlNoResult.setVisibility(View.GONE);
        }
        mAdapter.setHistory(locationList);
    }

    public void showSearchResult(List<Location> locationList) {
        if (locationList == null || locationList.isEmpty()) {
            mAdapter.setResults(null);
            mLlNoResult.setVisibility(View.VISIBLE);
        } else {
            if (mLlNoResult.getVisibility() == VISIBLE) {
                mLlNoResult.setVisibility(View.GONE);
            }
            mAdapter.setResults(locationList);
        }
    }

    private void enableCancel(boolean enable) {
        if (enable) {
            if (mIvCancel.getVisibility() != VISIBLE) {
                mIvCancel.setVisibility(VISIBLE);
            }
        } else {
            if (mIvCancel.getVisibility() == VISIBLE) {
                mIvCancel.setVisibility(INVISIBLE);
            }
        }
    }

    public static class SearchCmd implements ICommand {
        public String keyword;
    }

    public static class CancelCmd implements ICommand {}
}