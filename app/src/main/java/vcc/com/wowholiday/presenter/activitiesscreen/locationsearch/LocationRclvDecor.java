package vcc.com.wowholiday.presenter.activitiesscreen.locationsearch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import common.presenter.adapter.decor.DividerDecorator;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 11/27/2019.
 */
public class LocationRclvDecor extends RecyclerView.ItemDecoration {

    private Drawable mDivider;
    private int mPadding;

    public LocationRclvDecor(Context context, Drawable drawable) {
        mDivider = drawable;
        mPadding = context.getResources().getDimensionPixelSize(R.dimen.dimen_16);
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        int dividerLeft = parent.getPaddingLeft() + mPadding;
        int dividerRight = parent.getWidth() - parent.getPaddingRight() - mPadding;
        int count = parent.getChildCount();
        for (int i = 1; i < count - 1; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int dividerTop = child.getBottom() + params.bottomMargin;
            int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();
            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            mDivider.draw(c);
        }
    }


}
