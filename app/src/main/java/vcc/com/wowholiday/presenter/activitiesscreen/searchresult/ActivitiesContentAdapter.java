package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.graphics.Outline;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;
import vcc.com.wowholiday.presenter.widget.recyclerview.LoadMoreAdapter;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */
public class ActivitiesContentAdapter extends LoadMoreAdapter {

    private IPresenter mPresenter;
    private IImageLoader mImageLoader = GlideImageLoader.getInstance();

    public ActivitiesContentAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        Object data = mDataSet.get(position);
        if (data instanceof ActivitiesInfo) {
            return 1;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getLayoutResource(int viewType) {
        if (viewType == 1) {
            return R.layout.item_search_result_activities;
        } else {
            return super.getLayoutResource(viewType);
        }
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        if (viewType == 1) {
            return new ActivitiesVH(itemView);
        } else {
            return super.onCreateVH(itemView, viewType);
        }
    }

    private class ActivitiesVH extends BaseRclvHolder<ActivitiesInfo> {
        ImageView ivAvatar;
        TextView tvName;
        RatingBar ratingBar;
        TextView tvRatingCount;
        TextViewWithIcon tvicLocation;
        TextView tvOriginPrice;
        TextView tvDiscountPrice;

        public ActivitiesVH(@NonNull final View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.search_result_activities_itm_iv_avatar);
            tvName = itemView.findViewById(R.id.search_result_activities_itm_tv_name);
            ratingBar = itemView.findViewById(R.id.search_result_activities_itm_rating);
            tvRatingCount = itemView.findViewById(R.id.search_result_activities_itm_tv_rate_count);
            tvicLocation = itemView.findViewById(R.id.search_result_activities_itm_tvic_loc);
            tvOriginPrice = itemView.findViewById(R.id.search_result_activities_itm_tv_origin_price);
            tvDiscountPrice = itemView.findViewById(R.id.search_result_activities_itm_tv_discount_price);

            tvOriginPrice.setPaintFlags(tvOriginPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            ivAvatar.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    int corner = (int) view.getResources().getDimension(R.dimen.corner_8);
                    outline.setRoundRect(0, 0,
                            view.getWidth() + itemView.getResources().getDimensionPixelSize(R.dimen.dimen_16),
                            view.getHeight(), corner);
                }
            });
            ivAvatar.setClipToOutline(true);

            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    ActivitiesItemSelectedCmd cmd = new ActivitiesItemSelectedCmd();
                    cmd.activitiesInfo = (ActivitiesInfo) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(ActivitiesInfo vhData) {
            super.onBind(vhData);
            mImageLoader.loadImage(itemView, vhData.getAvatarUrl(), ivAvatar);
            tvName.setText(vhData.getName());
            ratingBar.setRating(vhData.getRating());
            tvRatingCount.setText(itemView.getResources().getString(R.string.rating_format, vhData.getRateCount()));
            tvicLocation.setText(vhData.getLocation().getName());

            String originPrice = vhData.getOriginalPrice();
            String discountPrice = vhData.getPrice();
            String displayPrice = "";
            String displayOrigin = "";
            if (TextUtils.isEmpty(originPrice)) {
                displayPrice = discountPrice;
                displayOrigin = "";
            } else if (!originPrice.equals(discountPrice)) {
                if (TextUtils.isEmpty(discountPrice)) {
                    displayOrigin = "";
                    displayPrice = originPrice;
                } else {
                    displayOrigin = originPrice;
                    displayPrice = discountPrice;
                }
            } else {
                displayOrigin = "";
                displayPrice = discountPrice;
            }

            if (TextUtils.isEmpty(displayOrigin)) {
                tvOriginPrice.setText("");
            } else {
                tvOriginPrice.setText(String.format("%s %s", MoneyFormatter.defaultFormat(displayOrigin),
                        vhData.getCurrencyUnit()));
            }
            tvDiscountPrice.setText(String.format("%s %s", MoneyFormatter.defaultFormat(displayPrice),
                    vhData.getCurrencyUnit()));
        }
    }


    public static class ActivitiesItemSelectedCmd implements ICommand {
        public ActivitiesInfo activitiesInfo;
    }
}
