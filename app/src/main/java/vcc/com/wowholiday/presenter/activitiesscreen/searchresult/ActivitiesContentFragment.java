package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.content.Intent;

import java.util.List;

import common.presenter.WHApiActionCallback;
import common.presenter.WHTabFragment;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.activities.ActivitiesSearchAction;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.presenter.activitiesscreen.activitydetail.ActivitiesDetailActivity;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */
public class ActivitiesContentFragment extends WHTabFragment<ActivitiesContentView> {

    private List<ActivitiesInfo> mActivitiesInfoList;

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_activities_content;
    }

    @Override
    public Config onCreateConfig() {
        return new ResultFragmentConfig();
    }

    @Override
    protected void onShow(boolean isShowing, int flag) {
        super.onShow(isShowing, flag);
        if (isShowing && mActivitiesInfoList != null) {
            getActivities();
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        getActivities();
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ActivitiesContentAdapter.ActivitiesItemSelectedCmd) {
            gotoDetail(((ActivitiesContentAdapter.ActivitiesItemSelectedCmd) command).activitiesInfo);
        }
    }

    private void getActivities() {
        ActivitiesSearchAction action = new ActivitiesSearchAction();
        mActionManager.executeAction(action, null, new WHApiActionCallback<List<ActivitiesInfo>>(this) {
            @Override
            public void onSuccess(List<ActivitiesInfo> responseValue) {
                super.onSuccess(responseValue);
                mActivitiesInfoList = responseValue;
                mView.showActivities(responseValue);
            }
        }, new ThirdPartyScheduler());
    }

    private void gotoDetail(ActivitiesInfo activitiesInfo) {
        Intent itn = new Intent(getContext(), ActivitiesDetailActivity.class);
        ActivitiesDetailActivity.createParams(itn, activitiesInfo);
        startActivity(itn);
    }
}
