package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.content.Context;
import android.util.AttributeSet;

import java.util.List;

import common.presenter.adapter.decor.BottomPaddingDecorator;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.TopPaddingDecorator;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.presenter.widget.recyclerview.WrapRecyclerView;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */
public class ActivitiesContentView extends BaseFrameView {

    private ActivitiesContentAdapter mAdapter;
    private WrapRecyclerView mRclvContent;

    public ActivitiesContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mRclvContent = findViewById(R.id.activities_content_frg_rclv_content);
        mRclvContent.setNoItemImage(R.drawable.ic_activity_24px);
        mRclvContent.setNoItemText(R.string.search_activities_subcate_no_item);
        int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        mRclvContent.addItemDecoration(new TopPaddingDecorator(padding));
        mRclvContent.addItemDecoration(new BottomPaddingDecorator(padding));
        mRclvContent.addItemDecoration(new SpaceItemDecoration(padding));
        mAdapter = new ActivitiesContentAdapter(mPresenter);
        mRclvContent.setAdapter(mAdapter);
    }

    public void showActivities(List<ActivitiesInfo> activities) {
        mRclvContent.reset(activities);
    }
}
