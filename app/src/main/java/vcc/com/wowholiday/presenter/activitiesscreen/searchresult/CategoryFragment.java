package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import common.presenter.WHTabFragment;
import quangph.com.mvp.mvp.ICommand;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */
public class CategoryFragment extends WHTabFragment<CategoryView> {

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_category;
    }

    @Override
    public Config onCreateConfig() {
        ResultFragmentConfig config = new ResultFragmentConfig();
        config.isShowingSubCate = true;
        return config;
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof SubCateContentView.ShowSubCateCmd) {
            mView.showSubCate(((SubCateContentView.ShowSubCateCmd) command).isShow);
        }
    }
}
