package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import common.presenter.WHFragment;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.decor.EndPaddingDecorator;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.StartPaddingDecorator;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */
public class CategoryView extends BaseFrameView {

    private RecyclerView mRrclvSubCate;

    public CategoryView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        ViewPager vp = findViewById(R.id.category_frag_vp_subcate);
        WHFragment fragment = (WHFragment) mPresenter;
        vp.setAdapter(new SubCateVpAdapter(fragment.getChildFragmentManager()));

        mRrclvSubCate = findViewById(R.id.category_frag_rclv_subcate);
        mRrclvSubCate.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mRrclvSubCate.addItemDecoration(new StartPaddingDecorator(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        mRrclvSubCate.addItemDecoration(new EndPaddingDecorator(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        mRrclvSubCate.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelOffset(R.dimen.dimen_8),
                LinearLayoutManager.HORIZONTAL));
        SubcateAdapter subcateAdapter = new SubcateAdapter();
        mRrclvSubCate.setAdapter(subcateAdapter);
        subcateAdapter.reset(dummySubCat());
    }

    public void showSubCate(boolean isShow) {
        if (isShow) {
            if (mRrclvSubCate.getVisibility() != VISIBLE) {
                mRrclvSubCate.setVisibility(VISIBLE);
                //mRrclvSubCate.animate().alphaBy(1);
            }
        } else if (mRrclvSubCate.getVisibility() == VISIBLE) {
            mRrclvSubCate.setVisibility(INVISIBLE);
            /*mRrclvSubCate.animate().alphaBy(-1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mRrclvSubCate.setVisibility(INVISIBLE);
                }
            });*/
        }
    }

    List<String> dummySubCat() {
        List<String> subcates = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            subcates.add("Tro choi");
        }
        return subcates;
    }


    private class SubCateVpAdapter extends FragmentPagerAdapter {
        public SubCateVpAdapter(@NonNull FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return new SubCateContentFragment();
        }

        @Override
        public int getCount() {
            return 10;
        }
    }


    private class SubcateAdapter extends BaseRclvAdapter {

        private int mCurrSelected;

        @Override
        public int getLayoutResource(int viewType) {
            return R.layout.item_subcate_label;
        }

        @Override
        public BaseRclvHolder onCreateVH(View itemView, int viewType) {
            return new SubcateLabelVH(itemView);
        }

        private class SubcateLabelVH extends BaseRclvHolder<String> {

            TextView tvSubcateLabel;

            public SubcateLabelVH(@NonNull View itemView) {
                super(itemView);
                tvSubcateLabel = (TextView) itemView;
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = getAdapterPosition();
                        if (pos != mCurrSelected) {
                            int last = mCurrSelected;
                            mCurrSelected = pos;
                            notifyItemChanged(last);
                            notifyItemChanged(mCurrSelected);
                        }
                    }
                });
            }

            @Override
            public void onBind(String vhData) {
                super.onBind(vhData);
                tvSubcateLabel.setSelected(mCurrSelected == getAdapterPosition());
                tvSubcateLabel.setText(vhData);
            }
        }
    }
}
