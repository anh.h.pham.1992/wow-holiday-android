package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.WHApplication;
import vcc.com.wowholiday.presenter.dialog.sort.ISelectorType;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */
public enum SORT_TYPE implements ISelectorType {
    MOST_POPULAR {
        @Override
        public String getTitle() {
            return WHApplication.getInstance().getResources().getString(R.string.most_popular);
        }

        @Override
        public int getCode() {
            return 1;
        }
    },
    LOWEST_PRICE {
        @Override
        public String getTitle() {
            return WHApplication.getInstance().getResources().getString(R.string.lowest_price);
        }

        @Override
        public int getCode() {
            return 2;
        }
    },
    HIGHEST_PRICE {
        @Override
        public String getTitle() {
            return WHApplication.getInstance().getResources().getString(R.string.highest_price);
        }

        @Override
        public int getCode() {
            return 3;
        }
    },
    HIGHEST_RATE {
        @Override
        public String getTitle() {
            return WHApplication.getInstance().getResources().getString(R.string.highest_rate);
        }

        @Override
        public int getCode() {
            return 4;
        }
    };
}
