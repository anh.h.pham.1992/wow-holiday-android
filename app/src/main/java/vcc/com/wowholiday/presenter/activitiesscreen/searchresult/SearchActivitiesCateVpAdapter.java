package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

import vcc.com.wowholiday.model.Category;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */
public class SearchActivitiesCateVpAdapter extends FragmentPagerAdapter {

    private List<Category> mCateList;

    public SearchActivitiesCateVpAdapter(@NonNull FragmentManager fm, List<Category> categoryList) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mCateList = categoryList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new ActivitiesContentFragment();
        } else {
            return new CategoryFragment();
        }
    }

    @Override
    public int getCount() {
        return 7;
    }
}
