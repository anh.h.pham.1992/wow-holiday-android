package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.content.Intent;

import common.presenter.WHActivity;
import common.presenter.WHTabFragment;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesBookingProperty;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */

@Layout(R.layout.activity_search_result)
public class SearchResultActivity extends WHActivity<SearchResultView> {

    private static final String ACTIVITIES_BOOKING_PROPERTY = "ACTIVITIES_BOOKING_PROPERTY";

    private ActivitiesBookingProperty mBookingProperty;

    public static void createParams(Intent itn, ActivitiesBookingProperty property) {
        itn.putExtra(ACTIVITIES_BOOKING_PROPERTY, property);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mBookingProperty = getIntent().getParcelableExtra(ACTIVITIES_BOOKING_PROPERTY);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof WHTabFragment.OnShowFragmentCmd) {
            if (((WHTabFragment.OnShowFragmentCmd) command).isShow) {
                //showTabSubCate(((WHTabFragment.OnShowFragmentCmd) command).fragment);
            }
        }
    }

    private void showTabSubCate(WHTabFragment fragment) {
        ResultFragmentConfig config = (ResultFragmentConfig) fragment.getConfig();
        mView.showSubCateTab(config.isShowingSubCate);
    }
}
