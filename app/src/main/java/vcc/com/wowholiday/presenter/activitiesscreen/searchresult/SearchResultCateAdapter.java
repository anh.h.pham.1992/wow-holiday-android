package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.graphics.Color;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Category;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */
public class SearchResultCateAdapter extends BaseRclvAdapter {

    private IImageLoader mIImageLoader = GlideImageLoader.getInstance();
    private int mCurrSelected;
    private IOnCateSelectedListener mListener;

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_search_result_cate;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new CateVH(itemView);
    }

    public void setSelectedListener(IOnCateSelectedListener listener) {
        mListener = listener;
    }

    private class CateVH extends BaseRclvHolder<Category> {

        FrameLayout flIconContainer;
        ImageView ivIcon;
        TextView tvCateName;

        public CateVH(@NonNull View itemView) {
            super(itemView);
            flIconContainer = itemView.findViewById(R.id.search_result_itm_fl_cate_container);
            ivIcon = itemView.findViewById(R.id.search_result_itm_iv_cate_icon);
            tvCateName = itemView.findViewById(R.id.search_result_itm_tv_cate_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != mCurrSelected) {
                        int last = mCurrSelected;
                        mCurrSelected = pos;
                        notifyItemChanged(last);
                        notifyItemChanged(mCurrSelected);
                        mListener.onSelected((Category) mDataSet.get(pos), pos);
                    }
                }
            });
        }

        @Override
        public void onBind(Category vhData) {
            super.onBind(vhData);
            //mIImageLoader.loadImage(itemView, vhData.getIconUrl(), ivIcon);
            tvCateName.setText(vhData.getName());
            if (getAdapterPosition() == mCurrSelected) {
                flIconContainer.setSelected(true);
                tvCateName.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.gun_metal));
            } else {
                flIconContainer.setSelected(false);
                tvCateName.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.steel));
            }
        }
    }


    public interface IOnCateSelectedListener {
        void onSelected(Category category, int position);
    }
}
