package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.decor.EndPaddingDecorator;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.StartPaddingDecorator;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Category;
import vcc.com.wowholiday.presenter.dialog.sort.SelectorBottomSheet;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */
public class SearchResultView extends BaseRelativeView {

    private FrameLayout mFlSubCate;
    private RecyclerView mRclvSubCate;
    private SearchResultCateAdapter mCateAdapter;
    private int mCurrPage;

    public SearchResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        //mFlSubCate = findViewById(R.id.search_result_act_rclv_rl_subcate);

        final ViewPager vp = findViewById(R.id.search_result_act_viewpager);

        vp.setAdapter(new SearchActivitiesCateVpAdapter(((AppCompatActivity)getContext()).getSupportFragmentManager(), new ArrayList<Category>()));
        vp.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mCurrPage = position;
            }
        });

        RecyclerView rclv = findViewById(R.id.search_result_act_rclv_cate);
        rclv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        final int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        rclv.addItemDecoration(new StartPaddingDecorator(padding));
        rclv.addItemDecoration(new EndPaddingDecorator(padding));
        int space = getResources().getDimensionPixelSize(R.dimen.dimen_8);
        rclv.addItemDecoration(new SpaceItemDecoration(space, LinearLayoutManager.HORIZONTAL));
        mCateAdapter = new SearchResultCateAdapter();
        mCateAdapter.setSelectedListener(new SearchResultCateAdapter.IOnCateSelectedListener() {
            @Override
            public void onSelected(Category category, int position) {
                vp.setCurrentItem(position);
            }
        });
        rclv.setAdapter(mCateAdapter);
        mCateAdapter.reset(dummyCat());

        /*mRclvSubCate = findViewById(R.id.search_result_act_rclv_sub_cate);
        mRclvSubCate.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mRclvSubCate.addItemDecoration(new StartPaddingDecorator(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        mRclvSubCate.addItemDecoration(new EndPaddingDecorator(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        mRclvSubCate.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelOffset(R.dimen.dimen_8),
                LinearLayoutManager.HORIZONTAL));
        SubcateAdapter subcateAdapter = new SubcateAdapter();
        mRclvSubCate.setAdapter(subcateAdapter);
        subcateAdapter.reset(dummySubCat());*/

        /*AppBarLayout appbar = findViewById(R.id.search_result_act_appbar);
        appbar.addOnOffsetChangedListener(new AppbarOffsetListener());*/

        findViewById(R.id.search_result_act_ll_filter_panel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterBottomSheet sheet = new FilterBottomSheet();
                sheet.show((AppCompatActivity) getContext(), "filter");
            }
        });

        findViewById(R.id.search_result_act_fl_sort_panel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectorBottomSheet sheet = new SelectorBottomSheet();
                sheet.showSelectList(SORT_TYPE.MOST_POPULAR, SORT_TYPE.LOWEST_PRICE, SORT_TYPE.HIGHEST_PRICE, SORT_TYPE.HIGHEST_RATE);
                sheet.showTitle(R.string.sort);
                sheet.show((AppCompatActivity) getContext(), "sort");
            }
        });
    }

    public void showSubCateTab(boolean isShow) {
        if (isShow) {
            //mFlSubCate.setVisibility(VISIBLE);
            /*if (mFlSubCate.getAlpha() == 0) {
                mFlSubCate.animate().alphaBy(1);
            }*/
        } else {
            //mFlSubCate.setVisibility(GONE);
            /*if (mFlSubCate.getAlpha() == 1f) {
                mFlSubCate.animate().alphaBy(0);
            }*/
        }
    }

    List<Category> dummyCat() {
        List<Category> categories = new ArrayList<>();;
        for (int i = 0; i < 7; i++) {
            Category cat = new Category();
            cat.setIconUrl("http://www.catman.global/wp-content/uploads/2015/08/Shopper-Marketing-Icon.png");
            if (i == 0) {
                cat.setName("Tat ca hoat dong");
            } else {
                cat.setName("Cate " + i);
            }

            categories.add(cat);
        }
        return categories;
    }

    List<String> dummySubCat() {
        List<String> subcates = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            subcates.add("Tro choi");
        }
        return subcates;
    }


    private class AppbarOffsetListener implements AppBarLayout.OnOffsetChangedListener {

        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            if (mCurrPage == 0) return;
            Log.e("Appbar", "vertical :" + verticalOffset);
            if (verticalOffset == 0) {
                //toggleSort(false);
            } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                //toggleSort(true);
            }
        }

        private void toggleSort(boolean isShow) {
            if (isShow) {
                postOnAnimation(new Runnable() {
                    @Override
                    public void run() {
                        mRclvSubCate.setVisibility(VISIBLE);
                        mRclvSubCate.animate().alpha(1f);
                    }
                });
            } else {
                postOnAnimation(new Runnable() {
                    @Override
                    public void run() {
                        mRclvSubCate.animate().alpha(0f);
                    }
                });
            }
        }
    }

    /*private class SubcateAdapter extends BaseRclvAdapter {

        private int mCurrSelected;

        @Override
        public int getLayoutResource(int viewType) {
            return R.layout.item_subcate_label;
        }

        @Override
        public BaseRclvHolder onCreateVH(View itemView, int viewType) {
            return new SubcateLabelVH(itemView);
        }

        private class SubcateLabelVH extends BaseRclvHolder<String> {

            TextView tvSubcateLabel;

            public SubcateLabelVH(@NonNull View itemView) {
                super(itemView);
                tvSubcateLabel = (TextView) itemView;
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = getAdapterPosition();
                        if (pos != mCurrSelected) {
                            int last = mCurrSelected;
                            mCurrSelected = pos;
                            notifyItemChanged(last);
                            notifyItemChanged(mCurrSelected);
                        }
                    }
                });
            }

            @Override
            public void onBind(String vhData) {
                super.onBind(vhData);
                tvSubcateLabel.setSelected(mCurrSelected == getAdapterPosition());
                tvSubcateLabel.setText(vhData);
            }
        }
    }*/
}
