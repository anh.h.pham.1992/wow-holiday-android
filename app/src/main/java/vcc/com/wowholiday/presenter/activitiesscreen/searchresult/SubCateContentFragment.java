package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.util.Log;

import androidx.fragment.app.Fragment;

import java.util.List;

import common.presenter.WHApiActionCallback;
import common.presenter.WHTabFragment;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.activities.ActivitiesSearchAction;
import vcc.com.wowholiday.model.ActivitiesInfo;

/**
 * Created by QuangPH on 2020-02-10.
 */
public class SubCateContentFragment extends WHTabFragment<SubCateContentView> {
    private List<ActivitiesInfo> mActivitiesInfoList;

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_activities_sub_cate;
    }

    @Override
    public IPresenter getParentPresenter() {
        return (IPresenter) getParentFragment();
    }

    @Override
    protected void onShow(boolean isShowing, int flag) {
        super.onShow(isShowing, flag);
        if (isShowing && mActivitiesInfoList == null) {
            getActivities();
        }
    }

    /*@Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof SubCateContentView.ShowSubCateCmd) {

        }
    }*/

    private void getActivities() {
        ActivitiesSearchAction action = new ActivitiesSearchAction();
        mActionManager.executeAction(action, null, new WHApiActionCallback<List<ActivitiesInfo>>(this) {
            @Override
            public void onSuccess(List<ActivitiesInfo> responseValue) {
                super.onSuccess(responseValue);
                mActivitiesInfoList = responseValue;
                mView.showActivities(responseValue);
            }
        }, new ThirdPartyScheduler());
    }
}
