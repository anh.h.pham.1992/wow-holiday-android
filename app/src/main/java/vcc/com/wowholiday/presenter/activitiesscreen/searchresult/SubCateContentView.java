package vcc.com.wowholiday.presenter.activitiesscreen.searchresult;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import common.presenter.adapter.decor.BottomPaddingDecorator;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.TopPaddingDecorator;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesInfo;

/**
 * Created by QuangPH on 2020-02-10.
 */
public class SubCateContentView extends BaseFrameView {

    private ActivitiesContentAdapter mAdapter;

    private float mTotalScroll;

    public SubCateContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        RecyclerView rclv = findViewById(R.id.activities_frag_sub_cate_rclv_content);
        rclv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        rclv.addItemDecoration(new TopPaddingDecorator(getResources().getDimensionPixelOffset(R.dimen.search_result_sub_cate_height)));
        rclv.addItemDecoration(new BottomPaddingDecorator(padding));
        rclv.addItemDecoration(new SpaceItemDecoration(padding));
        mAdapter = new ActivitiesContentAdapter(mPresenter);
        rclv.setAdapter(mAdapter);
        rclv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mTotalScroll += dy;
                Log.e("Scroll", "dy: " + mTotalScroll);
                ShowSubCateCmd cmd = new ShowSubCateCmd();
                if (mTotalScroll > getResources().getDimensionPixelOffset(R.dimen.search_result_sub_cate_height)) {
                    cmd.isShow = false;
                } else {
                    cmd.isShow = true;
                }
                mPresenter.executeCommand(cmd);
            }
        });
    }

    public void showActivities(List<ActivitiesInfo> activities) {
        mAdapter.reset(activities);
    }


    public static class ShowSubCateCmd implements ICommand {
        public boolean isShow;
    }
}
