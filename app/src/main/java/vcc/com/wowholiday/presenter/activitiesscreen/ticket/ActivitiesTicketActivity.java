package vcc.com.wowholiday.presenter.activitiesscreen.ticket;

import android.content.Intent;
import android.view.View;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.presenter.activitiesscreen.activitydetail.ActivitiesDetailActivity;
import vcc.com.wowholiday.presenter.mainscreen.MainActivity;

/**
 * Created by Pham Hai Quang on 10/28/2019.
 */

@Layout(R.layout.activity_eticket_activities)
public class ActivitiesTicketActivity extends WHActivity<ActivitiesTicketView> {

    private ActivitiesTicket mTicket;

    public static void createParams(Intent itn, ActivitiesTicket ticket) {
        itn.putExtra("booking", ticket);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        findViewById(R.id.action_bar_text_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itn = new Intent(ActivitiesTicketActivity.this, MainActivity.class);
                itn.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(itn);
                finish();
            }
        });
        mTicket = getIntent().getParcelableExtra("booking");
        mView.showTicket(mTicket);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ActivitiesTicketView.SeeDetailCmd) {
            Intent itn = new Intent(this, ActivitiesDetailActivity.class);
            ActivitiesInfo info = new ActivitiesInfo();
            info.setID(mTicket.getActiviesID());
            info.setToken(mTicket.getActivitiesToken());
            ActivitiesDetailActivity.createParams(itn, info);
            startActivity(itn);
        }
    }
}
