package vcc.com.wowholiday.presenter.activitiesscreen.ticket;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.model.BOOKING_STATUS;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;

/**
 * Created by Pham Hai Quang on 10/28/2019.
 */
public class ActivitiesTicketView extends BaseRelativeView {

    public ActivitiesTicketView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.e_ticket_activities_act_iv_copy).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Đã sao chép mã", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.e_ticket_activities_act_tv_detail).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new SeeDetailCmd());
            }
        });
    }

    public void showTicket(ActivitiesTicket ticket) {
        TextView tvStatus = findViewById(R.id.flight_e_ticket_act_tv_status);
        if (ticket.getStatus() == BOOKING_STATUS.WAITING_PAY) {
            tvStatus.setBackgroundResource(R.drawable.selector_goldenrod_bg_corner_small);
            tvStatus.setText("Chờ thanh toán");
        } else if (ticket.getStatus() == BOOKING_STATUS.CONFIRMED) {
            tvStatus.setBackgroundResource(R.drawable.shape_green_bg_corner_small);
            tvStatus.setText("Đã xác nhận");
        } else if (ticket.getStatus() == BOOKING_STATUS.CANCELED) {
            tvStatus.setBackgroundResource(R.drawable.shape_red_bg_corner_small);
            tvStatus.setText("Đã hủy");
        } else {
            tvStatus.setVisibility(GONE);
        }

        TextView tvName = findViewById(R.id.e_ticket_activities_act_tv_name);
        tvName.setText(ticket.getName());

        TextView tvTime = findViewById(R.id.e_ticket_activities_act_tv_time);
        tvTime.setText(ticket.getDate() + ", " + ticket.getStartTime() + " - " + ticket.getEndTime());

        TextView startTime = findViewById(R.id.e_ticket_activities_act_tv_start_time);
        startTime.setText(ticket.getStartTime() + " - " + ticket.getDate());

        TextView endTime = findViewById(R.id.e_ticket_activities_act_tv_end_time);
        endTime.setText(ticket.getEndTime() + " - " + ticket.getDate());

        TextView tvTotal = findViewById(R.id.e_ticket_activities_act_tv_total);
        tvTotal.setText(MoneyFormatter.defaultFormat(String.valueOf(ticket.getPrice())) + " VNĐ");

        TextView tvContactName = findViewById(R.id.e_ticket_activities_act_tv_booking_person);
        tvContactName.setText(ticket.getContact().getName());
        TextView tvContactInfo = findViewById(R.id.e_ticket_activities_act_tv_booking_person_info);
        tvContactInfo.setText(ticket.getContact().getPhone() + " - " + ticket.getContact().getEmail());
    }


    public static class SeeDetailCmd implements ICommand {}
}
