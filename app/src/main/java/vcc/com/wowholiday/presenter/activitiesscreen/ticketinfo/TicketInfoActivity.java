package vcc.com.wowholiday.presenter.activitiesscreen.ticketinfo;

import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.presenter.activitiesscreen.finalbill.FinalBillActivity;
import vcc.com.wowholiday.presenter.contactinfo.ContactInfoActivity;

/**
 * Created by Pham Hai Quang on 10/25/2019.
 */

@Layout(R.layout.activity_ticket_info)
public class TicketInfoActivity extends WHActivity<TicketInfoView> {

    private static final int CONTACT_RC = 3920;

    public static void createParams(Intent itn, ActivitiesDetail detail) {
        itn.putExtra("detail", detail);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CONTACT_RC && resultCode == RESULT_OK) {
            mView.showUserInfo(data.getStringExtra(ContactInfoActivity.NAME_KEY),
                    data.getStringExtra(ContactInfoActivity.PHONE_KEY),
                    data.getStringExtra(ContactInfoActivity.EMAIL_KEY));
        } else if (requestCode == 3456 && resultCode == RESULT_OK) {
            mView.showUserInfo(data.getStringExtra(ContactInfoActivity.NAME_KEY),
                    data.getStringExtra(ContactInfoActivity.PHONE_KEY),
                    data.getStringExtra(ContactInfoActivity.EMAIL_KEY));
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        ActivitiesDetail detail = getIntent().getParcelableExtra("detail");
        mView.showActivitiesInfo(detail);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof TicketInfoView.ContinueCmd) {
            gotoFinalBill(((TicketInfoView.ContinueCmd) command).ticket);
        } else if (command instanceof TicketInfoView.EditInfoCmd) {
            gotoEditUserInfo(((TicketInfoView.EditInfoCmd) command).name,
                    ((TicketInfoView.EditInfoCmd) command).phone,
                    ((TicketInfoView.EditInfoCmd) command).email);
        } else if (command instanceof TicketInfoView.FillContactInfoCmd) {
            Intent itn = new Intent(this, ContactInfoActivity.class);
            startActivityForResult(itn, 3456);
        }
    }

    private void gotoEditUserInfo(String name, String phone, String email) {
        Intent itn = new Intent(this, ContactInfoActivity.class);
        ContactInfoActivity.createParams(itn, name, phone, email, null, null);
        startActivityForResult(itn, CONTACT_RC);
    }

    private void gotoFinalBill(ActivitiesTicket ticket) {
        Intent itn = new Intent(this, FinalBillActivity.class);
        FinalBillActivity.createParams(itn, ticket);
        startActivity(itn);
    }
}
