package vcc.com.wowholiday.presenter.activitiesscreen.ticketinfo;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.transition.TransitionManager;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.TicketContactInfo;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.CheckboxWithRightText;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/25/2019.
 */
public class TicketInfoView extends BaseRelativeView {

    private EditText mEdtOtherName;
    private TextView mTvMustFill;
    private CheckboxWithRightText mCbForMe;
    private CheckboxWithRightText mCbForOther;
    private TextView mTvName;
    private TextView mTvPhone;
    private TextView mTvEmail;
    private TextView mTvTimeVal;
    private TextView mTvTicketForm;
    private TextView mTvAdultCount;
    private TextView mTvChildCount;

    private ActivitiesDetail mDetail;

    public TicketInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mEdtOtherName = findViewById(R.id.ticket_info_act_edt_other_user_name);
        mTvMustFill = findViewById(R.id.ticket_info_act_tv_must_fill);
        mCbForMe = findViewById(R.id.ticket_info_act_cb_for_me);
        mCbForOther = findViewById(R.id.ticket_info_act_cb_for_other);
        mTvName = findViewById(R.id.ticket_info_act_tv_username);
        mTvPhone = findViewById(R.id.ticket_info_act_tv_phone);
        mTvEmail = findViewById(R.id.ticket_info_act_tv_email);
        mTvTimeVal = findViewById(R.id.ticket_info_act_tv_time_val);
        mTvTicketForm = findViewById(R.id.ticket_info_act_tv_type_val);
        mTvAdultCount = findViewById(R.id.ticket_info_act_tv_adult_count_val);
        mTvChildCount = findViewById(R.id.ticket_info_act_tv_child_count_val);

        mCbForMe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCbForOther.isChecked()) {
                    if (!mCbForMe.isChecked()) {
                        mCbForMe.setChecked(true);
                    }
                } else {
                    mCbForOther.setChecked(false);
                    showOtherUserPanel(false);
                }
            }
        });

        mCbForOther.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCbForMe.isChecked()) {
                    if (!mCbForOther.isChecked()) {
                        mCbForOther.setChecked(true);
                    }
                } else {
                    mCbForMe.setChecked(false);
                    showOtherUserPanel(true);
                }
            }
        });

        findViewById(R.id.ticket_info_act_btn_continue).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ContinueCmd cmd = new ContinueCmd();
                cmd.ticket = collectInfo();
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.ticket_info_act_tv_edit).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditInfoCmd cmd = new EditInfoCmd();
                cmd.name = mTvName.getText().toString();
                cmd.phone = mTvPhone.getText().toString();
                cmd.email = mTvEmail.getText().toString();
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.ticket_info_act_tv_fill_contact_info).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new FillContactInfoCmd());
            }
        });
    }

    public void showActivitiesInfo(ActivitiesDetail detail) {
        mDetail = detail;
        TextView tvActName = findViewById(R.id.ticket_info_act_tv_actname);
        tvActName.setText(detail.getName());

        DateInfo dateInfo = detail.getDateInfo();
        mTvTimeVal.setText(TimeFormatUtil.getDefaultDateFormatFromISO8601UTC(dateInfo.getStartDate()) + " " + dateInfo.getStartTime() + " - " + dateInfo.getEndTime());

        TextView tvAdult = findViewById(R.id.ticket_info_act_tv_adult_count_val);
        TextView tvChild = findViewById(R.id.ticket_info_act_tv_child_count_val);
        for (PaxInfo p : detail.getPaxInfoList()) {
            if (p.getType().equals(PaxInfo.ADULT)) {
                tvAdult.setText(String.valueOf(p.getQuantity()));
            } else if (p.getType().equals(PaxInfo.CHILD)) {
                tvChild.setText(String.valueOf(p.getQuantity()));
            }
        }

        TextView tvPrice = findViewById(R.id.ticket_info_act_tv_price);
        tvPrice.setText(MoneyFormatter.defaultFormat(detail.getPrice()) + " VNĐ");
    }

    public void showUserInfo(String name, String phone, String email) {
        findViewById(R.id.ticket_info_act_ll_info).setVisibility(VISIBLE);
        findViewById(R.id.ticket_info_act_ll_no_info).setVisibility(GONE);
        mTvName.setText(name);
        mTvPhone.setText(phone);
        mTvEmail.setText(email);
    }

    private void showOtherUserPanel(boolean isShown) {
        TransitionManager.beginDelayedTransition((ViewGroup) findViewById(R.id.ticket_info_act_ll_info));
        mEdtOtherName.setVisibility(isShown ? VISIBLE : GONE);
        mTvMustFill.setVisibility(isShown ? VISIBLE : GONE);
        if (isShown) {
            post(new Runnable() {
                @Override
                public void run() {
                    ScrollView v = findViewById(R.id.ticket_info_act_scr_content);
                    v.fullScroll(View.FOCUS_DOWN);
                }
            });
        }
    }

    private ActivitiesTicket collectInfo() {
        ActivitiesTicket ticket = new ActivitiesTicket();
        ticket.setStartTime(mDetail.getDateInfo().getStartTime());
        ticket.setEndTime(mDetail.getDateInfo().getEndTime());
        ticket.setDate(TimeFormatUtil.getDefaultDateFormatFromISO8601UTC(mDetail.getDateInfo().getStartDate()));

        TicketContactInfo info = new TicketContactInfo();
        info.setName(mTvName.getText().toString());
        info.setPhone(mTvPhone.getText().toString());
        info.setEmail(mTvEmail.getText().toString());
        ticket.setContact(info);

        if (TextUtils.isEmpty(mTvAdultCount.getText().toString())) {
            ticket.setAdultCount(0);
        } else {
            ticket.setAdultCount(Integer.valueOf(mTvAdultCount.getText().toString()));
        }
        if (TextUtils.isEmpty(mTvChildCount.getText().toString())) {
            ticket.setChildCount(0);
        } else {
            ticket.setChildCount(Integer.valueOf(mTvChildCount.getText().toString()));
        }

        ticket.setPaxInfoList(mDetail.getPaxInfoList());
        ticket.setPrice(Float.valueOf(mDetail.getPrice()));
        return ticket;
    }


    public static class ContinueCmd implements ICommand {
        public ActivitiesTicket ticket;
    }

    public static class EditInfoCmd implements ICommand {
        public String name;
        public String phone;
        public String email;
    }
    public static class FillContactInfoCmd implements ICommand {}
}
