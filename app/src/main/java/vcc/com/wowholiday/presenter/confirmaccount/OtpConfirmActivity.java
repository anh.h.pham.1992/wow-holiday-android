package vcc.com.wowholiday.presenter.confirmaccount;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.RegisterType;
import vcc.com.wowholiday.presenter.profileinfo.ProfileInfoActivity;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */

@Layout(R.layout.activity_otp_confirm)
public class OtpConfirmActivity extends WHActivity<OtpConfirmView> {

    private String mEmailOrPhone;
    private int mType;

    public static void createParams(Intent itn, String emailOrPhone, int type) {
        itn.putExtra("email_phone", emailOrPhone);
        itn.putExtra("type", type);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            setResult(resultCode);
            finish();
        } else {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mEmailOrPhone = getIntent().getStringExtra("email_phone");
        mType = getIntent().getIntExtra("type", RegisterType.PHONE);
        mView.showPhoneNumber(mEmailOrPhone);

        mView.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        }, 500);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof OtpConfirmView.ConfirmCmd) {
            Intent itn = new Intent(this, ProfileInfoActivity.class);
            ProfileInfoActivity.createParams(itn, mEmailOrPhone, mType);
            startActivityForResult(itn, 1234);
            //finish();
        }
    }
}
