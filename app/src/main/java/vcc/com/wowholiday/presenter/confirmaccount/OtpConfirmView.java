package vcc.com.wowholiday.presenter.confirmaccount;

import android.content.Context;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.util.SpannableBuilder;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */
public class OtpConfirmView extends BaseLinearView {

    private List<OtpItemEditText> mTvOtpList = new ArrayList<>();
    private int mCurrCodeIndex = 0;

    public OtpConfirmView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mTvOtpList.add((OtpItemEditText) findViewById(R.id.otp_confirm_act_tv_1));
        mTvOtpList.add((OtpItemEditText) findViewById(R.id.otp_confirm_act_tv_2));
        mTvOtpList.add((OtpItemEditText) findViewById(R.id.otp_confirm_act_tv_3));
        mTvOtpList.add((OtpItemEditText) findViewById(R.id.otp_confirm_act_tv_4));
        mTvOtpList.add((OtpItemEditText) findViewById(R.id.otp_confirm_act_tv_5));
        mTvOtpList.add((OtpItemEditText) findViewById(R.id.otp_confirm_act_tv_6));

        for (OtpItemEditText tv : mTvOtpList) {
            tv.setOnFocusListener(new OtpItemFocus());
        }
        initNotReceive();

        findViewById(R.id.otp_confirm_act_btn_ok).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new ConfirmCmd());
            }
        });
    }

    public void showPhoneNumber(String phone) {
        TextView tv = findViewById(R.id.otp_confirm_act_tv_phone);
        SpannableBuilder builder = new SpannableBuilder(getResources().getString(R.string.otp_send_to));
        builder.appendText("\n")
                .appendText(phone).withSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.dark_slate_blue)))
                .appendText("\n")
                .appendText(getResources().getString(R.string.pls_enter_otp));
        tv.setText(builder.getSpannedText());
    }

    public void nextCode(int code) {
        if (mCurrCodeIndex >= mTvOtpList.size() - 1) {
            return;
        }
        mCurrCodeIndex++;
        mTvOtpList.get(mCurrCodeIndex).setText(String.valueOf(code));
    }

    public void deleteCode() {
        if (mCurrCodeIndex < 0) {
            return;
        }
        mTvOtpList.get(mCurrCodeIndex).setText("");
        mCurrCodeIndex--;
    }

    private void initNotReceive() {
        TextView tv = findViewById(R.id.otp_confirm_act_tv_not_receive);
        ClickableSpan resendSpan = new ClickableSpan() {
            public void onClick(View view) {
                Log.e("Login", "create");
            }
        };
        SpannableBuilder builder = new SpannableBuilder(getResources().getString(R.string.not_receive_otp));
        builder.appendText(" ").appendText(getResources().getString(R.string.resend)).withSpan(resendSpan);
        tv.setText(builder.getSpannedText());
    }

    private void focusToward() {
        if (mCurrCodeIndex >= mTvOtpList.size() - 1) {
            return;
        }
        mCurrCodeIndex++;
        mTvOtpList.get(mCurrCodeIndex).requestFocus();
    }

    private void focusBackward() {
        if (mCurrCodeIndex < 0) {
            return;
        }
        mCurrCodeIndex--;
        mTvOtpList.get(mCurrCodeIndex).requestFocus();
    }

    private class OtpItemFocus implements OtpItemEditText.IOnFocus {

        @Override
        public void onNextFocus() {
            focusToward();
        }

        @Override
        public void onBackFocus() {
            focusBackward();
            mTvOtpList.get(mCurrCodeIndex).setText("");
        }
    }

    public static class ConfirmCmd implements ICommand {}
}
