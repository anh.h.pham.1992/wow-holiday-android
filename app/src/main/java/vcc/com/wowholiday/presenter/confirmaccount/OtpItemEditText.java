package vcc.com.wowholiday.presenter.confirmaccount;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;

import androidx.appcompat.widget.AppCompatEditText;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class OtpItemEditText extends AppCompatEditText {

    private IOnFocus mFocusListener;

    public OtpItemEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        addTextChangedListener(new OptTextWatcher());

        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start,
                                       int end, Spanned dest, int dstart, int dend) {
                if (getText().toString().length() >= 1) {
                    return "";
                }

                for (int i = start; i < end; i++) {
                    if (!Character.isDigit(source.charAt(i)) &&
                            !Character.toString(source.charAt(i)).equals("_") &&
                            !Character.toString(source.charAt(i)).equals("-")) {
                        return "";
                    }
                }
                return null;
            }
        };
        setFilters(new InputFilter[]{filter});
        setCursorVisible(false);
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        return new OtpInputConnection(super.onCreateInputConnection(outAttrs), true);
    }

    public void setOnFocusListener(IOnFocus listener) {
        mFocusListener = listener;
    }

    private class OtpInputConnection extends InputConnectionWrapper {

        public OtpInputConnection(InputConnection target, boolean mutable) {
            super(target, mutable);
        }

        @Override
        public boolean deleteSurroundingText(int beforeLength, int afterLength) {
            if (getText().toString().length() == 0) {
                mFocusListener.onBackFocus();
            }
            return super.deleteSurroundingText(beforeLength, afterLength);
        }
    }

    private class OptTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!s.toString().isEmpty()) {
                mFocusListener.onNextFocus();
            }
        }
    }

    public interface IOnFocus {
        void onNextFocus();
        void onBackFocus();
    }
}
