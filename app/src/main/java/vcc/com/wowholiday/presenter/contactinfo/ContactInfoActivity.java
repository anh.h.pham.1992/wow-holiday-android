package vcc.com.wowholiday.presenter.contactinfo;

import android.content.Intent;
import android.view.View;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.ActionException;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.valid.ValidEmailAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidPhoneNumberAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidUserNameAction;
import vcc.com.wowholiday.presenter.widget.ActionBarView;

/**
 * Created by Pham Hai Quang on 10/26/2019.
 */

@Layout(R.layout.activity_contact_info)
public class ContactInfoActivity extends WHActivity<ContactInfoView> {

    public static final String NAME_KEY = "NAME_KEY";
    public static final String PHONE_KEY = "PHONE_KEY";
    public static final String EMAIL_KEY = "EMAIL_KEY";
    public static final String DESCRIPTION_KEY = "DESCRIPTION_KEY";
    public static final String TITLE_KEY = "TITLE_KEY";

    public static void createParams(Intent itn, String name, String phone, String email, String description, String title) {
        itn.putExtra(NAME_KEY, name);
        itn.putExtra(PHONE_KEY, phone);
        itn.putExtra(EMAIL_KEY, email);
        itn.putExtra(DESCRIPTION_KEY, description);
        itn.putExtra(TITLE_KEY, title);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        findViewById(R.id.action_bar_text_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        Intent itn = getIntent();
        if (itn != null) {
            mView.showUserInfo(itn.getStringExtra(NAME_KEY),
                    itn.getStringExtra(PHONE_KEY),
                    itn.getStringExtra(EMAIL_KEY));
            mView.showDescription(itn.getStringExtra(DESCRIPTION_KEY));
            mView.showTitle(itn.getStringExtra(TITLE_KEY));
        } else {
            mView.showDescription(null);
        }

        ActionBarView actionBarView = findViewById(R.id.action_bar);
        actionBarView.setRightTextOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ContactInfoView.ConfirmCmd) {
            confirm(((ContactInfoView.ConfirmCmd) command).name,
                    ((ContactInfoView.ConfirmCmd) command).phone,
                    ((ContactInfoView.ConfirmCmd) command).email);
        }
    }

    private void confirm(String name, String phone, String email) {
        if (validPhone(phone) && validEmail(email)) {
            Intent itn = new Intent();
            itn.putExtra(NAME_KEY, name);
            itn.putExtra(PHONE_KEY, phone);
            itn.putExtra(EMAIL_KEY, email);
            setResult(RESULT_OK, itn);
            finish();
        }
    }

    private boolean validName(String name) {
        ValidUserNameAction.ValidUserNameReq req = new ValidUserNameAction.ValidUserNameReq();
        //req.userName = name;
        ValidUserNameAction action = new ValidUserNameAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            mView.showUserNameError(getString(R.string.error_format_world));
            return false;
        }
    }

    private boolean validPhone(String phone) {
        ValidPhoneNumberAction.ValidPhoneNumberReq req = new ValidPhoneNumberAction.ValidPhoneNumberReq();
        req.phoneNumber = phone;
        ValidPhoneNumberAction action = new ValidPhoneNumberAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            mView.showPhoneError(getResources().getString(R.string.phone_format_error));
            return false;
        }
    }

    private boolean validEmail(String email) {
        ValidEmailAction.ValidEmailReq req = new ValidEmailAction.ValidEmailReq();
        req.email = email;
        ValidEmailAction action = new ValidEmailAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            mView.showEmailError(getResources().getString(R.string.email_format_error));
            return false;
        }
    }

}
