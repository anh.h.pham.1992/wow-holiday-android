package vcc.com.wowholiday.presenter.contactinfo;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.RightErrorTextInputLayout;

/**
 * Created by Pham Hai Quang on 10/26/2019.
 */
public class ContactInfoView extends BaseLinearView {

    private RightErrorTextInputLayout mTilName;
    private RightErrorTextInputLayout mTilPhone;
    private RightErrorTextInputLayout mTilEmail;

    public ContactInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mTilName = findViewById(R.id.contact_info_act_til_name);
        mTilPhone = findViewById(R.id.contact_info_act_til_phone);
        mTilEmail = findViewById(R.id.contact_info_act_til_email);

        findViewById(R.id.contact_info_act_btn_confirm).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeErrorMsg();
                if (valid()) {
                    ConfirmCmd cmd = new ConfirmCmd();
                    cmd.name = mTilName.getEditText().getText().toString();
                    cmd.phone = mTilPhone.getEditText().getText().toString();
                    cmd.email = mTilEmail.getEditText().getText().toString();
                    mPresenter.executeCommand(cmd);
                }
            }
        });
    }

    public void showUserInfo(String name, String phone, String email) {
        if (!TextUtils.isEmpty(name)) {
            mTilName.getEditText().setText(name);
        }

        if (!TextUtils.isEmpty(phone)) {
            mTilPhone.getEditText().setText(phone);
        }

        if (!TextUtils.isEmpty(email)) {
            mTilEmail.getEditText().setText(email);
        }
    }

    public void showDescription(String description) {
        TextView tvDescription = findViewById(R.id.contact_info_act_tv_description);
        if (!TextUtils.isEmpty(description)) {
            tvDescription.setText(description);
        } else {
            tvDescription.setVisibility(View.INVISIBLE);
        }
    }

    public void showTitle(String title) {
        TextView tvTitle = findViewById(R.id.contact_info_act_tv_title);
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }
    }

    private boolean valid() {
        boolean isValid = true;
        if (TextUtils.isEmpty(mTilName.getEditText().getText().toString())) {
            mTilName.setError(getResources().getString(R.string.must_fill_info_2));
            isValid = false;
        }
        if (TextUtils.isEmpty(mTilPhone.getEditText().getText().toString())) {
            mTilPhone.setError(getResources().getString(R.string.must_fill_info_2));
            isValid = false;
        }
        if (TextUtils.isEmpty(mTilEmail.getEditText().getText().toString())) {
            mTilEmail.setError(getResources().getString(R.string.must_fill_info_2));
            isValid = false;
        }
        return isValid;
    }

    public void showUserNameError(String error) {
        mTilName.setError(error);
    }

    public void showPhoneError(String error) {
        mTilPhone.setError(error);
    }

    public void showEmailError(String error) {
        mTilEmail.setError(error);
    }

    private void removeErrorMsg() {
        mTilName.setError(null);
        mTilPhone.setError(null);
        mTilEmail.setError(null);
    }

    public static class ConfirmCmd implements ICommand {
        public String name;
        public String phone;
        public String email;
    }
}
