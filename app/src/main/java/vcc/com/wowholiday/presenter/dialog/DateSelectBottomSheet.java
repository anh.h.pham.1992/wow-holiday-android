package vcc.com.wowholiday.presenter.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Calendar;

import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.calendar.CalendarView;
import vcc.com.wowholiday.util.DateUtil;
import vcc.com.wowholiday.presenter.widget.calendar.IOnDaySelectedListener;

/**
 * Created by Pham Hai Quang on 10/25/2019.
 */
public class DateSelectBottomSheet extends WHBottomSheetDialog {
    private Calendar mSelectedDate;
    private Calendar mMinDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_date_selector, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CalendarView cal = view.findViewById(R.id.date_selector_dlg_cal);
        cal.addSelectedListener(new IOnDaySelectedListener() {
            @Override
            public void onSelected(Calendar oldSelected, Calendar selectedDay) {
                mSelectedDate = selectedDay;
            }
        });

        if (mSelectedDate == null) {
            mSelectedDate = DateUtil.getCalendar();
        }

        if (mMinDate == null) {
            mMinDate = DateUtil.getCalendar();
        }
        cal.setMinDate(mMinDate);
        cal.setDate(mSelectedDate);

        view.findViewById(R.id.date_selector_dlg_tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void initDate(Calendar calendar) {
        mSelectedDate = calendar;
    }

    public void setMinDate(Calendar calendar) {
        mMinDate = calendar;
    }

    public Calendar getSelectedDate() {
        return mSelectedDate;
    }
}
