package vcc.com.wowholiday.presenter.dialog.countselector;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/29/2019.
 */
public class CountSelectorBottomSheet extends WHBottomSheetDialog {

    private CountSelectorItem[] mItems;
    private @StringRes int mTitleId;
    private IOnCountSelectListener mOnCountSelectedListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_count_select_bottomsheet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvTitle = view.findViewById(R.id.count_select_bottomsheet_dlg_tv_title);
        tvTitle.setText(mTitleId);

        view.findViewById(R.id.count_select_bottomsheet_dlg_tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        build((LinearLayout) view.findViewById(R.id.count_select_bottomsheet_dlg_ll_container), mItems);
    }

    public void addItems(CountSelectorItem...items) {
        mItems = items;
    }

    public void setOnCountSelectedListener(IOnCountSelectListener listener) {
        mOnCountSelectedListener = listener;
    }

    public void setTitle(@StringRes int titleId) {
        mTitleId = titleId;
    }

    private void build(LinearLayout container, CountSelectorItem...items) {
        mItems = items;
        for (int i = 0; i < items.length; i++) {
            addIem(i, items[i], container);
            if (i != items.length - 1) {
                addDivider(container);
            }
        }
    }

    private void addIem(final int rowIndex, final CountSelectorItem item, LinearLayout container) {
        final View itemView = LayoutInflater.from(getContext()).inflate(R.layout.layout_count_select_item,
                container, false);
        final TextView tvTitle = itemView.findViewById(R.id.count_select_layout_tv_title);
        if (item.count < item.min) {
            item.count = item.min;
        }

        if (item.count > item.max) {
            item.count = item.max;
        }
        mOnCountSelectedListener.onCountChange(tvTitle, rowIndex, item);

        itemView.findViewById(R.id.count_select_layout_iv_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.count > item.min) {
                    item.count--;
                    mOnCountSelectedListener.onCountChange(tvTitle , rowIndex, item);
                }
            }
        });

        itemView.findViewById(R.id.count_select_layout_iv_plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.count < item.max) {
                    item.count++;
                    mOnCountSelectedListener.onCountChange(tvTitle , rowIndex, item);
                }
            }
        });
        container.addView(itemView);
    }

    private void addDivider(LinearLayout container) {
        View view = new View(getContext());
        view.setBackgroundColor(ContextCompat.getColor(container.getContext(), R.color.light_grey));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.divider_size));
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        container.addView(view, params);
    }


    public interface IOnCountSelectListener {
        void onCountChange(TextView tvTitle, int rowIndex, CountSelectorItem item);
    }
}
