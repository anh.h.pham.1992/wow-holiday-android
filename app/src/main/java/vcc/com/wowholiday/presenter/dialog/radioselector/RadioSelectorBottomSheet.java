package vcc.com.wowholiday.presenter.dialog.radioselector;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import common.presenter.SafeClicked;
import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/29/2019.
 */
public class RadioSelectorBottomSheet extends WHBottomSheetDialog {

    private RadioSelectorItem[] mSelectorItems;
    private View[] mCheckboxs;
    private int mSelectedIndex = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_radio_selector_bottomsheet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.radio_select_bottomsheet_dlg_tv_save).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                dismiss();
            }
        });

        mCheckboxs = new View[mSelectorItems.length];
        LinearLayout container = view.findViewById(R.id.radio_select_bottomsheet_dlg_ll_container);
        for (int i = 0; i < mSelectorItems.length; i++) {
            addItem(i, container);
            if (i != mSelectorItems.length - 1) {
                addDivider(container);
            }
        }
        notifySelected();
    }

    public void addItems(RadioSelectorItem...items) {
        mSelectorItems = items;
    }

    public RadioSelectorItem getSelectedItem() {
        return mSelectorItems[mSelectedIndex];
    }

    private void addItem(final int index, LinearLayout container) {
        RadioSelectorItem item = mSelectorItems[index];
        View itemView = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_radio_selector_item, container, false);
        TextView tvTitle = itemView.findViewById(R.id.radio_selector_item_layout_tv_title);
        tvTitle.setText(item.title);
        TextView tvDes = itemView.findViewById(R.id.radio_selector_item_layout_tv_des);
        tvDes.setText(item.description);
        container.addView(itemView);
        itemView.findViewById(R.id.radio_selector_item_layout_v_cb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedIndex != index) {
                    mSelectedIndex = index;
                    notifySelected();
                }
            }
        });
        mCheckboxs[index] = itemView.findViewById(R.id.radio_selector_item_layout_v_cb);
    }

    private void addDivider(LinearLayout container) {
        View view = new View(container.getContext());
        view.setBackgroundColor(getResources().getColor(R.color.light_grey));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.divider_size));
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dimen_32);
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.dimen_8);
        container.addView(view, params);
    }

    private void notifySelected() {
        for (int i = 0; i < mCheckboxs.length; i++) {
            mCheckboxs[i].setSelected(i == mSelectedIndex);
        }
    }
}
