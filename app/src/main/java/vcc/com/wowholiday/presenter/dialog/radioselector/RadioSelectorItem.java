package vcc.com.wowholiday.presenter.dialog.radioselector;

/**
 * Created by Pham Hai Quang on 10/29/2019.
 */
public class RadioSelectorItem<T> {
    public String title;
    public String description;

    public T data;
}
