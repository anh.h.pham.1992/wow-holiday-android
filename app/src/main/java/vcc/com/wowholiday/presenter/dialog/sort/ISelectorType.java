package vcc.com.wowholiday.presenter.dialog.sort;

/**
 * Created by Pham Hai Quang on 10/25/2019.
 */
public interface ISelectorType {
    String getTitle();
    int getCode();
}
