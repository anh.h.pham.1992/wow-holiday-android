package vcc.com.wowholiday.presenter.dialog.sort;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/25/2019.
 */
public class SelectorBottomSheet extends WHBottomSheetDialog {

    private ISelectorType[] mSelects;
    private @StringRes int mTitleID;
    private ISelectorType mSelectedSort;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_selector_bottomsheet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout container = view.findViewById(R.id.selector_bottomsheet_dlg_ll_selector);
        build(container, mSelects);
        TextView tv = view.findViewById(R.id.selector_bottomsheet_dlg_tv_label);
        tv.setText(mTitleID);
    }

    public void showSelectList(ISelectorType...selects) {
        mSelects = selects;
    }

    public void showTitle(@StringRes int id) {
        mTitleID = id;
    }

    private void build(LinearLayout container, ISelectorType...types) {
        if (mSelectedSort == null) {
            mSelectedSort = types[0];
        }

        for (int i = 0; i < types.length; i++) {
            ISelectorType sort = types[i];
            addSelectItem(sort, container);
            if (i != types.length - 1) {
                addDivider(container);
            }
        }
        notifySelected(container);
    }

    private void addSelectItem(final ISelectorType select, final LinearLayout container) {
        final View view = LayoutInflater.from(container.getContext()).inflate(R.layout.layout_sort_item, container, false);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.item_height_2) );
        view.setLayoutParams(params);
        container.addView(view);
        TextView tvTitle = view.findViewById(R.id.sort_item_layout_tv_title);
        tvTitle.setText(select.getTitle());
        view.setTag(select);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!view.isSelected()) {
                    mSelectedSort = select;
                    notifySelected(container);
                }
            }
        });
    }

    private void addDivider(LinearLayout container) {
        View divider = new View(getContext());
        divider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.light_grey));
        int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.divider_size));
        params.leftMargin = padding;
        params.rightMargin = padding;
        container.addView(divider, params);
    }

    private void notifySelected(LinearLayout container) {
        int count = container.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = container.getChildAt(i);
            Object tag = child.getTag();
            if (tag instanceof ISelectorType) {
                TextView tvTitle = child.findViewById(R.id.sort_item_layout_tv_title);
                ImageView ivTick = child.findViewById(R.id.sort_item_layout_iv_tick);
                if (tag.equals(mSelectedSort)) {
                    ivTick.setVisibility(View.VISIBLE);
                    tvTitle.setTextColor(ContextCompat.getColor(container.getContext(), R.color.turquoise_blue));
                } else {
                    ivTick.setVisibility(View.INVISIBLE);
                    tvTitle.setTextColor(ContextCompat.getColor(container.getContext(), R.color.steel));
                }
            }
        }
    }
}
