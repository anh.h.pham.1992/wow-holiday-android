package vcc.com.wowholiday.presenter.editprofile;

import android.content.Intent;
import android.widget.Toast;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.ActionException;
import quangph.com.mvp.mvp.action.scheduler.AsyncTaskScheduler;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.SaveUserAction;
import vcc.com.wowholiday.domain.usecase.UpdateProfileAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidUserNameAction;
import vcc.com.wowholiday.infras.WHSharedPreference;
import vcc.com.wowholiday.model.User;

/**
 * Created by Pham Hai Quang on 10/14/2019.
 */

@Layout(R.layout.activity_edit_profile)
public class EditProfileActivity extends WHActivity<EditProfileView> {

    public static final String USER = "user";

    private User mUser;
    private User mEditingUser;

    public static void buildParam(Intent itn, User user) {
        itn.putExtra(USER, user);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mUser = getIntent().getParcelableExtra(USER);
        mEditingUser = mUser.copy();
        mView.showUserInfo(mEditingUser);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof EditProfileView.CompeleteCmd) {
            doUpdateProfile(((EditProfileView.CompeleteCmd) command).user);
        }
    }

    private void doUpdateProfile(final User newUser) {
        ValidUserNameAction.ValidUserNameReq req = new ValidUserNameAction.ValidUserNameReq();
        req.firstName = newUser.getFirstName();
        req.lastName = newUser.getLastName();
        ValidUserNameAction action = new ValidUserNameAction();
        action.setRequestValue(req);
        try {
            if (action.execute()) {
                UpdateProfileAction.UpdateProfileRV rv = new UpdateProfileAction.UpdateProfileRV();
                rv.user = newUser;
                showLoading();
                mActionManager.executeAction(new UpdateProfileAction(), rv, new WHApiActionCallback<Boolean>(this){
                    @Override
                    public void onSuccess(Boolean responseValue) {
                        super.onSuccess(responseValue);
                        if (responseValue) {
                            saveUser(newUser);
                            Intent itn = new Intent();
                            itn.putExtra(USER, newUser);
                            setResult(RESULT_OK, itn);
                            finish();
                        } else {
                            Toast.makeText(EditProfileActivity.this, "Có lỗi xảy ra!", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new ThirdPartyScheduler());
            }
        } catch (ActionException e) {
            e.printStackTrace();
            mView.showUserNameError(getString(R.string.empty_error));
        }
    }

    private void saveUser(User user) {
        SaveUserAction.SaveUserRv rv = new SaveUserAction.SaveUserRv();
        rv.user = user;
        mActionManager.executeAction(new SaveUserAction(), rv, null, new AsyncTaskScheduler());
    }
}
