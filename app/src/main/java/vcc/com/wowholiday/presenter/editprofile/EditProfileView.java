package vcc.com.wowholiday.presenter.editprofile;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;
import java.util.Date;

import common.presenter.WHBottomSheetDialog;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.widget.RadioRightText;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/14/2019.
 */
public class EditProfileView extends BaseLinearView {

    private EditText mEdtLastName;
    private EditText mEdtFirstName;
    private EditText mEdtBirth;
    private RadioRightText mRadioMr;
    private RadioRightText mRadioMrs;

    private User mUser;

    public EditProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mEdtLastName = findViewById(R.id.edit_profile_act_edt_last_name);
        mEdtFirstName = findViewById(R.id.edit_profile_act_edt_first_name);
        mEdtBirth = findViewById(R.id.edit_profile_act_edt_birth);
        mEdtBirth.setInputType(InputType.TYPE_NULL);
        mEdtBirth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                if (!TextUtils.isEmpty(mUser.getBirthday())) {
                    cal.setTimeInMillis(TimeFormatUtil.fromISO8601UTCToMillis(mUser.getBirthday()));
                }
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.YEAR, year);
                        cal.set(Calendar.MONTH, month);
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        mUser.setBirthday(TimeFormatUtil.toISO8601UTC(cal.getTime()));
                        mEdtBirth.setText(TimeFormatUtil.getDefaultDateFormat(cal));
                    }
                }, year, month, day);
                dialog.show();
            }
        });

        mRadioMr = findViewById(R.id.edit_profile_act_radio_mr);
        mRadioMrs = findViewById(R.id.edit_profile_act_radio_mrs);
        mRadioMr.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMale(true);
            }
        });
        mRadioMrs.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMale(false);
            }
        });

        findViewById(R.id.edit_profile_act_btn_complete).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mUser.setLastName(mEdtLastName.getText().toString());
                mUser.setFirstName(mEdtFirstName.getText().toString());
                CompeleteCmd cmd = new CompeleteCmd();
                cmd.user = mUser;
                mPresenter.executeCommand(cmd);
            }
        });
    }

    public void showUserInfo(User user) {
        mUser = user;
        mEdtLastName.setText(user.getLastName());
        mEdtFirstName.setText(user.getFirstName());
        checkMale(user.getGender().equals(User.MALE));

        if (!TextUtils.isEmpty(user.getBirthday())) {
            Date date = TimeFormatUtil.fromISO8601UTC(user.getBirthday());
            mEdtBirth.setText(TimeFormatUtil.getDefaultDateFormat(date));
        } else {
            mEdtBirth.setText("");
        }
    }

    public void showUserNameError(String error) {
        TextView tvError = findViewById(R.id.edit_profile_act_tv_name_error);
        tvError.setText(error);
        tvError.setVisibility(View.VISIBLE);
    }

    private void checkMale(boolean isChecked) {
        if (isChecked) {
            mRadioMr.setChecked(true);
            mRadioMrs.setChecked(false);
            mUser.setGender(User.MALE);
        } else {
            mRadioMr.setChecked(false);
            mRadioMrs.setChecked(true);
            mUser.setGender(User.FEMALE);
        }
    }

    public static class CompeleteCmd implements ICommand {
        public User user;
    }
}
