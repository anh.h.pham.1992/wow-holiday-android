package vcc.com.wowholiday.presenter.flight;

import vcc.com.wowholiday.model.air.AirSeat;
import vcc.com.wowholiday.presenter.dialog.radioselector.RadioSelectorItem;

/**
 * Created by QuangPH on 2020-01-11.
 */
public class DummyData {

    public static RadioSelectorItem<AirSeat>[] seatType() {
        RadioSelectorItem<AirSeat>[] result = new RadioSelectorItem[4];

        AirSeat eco = new AirSeat();
        eco.setType(1);
        eco.setName("Economy");
        eco.setDescription("Bay đẳng cấp với quầy làm thủ tục và khu ghế ngồi riêng");
        RadioSelectorItem itm0  = new RadioSelectorItem();
        itm0.title = eco.getName();
        itm0.description = eco.getDescription();
        itm0.data = eco;
        result[0] = itm0;

        AirSeat pre = new AirSeat();
        pre.setType(2);
        pre.setName("Premium");
        pre.setDescription("Chi phí hợp lý với bữa ăn ngon và chỗ để chân rộng rãi");
        RadioSelectorItem itm1  = new RadioSelectorItem();
        itm1.title = pre.getName();
        itm1.description = pre.getDescription();
        itm1.data = pre;
        result[1] = itm1;

        AirSeat business = new AirSeat();
        business.setType(3);
        business.setName("Business");
        business.setDescription("Bay tiết kiệm, đáp ứng mọi nhu cầu cơ bản của bạn");
        RadioSelectorItem itm2  = new RadioSelectorItem();
        itm2.title = business.getName();
        itm2.description = business.getDescription();
        itm2.data = business;
        result[2] = itm2;

        AirSeat firstClass = new AirSeat();
        firstClass.setType(4);
        firstClass.setName("First Class");
        firstClass.setDescription("Hạng cao cấp nhất với dịch vụ 5 sao được cá nhân hóa");
        RadioSelectorItem itm3  = new RadioSelectorItem();
        itm3.title = firstClass.getName();
        itm3.description = firstClass.getDescription();
        itm3.data = firstClass;
        result[3] = itm3;

        return result;
    }
}
