package vcc.com.wowholiday.presenter.flight;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.ConstraintLayout;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Flight;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by QuangPH on 2020-02-06.
 */
public class FlightLineTicketView extends ConstraintLayout {

    public FlightLineTicketView(Context context) {
        super(context);
        compound();
    }

    public FlightLineTicketView(Context context, AttributeSet attrs) {
        super(context, attrs);
        compound();
    }

    public void setFlightDirectionTitle(@StringRes int title) {
        TextView tvDirection = findViewById(R.id.flight_line_ticket_layout_tv_direction);
        tvDirection.setText(title);
    }

    public void showFlightWayInfo(FlightWay way) {
        TextView tvStart = findViewById(R.id.flight_line_ticket_layout_tv_start_point);
        tvStart.setText(String.format("%s(%s)", way.getStartLocation().getName(), way.getStartLocation().getID()));

        TextView tvEnd = findViewById(R.id.flight_line_ticket_layout_tv_end_point);
        tvEnd.setText(String.format("%s(%s)", way.getEndLocation().getName(), way.getEndLocation().getID()));

        TextView tvDate = findViewById(R.id.flight_line_ticket_layout_tv_time);
        String time = TimeFormatUtil.getDayOfWeekDateFromISO8601UTC(way.getDateInfo().getStartDate())
                + " - " + String.format("%s - %s",
                TimeFormatUtil.getDefaultTimeFormatFromISO8601UTC(way.getDateInfo().getStartDate()),
                TimeFormatUtil.getDefaultTimeFormatFromISO8601UTC(way.getDateInfo().getEndDate()));
        tvDate.setText(time);

        TextView tvFlight = findViewById(R.id.flight_line_ticket_layout_tv_flight);
        String transits = way.getVendors();
        Flight[] flights = way.getTransits();
        if (flights.length == 1) {
            transits += " - " + getResources().getString(R.string.direct_air);
        } else {
            transits += " - " + getResources().getString(R.string.number_transit, (flights.length - 1));
        }
        tvFlight.setText(transits);
    }

    public void canShowDetail(boolean show) {
        findViewById(R.id.flight_line_ticket_layout_tv_detail).setVisibility(show? VISIBLE : GONE);
    }

    public void showWarningInfo(boolean show) {
        findViewById(R.id.flight_line_ticket_layout_tvic_no_cash_back).setVisibility(show ? VISIBLE : GONE);
        findViewById(R.id.flight_line_ticket_layout_tvic_no_change_way).setVisibility(show ? VISIBLE : GONE);
    }

    public void setSeeDetailListener(View.OnClickListener listener) {
        findViewById(R.id.flight_line_ticket_layout_tv_detail).setOnClickListener(listener);
    }

    private void compound() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_flight_line_ticket, this, true);
    }
}
