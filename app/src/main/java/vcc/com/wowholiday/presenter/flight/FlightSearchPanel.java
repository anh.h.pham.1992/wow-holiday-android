package vcc.com.wowholiday.presenter.flight;

import android.content.Context;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.Calendar;

import common.presenter.SafeClicked;
import common.presenter.WHBottomSheetDialog;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseConstraintView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.air.AirSeat;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.presenter.dialog.DateSelectBottomSheet;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorBottomSheet;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorItem;
import vcc.com.wowholiday.presenter.dialog.radioselector.RadioSelectorBottomSheet;
import vcc.com.wowholiday.presenter.util.SpannableBuilder;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */
public class FlightSearchPanel extends BaseConstraintView {

    private TextViewWithIcon mTvicStartDate;
    private TextViewWithIcon mTvicEndDate;
    private CheckBox mCbTwoWay;
    private TextViewWithIcon mTvicPersonCount;
    private TextViewWithIcon mTvicSeatType;
    private TextView mTvSeatWarning;
    private TextViewWithIcon mTvicStartLoc;
    private TextViewWithIcon mTvicEndLoc;
    private TextView mTvDestinationWarning;
    private TextView mTvStartDateWarning;
    private TextView mTvPersonWarning;
    private TextView mTvStartLocWarning;

    private FlightBookingProperty mFlightBookingProperty;

    public FlightSearchPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        mFlightBookingProperty = new FlightBookingProperty();
        AirSeat defaultSeat = dummyDefault();
        mFlightBookingProperty.setSeatType(defaultSeat);

        compound();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTvicStartDate = findViewById(R.id.flight_search_panel_layout_tv_start_date);
        mTvicEndDate = findViewById(R.id.flight_search_panel_layout_tv_end_date);
        mCbTwoWay = findViewById(R.id.flight_search_panel_layout_cb_two_way);
        mTvicPersonCount = findViewById(R.id.flight_search_panel_layout_tvic_person_count);
        mTvicStartLoc = findViewById(R.id.flight_search_layout_tvic_start_des);
        mTvicEndLoc = findViewById(R.id.flight_search_panel_layout_tvic_stop_des);
        mTvDestinationWarning = findViewById(R.id.flight_search_panel_layout_tv_warning);
        mTvicSeatType = findViewById(R.id.flight_search_panel_layout_tvic_seat_type);
        mTvSeatWarning = findViewById(R.id.flight_search_panel_layout_tv_seat_warning);
        mTvStartDateWarning = findViewById(R.id.flight_search_panel_layout_tv_start_date_warning);
        mTvPersonWarning = findViewById(R.id.flight_search_panel_layout_tv_person_warning);
        mTvStartLocWarning = findViewById(R.id.flight_search_panel_layout_tv_start_loc_warning);

        mTvicStartDate.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                final DateSelectBottomSheet sheet = new DateSelectBottomSheet();
                Calendar date = mFlightBookingProperty.getStartDate() == null
                        ? mFlightBookingProperty.getDefaultDate() : mFlightBookingProperty.getStartDate();
                sheet.initDate(date);
                sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Calendar selectedDate = sheet.getSelectedDate();
                        mFlightBookingProperty.setStartDate(selectedDate);
                        mTvicStartDate.setText(TimeFormatUtil.getDefaultDateFormat(selectedDate));

                        mCbTwoWay.setEnabled(true);
                    }
                });
                sheet.show((AppCompatActivity) getContext(), "start_date");
            }
        });

        mTvicEndDate.setEnabled(false);
        mTvicEndDate.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                final DateSelectBottomSheet sheet = new DateSelectBottomSheet();
                sheet.initDate(mFlightBookingProperty.getEndDate());
                sheet.setMinDate(mFlightBookingProperty.getStartDate());
                sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Calendar selectedDate = sheet.getSelectedDate();
                        mFlightBookingProperty.setEndDate(selectedDate);
                        mTvicEndDate.setText(TimeFormatUtil.getDefaultDateFormat(selectedDate));
                    }
                });
                sheet.show((AppCompatActivity) getContext(), "end_date");
            }
        });

        mCbTwoWay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableEndDate(isChecked);
                if (isChecked) {
                    mFlightBookingProperty.setRoundTrip(true);
                    mTvicEndDate.setText(TimeFormatUtil.getDefaultDateFormat(mFlightBookingProperty.getEndDate()));
                } else {
                    mFlightBookingProperty.setRoundTrip(false);
                    mTvicEndDate.setText("");
                }
            }
        });

        mTvicPersonCount.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                CountSelectorBottomSheet sheet = new CountSelectorBottomSheet();
                sheet.addItems(createSelectorPersonCountItem());
                sheet.setOnCountSelectedListener(new CountSelectorBottomSheet.IOnCountSelectListener() {
                    @Override
                    public void onCountChange(TextView tvTitle, int rowIndex, CountSelectorItem item) {
                        showPersonCountLabel(tvTitle, rowIndex, item.count);
                        updatePeopleCount(rowIndex, item.count);
                    }
                });
                sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mTvicPersonCount.setText(String.valueOf(mFlightBookingProperty.getAdultCount()
                                + mFlightBookingProperty.getChildrenCount()
                                + mFlightBookingProperty.getNewbornCount())
                                + " " + getResources().getString(R.string.person));
                    }
                });
                sheet.setTitle(R.string.person_count);
                sheet.show((AppCompatActivity) getContext(), "person_count");
            }
        });

        mTvicStartLoc.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                StartLocationCmd cmd = new StartLocationCmd();
                cmd.property = mFlightBookingProperty;
                mPresenter.executeCommand(cmd);
            }
        });

        mTvicEndLoc.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                EndLocationCmd cmd = new EndLocationCmd();
                cmd.property = mFlightBookingProperty;
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.flight_search_panel_layout_bt_search).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                if (validInfo()) {
                    SearchCmd cmd = new SearchCmd();
                    cmd.property = mFlightBookingProperty;
                    mPresenter.executeCommand(cmd);
                }
            }
        });

        findViewById(R.id.flight_search_panel_layout_iv_reverse).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Location start = mFlightBookingProperty.getStartLoc();
                Location end = mFlightBookingProperty.getEndLoc();
                mFlightBookingProperty.setStartLoc(end);
                mFlightBookingProperty.setEndLoc(start);
                if (end != null) {
                    mTvicStartLoc.setText(end.getName());
                }
                if (start != null) {
                    mTvicEndLoc.setText(start.getName());
                }
            }
        });

        mTvicSeatType.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                showSeatType();
            }
        });
        mTvicSeatType.setText(mFlightBookingProperty.getSeatType().getName());
    }

    public void initFlightBookingProperty(FlightBookingProperty property) {
        mFlightBookingProperty = property;
        mTvicStartLoc.setText(property.getStartLoc().getName());
        mTvicEndLoc.setText(property.getEndLoc().getName());
        mTvicStartDate.setText(TimeFormatUtil.getDefaultDateFormat(property.getStartDate()));
        if (property.isRoundTrip()) {
            mCbTwoWay.setChecked(true);
            mCbTwoWay.setEnabled(true);
            mTvicEndDate.setText(TimeFormatUtil.getDefaultDateFormat(property.getEndDate()));
        }

        mCbTwoWay.setEnabled(property.getStartDate() != null);
        mTvicPersonCount.setText(String.valueOf(property.getTotalCount()));


    }

    public void setStartLocation(Location location) {
        mFlightBookingProperty.setStartLoc(location);
        mTvicStartLoc.setText(location.getName());
        if (validLocation()) {
            mTvDestinationWarning.setVisibility(VISIBLE);
        } else {
            mTvDestinationWarning.setVisibility(GONE);
        }
    }

    public void setEndLocation(Location location) {
        mFlightBookingProperty.setEndLoc(location);
        mTvicEndLoc.setText(location.getName());
        if (validLocation()) {
            mTvDestinationWarning.setVisibility(VISIBLE);
        } else {
            mTvDestinationWarning.setVisibility(GONE);
        }
    }

    private void compound() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_flight_search_panel, this, true);
    }

    private void enableEndDate(boolean enable) {
        if (enable) {
            mTvicEndDate.setEnabled(true);
            mTvicEndDate.setTextColor(ContextCompat.getColor(getContext(), R.color.gun_metal));
            mTvicEndDate.setLeftIconTint(ContextCompat.getColor(getContext(), R.color.gun_metal));
        } else {
            mTvicEndDate.setEnabled(false);
            mTvicEndDate.setTextColor(ContextCompat.getColor(getContext(), R.color.light_grey));
            mTvicEndDate.setLeftIconTint(ContextCompat.getColor(getContext(), R.color.light_grey));
        }
    }

    private CountSelectorItem[] createSelectorPersonCountItem() {
        CountSelectorItem[] selectors = new CountSelectorItem[3];
        CountSelectorItem adult = new CountSelectorItem();
        adult.min = 1;
        adult.max = 7;
        adult.count = mFlightBookingProperty.getAdultCount();
        selectors[0] = adult;

        CountSelectorItem child = new CountSelectorItem();
        child.min = 0;
        child.max = 6;
        child.count = mFlightBookingProperty.getChildrenCount();
        selectors[1] = child;

        CountSelectorItem newBorn = new CountSelectorItem();
        newBorn.min = 0;
        newBorn.max = 4;
        newBorn.count = mFlightBookingProperty.getNewbornCount();
        selectors[2] = newBorn;

        return selectors;
    }

    private void showPersonCountLabel(TextView tvLabel, int rowIndex, int count) {
        SpannableBuilder builder = new SpannableBuilder();
        String mainTitle = "";
        if (rowIndex == 0) {
            mainTitle = getResources().getString(R.string.adult_count_format, count);
        } else if (rowIndex == 1){
            mainTitle = getResources().getString(R.string.child_count_format, count);
        } else if (rowIndex == 2) {
            mainTitle = getResources().getString(R.string.new_born_count_format, count);
        }
        builder.appendText(mainTitle).withSpan(new ForegroundColorSpan(getResources().getColor(R.color.dark)));
        builder.appendText("\n");
        String subTitle = "";
        if (rowIndex == 0) {
            subTitle = getResources().getString(R.string.flight_adult_year_old);
        } else if (rowIndex == 1){
            subTitle = getResources().getString(R.string.flight_child_year_old);
        } else if (rowIndex == 2) {
            subTitle = getResources().getString(R.string.flight_new_born_year_old);
        }
        builder.appendText(subTitle).withSpan(new ForegroundColorSpan(getResources().getColor(R.color.light_periwinkle)))
                .withSpan(new TextAppearanceSpan(getContext(), R.style.FlightPersonCountSubTitle));
        tvLabel.setText(builder.getSpannedText());
    }

    private void updatePeopleCount(int rowIndex, int count) {
        if (rowIndex == 0) {
            mFlightBookingProperty.setAdultCount(count);
        } else if (rowIndex == 1) {
            mFlightBookingProperty.setChildrenCount(count);
        } else if (rowIndex == 2) {
            mFlightBookingProperty.setNewbornCount(count);
        }
    }

    private boolean validLocation() {
        if (mFlightBookingProperty.getStartLoc() != null) {
            return mFlightBookingProperty.getStartLoc().equals(mFlightBookingProperty.getEndLoc());
        } else {
            return true;
        }
    }

    private boolean validInfo() {
        if (mFlightBookingProperty.getStartLoc() == null) {
            mTvStartLocWarning.setVisibility(VISIBLE);
            return false;
        }
        mTvStartLocWarning.setVisibility(GONE);

        if (mFlightBookingProperty.getEndLoc() == null) {
            mTvDestinationWarning.setVisibility(VISIBLE);
            mTvDestinationWarning.setText(R.string.must_fill_info);
            return false;
        } else {
            if (validLocation()) {
                mTvDestinationWarning.setVisibility(VISIBLE);
                mTvDestinationWarning.setText(R.string.flight_destination_warning);
                return false;
            } else {
                mTvDestinationWarning.setVisibility(GONE);
            }
        }

        if (mFlightBookingProperty.getStartDate() == null) {
            mTvStartDateWarning.setVisibility(VISIBLE);
            return false;
        }

        mTvStartDateWarning.setVisibility(GONE);

        if (mFlightBookingProperty.getAdultCount() == 0
                && mFlightBookingProperty.getChildrenCount() == 0
                && mFlightBookingProperty.getNewbornCount() == 0) {
            mTvPersonWarning.setVisibility(VISIBLE);
            return false;
        }
        mTvPersonWarning.setVisibility(GONE);

        if (mFlightBookingProperty.getSeatType() == null) {
            mTvSeatWarning.setVisibility(VISIBLE);
            return false;
        }

        mTvSeatWarning.setVisibility(GONE);
        return true;
    }

    private void showSeatType() {
        final RadioSelectorBottomSheet sheet = new RadioSelectorBottomSheet();
        sheet.addItems(DummyData.seatType());
        sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
            @Override
            public void onDismiss() {
                AirSeat seat = (AirSeat) sheet.getSelectedItem().data;
                mTvicSeatType.setText(seat.getName());
                mFlightBookingProperty.setSeatType(seat);
            }
        });
        sheet.show((AppCompatActivity) getContext(), "seattype");
    }

    AirSeat dummyDefault() {
        AirSeat eco = new AirSeat();
        eco.setType(1);
        eco.setName("Economy");
        eco.setDescription("Bay đẳng cấp với quầy làm thủ tục và khu ghế ngồi riêng");
        return eco;
    }

    public static class StartLocationCmd implements ICommand {
        public FlightBookingProperty property;
    }
    public static class EndLocationCmd implements ICommand {
        public FlightBookingProperty property;
    }

    public static class SearchCmd implements ICommand {
        public FlightBookingProperty property;
    }
}
