package vcc.com.wowholiday.presenter.flight;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Baggage;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.model.air.PaxInfo;

/**
 * Created by QuangPH on 2020-02-06.
 */
public class FlightTicketPersonInfo extends ConstraintLayout {

    public static final int DEPART = 1;
    public static final int ARRIVAL = 2;

    public FlightTicketPersonInfo(Context context) {
        super(context);
        compound();
    }

    public void showPersonInfo(Passenger passenger, int wayType) {
        TextView tvName = findViewById(R.id.flight_e_ticket_person_info_tv_name);
        tvName.setText(String.format("%s %s", passenger.getFistName(), passenger.getLastName()));

        TextView tvTag = findViewById(R.id.flight_e_ticket_person_info_tv_tag);
        PaxInfo info = passenger.getPaxInfo();
        if (PaxInfo.ADULT.equals(info.getType())) {
            tvTag.setText(R.string.adult);
        } else if (PaxInfo.CHILD.equals(info.getType())) {
            tvTag.setText(R.string.child);
        } else if (PaxInfo.NEW_BORN.equals(info.getType())) {
            tvTag.setText(R.string.new_born);
        }

        Baggage baggage = null;
        if (wayType == DEPART) {
            baggage = passenger.getBaggageDepart();
        } else if (wayType == ARRIVAL) {
            baggage = passenger.getBaggageArrival();
        }

        TextView weight = findViewById(R.id.flight_e_ticket_person_info_tv_baggage_weight);
        if (baggage != null) {
            weight.setText(String.format("%skg", baggage.getWeight()));
        } else {
            weight.setText(String.format("%skg", 0));
        }
    }

    private void compound() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_flight_e_ticket_person_info, this, true);
    }
}
