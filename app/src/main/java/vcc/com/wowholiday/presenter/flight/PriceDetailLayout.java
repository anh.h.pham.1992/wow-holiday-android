package vcc.com.wowholiday.presenter.flight;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Baggage;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.Passenger;

public class PriceDetailLayout extends FrameLayout {
    private LinearLayout mLlDetail;
    private List<Passenger> mPassengerList;
    private boolean isCollapse = true;
    private Map<String, BaggeViewHolder> mBaggeVHCache = new HashMap<>();

    public PriceDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        compound();
        init();
    }

    private void compound() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_flight_pricec_detail, this, true);
    }

    private void init() {
        mLlDetail = findViewById(R.id.price_detail_layout_ll_detail);
        post(new Runnable() {
            @Override
            public void run() {
                mLlDetail.setTranslationY(mLlDetail.getHeight());
            }
        });
        findViewById(R.id.price_detail_layout_tvic_money_label).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
            }
        });
    }

    public void toggle() {
        if (isCollapse) {
            isCollapse = false;
            update();
            mLlDetail.post(new Runnable() {
                @Override
                public void run() {
                    mLlDetail.animate().translationYBy(-mLlDetail.getHeight());
                }
            });
        } else {
            isCollapse = true;
            mLlDetail.animate().translationYBy(mLlDetail.getHeight());
        }
    }

    public void setPassengerList(List<Passenger> passengerList) {
        mPassengerList = passengerList;
    }

    public void setFlightTrip(FlightTrip trip) {
        TextView tvPrice = findViewById(R.id.price_detail_layout_tv_total);
        tvPrice.setText(trip.getPrice() + " " + trip.getCurrencyUnit());
    }

    public void setSelectedListener(OnClickListener listener) {
        findViewById(R.id.price_detail_layout_tv_selected).setOnClickListener(listener);
    }

    public void update() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        if (hasBaggageDepart()) {
            LinearLayout llContainer = findViewById(R.id.price_detail_layout_ll_addon_depart);
            for (Passenger p : mPassengerList) {
                if (p.getBaggageDepart() != null) {
                    Baggage baggage = p.getBaggageDepart();
                    String tag = createTag(baggage, true);
                    updateBaggage(p, baggage, tag, inflater, llContainer);
                }
            }
        } else {
            findViewById(R.id.price_detail_layout_tv_baggage_depart).setVisibility(GONE);
        }

        if (hasBaggageArrival()) {
            LinearLayout llContainer = findViewById(R.id.price_detail_layout_ll_addon_arrival);
            for (Passenger p : mPassengerList) {
                if (p.getBaggageArrival() != null) {
                    Baggage baggage = p.getBaggageArrival();
                    String tag = createTag(baggage, false);
                    updateBaggage(p, baggage, tag, inflater, llContainer);
                }
            }
        } else {
            findViewById(R.id.price_detail_layout_tv_baggage_arrival).setVisibility(GONE);
        }
    }

    private void updateBaggage(Passenger passenger, Baggage baggage, String tag,
                               LayoutInflater inflater, LinearLayout llContainer) {
        BaggeViewHolder vh = mBaggeVHCache.get(tag);
        if (vh != null) {
            vh.tvLeft.setText(passenger.getFistName() + " " +passenger.getLastName() + " 1x " + baggage.getWeight() + "kg");
            vh.tvRight.setText(baggage.getPrice() + "VND");
        } else {
            vh = addBaggage(inflater, llContainer, passenger.getBaggageDepart(), passenger);
            mBaggeVHCache.put(tag, vh);
        }
    }

    private BaggeViewHolder addBaggage(LayoutInflater inflater, LinearLayout container,
                                       Baggage baggage, Passenger passenger) {
        View itemView = inflater.inflate(R.layout.layout_textview_right_left_gravity, container, false);
        container.addView(itemView);
        TextView tvLeft = itemView.findViewById(R.id.textview_right_left_gravity_tv_left);
        tvLeft.setText(passenger.getFistName() + " " +passenger.getLastName() + " 1x " + baggage.getWeight());
        TextView tvRight = itemView.findViewById(R.id.textview_right_left_gravity_tv_right);
        tvRight.setText(baggage.getPrice() + "VND");

        BaggeViewHolder vh = new BaggeViewHolder();
        vh.itemView = itemView;
        vh.tvLeft = tvLeft;
        vh.tvRight = tvRight;
        return vh;
    }

    private boolean hasBaggageDepart() {
        if (mPassengerList == null) return false;
        for (Passenger p : mPassengerList) {
            if (p.getBaggageDepart() != null) {
                return true;
            }
        }
        return false;
    }

    private boolean hasBaggageArrival() {
        if (mPassengerList == null) return false;
        for (Passenger p : mPassengerList) {
            if (p.getBaggageArrival() != null) {
                return true;
            }
        }
        return false;
    }

    private String createTag(Baggage baggage, boolean isDepart) {
        return baggage.getPrice() + "_" + baggage.getWeight() + "_" + (isDepart ? "depart" : "arrival");
    }


    class BaggeViewHolder {
        View itemView;
        TextView tvLeft;
        TextView tvRight;
    }
}
