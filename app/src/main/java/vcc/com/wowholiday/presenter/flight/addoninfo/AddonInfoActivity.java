package vcc.com.wowholiday.presenter.flight.addoninfo;

import android.content.Intent;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.presenter.flight.baggage.BaggageActivity;
import vcc.com.wowholiday.presenter.flight.eticket.FlightETicketActivity;

@Layout(R.layout.activity_flight_add_info)
public class AddonInfoActivity extends WHActivity<AddonInfoView> {

    public static final String FLIGHT_TRIP_KEY = "FLIGHT_TRIP_KEY";
    public static final String PASSENGER_KEY = "PASSENGER_KEY";

    private static final int BAGGAGE_RQ = 9832;
    private List<Passenger> mPassengerList;
    private FlightTrip mFlightTrip;

    private boolean hasBaggage;

    public static void createParams(Intent itn, FlightTrip trip, ArrayList<Passenger> passengerList) {
        itn.putExtra(FLIGHT_TRIP_KEY, trip);
        itn.putParcelableArrayListExtra(PASSENGER_KEY, passengerList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BAGGAGE_RQ) {
            mPassengerList = data.getParcelableArrayListExtra(BaggageActivity.PASSENGER_KEY);
            mView.showPassengerInfo(mPassengerList);
            mView.updatePriceLayout();
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mFlightTrip = getIntent().getParcelableExtra(FLIGHT_TRIP_KEY);
        mPassengerList = getIntent().getParcelableArrayListExtra(PASSENGER_KEY);
        hasBaggage = hasBaggage();
        mView.showWarning(hasBaggage);
        mView.showPriceDetail(mFlightTrip);
        mView.showPassengerInfo(mPassengerList);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof AddonInfoView.SelectBaggageCmd) {
            Intent itn = new Intent(this, BaggageActivity.class);
            BaggageActivity.createParams(itn, mFlightTrip, (ArrayList<Passenger>) mPassengerList);
            startActivityForResult(itn, BAGGAGE_RQ);
        } else if (command instanceof AddonInfoView.ContinueCmd) {
            Intent itn = new Intent(this, FlightETicketActivity.class);
            FlightETicketActivity.createParams(itn, mFlightTrip, (ArrayList<Passenger>) mPassengerList);
            startActivity(itn);

        }
    }

    private boolean hasBaggage() {
        for (Passenger p : mPassengerList) {
            if (p.getBaggageDepart() != null || p.getBaggageArrival() != null) {
                return true;
            }
        }
        return false;
    }
}
