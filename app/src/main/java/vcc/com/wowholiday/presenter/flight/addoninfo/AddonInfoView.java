package vcc.com.wowholiday.presenter.flight.addoninfo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.presenter.flight.PriceDetailLayout;

public class AddonInfoView extends BaseRelativeView {

    private PriceDetailLayout mPriceLayout;

    public AddonInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.flight_ad_info_act_rl_warning).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new SelectBaggageCmd());
            }
        });

        mPriceLayout = findViewById(R.id.flight_add_info_act_price_detail);
        mPriceLayout.setSelectedListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new ContinueCmd());
            }
        });
    }

    public void showPriceDetail(FlightTrip trip) {
        mPriceLayout.setFlightTrip(trip);
    }

    public void showPassengerInfo(List<Passenger> passengerList) {
        mPriceLayout.setPassengerList(passengerList);
    }

    public void showWarning(boolean hasBaggage) {
        if (hasBaggage) {
            TextView tv = findViewById(R.id.flight_ad_info_act_tv_warning);
            tv.setText(getResources().getString(R.string.baggage_note));
        }
    }

    public void updatePriceLayout() {
        mPriceLayout.update();
    }

    public static class SelectBaggageCmd implements ICommand{}
    public static class ContinueCmd implements ICommand{}
}
