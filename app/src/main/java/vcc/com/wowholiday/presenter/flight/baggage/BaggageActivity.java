package vcc.com.wowholiday.presenter.flight.baggage;

import android.content.Intent;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.Passenger;

@Layout(R.layout.activity_baggage)
public class BaggageActivity extends WHActivity<BaggageView> {

    public static final String FLIGHT_TRIP_KEY = "FLIGHT_TRIP_KEY";
    public static final String PASSENGER_KEY = "PASSENGER_KEY";

    private List<Passenger> mPassengerList;
    private FlightTrip mFlightTrip;

    public static void createParams(Intent itn, FlightTrip trip, ArrayList<Passenger> passengerList) {
        itn.putExtra(FLIGHT_TRIP_KEY, trip);
        itn.putParcelableArrayListExtra(PASSENGER_KEY, passengerList);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mFlightTrip = getIntent().getParcelableExtra(FLIGHT_TRIP_KEY);
        mPassengerList = getIntent().getParcelableArrayListExtra(PASSENGER_KEY);
        mView.showTotal(mFlightTrip, mPassengerList);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof BaggageView.ConfirmCmd) {
            Intent itn = new Intent();
            itn.putParcelableArrayListExtra(PASSENGER_KEY, (ArrayList<? extends Parcelable>) mPassengerList);
            setResult(RESULT_OK, itn);
            finish();
        }
    }
}
