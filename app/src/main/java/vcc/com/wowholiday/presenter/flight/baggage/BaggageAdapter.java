package vcc.com.wowholiday.presenter.flight.baggage;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.BaseVHData;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.StartPaddingDecorator;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Baggage;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.Passenger;

public class BaggageAdapter extends BaseRclvAdapter {
    private static final int HEADER = 1;
    private static final int BAGGAGE_DEPART = 2;
    private static final int BAGGAGE_ARRIVAL = 3;

    private BaggeAdapterItemSelector mListener;

    public BaggageAdapter(BaggeAdapterItemSelector listener) {
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return ((BaseVHData)mDataSet.get(position)).type;
    }

    @Override
    public int getLayoutResource(int viewType) {
        if (viewType == HEADER) {
            return R.layout.item_baggage_way_header;
        } else if (viewType == BAGGAGE_DEPART || viewType == BAGGAGE_ARRIVAL) {
            return R.layout.item_baggage_select;
        }
        return 0;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        if (viewType == HEADER) {
            return new HeaderVH(itemView);
        } else if (viewType == BAGGAGE_DEPART || viewType == BAGGAGE_ARRIVAL) {
            return new BaggageVH(itemView);
        }
        return null;
    }

    public void setData(FlightTrip trip, List<Passenger> passengerList) {
        HeaderVHData departHeader = new HeaderVHData();
        departHeader.title = "Chiều đi";
        departHeader.realData = trip.getDepartureWay();
        mDataSet.add(departHeader);

        for (Passenger p : passengerList) {
            BaggageVHData depart = new BaggageVHData();
            depart.realData = p;
            depart.type = BAGGAGE_DEPART;
            mDataSet.add(depart);
        }


        if (trip.getArrivalWay() != null) {
            HeaderVHData arrivalHeader = new HeaderVHData();
            arrivalHeader.title = "Chiều về";
            arrivalHeader.realData = trip.getArrivalWay();
            mDataSet.add(arrivalHeader);

            for (Passenger p : passengerList) {
                BaggageVHData arrival = new BaggageVHData();
                arrival.realData = p;
                arrival.type = BAGGAGE_ARRIVAL;
                mDataSet.add(arrival);
            }
        }
        notifyDataSetChanged();
    }

    private class HeaderVH extends BaseRclvHolder<HeaderVHData> {
        TextView tvWay;
        TextView tvVendor;

        public HeaderVH(@NonNull View itemView) {
            super(itemView);
            tvWay = itemView.findViewById(R.id.baggage_way_header_itm_tv_way);
            tvVendor = itemView.findViewById(R.id.baggage_way_header_itm_tv_air);
        }

        @Override
        public void onBind(HeaderVHData vhData) {
            super.onBind(vhData);
            tvWay.setText(vhData.title);
            tvVendor.setText(vhData.realData.getVendors());
        }
    }

    private class BaggageVH extends BaseRclvHolder<BaggageVHData> {

        TextView tvPassengerName;
        TextView tvPassengerBaggageWeight;
        RecyclerView rclvBaggage;
        BaggageSelectorAdapter adapter;

        public BaggageVH(@NonNull View itemView) {
            super(itemView);
            tvPassengerName = itemView.findViewById(R.id.baggage_select_itm_tv_passenger_name);
            tvPassengerBaggageWeight = itemView.findViewById(R.id.baggage_select_itm_tv_passenger_baggage_weight);
            rclvBaggage = itemView.findViewById(R.id.baggage_select_itm_rclv_weight_select);
            rclvBaggage.setLayoutManager(new LinearLayoutManager(itemView.getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            adapter = new BaggageSelectorAdapter();
            adapter.setBaggageSelectedListener(new BaggageSelectorAdapter.IBaggageSelectedListener() {
                @Override
                public void onSelected(Baggage baggage) {
                    BaggageVHData data = (BaggageVHData) mDataSet.get(getAdapterPosition());
                    if (data.type == BAGGAGE_ARRIVAL) {
                        data.realData.setBaggageArrival(baggage);
                    } else {
                        data.realData.setBaggageDepart(baggage);
                    }
                    tvPassengerBaggageWeight.setText(baggage.getWeight() + " kg");
                    mListener.onBaggeSelected();
                }
            });
            rclvBaggage.setAdapter(adapter);
            rclvBaggage.addItemDecoration(
                    new StartPaddingDecorator(itemView.getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
            rclvBaggage.addItemDecoration(
                    new SpaceItemDecoration(itemView.getResources().getDimensionPixelOffset(R.dimen.dimen_8),
                            LinearLayoutManager.HORIZONTAL));
        }

        @Override
        public void onBind(BaggageVHData vhData) {
            super.onBind(vhData);
            adapter.reset(vhData.baggages);
            adapter.setSelectedPosition(vhData.getSelectedBaggage());
            tvPassengerName.setText(vhData.realData.getFistName() + " " + vhData.realData.getLastName());
            if (vhData.type == BAGGAGE_DEPART) {
                Baggage baggageDepart = vhData.realData.getBaggageDepart();
                if (baggageDepart == null) {
                    tvPassengerBaggageWeight.setText("0 kg");
                } else {
                    tvPassengerBaggageWeight.setText(baggageDepart.getWeight() + " kg");
                }
            } else {
                Baggage baggageArrival = vhData.realData.getBaggageArrival();
                if (baggageArrival == null) {
                    tvPassengerBaggageWeight.setText("0 kg");
                } else {
                    tvPassengerBaggageWeight.setText(baggageArrival.getWeight() + " kg");
                }
            }
        }


    }

    private class HeaderVHData extends BaseVHData<FlightWay> {
        String title;
        HeaderVHData() {
            type = HEADER;
        }
    }

    private class BaggageVHData extends BaseVHData<Passenger> {
        public List<Baggage> baggages = dummy();

        int getSelectedBaggage() {
            if (type == BAGGAGE_DEPART) {
                Baggage selected = realData.getBaggageDepart();
                if (selected != null) {
                    for (int i = 0; i < baggages.size(); i++) {
                        if (baggages.get(i).equals(selected)) {
                            return i;
                        }
                    }
                }
                return 0;
            } else {
                Baggage selected = realData.getBaggageArrival();
                if (selected != null) {
                    for (int i = 0; i < baggages.size(); i++) {
                        if (baggages.get(i).equals(selected)) {
                            return i;
                        }
                    }
                }
                return 0;
            }
        }

        List<Baggage> dummy() {
            List<Baggage> baggages = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                Baggage b = new Baggage();
                b.setWeight(i*5);
                b.setPrice(10000 * i * 5);
                baggages.add(b);
            }
            return baggages;
        }
    }


    public interface BaggeAdapterItemSelector {
        void onBaggeSelected();
    }
}
