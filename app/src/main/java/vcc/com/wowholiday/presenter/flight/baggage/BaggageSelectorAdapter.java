package vcc.com.wowholiday.presenter.flight.baggage;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Baggage;

public class BaggageSelectorAdapter extends BaseRclvAdapter {

    private IBaggageSelectedListener mListener;
    private int mSelectedPosition = 0;

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_baggage_selector;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new VH(itemView);
    }

    public void setBaggageSelectedListener(IBaggageSelectedListener listener) {
        mListener = listener;
    }

    public void setSelectedPosition(int position) {
        mSelectedPosition = position;
    }

    private class VH extends BaseRclvHolder<Baggage> {
        TextView tvWeight;
        TextView tvPrice;

        public VH(@NonNull View itemView) {
            super(itemView);
            tvWeight = itemView.findViewById(R.id.baggage_selector_itm_tv_weight);
            tvPrice = itemView.findViewById(R.id.baggage_selector_itm_tv_price);
            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    int pos = getAdapterPosition();
                    if (pos != mSelectedPosition) {
                        int old = mSelectedPosition;
                        mSelectedPosition = pos;
                        notifyItemChanged(old);
                        notifyItemChanged(mSelectedPosition);
                        mListener.onSelected((Baggage) mDataSet.get(pos));
                    }
                }
            });
        }

        @Override
        public void onBind(Baggage vhData) {
            super.onBind(vhData);
            tvWeight.setText(vhData.getWeight() + " kg");
            tvPrice.setText(vhData.getPrice() + " VND");

            if (getAdapterPosition() == mSelectedPosition) {
                itemView.setSelected(true);
                tvPrice.setTextColor(itemView.getResources().getColor(R.color.turquoise_blue));
                tvWeight.setTextColor(itemView.getResources().getColor(R.color.turquoise_blue));
            } else {
                itemView.setSelected(false);
                tvPrice.setTextColor(itemView.getResources().getColor(R.color.light_periwinkle_2));
                tvWeight.setTextColor(itemView.getResources().getColor(R.color.dark));
            }
        }
    }

    public interface IBaggageSelectedListener {
        void onSelected(Baggage baggage);
    }
}
