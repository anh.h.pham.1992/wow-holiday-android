package vcc.com.wowholiday.presenter.flight.baggage;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.Passenger;

public class BaggageView extends BaseLinearView {

    private RecyclerView mRclvBaggage;
    private TextView mTvTotal;
    private TextView mTvDepart;
    private TextView mTvArrival;
    private TextView mTvDepartPrice;
    private TextView mTvArrivalPrice;
    private BaggageAdapter mAdapter;

    private List<Passenger> mPassengerList;

    public BaggageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mRclvBaggage = findViewById(R.id.baggage_act_rclv_baggage);
        mRclvBaggage.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        mTvTotal = findViewById(R.id.baggage_act_tv_total_price);
        mTvDepart = findViewById(R.id.baggage_act_tv_tv_depart);
        mTvArrival = findViewById(R.id.baggage_act_tv_tv_arrival);
        mTvDepartPrice = findViewById(R.id.baggage_act_tv_tv_depart_price);
        mTvArrivalPrice = findViewById(R.id.baggage_act_tv_tv_arrival_price);

        findViewById(R.id.baggage_act_tv_tv_confirm).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new ConfirmCmd());
            }
        });

        mAdapter = new BaggageAdapter(new BaggageAdapter.BaggeAdapterItemSelector() {
            @Override
            public void onBaggeSelected() {
                updateTotalBaggePanel();
            }
        });
        mRclvBaggage.setAdapter(mAdapter);
    }

    public void showTotal(FlightTrip trip, List<Passenger> passengerList) {
        mPassengerList = passengerList;

        FlightWay departureWay = trip.getDepartureWay();
        mTvDepart.setText(departureWay.getStartLocation().getID()
                + " - " + departureWay.getEndLocation().getID());
        FlightWay arrivalWay = trip.getArrivalWay();
        if (arrivalWay != null) {
            mTvArrival.setText(arrivalWay.getStartLocation().getID()
                    + " - " + arrivalWay.getEndLocation().getID());
        }

        mAdapter.setData(trip, passengerList);
        updateTotalBaggePanel();
    }

    private void updateTotalBaggePanel() {
        double arrival = 0;
        double depart = 0;
        for (Passenger p : mPassengerList) {
            if (p.getBaggageArrival() != null) {
                arrival += p.getBaggageArrival().getPrice();
            }

            if (p.getBaggageDepart() != null) {
                depart += p.getBaggageDepart().getPrice();
            }
        }
        if (depart != 0) {
            mTvDepartPrice.setText(depart + " VND");
        }
        if (arrival != 0) {
            mTvArrivalPrice.setText(arrival + " VND");
        }

        double total = depart + arrival;
        if (total != 0) {
            mTvTotal.setText(total + " VND");
        }
    }

    public static class ConfirmCmd implements ICommand {}
}
