package vcc.com.wowholiday.presenter.flight.bookingresult;

import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.FlightSearchAction;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightDataDTO;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.presenter.flight.FlightSearchPanel;
import vcc.com.wowholiday.presenter.flight.flightdetail.FlightBookingDetailActivity;
import vcc.com.wowholiday.presenter.flight.summary.FlightSummaryActivity;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */

@Layout(R.layout.activity_flight_booking_result)
public class FlightBookingResultActivity extends WHActivity<FlightBookingResultView> {

    public static final String BOOKING_PROPERTY = "BOOKING_PROPERTY";
    public static final String DEPARTURE_TYPE = "DEPARTURE";
    public static final String FLIGHT_DATA = "FLIGHT_DATA";
    public static final String FLIGHT_TRIP = "FLIGHT_TRIP";

    public static final int DEPARTURE = 1;
    public static final int ARRIVAL = 2;

    private int mType = DEPARTURE;
    private FlightDataDTO mFlightData;
    private FlightBookingProperty mSearchProperty;
    private FlightTrip mTripContainDepartWay;

    public static void createParam(Intent itn, FlightBookingProperty property, int type) {
        itn.putExtra(BOOKING_PROPERTY, property);
        itn.putExtra(DEPARTURE_TYPE, type);
    }

    public static void createParam(Intent itn, FlightDataDTO dto, FlightTrip trip,
                                   FlightBookingProperty property, int type) {
        itn.putExtra(FLIGHT_DATA, dto);
        itn.putExtra(DEPARTURE_TYPE, type);
        itn.putExtra(FLIGHT_TRIP, trip);
        itn.putExtra(BOOKING_PROPERTY, property);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234) {
            if (resultCode == RESULT_CANCELED) {
                mView.animExpand();
            } else {
                mView.animCollapseSearchPanel();
            }
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mType = getIntent().getIntExtra(DEPARTURE_TYPE, DEPARTURE);
        mSearchProperty = getIntent().getParcelableExtra(BOOKING_PROPERTY);
        mView.showTitle(mSearchProperty);
        if (mType == DEPARTURE) {
            doSearch(mSearchProperty);
        } else {
            mFlightData = getIntent().getParcelableExtra(FLIGHT_DATA);
            mTripContainDepartWay = getIntent().getParcelableExtra(FLIGHT_TRIP);
            mView.showArrivalFlightTrip(mFlightData, mTripContainDepartWay);
        }
        //mView.enableChangeProperty(mType == DEPARTURE);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FlightBookingResultAdapter.DetailFlightCmd) {
            gotoDetailFlight(((FlightBookingResultAdapter.DetailFlightCmd) command).flight);
        } else if (command instanceof FlightBookingResultAdapter.SelectedFlightCmd) {
            gotoSummary(((FlightBookingResultAdapter.SelectedFlightCmd) command).flight);
        } else if (command instanceof FlightSearchPanel.SearchCmd) {
            mView.animCollapseSearchPanel();
            doSearch(((FlightSearchPanel.SearchCmd) command).property);
        } else if (command instanceof FlightBookingResultAdapter.ItemFlightCmd) {
            handleFlightSelect(((FlightBookingResultAdapter.ItemFlightCmd) command).flight,
                    ((FlightBookingResultAdapter.ItemFlightCmd) command).selectedWay);
        } else if (command instanceof FlightBookingResultView.ChangeDepartWayCmd) {
            setResult(RESULT_OK);
            finish();
        } else if (command instanceof FlightBookingResultView.ChangeActionbarCmd) {
            if (mType == ARRIVAL) {
                setResult(RESULT_CANCELED);
                finish();
            } else {
                mView.toggleSearchPanel();
            }
        }
    }

    private void doSearch(final FlightBookingProperty property) {
        FlightSearchAction.FlightSearchRv rv = new FlightSearchAction.FlightSearchRv();
        rv.property = property;
        mActionManager.executeAction(new FlightSearchAction(), rv, new WHApiActionCallback<FlightDataDTO>(this) {

            @Override
            public void onStart() {
                super.onStart();
                showLoading();
            }

            @Override
            public void onSuccess(FlightDataDTO responseValue) {
                super.onSuccess(responseValue);
                mFlightData = responseValue;
                mView.showFlightTrip(responseValue, property);
            }
        }, new ThirdPartyScheduler());
    }

    private void gotoDetailFlight(FlightTrip trip) {
        Intent itn = new Intent(this, FlightBookingDetailActivity.class);
        FlightBookingDetailActivity.createParams(itn,
                mType == DEPARTURE ? trip.getDepartureWay() : trip.getArrivalWay(), trip, mSearchProperty);
        startActivity(itn);
    }

    private void gotoSummary(FlightTrip trip) {
        Intent itn = new Intent(this, FlightSummaryActivity.class);
        FlightSummaryActivity.createParams(itn, trip, mSearchProperty);
        startActivity(itn);
    }

    private void handleFlightSelect(FlightTrip trip, FlightWay selectedWay) {
        if (mSearchProperty.isRoundTrip()) {
            if (mType == ARRIVAL) {
                //TODO: this line just for Test: the trip in which we select depart way,
                // then in arrival screen we select arrival way of other trip, so we can set arrival way to the trip
                mTripContainDepartWay.setArrivalWay(selectedWay);
                Intent itn = new Intent(this, FlightSummaryActivity.class);
                FlightSummaryActivity.createParams(itn, mTripContainDepartWay, mSearchProperty);
                startActivity(itn);
            } else {
                Intent itn = new Intent(this, FlightBookingResultActivity.class);
                FlightBookingResultActivity.createParam(itn, mFlightData, trip, mSearchProperty, ARRIVAL);
                startActivityForResult(itn, 1234);
            }
        } else {
            Intent itn = new Intent(this, FlightSummaryActivity.class);
            FlightSummaryActivity.createParams(itn, trip, mSearchProperty);
            startActivity(itn);
        }
    }
}
