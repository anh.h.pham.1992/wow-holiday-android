package vcc.com.wowholiday.presenter.flight.bookingresult;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Flight;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.recyclerview.LoadMoreAdapter;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */
public class FlightBookingResultAdapter extends LoadMoreAdapter {

    private static final int FLIGHT_ONE_WAY_TYPE = 1;
    private static final int FLIGHT_ROUND_TRIP_TYPE = 2;
    private IPresenter mPresenter;

    private boolean isDeparture = true;
    private boolean isCombo = false;

    public FlightBookingResultAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        Object data = mDataSet.get(position);
        if (data instanceof LoadMoreAdapter.LoadingVHData) {
            return LOADING_TYPE;
        } else {
            return isCombo ? FLIGHT_ROUND_TRIP_TYPE : FLIGHT_ONE_WAY_TYPE;
        }
    }

    @Override
    public int getLayoutResource(int viewType) {
        if (viewType == FLIGHT_ONE_WAY_TYPE) {
            return R.layout.item_flight_booking_result;
        } else if (viewType == FLIGHT_ROUND_TRIP_TYPE) {
            return R.layout.item_flight_round_trip;
        }
        return super.getLayoutResource(viewType);
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        if (viewType == FLIGHT_ONE_WAY_TYPE) {
            return new OneWayVH(itemView);
        } else if (viewType == FLIGHT_ROUND_TRIP_TYPE) {
            return new RoundTripVH(itemView);
        }
        return super.onCreateVH(itemView, viewType);
    }

    public void setDepartureWay(boolean isDeparture) {
        this.isDeparture = isDeparture;
    }

    public void setCombo(boolean combo) {
        isCombo = combo;
    }

    private class OneWayVH extends BaseRclvHolder<FlightTrip> {

        TextView tvCompanyName;
        TextView tvTime;
        TextView tvDuration;
        TextView tvTransit;
        TextView tvPrice;

        public OneWayVH(@NonNull View itemView) {
            super(itemView);
            tvCompanyName = itemView.findViewById(R.id.flight_booking_itm_tv_flight_company);
            tvTime = itemView.findViewById(R.id.flight_booking_itm_tv_time);
            tvDuration = itemView.findViewById(R.id.flight_booking_itm_tv_dur);
            tvTransit = itemView.findViewById(R.id.flight_booking_itm_tv_transit);
            tvPrice = itemView.findViewById(R.id.flight_booking_itm_tv_price);
            itemView.findViewById(R.id.flight_booking_itm_tv_detail).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    DetailFlightCmd cmd = new DetailFlightCmd();
                    cmd.flight = (FlightTrip) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemFlightCmd cmd = new ItemFlightCmd();
                    cmd.flight = (FlightTrip) mDataSet.get(getAdapterPosition());
                    cmd.selectedWay = isDeparture ? cmd.flight.getDepartureWay() : cmd.flight.getArrivalWay();
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(FlightTrip vhData) {
            super.onBind(vhData);
            FlightWay way = isDeparture ? vhData.getDepartureWay() : vhData.getArrivalWay();
            tvCompanyName.setText(vhData.getVendor().getName());
            String date = TimeFormatUtil.getDefaultDayFormatFromISO8601UTC(way.getDateInfo().getStartDate());
            tvTime.setText(String.format("%s - %s (%s)",
                    way.getStartTime(),
                    way.getEndTime(),
                    date));
            int[] dur = way.getDuration();
            tvDuration.setText(itemView.getResources().getString(R.string.air_duration_format_2, dur[0], dur[1]));
            String transit = way.getTransit();
            if (TextUtils.isEmpty(transit)) {
                transit = itemView.getResources().getString(R.string.direct_air);
            }
            tvTransit.setText(transit);
            tvPrice.setText(String.format("%s VNĐ", MoneyFormatter.defaultFormat(String.valueOf(vhData.getPrice()))));
        }
    }

    private class RoundTripVH extends BaseRclvHolder<FlightTrip> {

        TextView tvDepartStart;
        TextView tvDepartEnd;
        TextView tvDepartVendors;
        TextView tvDepartTransits;
        TextView tvDepartTime;
        TextView tvArrivalStart;
        TextView tvArrivalEnd;
        TextView tvArrivalVendors;
        TextView tvArrivalTransits;
        TextView tvArrivalTime;
        TextView tvPrice;

        public RoundTripVH(@NonNull View itemView) {
            super(itemView);
            tvDepartStart = itemView.findViewById(R.id.flight_round_trip_itm_tv_depart_start_point);
            tvDepartEnd = itemView.findViewById(R.id.flight_round_trip_itm_tv_depart_end_point);
            tvDepartVendors = itemView.findViewById(R.id.flight_round_trip_itm_tv_depart_vendors);
            tvDepartTransits = itemView.findViewById(R.id.flight_round_trip_itm_tv_depart_transits);
            tvDepartTime = itemView.findViewById(R.id.flight_round_trip_itm_tv_depart_time);
            tvArrivalStart = itemView.findViewById(R.id.flight_round_trip_itm_tv_arrival_start_point);
            tvArrivalEnd = itemView.findViewById(R.id.flight_round_trip_itm_tv_arrival_end_point);
            tvArrivalVendors = itemView.findViewById(R.id.flight_round_trip_itm_tv_arrival_vendors);
            tvArrivalTransits = itemView.findViewById(R.id.flight_round_trip_itm_tv_arrival_transits);
            tvArrivalTime = itemView.findViewById(R.id.flight_round_trip_itm_tv_arrival_time);
            tvPrice = itemView.findViewById(R.id.flight_round_trip_itm_tv_price);
            itemView.findViewById(R.id.flight_round_trip_itm_tv_select).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    SelectedFlightCmd cmd = new SelectedFlightCmd();
                    cmd.flight = (FlightTrip) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
            itemView.findViewById(R.id.flight_round_trip_itm_tv_detail).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    DetailFlightCmd cmd = new DetailFlightCmd();
                    cmd.flight = (FlightTrip) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(FlightTrip vhData) {
            super.onBind(vhData);
            FlightWay departure = vhData.getDepartureWay();
            tvDepartStart.setText(String.format("%s (%s)", departure.getStartLocation().getName(),
                    departure.getStartLocation().getID()));
            tvDepartEnd.setText(String.format("%s (%s)", departure.getEndLocation().getName(),
                    departure.getEndLocation().getID()));
            tvDepartVendors.setText(departure.getVendors());
            String departTransit = departure.getTransitIDs();
            if (TextUtils.isEmpty(departTransit)) {
                tvDepartTransits.setText(itemView.getResources().getString(R.string.direct_air));
            } else {
                tvDepartTransits.setText(itemView.getResources().getString(R.string.transit, departTransit));
            }

            int[] durDepart = departure.getDuration();
            String durDepartStr = "";
            if (durDepart[1] == 0) {
                durDepartStr = itemView.getResources().getString(R.string.air_duration_format_1, durDepart[0]);
            } else {
                durDepartStr = itemView.getResources().getString(R.string.air_duration_format_2,
                        durDepart[0], durDepart[1]);
            }

            tvDepartTime.setText(String.format("%s - %s(%s) | %s",
                    departure.getStartTime(), departure.getEndTime(), departure.getEndDay(), durDepartStr));

            FlightWay arrival = vhData.getArrivalWay();
            tvArrivalStart.setText(String.format("%s (%s)", arrival.getStartLocation().getName(),
                    arrival.getStartLocation().getID()));
            tvArrivalEnd.setText(String.format("%s (%s)", arrival.getEndLocation().getName(),
                    arrival.getEndLocation().getID()));
            tvArrivalVendors.setText(arrival.getVendors());

            String arrivalTransit = arrival.getTransitIDs();
            if (TextUtils.isEmpty(arrivalTransit)) {
                tvArrivalTransits.setText(itemView.getResources().getString(R.string.direct_air));
            } else {
                tvArrivalTransits.setText(itemView.getResources().getString(R.string.transit, arrivalTransit));
            }
            int[] durArrival = arrival.getDuration();
            String durArrivalStr = "";
            if (durArrival[1] == 0) {
                durArrivalStr = itemView.getResources().getString(R.string.air_duration_format_1,
                        durArrival[0]);
            } else {
                durArrivalStr = itemView.getResources().getString(R.string.air_duration_format_2,
                        durArrival[0], durArrival[1]);
            }
            tvArrivalTime.setText(String.format("%s - %s(%s) | %s", arrival.getStartTime(),
                    arrival.getEndTime(), arrival.getEndDay(), durArrivalStr));

            tvPrice.setText(String.format("%s %s", MoneyFormatter.defaultFormat(vhData.getPrice()),
                    vhData.getCurrencyUnit()));
        }
    }


    public static class DetailFlightCmd implements ICommand {
        public FlightTrip flight;
    }

    public static class ItemFlightCmd implements ICommand {
        public FlightTrip flight;
        public FlightWay selectedWay;
    }

    public static class SelectedFlightCmd implements ICommand {
        public FlightTrip flight;
    }
}
