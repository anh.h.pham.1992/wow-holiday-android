package vcc.com.wowholiday.presenter.flight.bookingresult;

import android.content.Context;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import common.presenter.SafeClicked;
import common.presenter.WHBottomSheetDialog;
import common.presenter.adapter.decor.DividerDecorator;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightDataDTO;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.presenter.dialog.sort.SelectorBottomSheet;
import vcc.com.wowholiday.presenter.flight.FlightSearchPanel;
import vcc.com.wowholiday.presenter.flight.bookingresult.filter.FlightFilterBottomSheet;
import vcc.com.wowholiday.presenter.flight.bookingresult.filter.FlightFilterFactory;
import vcc.com.wowholiday.presenter.flight.bookingresult.filter.FlightFilterItem;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.util.SpannableBuilder;
import vcc.com.wowholiday.presenter.widget.recyclerview.WrapRecyclerView;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */
public class FlightBookingResultView extends BaseLinearView {

    private static final int EXPAND_MODE = 1;
    private static final int COLLAPSE_MODE = 2;

    private WrapRecyclerView mRclvContent;
    private FlightSearchPanel mSearchPanel;
    private TextView mTvWayType;
    private TextView mTvTripName;
    private TextView mTvTripTime;
    private View mVIndicator;

    private int mSearchPanelMode = COLLAPSE_MODE;
    private FlightDataDTO mFlightDTO;
    private List<FlightFilterItem> mSelectedFilterList = new ArrayList<>();
    private List<FlightFilterItem> mAllFilterList = new ArrayList<>();
    private FlightBookingResultAdapter mAdapter;

    public FlightBookingResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean hasMVPChildren() {
        return true;
    }

    @Override
    public void onInitView() {
        mTvWayType = findViewById(R.id.flight_booking_result_act_tv_way_type);
        mSearchPanel = findViewById(R.id.flight_booking_result_act_flight_search_panel);
        mRclvContent = findViewById(R.id.flight_booking_result_act_rclv_content);
        mTvTripName = findViewById(R.id.flight_booking_result_act_trip_name);
        mTvTripTime = findViewById(R.id.flight_booking_result_act_trip_time);
        mVIndicator = findViewById(R.id.flight_booking_result_act_ll_filter_indicator);
        mRclvContent.addItemDecoration(new DividerDecorator(getContext(), R.drawable.shape_divider_vert));
        mAdapter = new FlightBookingResultAdapter(mPresenter);
        mRclvContent.setAdapter(mAdapter);

        mSearchPanel.addOnLayoutChangeListener(new OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
                                       int oldTop, int oldRight, int oldBottom) {
                if (mSearchPanelMode == EXPAND_MODE) {
                    mTvWayType.setY(mTvWayType.getY() + bottom - oldBottom);
                    mRclvContent.setY(mRclvContent.getY() + bottom - oldBottom);
                }
            }
        });

        post(new Runnable() {
            @Override
            public void run() {
                mSearchPanel.setY(-mSearchPanel.getHeight());
            }
        });

        findViewById(R.id.action_bar_text_right).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new ChangeActionbarCmd());
            }
        });

        findViewById(R.id.flight_booking_result_act_fl_sort_panel).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                SelectorBottomSheet sheet = new SelectorBottomSheet();
                sheet.showTitle(R.string.sort);
                sheet.showSelectList(FlightSortFactory.makeSorts(mFlightDTO.flightSortList));
                sheet.show((AppCompatActivity) getContext(), "sort");
            }
        });

        findViewById(R.id.flight_booking_result_act_ll_filter_panel).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                final FlightFilterBottomSheet sheet = new FlightFilterBottomSheet(mFlightDTO.flightFilterList);
                sheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mSelectedFilterList = sheet.getSelectedItemList();
                        mVIndicator.setVisibility(mSelectedFilterList.isEmpty() ? INVISIBLE : VISIBLE);
                    }
                });
                sheet.setOnRefreshListener(new FlightFilterBottomSheet.IOnRefreshFilter() {
                    @Override
                    public void onRefresh() {
                        mSelectedFilterList.clear();
                    }
                });
                buildFilterItem();
                sheet.setAllFilterItem(mAllFilterList);
                sheet.setSelectedList(mSelectedFilterList);
                sheet.show((AppCompatActivity) getContext(), "flight_filter");
            }
        });
    }

    public void showTitle(FlightBookingProperty property) {
        mSearchPanel.initFlightBookingProperty(property);
        mTvTripName.setText(String.format("%s - %s", property.getStartLoc().getCity(), property.getEndLoc().getCity()));
        mTvTripTime.setText(String.format("%s - %s, %s", TimeFormatUtil.getDefaultDateFormat(
                property.getStartDate()), property.getTotalCount() + " " + getResources().getString(R.string.air_seat),
                getResources().getString(R.string.air_seat) + " " + property.getSeatType().getName()));
    }

    public void showFlightTrip(FlightDataDTO data, FlightBookingProperty property) {
        boolean isCombo = property.isRoundTrip()
                && (!property.getStartLoc().getCountryID().equals("VN")
                || !property.getEndLoc().getCountryID().equals("VN"));
        mAdapter.setCombo(isCombo);
        mAdapter.setDepartureWay(true);
        mTvWayType.setVisibility(!isCombo ? VISIBLE : GONE);
        findViewById(R.id.flight_booking_result_act_rl_depart_way).setVisibility(GONE);
        mFlightDTO = data;
        mRclvContent.addItemsAndNotify(data.flightTripList, false);
    }

    public void showArrivalFlightTrip(FlightDataDTO data, FlightTrip trip) {
        mAdapter.setDepartureWay(false);
        mAdapter.setCombo(false);
        mTvWayType.setVisibility(VISIBLE);
        mTvWayType.setText(R.string.arrival_air_ticket_select);
        mFlightDTO = data;
        showDepartWay(trip.getDepartureWay(), trip);
        mRclvContent.addItemsAndNotify(data.flightTripList, false);
    }

    public void animExpand() {
        if (mSearchPanelMode == COLLAPSE_MODE) {
            mSearchPanelMode = EXPAND_MODE;
            mSearchPanel.animate().translationYBy(mSearchPanel.getHeight());
            mRclvContent.animate().translationYBy(mSearchPanel.getHeight());
            mTvWayType.animate().translationYBy(mSearchPanel.getHeight());
        }
    }

    public void animCollapseSearchPanel() {
        if (mSearchPanelMode == EXPAND_MODE) {
            mSearchPanelMode = COLLAPSE_MODE;
            mSearchPanel.animate().translationYBy(-mSearchPanel.getHeight());
            mRclvContent.animate().translationYBy(-mSearchPanel.getHeight());
            mTvWayType.animate().translationYBy(-mSearchPanel.getHeight());
        }
    }

    public void toggleSearchPanel() {
        if (mSearchPanelMode == COLLAPSE_MODE) {
            animExpand();
        } else {
            animCollapseSearchPanel();
        }
    }
    /*public void enableChangeProperty(boolean enable) {
        findViewById(R.id.action_bar_text_right).setVisibility(enable ? VISIBLE : INVISIBLE);
    }*/

    private void buildFilterItem() {
        if (mAllFilterList.isEmpty()) {
            mAllFilterList.addAll(FlightFilterFactory.makeFilter(AppConfig.AirFilterName.START_TIME,
                    mFlightDTO.flightFilterList, mSelectedFilterList));
            mAllFilterList.addAll(FlightFilterFactory.makeFilter(AppConfig.AirFilterName.END_TIME,
                    mFlightDTO.flightFilterList, mSelectedFilterList));
            mAllFilterList.addAll(FlightFilterFactory.makeFilter(AppConfig.AirFilterName.AIR,
                    mFlightDTO.flightFilterList, mSelectedFilterList));
            mAllFilterList.addAll(FlightFilterFactory.makeFilter(AppConfig.AirFilterName.STOP,
                    mFlightDTO.flightFilterList, mSelectedFilterList));
        }
    }

    private void showDepartWay(FlightWay way, FlightTrip trip) {
        findViewById(R.id.flight_booking_result_act_rl_depart_way).setVisibility(VISIBLE);
        findViewById(R.id.flight_booking_result_tv_depart_change).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new ChangeDepartWayCmd());
            }
        });

        int steel = ContextCompat.getColor(getContext(), R.color.steel);
        int dark = ContextCompat.getColor(getContext(), R.color.dark);
        String loc = way.getStartLocation().getCity() + " - " + way.getEndLocation().getCity() + " - ";
        String time = way.getStartTime() + " - " + way.getEndTime();
        SpannableBuilder timeBuilder = new SpannableBuilder();
        timeBuilder.appendText(loc)
                .withSpan(new ForegroundColorSpan(steel))
                .appendText(time)
                .withSpan(new ForegroundColorSpan(dark));

        TextView tvTime = findViewById(R.id.flight_booking_result_tv_depart_time);
        tvTime.setText(timeBuilder.getSpannedText(), TextView.BufferType.SPANNABLE);

        String vendor = trip.getVendor().getName() + " - ";
        String price = MoneyFormatter.defaultFormat(String.valueOf(trip.getPrice())) + " VNĐ";
        SpannableBuilder priceBuilder = new SpannableBuilder();
        priceBuilder.appendText(vendor)
                .withSpan(new ForegroundColorSpan(steel))
                .appendText(price)
                .withSpan(new ForegroundColorSpan(dark));
        TextView tvPrice = findViewById(R.id.flight_booking_result_tv_depart_price);
        tvPrice.setText(priceBuilder.getSpannedText(), TextView.BufferType.SPANNABLE);
    }


    public static class ChangeDepartWayCmd implements ICommand{}
    public static class ChangeActionbarCmd implements ICommand{}
}
