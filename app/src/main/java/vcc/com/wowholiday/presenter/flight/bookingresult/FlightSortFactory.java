package vcc.com.wowholiday.presenter.flight.bookingresult;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.WHApplication;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.model.Sort;
import vcc.com.wowholiday.presenter.dialog.sort.ISelectorType;

/**
 * Created by Pham Hai Quang on 11/16/2019.
 */
public class FlightSortFactory {
    public static ISelectorType[] makeSorts(List<Sort> sortList) {
        List<ISelectorType> results = new ArrayList<>();
        Sort price = findSort(AppConfig.AirSortName.PRICE, sortList);
        if (price != null) {
            price.setOrder(Sort.ASC);
            results.add(create(price, WHApplication.getInstance().getResources().getString(R.string.lowest_price)));
        }

        Sort start = findSort(AppConfig.AirSortName.START_TIME, sortList);
        if (start != null) {
            Sort early = new Sort();
            early.setOrder(Sort.ASC);
            early.setName(AppConfig.AirSortName.START_TIME);
            results.add(create(early, WHApplication.getInstance().getResources().getString(R.string.early_takeoff)));

            Sort latest = new Sort();
            latest.setName(AppConfig.AirSortName.START_TIME);
            latest.setOrder(Sort.DESC);
            results.add(create(latest, WHApplication.getInstance().getResources().getString(R.string.latest_takeoff)));
        }

        Sort end = findSort(AppConfig.AirSortName.END_TIME, sortList);
        if (end != null) {
            Sort early = new Sort();
            early.setOrder(Sort.ASC);
            early.setName(AppConfig.AirSortName.END_TIME);
            results.add(create(early, WHApplication.getInstance().getResources().getString(R.string.early_arrive)));

            /*Sort latest = new Sort();
            latest.setName(AppConfig.AirSortName.END_TIME);
            latest.setOrder(Sort.DESC);
            results.add(create(latest, WHApplication.getInstance().getResources().getString(R.string.latest_arrive)));*/
        }

        Sort dur = findSort(AppConfig.AirSortName.DURATION, sortList);
        if (dur != null) {
            dur.setOrder(Sort.ASC);
            results.add(create(dur, WHApplication.getInstance().getResources().getString(R.string.shortest_dur_fly)));
        }
        return results.toArray(new ISelectorType[results.size()]);
    }

    private static Sort findSort(String name, List<Sort> sortList) {
        for (Sort s : sortList) {
            if (s.getName().equals(name)) {
                return s;
            }
        }
        return null;
    }

    private static ISelectorType create(final Sort sort, final String title) {
        return new ISelectorType() {
            @Override
            public String getTitle() {
                return title;
            }

            @Override
            public int getCode() {
                return sort.getOrder();
            }
        };
    }
}
