package vcc.com.wowholiday.presenter.flight.bookingresult.filter;

/**
 * Created by QuangPH on 2020-01-13.
 */
public class CheckFilterItem extends FlightFilterItem {

    private String mValue;

    public CheckFilterItem(String name, int index, String value) {
        super(name, index);
        mValue = value;
    }

    @Override
    public String getTitle() {
        return mValue;
    }
}
