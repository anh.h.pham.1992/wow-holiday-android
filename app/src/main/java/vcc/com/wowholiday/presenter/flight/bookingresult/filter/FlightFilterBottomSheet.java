package vcc.com.wowholiday.presenter.flight.bookingresult.filter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import common.presenter.SafeClicked;
import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.model.air.FlightFilter;
import vcc.com.wowholiday.presenter.widget.ExpandableLayout;
import vcc.com.wowholiday.presenter.widget.checklist.CheckItemListView;
import vcc.com.wowholiday.presenter.widget.checklist.IChecklistItem;

/**
 * Created by Pham Hai Quang on 10/31/2019.
 */
public class FlightFilterBottomSheet extends WHBottomSheetDialog {

    private CheckItemListView mStartTimeGroupView;
    private CheckItemListView mEndTimeGroupView;
    private CheckItemListView mAirTimeGroupView;
    private CheckItemListView mStopTimeGroupView;

    private List<FlightFilter> mFilterList;
    private List<FlightFilterItem> mSelectedFilterList;
    private List<FlightFilterItem> mAllFilterList;
    private IOnRefreshFilter mRefreshListener;


    public FlightFilterBottomSheet(List<FlightFilter> filterList) {
        mFilterList = filterList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_flight_filter, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initStartTime(view);
        initEndTime(view);
        initAir(view);
        initStop(view);

        view.findViewById(R.id.date_selector_dlg_tv_save).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                dismiss();
            }
        });

        view.findViewById(R.id.date_selector_dlg_tv_refresh).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                for (FlightFilterItem itm : mSelectedFilterList) {
                    itm.setSelected(false);
                    mStartTimeGroupView.notifyItemChanged(itm);
                    mEndTimeGroupView.notifyItemChanged(itm);
                    mAirTimeGroupView.notifyItemChanged(itm);
                    mStopTimeGroupView.notifyItemChanged(itm);
                }
                mSelectedFilterList.clear();
                mRefreshListener.onRefresh();
            }
        });
    }

    public List<FlightFilterItem> getSelectedItemList() {
        return mSelectedFilterList;
    }

    public void setSelectedList(List<FlightFilterItem> selectedList) {
        mSelectedFilterList = selectedList;
    }

    public void setAllFilterItem(List<FlightFilterItem> allFilterItemList) {
        mAllFilterList = allFilterItemList;
    }

    public void setOnRefreshListener(IOnRefreshFilter listener) {
        mRefreshListener = listener;
    }

    private void initStartTime(View container) {
        final ExpandableLayout ll = container.findViewById(R.id.flight_filter_dlg_exp_start_time);

        IChecklistItem[] items = findFilter(AppConfig.AirFilterName.START_TIME);
        if (items == null) {
            ll.setVisibility(View.GONE);
            container.findViewById(R.id.flight_filter_dlg_v_start_time_divider).setVisibility(View.GONE);
        } else {
            mStartTimeGroupView = container.findViewById(R.id.flight_filter_dlg_clv_start_time);
            mStartTimeGroupView.setSelectedListener(new CheckItemListView.ISelectedListener() {
                @Override
                public void onSelected(IChecklistItem item) {
                    mSelectedFilterList.add((FlightFilterItem) item);
                }

                @Override
                public void onUnselected(IChecklistItem item) {
                    mSelectedFilterList.remove(item);
                }
            });
            mStartTimeGroupView.show(items);
            container.findViewById(R.id.flight_filter_dlg_iv_start_time).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    ll.toggle();
                    view.setSelected(!view.isSelected());
                    view.setRotation(view.isSelected() ? 90 : 270);
                }
            });
            ll.setExpand(true);
        }

    }

    private void initEndTime(View container) {
        final ExpandableLayout ll = container.findViewById(R.id.flight_filter_dlg_exp_end_time);
        IChecklistItem[] items = findFilter(AppConfig.AirFilterName.END_TIME);

        if (items == null) {
            ll.setVisibility(View.GONE);
            container.findViewById(R.id.flight_filter_dlg_v_end_time_divider).setVisibility(View.GONE);
        } else {
            mEndTimeGroupView = container.findViewById(R.id.flight_filter_dlg_clv_end_time);
            mEndTimeGroupView.setSelectedListener(new CheckItemListView.ISelectedListener() {
                @Override
                public void onSelected(IChecklistItem item) {
                    mSelectedFilterList.add((FlightFilterItem) item);
                }

                @Override
                public void onUnselected(IChecklistItem item) {
                    mSelectedFilterList.remove(item);
                }
            });
            mEndTimeGroupView.show(items);
            container.findViewById(R.id.flight_filter_dlg_iv_end_time).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    ll.toggle();
                    view.setSelected(!view.isSelected());
                    view.setRotation(view.isSelected() ? 90 : 270);
                }
            });
            ll.setExpand(true);
        }
    }

    private void initAir(View container) {
        final ExpandableLayout ll = container.findViewById(R.id.flight_filter_dlg_exp_air);
        IChecklistItem[] items = findFilter(AppConfig.AirFilterName.AIR);

        if (items == null) {
            ll.setVisibility(View.GONE);
            container.findViewById(R.id.flight_filter_dlg_v_air_divider).setVisibility(View.GONE);
        } else {
            mAirTimeGroupView = container.findViewById(R.id.flight_filter_dlg_clv_air);
            mAirTimeGroupView.setSelectedListener(new CheckItemListView.ISelectedListener() {
                @Override
                public void onSelected(IChecklistItem item) {
                    mSelectedFilterList.add((FlightFilterItem) item);
                }

                @Override
                public void onUnselected(IChecklistItem item) {
                    mSelectedFilterList.remove(item);
                }
            });
            mAirTimeGroupView.show(items);
            container.findViewById(R.id.flight_filter_dlg_iv_air).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    ll.toggle();
                    view.setSelected(!view.isSelected());
                    view.setRotation(view.isSelected() ? 90 : 270);
                }
            });
            ll.setExpand(true);
        }
    }

    private void initStop(View container) {
        final ExpandableLayout ll = container.findViewById(R.id.flight_filter_dlg_exp_stop);
        IChecklistItem[] items = findFilter(AppConfig.AirFilterName.STOP);

        if (items == null) {
            ll.setVisibility(View.GONE);
        } else {
            mStopTimeGroupView = container.findViewById(R.id.flight_filter_dlg_clv_stop);
            mStopTimeGroupView.setSelectedListener(new CheckItemListView.ISelectedListener() {
                @Override
                public void onSelected(IChecklistItem item) {
                    mSelectedFilterList.add((FlightFilterItem) item);
                }

                @Override
                public void onUnselected(IChecklistItem item) {
                    mSelectedFilterList.remove(item);
                }
            });
            mStopTimeGroupView.show(items);
            container.findViewById(R.id.flight_filter_dlg_iv_stop).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    ll.toggle();
                    view.setSelected(!view.isSelected());
                    view.setRotation(view.isSelected() ? 90 : 270);
                }
            });
            ll.setExpand(true);
        }
    }

    private FlightFilterItem[] findFilter(String name) {
        List<FlightFilterItem> filters = new ArrayList<>();
        for (FlightFilterItem itm : mAllFilterList) {
            if (itm.getName().equals(name)) {
                filters.add(itm);
            }
        }
        return filters.toArray(new FlightFilterItem[filters.size()]);
    }


    public interface IOnRefreshFilter {
        void onRefresh();
    }
}
