package vcc.com.wowholiday.presenter.flight.bookingresult.filter;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.model.air.FlightFilter;
import vcc.com.wowholiday.presenter.widget.checklist.IChecklistItem;

/**
 * Created by Pham Hai Quang on 11/15/2019.
 */
public class FlightFilterFactory {
    public static List<FlightFilterItem> makeFilter(String filterName, List<FlightFilter> filterList,
                                              List<FlightFilterItem> selectedFilterList) {

        List<FlightFilterItem> results = new ArrayList<>();
        final FlightFilter filter = findFilter(filterName, filterList);
        if (filter == null) {
            return results;
        }

        if (filter.getType().equals(FlightFilter.RANGE_LIST_TYPE)) {
            for (int i = 0; i < filter.getRangeValues().length; i++) {
                RangeFilterItem item = new RangeFilterItem(filter.getName(), i, filter.getRangeValues()[i]);
                //checkSelected(item, selectedFilterList);
                results.add(item);
            }
        } else if (filter.getType().equals(FlightFilter.CHECKBOX_TYPE)) {
            for (int i = 0; i < filter.getValues().length; i++) {
                CheckFilterItem item = new CheckFilterItem(filter.getName(), i, filter.getValues()[i]);
                //checkSelected(item, selectedFilterList);
                results.add(item);
            }
        }
        return results;
    }

    private static FlightFilter findFilter(String filterName, List<FlightFilter> filterList) {
        for (FlightFilter f : filterList) {
            if (f.getName().equals(filterName)) {
                return f;
            }
        }
        return null;
    }

    private static void checkSelected(FlightFilterItem item, List<FlightFilterItem> selectedFilterList) {
        if (selectedFilterList.isEmpty()) return;
        for (IChecklistItem selected : selectedFilterList) {
            if (item.getId().equals(selected.getId())) {
                item.setSelected(selected.isSelected());
                break;
            }
        }
    }
}
