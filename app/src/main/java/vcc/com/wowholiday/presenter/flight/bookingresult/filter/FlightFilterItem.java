package vcc.com.wowholiday.presenter.flight.bookingresult.filter;

import vcc.com.wowholiday.presenter.widget.checklist.IChecklistItem;

/**
 * Created by QuangPH on 2020-01-13.
 */
public abstract class FlightFilterItem implements IChecklistItem {
    private String mName;
    private int mIndex;
    private boolean isSelected;

    public FlightFilterItem(String name, int index) {
        mName = name;
        mIndex = index;
    }

    @Override
    public String getId() {
        return mName + "_" + mIndex;
    }

    @Override
    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    public String getName() {
        return mName;
    }
}
