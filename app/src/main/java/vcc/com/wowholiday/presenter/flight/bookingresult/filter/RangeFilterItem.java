package vcc.com.wowholiday.presenter.flight.bookingresult.filter;

import vcc.com.wowholiday.model.RangeValue;

/**
 * Created by QuangPH on 2020-01-13.
 */
public class RangeFilterItem extends FlightFilterItem {

    private RangeValue mValue;

    public RangeFilterItem(String name, int index, RangeValue value) {
        super(name, index);
        mValue = value;
    }

    @Override
    public String getTitle() {
        return mValue.getMinValue() + " - " + mValue.getMaxValue();
    }


}
