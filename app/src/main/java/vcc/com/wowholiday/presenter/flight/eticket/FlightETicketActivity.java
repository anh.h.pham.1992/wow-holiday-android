package vcc.com.wowholiday.presenter.flight.eticket;

import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.presenter.mainscreen.MainActivity;

/**
 * Created by QuangPH on 2020-02-06.
 */

@Layout(R.layout.activity_flight_eticket)
public class FlightETicketActivity extends WHActivity<FlightETicketView> {

    public static final String FLIGHT_TRIP_KEY = "FLIGHT_TRIP_KEY";
    public static final String PASSENGER_KEY = "PASSENGER_KEY";

    public static void createParams(Intent itn, FlightTrip trip, ArrayList<Passenger> passengerList) {
        itn.putExtra(FLIGHT_TRIP_KEY, trip);
        itn.putParcelableArrayListExtra(PASSENGER_KEY, passengerList);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        List<Passenger> passengerList = getIntent().getParcelableArrayListExtra(PASSENGER_KEY);
        FlightTrip trip = getIntent().getParcelableExtra(FLIGHT_TRIP_KEY);
        mView.showFlightInfo(trip, passengerList);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FlightETicketView.CloseCmd) {
            Intent itn = new Intent(this, MainActivity.class);
            itn.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(itn);
            finish();
        }
    }
}
