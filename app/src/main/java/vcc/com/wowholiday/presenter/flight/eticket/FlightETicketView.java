package vcc.com.wowholiday.presenter.flight.eticket;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.presenter.flight.FlightLineTicketView;
import vcc.com.wowholiday.presenter.flight.FlightTicketPersonInfo;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.RightLeftTextView;

/**
 * Created by QuangPH on 2020-02-06.
 */
public class FlightETicketView extends BaseRelativeView {

    public FlightETicketView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.action_bar_text_right).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new CloseCmd());
            }
        });
    }

    public void showFlightInfo(FlightTrip trip, List<Passenger> passengerList) {
        FlightLineTicketView lineTicketDepart = findViewById(R.id.flight_e_ticket_act_fltvDepart);
        lineTicketDepart.showWarningInfo(false);
        lineTicketDepart.canShowDetail(false);
        lineTicketDepart.setFlightDirectionTitle(R.string.flight_departure);
        lineTicketDepart.showFlightWayInfo(trip.getDepartureWay());

        LinearLayout llDepart = findViewById(R.id.flight_e_ticket_act_ll_person_depart);
        for (int i = 0; i < passengerList.size(); i++) {
            Passenger p = passengerList.get(i);
            addPassengerView(llDepart, p, true);
            if (i < passengerList.size() - 1) {
                addPassengerDivider(llDepart);
            }
        }

        FlightWay arrival = trip.getArrivalWay();
        if (arrival != null) {
            findViewById(R.id.flight_e_ticket_act_constArrivalContent).setVisibility(VISIBLE);
            FlightLineTicketView lineTicketArrival = findViewById(R.id.flight_e_ticket_act_fltvArrival);
            lineTicketArrival.showWarningInfo(false);
            lineTicketArrival.canShowDetail(false);
            lineTicketArrival.setFlightDirectionTitle(R.string.flight_arrival);
            lineTicketArrival.showFlightWayInfo(arrival);

            LinearLayout llArrival = findViewById(R.id.flight_e_ticket_act_ll_person_arrival);
            for (int i = 0; i < passengerList.size(); i++) {
                Passenger p = passengerList.get(i);
                addPassengerView(llArrival, p, false);
                if (i < passengerList.size() - 1) {
                    addPassengerDivider(llArrival);
                }
            }
        } else {
            findViewById(R.id.flight_e_ticket_act_constArrivalContent).setVisibility(GONE);
        }

        RightLeftTextView tvPrice = findViewById(R.id.flight_e_ticket_act_rltv_price);
        tvPrice.setRightText(MoneyFormatter.defaultFormat(trip.getPrice()) + " " + trip.getCurrencyUnit());
    }

    private void addPassengerView(LinearLayout llContainer, Passenger passenger, boolean isDepart) {
        FlightTicketPersonInfo infoView = new FlightTicketPersonInfo(getContext());
        infoView.showPersonInfo(passenger, isDepart ? FlightTicketPersonInfo.DEPART : FlightTicketPersonInfo.ARRIVAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        llContainer.addView(infoView, params);
    }

    private void addPassengerDivider(LinearLayout llContainer) {
        View v = new View(getContext());
        v.setBackgroundResource(R.drawable.shape_divider_vert);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.divider_size));
        llContainer.addView(v, params);
    }


    public static class CloseCmd implements ICommand{}
}
