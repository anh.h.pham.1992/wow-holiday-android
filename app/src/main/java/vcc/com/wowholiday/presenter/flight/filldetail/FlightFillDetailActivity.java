package vcc.com.wowholiday.presenter.flight.filldetail;

import android.content.Intent;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.TicketContactInfo;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.presenter.contactinfo.ContactInfoActivity;
import vcc.com.wowholiday.presenter.flight.addoninfo.AddonInfoActivity;
import vcc.com.wowholiday.presenter.flight.flightdetail.FlightBookingDetailActivity;
import vcc.com.wowholiday.presenter.flight.passengerdetail.PassengerDetailActivity;

/**
 * Created by Pham Hai Quang on 11/21/2019.
 */

@Layout(R.layout.activity_flight_fill_detail)
public class FlightFillDetailActivity extends WHActivity<FlightFillDetailView> {

    private static final String FLIGHT_TRIP = "FLIGHT_TRIP";
    private static final String FLIGHT_SEARCH_PROPERTY = "FLIGHT_SEARCH_PROPERTY";
    private static final int FILL_CONTACT_RQ = 3245;
    private static final int ADD_PASSENGER_RQ = 3246;

    private FlightTrip mFlightTrip;
    private FlightBookingProperty mFlightBookingProperty;
    private TicketContactInfo mContactInfo;

    public static void createParams(Intent itn, FlightTrip trip, FlightBookingProperty property) {
        itn.putExtra(FLIGHT_TRIP, trip);
        itn.putExtra(FLIGHT_SEARCH_PROPERTY, property);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILL_CONTACT_RQ && resultCode == RESULT_OK) {
            mContactInfo = new TicketContactInfo();
            mContactInfo.setName(data.getStringExtra(ContactInfoActivity.NAME_KEY));
            mContactInfo.setEmail(data.getStringExtra(ContactInfoActivity.EMAIL_KEY));
            mContactInfo.setPhone(data.getStringExtra(ContactInfoActivity.PHONE_KEY));
            mView.showContactInfo(mContactInfo);
        } else if (requestCode == ADD_PASSENGER_RQ && resultCode == RESULT_OK) {
            mView.addPassenger((Passenger) data.getParcelableExtra(PassengerDetailActivity.PASSENGER_KEY));
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mFlightTrip = getIntent().getParcelableExtra(FLIGHT_TRIP);
        mFlightBookingProperty = getIntent().getParcelableExtra(FLIGHT_SEARCH_PROPERTY);
        mView.showFlightTrip(mFlightTrip, mFlightBookingProperty);
        mView.showContactInfo(null);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FlightFillDetailView.FillContactInfoCmd) {
            gotoFillContactDetail(null);
        } else if (command instanceof FlightFillDetailView.EditContactInfoCmd) {
            gotoFillContactDetail(mContactInfo);
        } else if (command instanceof FlightFillDetailView.AddPassengerCmd) {
            gotoPassengerDetail(null);
        } else if (command instanceof FlightFillDetailView.EditPassengerCmd) {
            gotoPassengerDetail(((FlightFillDetailView.EditPassengerCmd) command).passenger);
        } else if (command instanceof FlightFillDetailView.PaymentCmd) {
            Intent itn = new Intent(this, AddonInfoActivity.class);
            AddonInfoActivity.createParams(itn, mFlightTrip,
                    (ArrayList<Passenger>) ((FlightFillDetailView.PaymentCmd) command).passengerList);
            startActivity(itn);
        } else if (command instanceof FlightFillDetailView.SeeDetailCmd) {
            gotoFlightDetail(((FlightFillDetailView.SeeDetailCmd) command).way);
        }
    }

    private void gotoFillContactDetail(TicketContactInfo info) {
        Intent itn = new Intent(this, ContactInfoActivity.class);
        if (info == null) {
            ContactInfoActivity.createParams(itn, null, null, null,
                    getResources().getString(R.string.flight_contact_description), null);
        } else {
            ContactInfoActivity.createParams(itn, info.getName(), info.getPhone(), info.getEmail(),
                    getResources().getString(R.string.flight_contact_description), null);
        }

        startActivityForResult(itn, FILL_CONTACT_RQ);
    }

    private void gotoPassengerDetail(Passenger passenger) {
        Intent itn = new Intent(this, PassengerDetailActivity.class);
        PassengerDetailActivity.createParams(itn, passenger, false);
        startActivityForResult(itn, ADD_PASSENGER_RQ);
    }

    private void gotoFlightDetail(FlightWay way) {
        Intent itn = new Intent(this, FlightBookingDetailActivity.class);
        FlightBookingDetailActivity.createParams(itn, way, mFlightTrip, mFlightBookingProperty);
        startActivity(itn);
    }
}
