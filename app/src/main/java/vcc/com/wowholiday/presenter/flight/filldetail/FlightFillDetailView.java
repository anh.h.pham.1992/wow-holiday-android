package vcc.com.wowholiday.presenter.flight.filldetail;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseCoordinatorView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.TicketContactInfo;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.presenter.flight.FlightLineTicketView;
import vcc.com.wowholiday.presenter.flight.PriceDetailLayout;
import vcc.com.wowholiday.presenter.widget.DashgapDividerView;
import vcc.com.wowholiday.presenter.widget.RightLeftTextView;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;

/**
 * Created by Pham Hai Quang on 11/21/2019.
 */
public class FlightFillDetailView extends BaseCoordinatorView {

    private PriceDetailLayout mPriceSheet;
    private LinearLayout mLlPassenger;
    private List<PaxInfoView> mPaxInfoViewList = new ArrayList<>();
    private SparseArray<View> mPassengerViews = new SparseArray<>();
    private SparseArray<Passenger> mPassengers = new SparseArray<>();
    private int mCurrentPaxInfoView = -1;

    public FlightFillDetailView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mLlPassenger = findViewById(R.id.flight_fill_detail_act_ll_passenger_info);
        findViewById(R.id.flight_fill_detail_act_rltv_fill_contact_info).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new FillContactInfoCmd());
            }
        });

        findViewById(R.id.flight_fill_detail_act_tv_edit).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new EditContactInfoCmd());
            }
        });

        mPriceSheet = findViewById(R.id.flight_fill_detail_act_price_sheet);
        mPriceSheet.setSelectedListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                if (checkPassengerInfo() && checkContactInfo()) {
                    PaymentCmd cmd = new PaymentCmd();
                    cmd.passengerList = convertToList(mPassengers);
                    mPresenter.executeCommand(cmd);
                }
            }
        });
    }

    public void showFlightTrip(FlightTrip trip, FlightBookingProperty property) {
        showFlight(trip, property);
        addPaxInfos(trip.getPaxInfos());
        mPriceSheet.setFlightTrip(trip);
    }

    public void addPassenger(Passenger passenger) {
        passenger.setPaxInfo(mPaxInfoViewList.get(mCurrentPaxInfoView).info);
        mPassengers.put(mCurrentPaxInfoView, passenger);
        View item = mPassengerViews.get(mCurrentPaxInfoView);
        if (item == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            item = inflater.inflate(R.layout.layout_passenger_item, mLlPassenger, false);
            item.setTag(String.valueOf(mCurrentPaxInfoView));
            item.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    int index = Integer.valueOf((String) view.getTag());
                    EditPassengerCmd cmd = new EditPassengerCmd();
                    cmd.passenger = mPassengers.get(index);
                    mPresenter.executeCommand(cmd);
                }
            });
            mLlPassenger.removeViewAt(mCurrentPaxInfoView);
            mLlPassenger.addView(item, mCurrentPaxInfoView);
            mPassengerViews.put(mCurrentPaxInfoView, item);
            mPaxInfoViewList.get(mCurrentPaxInfoView).isFilled = true;
        }
        TextView tvName = item.findViewById(R.id.passenger_item_layout_tv_name);
        String gender = Passenger.MR.equals(passenger.getGender())
                ? getResources().getString(R.string.mr) : getResources().getString(R.string.mrs);
        tvName.setText(String.format("%s %s %s", gender, passenger.getFistName(), passenger.getLastName()));

        RightLeftTextView tvBirthday = item.findViewById(R.id.passenger_item_layout_rltv_birthday);
        tvBirthday.setRightText(passenger.getBirthday());

        RightLeftTextView tvNation = item.findViewById(R.id.passenger_item_layout_rltv_nation);
        tvNation.setRightText(passenger.getNation());

        RightLeftTextView tvPassport = item.findViewById(R.id.passenger_item_layout_rltv_passport);
        tvPassport.setRightText(passenger.getPassport());

        RightLeftTextView tvExpired = item.findViewById(R.id.passenger_item_layout_rltv_passport_expired);
        tvExpired.setRightText(passenger.getPassprortExpired());
    }

    public void showContactInfo(TicketContactInfo info) {
        findViewById(R.id.flight_fill_detail_act_cst_contact_info).setVisibility(info == null ? GONE : VISIBLE);
        findViewById(R.id.flight_fill_detail_act_rltv_fill_contact_info).setVisibility(info == null ? VISIBLE : GONE);
        if (info != null) {
            TextView tvName = findViewById(R.id.flight_fill_detail_act_tv_contact_name);
            tvName.setText(info.getName());

            TextView tvPhone = findViewById(R.id.flight_fill_detail_act_tv_contact_phone);
            tvPhone.setText(info.getPhone());

            TextView tvEmail = findViewById(R.id.flight_fill_detail_act_tv_contact_email);
            tvEmail.setText(info.getEmail());
        }
     }

    private void showFlight(FlightTrip trip, FlightBookingProperty property) {
        LinearLayout llContainer = findViewById(R.id.flight_fill_detail_act_ll_flight);
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        addWay(llContainer, layoutInflater, trip.getDepartureWay(), R.string.flight_departure);
        if (property.isRoundTrip()) {
            DashgapDividerView divider = new DashgapDividerView(getContext());
            divider.setOrientation(DashgapDividerView.ORIENTATION_HORIZONTAL);
            divider.setColor(ContextCompat.getColor(getContext(), R.color.dash_line_divider));
            divider.setDashGap(getResources().getDimensionPixelOffset(R.dimen.dimen_8),
                    getResources().getDimensionPixelOffset(R.dimen.dimen_8));
            divider.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.divider_size));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    getResources().getDimensionPixelSize(R.dimen.divider_size));
            params.topMargin = getResources().getDimensionPixelOffset(R.dimen.dimen_16);
            params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.dimen_16);
            llContainer.addView(divider, params);

            addWay(llContainer, layoutInflater, trip.getArrivalWay(), R.string.flight_arrival);
        }
    }

    private void addWay(LinearLayout container, LayoutInflater inflater, final FlightWay way, @StringRes int direction) {
        FlightLineTicketView line = new FlightLineTicketView(getContext());
        line.setFlightDirectionTitle(direction);
        line.showFlightWayInfo(way);
        line.setSeeDetailListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                SeeDetailCmd cmd = new SeeDetailCmd();
                cmd.way = way;
                mPresenter.executeCommand(cmd);
            }
        });
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        container.addView(line, params);
    }

    private void addPaxInfos(PaxInfo[] paxInfos) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        for (PaxInfo paxInfo : paxInfos) {
            addPaxInfoView(mLlPassenger, layoutInflater, paxInfo);
        }
    }

    private void addPaxInfoView(final LinearLayout llContainer, LayoutInflater inflater, PaxInfo paxInfo) {
        for (int i = 0; i < paxInfo.getQuantity(); i++) {
            View item = inflater.inflate(
                    R.layout.layout_add_flight_passenger, llContainer, false);
            TextViewWithIcon tvInfo = item.findViewById(R.id.flight_add_passenger_layout_tvic_info);
            String type = paxInfo.getType();
            if (type.equals(PaxInfo.ADULT)) {
                tvInfo.setText(getResources().getString(R.string.adult) + " " + (i + 1));
            } else if (type.equals(PaxInfo.CHILD)) {
                tvInfo.setText(getResources().getString(R.string.child) + " " + (i + 1));
            } else if (type.equals(PaxInfo.NEW_BORN)) {
                tvInfo.setText(getResources().getString(R.string.new_born) + " " + (i + 1));
            }

            llContainer.addView(item);

            PaxInfoView paxInfoView = new PaxInfoView();
            paxInfoView.info = paxInfo;
            paxInfoView.index = i + 1;
            paxInfoView.view = item;
            paxInfoView.viewPosition = llContainer.getChildCount() - 1;
            mPaxInfoViewList.add(paxInfoView);
            tvInfo.setTag("" + (llContainer.getChildCount() - 1));
            tvInfo.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    mCurrentPaxInfoView = Integer.valueOf((String) view.getTag());
                    mPresenter.executeCommand(new AddPassengerCmd());
                }
            });
        }
    }

    private boolean checkPassengerInfo() {
        boolean filledAll = true;
        for (PaxInfoView itm : mPaxInfoViewList) {
            if (!itm.isFilled) {
                filledAll = false;
                itm.view.setSelected(true);
                itm.view.findViewById(R.id.flight_add_passenger_layout_tv_error).setVisibility(VISIBLE);
            }
        }
        return filledAll;
    }

    private boolean checkContactInfo() {
        return findViewById(R.id.flight_fill_detail_act_cst_contact_info).getVisibility() == VISIBLE;
    }

    private static <C> List<C> convertToList(SparseArray<C> sparseArray) {
        if (sparseArray == null) return null;
        List<C> arrayList = new ArrayList<C>(sparseArray.size());

        for (int i = 0; i < sparseArray.size(); i++)
            arrayList.add(sparseArray.valueAt(i));
        return arrayList;
    }

    private static class PaxInfoView {
        PaxInfo info;
        int index;
        int viewPosition;
        View view;
        boolean isFilled = false;
    }

    public static class FillContactInfoCmd implements ICommand{}
    public static class EditContactInfoCmd implements ICommand{}
    public static class AddPassengerCmd implements ICommand {}
    public static class EditPassengerCmd implements ICommand {
        public Passenger passenger;
    }
    public static class PaymentCmd implements ICommand {
        List<Passenger> passengerList;
    }

    public static class SeeDetailCmd implements ICommand {
        public FlightWay way;
    }
}
