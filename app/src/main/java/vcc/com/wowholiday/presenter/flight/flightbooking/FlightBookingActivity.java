package vcc.com.wowholiday.presenter.flight.flightbooking;

import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.presenter.flight.FlightSearchPanel;
import vcc.com.wowholiday.presenter.flight.bookingresult.FlightBookingResultActivity;
import vcc.com.wowholiday.presenter.flight.locationsearch.FlightLocationSearchActivity;

/**
 * Created by Pham Hai Quang on 10/29/2019.
 */

@Layout(R.layout.activity_flight_booking)
public class FlightBookingActivity extends WHActivity<FlightBookingView> {

    private static final int START_LOC_SEARCH_RQ = 2345;
    private static final int END_LOC_SEARCH_RQ = 2346;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == START_LOC_SEARCH_RQ && resultCode == RESULT_OK) {
            Location location = data.getParcelableExtra(FlightLocationSearchActivity.LOC_SELECTED);
            mView.showStartLocation(location);
        } else if (requestCode == END_LOC_SEARCH_RQ && resultCode == RESULT_OK) {
            Location location = data.getParcelableExtra(FlightLocationSearchActivity.LOC_SELECTED);
            mView.showEndLocation(location);
        }
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FlightSearchPanel.StartLocationCmd) {
            gotoLocationScreen(START_LOC_SEARCH_RQ);
        } else if (command instanceof FlightSearchPanel.EndLocationCmd) {
            gotoLocationScreen(END_LOC_SEARCH_RQ);
        } else if (command instanceof FlightSearchPanel.SearchCmd) {
            gotoResultScreen(((FlightSearchPanel.SearchCmd) command).property);
        }
    }

    private void gotoLocationScreen(int requestCode) {
        Intent itn = new Intent(this, FlightLocationSearchActivity.class);
        startActivityForResult(itn, requestCode);
    }

    private void gotoResultScreen(FlightBookingProperty property) {
        Intent itn = new Intent(this, FlightBookingResultActivity.class);
        FlightBookingResultActivity.createParam(itn, property, FlightBookingResultActivity.DEPARTURE);
        startActivity(itn);
    }
}
