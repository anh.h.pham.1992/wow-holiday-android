package vcc.com.wowholiday.presenter.flight.flightbooking;

import android.content.Context;
import android.util.AttributeSet;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.presenter.flight.FlightSearchPanel;

/**
 * Created by Pham Hai Quang on 10/29/2019.
 */
public class FlightBookingView extends BaseRelativeView {

    private FlightSearchPanel mPanel;

    public FlightBookingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean hasMVPChildren() {
        return true;
    }

    @Override
    public void onInitView() {
        mPanel = findViewById(R.id.flight_booking_act_search_panel);
    }

    public void showStartLocation(Location location) {
        mPanel.setStartLocation(location);
    }

    public void showEndLocation(Location location) {
        mPanel.setEndLocation(location);
    }
}
