package vcc.com.wowholiday.presenter.flight.flightdetail;

import android.content.Intent;
import android.os.Parcelable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Flight;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.presenter.flight.filldetail.FlightFillDetailActivity;
import vcc.com.wowholiday.presenter.flight.summary.FlightSummaryActivity;

/**
 * Created by Pham Hai Quang on 11/5/2019.
 */

@Layout(R.layout.activity_flight_booking_detail)
public class FlightBookingDetailActivity extends WHActivity<FlightBookingDetailView> {

    private static final String TRANSIT = "TRANSIT";
    private static final String TRIP = "TRIP";
    private static final String SEARCH_PROPERTY = "SEARCH_PROPERTY";

    private FlightTrip mFlightTrip;
    private FlightBookingProperty mSearchProperty;

    public static void createParams(Intent itn, FlightWay flightWay, FlightTrip trip, FlightBookingProperty property) {
        itn.putExtra(TRANSIT, flightWay);
        itn.putExtra(TRIP, trip);
        itn.putExtra(SEARCH_PROPERTY, property);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();

        FlightWay flightWay = getIntent().getParcelableExtra(TRANSIT);
        mView.showFlights(flightWay);

        mFlightTrip = getIntent().getParcelableExtra(TRIP);
        mView.showTripInfo(mFlightTrip);

        mSearchProperty = getIntent().getParcelableExtra(SEARCH_PROPERTY);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FlightBookingDetailView.SelectCmd) {
            gotoFillDetail();
        }
    }

    /*private void gotoSummary() {
        Intent itn = new Intent(this, FlightSummaryActivity.class);
        FlightSummaryActivity.createParams(itn, mFlightTrip);
        startActivity(itn);
    }*/

    private void gotoFillDetail() {
        Intent itn = new Intent(this, FlightFillDetailActivity.class);
        FlightFillDetailActivity.createParams(itn, mFlightTrip, mSearchProperty);
        startActivity(itn);
    }
}
