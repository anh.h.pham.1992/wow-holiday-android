package vcc.com.wowholiday.presenter.flight.flightdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Flight;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.RightLeftTextView;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 11/5/2019.
 */
public class FlightBookingDetailView extends BaseRelativeView {

    private LayoutInflater mInflater = LayoutInflater.from(getContext());

    public FlightBookingDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.flight_booking_detail_act_tv_select).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new SelectCmd());
            }
        });
    }

    public void showFlights(FlightWay flightWay) {
        TextView tvTime = findViewById(R.id.flight_booking_detail_act_tv_trip_time);
        //int[] dur = TimeFormatUtil.getHourDuration(flightWay.getDuration());
        int[] dur = flightWay.getDuration();
        tvTime.setText(String.format("%s - %s - %s", flightWay.getStartLocation().getID(),
                flightWay.getEndLocation().getID(),
                getResources().getString(R.string.air_duration_format_2, dur[0], dur[1])));
        LinearLayout llTransitContainer = findViewById(R.id.flight_booking_detail_act_ll_transit);
        for (Flight transit : flightWay.getTransits()) {
            addTransit(llTransitContainer, transit);
        }
    }

    public void showTripInfo(FlightTrip trip) {
        TextView tvName = findViewById(R.id.flight_booking_detail_act_tv_trip_name);
        tvName.setText(trip.getVendor().getName());

        showFareDetail(trip.getPaxInfos());
        RightLeftTextView tv = findViewById(R.id.flight_booking_detail_act_rltv_total);
        tv.setRightText(MoneyFormatter.defaultFormat(trip.getPrice()) + " " + trip.getCurrencyUnit());

        TextView tvTotal = findViewById(R.id.flight_booking_detail_act_tv_total);
        tvTotal.setText(String.format("%s %s", MoneyFormatter.defaultFormat(trip.getPrice()), trip.getCurrencyUnit()));
    }

    private void addTransit(LinearLayout transistContainer, Flight transit) {
        View view = mInflater.inflate(R.layout.layout_flight_line, transistContainer, false);
        TextView tvPlaneName = view.findViewById(R.id.flight_line_layout_tv_plane_name);
        tvPlaneName.setText(transit.getVendors().getName());

        TextView tvPlaneCode = view.findViewById(R.id.flight_line_layout_tv_plane_code);
        tvPlaneCode.setText(transit.getVendors().getCode());

        TextView tvStartPoint = view.findViewById(R.id.flight_line_layout_tv_start_point);
        tvStartPoint.setText(String.format("%s(%s)", transit.getStartPoint().getCity(), transit.getStartPoint().getID()));

        TextView tvStartAirport = view.findViewById(R.id.flight_line_layout_tv_start_airport);
        tvStartAirport.setText(transit.getStartPoint().getName());

        TextView tvStartTime = view.findViewById(R.id.flight_line_layout_tv_start_time);
        tvStartTime.setText(TimeFormatUtil.getFullDateFormatFromISO8601UTC(transit.getDateInfo().getStartDate()));

        TextView tvDur = view.findViewById(R.id.flight_line_layout_tv_dur);
        int[]dur = TimeFormatUtil.getHourDuration(transit.getJourneyDuration());
        tvDur.setText(getResources().getString(R.string.flight_dur_format, dur[0], dur[1]));

        TextView tvEndPoint = view.findViewById(R.id.flight_line_layout_tv_end_point);
        tvEndPoint.setText(String.format("%s(%s)", transit.getEndPoint().getCity(), transit.getEndPoint().getID()));

        TextView tvEndAirport = view.findViewById(R.id.flight_line_layout_tv_end_airport);
        tvEndAirport.setText(transit.getEndPoint().getName());

        TextView tvEndTime = view.findViewById(R.id.flight_line_layout_tv_end_time);
        tvEndTime.setText(TimeFormatUtil.getFullDateFormatFromISO8601UTC(transit.getDateInfo().getEndDate()));
        transistContainer.addView(view);
    }

    private void showFareDetail(PaxInfo[] paxInfos) {
        LinearLayout llContainer = findViewById(R.id.flight_booking_detail_act_ll_fare);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (int i = 0; i < paxInfos.length; i++) {
            PaxInfo paxInfo = paxInfos[i];
            RightLeftTextView itemView = (RightLeftTextView)
                    inflater.inflate(R.layout.layout_flight_fare_item, llContainer, false);
            itemView.setRightText(MoneyFormatter.defaultFormat(paxInfo.getAmount()) + " " + paxInfo.getCurrencyCode());
            String left = "";
            if (paxInfo.getType().equals(PaxInfo.ADULT)) {
                left = getResources().getString(R.string.adult);
            } else if (paxInfo.getType().equals(PaxInfo.CHILD)) {
                left = getResources().getString(R.string.child);
            } else if (paxInfo.getType().equals(PaxInfo.NEW_BORN)) {
                left = getResources().getString(R.string.new_born);
            }
            itemView.setLeftText(left + " (x" + paxInfo.getQuantity() + ")");
            llContainer.addView(itemView);

            if (i < paxInfos.length - 1) {
                View divider = new View(getContext());
                divider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.light_grey));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                        getResources().getDimensionPixelSize(R.dimen.divider_size));
                llContainer.addView(divider, params);
            }
        }
    }


    public static class SelectCmd implements ICommand {}
}
