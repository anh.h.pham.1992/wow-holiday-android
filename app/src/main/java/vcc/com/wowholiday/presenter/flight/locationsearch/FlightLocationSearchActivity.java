package vcc.com.wowholiday.presenter.flight.locationsearch;

import android.content.Intent;

import java.util.List;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.GetFlightLocationAction;
import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */

@Layout(R.layout.activity_flight_location_search)
public class FlightLocationSearchActivity extends WHActivity<FlightLocationSearchView> {

    public static final String LOC_SELECTED = "LOC_SELECTED";

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FlightLocationSearchView.SearchCmd) {
            callSearchSuggestion(((FlightLocationSearchView.SearchCmd) command).keyword);
        } else if (command instanceof FlightLocationSearchAdapter.LocationSelectedCmd) {
            selectLocation(((FlightLocationSearchAdapter.LocationSelectedCmd) command).location);
        }
    }

    private void callSearchSuggestion(String keyword) {
        GetFlightLocationAction.RV rq = new GetFlightLocationAction.RV();
        rq.city = keyword;
        mActionManager.executeAction(new GetFlightLocationAction(), rq, new WHApiActionCallback<List<Location>>(this){
            @Override
            public void onSuccess(List<Location> responseValue) {
                super.onSuccess(responseValue);
                mView.showSuggestion(responseValue);
            }
        }, new ThirdPartyScheduler());
    }

    private void selectLocation(Location location) {
        back(location);
    }

    private void back(Location location) {
        Intent itn = new Intent();
        itn.putExtra(LOC_SELECTED, location);
        setResult(RESULT_OK, itn);
        finish();
    }
}
