package vcc.com.wowholiday.presenter.flight.locationsearch;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class FlightLocationSearchAdapter extends BaseRclvAdapter {
    private IPresenter mPresenter;

    public FlightLocationSearchAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_flight_location_search;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new LocVH(itemView);
    }


    private class LocVH extends BaseRclvHolder<Location> {

        TextView tvName;
        TextView tvAddr;

        public LocVH(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.flight_location_search_itm_tv_name);
            tvAddr = itemView.findViewById(R.id.flight_location_search_itm_tv_address);
            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    LocationSelectedCmd cmd = new LocationSelectedCmd();
                    cmd.location = (Location) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(Location vhData) {
            super.onBind(vhData);
            tvName.setText(vhData.getCity());
            tvAddr.setText(vhData.getAddress());
        }
    }


    public static class LocationSelectedCmd implements ICommand {
        public Location location;
    }
}
