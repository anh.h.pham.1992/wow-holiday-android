package vcc.com.wowholiday.presenter.flight.locationsearch;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */
public class FlightLocationSearchView extends BaseLinearView {

    private final int TIME_DELAY_HANDLER_USER_TYPING = 50;

    private EditText mEdtSearch;
    private FlightLocationSearchAdapter mAdapter;

    private Runnable mSearchRunnable = new Runnable() {
        @Override
        public void run() {
            SearchCmd cmd = new SearchCmd();
            cmd.keyword =  mEdtSearch.getText().toString().trim();
            mPresenter.executeCommand(cmd);
        }
    };

    public FlightLocationSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(mSearchRunnable);
    }

    @Override
    public void onInitView() {
        RecyclerView rclvSugesstion = findViewById(R.id.flight_location_search_act_rclv_suggestion);
        rclvSugesstion.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new FlightLocationSearchAdapter(mPresenter);
        rclvSugesstion.setAdapter(mAdapter);

        mEdtSearch  = findViewById(R.id.flight_location_search_act_tv_search);
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                removeCallbacks(mSearchRunnable);
                String key = mEdtSearch.getText().toString().trim();
                if (TextUtils.isEmpty(key) || key.length() < 3) {
                    return;
                }
                postDelayed(mSearchRunnable, TIME_DELAY_HANDLER_USER_TYPING);
            }
        });
    }

    public void showSuggestion(List<Location> locations) {
        mAdapter.reset(locations);
    }


    public static class SearchCmd implements ICommand {
        public String keyword;
    }
}
