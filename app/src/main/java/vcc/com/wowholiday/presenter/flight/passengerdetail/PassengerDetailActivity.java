package vcc.com.wowholiday.presenter.flight.passengerdetail;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Passenger;

/**
 * Created by Pham Hai Quang on 11/6/2019.
 */

@Layout(R.layout.activity_flight_passenger_detail)
public class PassengerDetailActivity extends WHActivity<PassengerDetailView> {

    public static final String PASSENGER_KEY = "PASSENGER_KEY";
    public static final String DOMESTIC_KEY = "DOMESTIC_KEY";

    public static void createParams(Intent itn, Passenger passenger, boolean isDomestic) {
        if (passenger != null) {
            itn.putExtra(PASSENGER_KEY, passenger);
        }
        itn.putExtra(DOMESTIC_KEY, isDomestic);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        Intent itn = getIntent();
        mView.showDomesticFlight(itn.getBooleanExtra(DOMESTIC_KEY, false));
        mView.showPassenger((Passenger) itn.getParcelableExtra(PASSENGER_KEY));
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof PassengerDetailView.ConfirmCmd) {
            Intent itn = new Intent();
            itn.putExtra(PASSENGER_KEY, ((PassengerDetailView.ConfirmCmd) command).passenger);
            setResult(RESULT_OK, itn);
            finish();
        } else if (command instanceof PassengerDetailView.CancelCmd) {
            finish();
        }
    }
}
