package vcc.com.wowholiday.presenter.flight.passengerdetail;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.Passenger;
import vcc.com.wowholiday.presenter.editprofile.DatePickerBottomSheet;
import vcc.com.wowholiday.presenter.widget.RadioRightText;
import vcc.com.wowholiday.presenter.widget.RightErrorTextInputLayout;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 11/6/2019.
 */
public class PassengerDetailView extends BaseLinearView {

    private RadioRightText mVRadioMr;
    private RadioRightText mVRadioMrs;
    private RightErrorTextInputLayout mTilFirstName;
    private RightErrorTextInputLayout mTilLastName;
    private TextView mTvBirthday;
    private TextView mTvNation;
    private RightErrorTextInputLayout mTilPassport;
    private TextView mTvPassportOfNation;
    private TextView mTvPassportExpired;
    private TextInputLayout mTilBirthday;
    private TextInputLayout mTilNation;
    private TextInputLayout mTilPassportOfNation;
    private TextInputLayout mTilPassportExpired;

    private Passenger mPassenger;
    private boolean isDomestic;

    public PassengerDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mVRadioMr = findViewById(R.id.flight_passenger_detail_act_rdo_mr);
        mVRadioMrs = findViewById(R.id.flight_passenger_detail_act_rdo_mrs);
        mTilFirstName = findViewById(R.id.flight_passenger_detail_act_til_first_name);
        mTilLastName = findViewById(R.id.flight_passenger_detail_act_til_last_name);
        mTvBirthday = findViewById(R.id.flight_passenger_detail_act_tv_birthday);
        mTvNation = findViewById(R.id.flight_passenger_detail_act_tv_nation);
        mTilPassport = findViewById(R.id.flight_passenger_detail_act_til_passport);
        mTvPassportOfNation = findViewById(R.id.flight_passenger_detail_act_tv_passport_of_nation);
        mTvPassportExpired = findViewById(R.id.flight_passenger_detail_act_tv_expired_date);
        mTilBirthday = findViewById(R.id.flight_passenger_detail_act_til_birthday);
        mTilNation = findViewById(R.id.flight_passenger_detail_act_til_nation);
        mTilPassportOfNation = findViewById(R.id.flight_passenger_detail_act_til_passport_of_nation);
        mTilPassportExpired = findViewById(R.id.flight_passenger_detail_act_til_expired_date);

        mVRadioMr.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!v.isSelected()) {
                    toggleGender(true);
                }
            }
        });
        mVRadioMrs.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!v.isSelected()) {
                    toggleGender(false);
                }
            }
        });

        findViewById(R.id.flight_passenger_detail_act_rl_birthday).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                showDataPicker(mTvBirthday);
            }
        });

        findViewById(R.id.flight_passenger_detail_act_rl_expired_date).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                showDataPicker(mTvPassportExpired);
            }
        });

        findViewById(R.id.flight_passenger_detail_act_btn_confirm).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                if (valid()) {
                    mPassenger.setGender(mVRadioMr.isChecked() ? Passenger.MR : Passenger.MRS);
                    mPassenger.setFistName(mTilFirstName.getEditText().getText().toString().trim());
                    mPassenger.setLastName(mTilLastName.getEditText().getText().toString().trim());
                    mPassenger.setBirthday(mTvBirthday.getText().toString());
                    mPassenger.setNation(mTvNation.getText().toString());
                    mPassenger.setPassport(mTilPassport.getEditText().getText().toString().trim());
                    mPassenger.setPassportOfNation(mTvPassportOfNation.getText().toString());
                    mPassenger.setPassprortExpired(mTvPassportExpired.getText().toString());
                    ConfirmCmd cmd = new ConfirmCmd();
                    cmd.passenger = mPassenger;
                    mPresenter.executeCommand(cmd);
                }
            }
        });

        findViewById(R.id.action_bar_text_right).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new CancelCmd());
            }
        });
    }

    public void showDomesticFlight(boolean isDomestic) {
        this.isDomestic = isDomestic;
        if (isDomestic) {
            findViewById(R.id.flight_passenger_detail_act_rl_nation).setVisibility(View.GONE);
            mTilPassport.setVisibility(View.GONE);
            findViewById(R.id.flight_passenger_detail_act_rl_passport_of_nation).setVisibility(View.GONE);
            findViewById(R.id.flight_passenger_detail_act_rl_expired_date).setVisibility(View.GONE);
        }
    }

    public void showPassenger(Passenger passenger) {
        if (passenger != null) {
            mPassenger = passenger;
        } else {
            mPassenger = new Passenger();
        }

        toggleGender(TextUtils.isEmpty(mPassenger.getGender())
                || mPassenger.getGender().equals(Passenger.MR));
        mTilFirstName.getEditText().setText(mPassenger.getFistName());
        mTilLastName.getEditText().setText(mPassenger.getLastName());
        mTvBirthday.setText(mPassenger.getBirthday());
        //mTvNation.setText(mPassenger.getNation());
        mTilPassport.getEditText().setText(mPassenger.getPassport());
        //mTvPassportOfNation.setText(mPassenger.getPassportOfNation());
        mTvPassportExpired.setText(mPassenger.getPassprortExpired());
    }

    private void toggleGender(boolean isMale) {
        mVRadioMr.setSelected(isMale);
        mVRadioMrs.setSelected(!isMale);
    }

    private void showDataPicker(final TextView anchorView) {
        Calendar cal = Calendar.getInstance();
        if (!TextUtils.isEmpty(anchorView.getText())) {
            cal.setTimeInMillis(TimeFormatUtil.getMillisFromDefaultDateFormat(anchorView.getText().toString()));
        }
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, month);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                anchorView.setText(TimeFormatUtil.getDefaultDateFormat(cal));
            }
        }, year, month, day);
        dialog.show();
    }

    private boolean valid() {
        String error = getResources().getString(R.string.must_fill_info);
        if (TextUtils.isEmpty(mTilFirstName.getEditText().getText().toString().trim())) {
            mTilFirstName.setError(error);
            return false;
        } else if (TextUtils.isEmpty(mTilLastName.getEditText().getText().toString().trim())) {
            mTilLastName.setError(error);
            return false;
        } else if (TextUtils.isEmpty(mTvBirthday.getText().toString())){
            mTilBirthday.setError(error);
            return false;
        }

        if (!isDomestic) {
            if (TextUtils.isEmpty(mTvNation.getText().toString())) {
                mTilNation.setError(error);
                return false;
            } else if (TextUtils.isEmpty(mTilPassport.getEditText().getText().toString())) {
                mTilPassport.setError(error);
                return false;
            } else if (TextUtils.isEmpty(mTvPassportOfNation.getText().toString())) {
                mTilPassportOfNation.setError(error);
                return false;
            } else if (TextUtils.isEmpty(mTvPassportExpired.getText().toString())) {
                mTilPassportExpired.setError(error);
                return false;
            }
        }

        return true;
    }

    public static class ConfirmCmd implements ICommand {
        public Passenger passenger;
    }

    public static class CancelCmd implements ICommand{}
}
