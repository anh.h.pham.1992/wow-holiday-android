package vcc.com.wowholiday.presenter.flight.summary;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.presenter.flight.filldetail.FlightFillDetailActivity;
import vcc.com.wowholiday.presenter.flight.flightdetail.FlightBookingDetailActivity;

/**
 * Created by Pham Hai Quang on 11/18/2019.
 */

@Layout(R.layout.activity_flight_summary)
public class FlightSummaryActivity extends WHActivity<FlightSummaryView> {

    private static final String FLIGHT_TRIP = "FLIGHT_TRIP";
    private static final String FLIGHT_SEARCH_PROPERTY = "FLIGHT_SEARCH_PROPERTY";

    private FlightTrip mFlightTrip;
    private FlightBookingProperty mSearchProperty;

    public static void createParams(Intent itn, FlightTrip trip, FlightBookingProperty property) {
        itn.putExtra(FLIGHT_TRIP, trip);
        itn.putExtra(FLIGHT_SEARCH_PROPERTY, property);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mFlightTrip = getIntent().getParcelableExtra(FLIGHT_TRIP);
        mSearchProperty = getIntent().getParcelableExtra(FLIGHT_SEARCH_PROPERTY);
        mView.showFlightWay(mFlightTrip, mSearchProperty);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FlightSummaryView.SelectTripCmd) {
            gotoFillDetail();
        } else if (command instanceof FlightSummaryView.SeeDetailCmd) {
            gotoDetailFlight(((FlightSummaryView.SeeDetailCmd) command).way);
        }
    }

    private void gotoFillDetail() {
        Intent itn = new Intent(this, FlightFillDetailActivity.class);
        FlightFillDetailActivity.createParams(itn, mFlightTrip, mSearchProperty);
        startActivity(itn);
    }

    private void gotoDetailFlight(FlightWay way) {
        Intent itn = new Intent(this, FlightBookingDetailActivity.class);
        FlightBookingDetailActivity.createParams(itn, way, mFlightTrip, mSearchProperty);
        startActivity(itn);
    }
}
