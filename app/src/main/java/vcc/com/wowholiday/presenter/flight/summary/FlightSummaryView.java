package vcc.com.wowholiday.presenter.flight.summary;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseCoordinatorView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.RightLeftTextView;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 11/18/2019.
 */
public class FlightSummaryView extends BaseCoordinatorView {

    private NestedScrollView mNscrvBottomSheet;

    public FlightSummaryView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mNscrvBottomSheet = findViewById(R.id.flight_summary_act_bottom_sheet);
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(mNscrvBottomSheet);
        bottomSheetBehavior.setPeekHeight((int) getResources().getDimension(R.dimen.flight_summary_price_peek_height));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        findViewById(R.id.flight_summary_act_tvic_money_label).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });

        findViewById(R.id.flight_summary_act_tv_selected).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new SelectTripCmd());
            }
        });
    }

    public void showFlightWay(FlightTrip trip, FlightBookingProperty property) {
        Location start = trip.getDepartureWay().getStartLocation();
        Location end = trip.getDepartureWay().getEndLocation();
        TextView tvStart = findViewById(R.id.flight_summary_act_tv_start_point);
        tvStart.setText(String.format("%s(%s)", start.getName(), start.getID()));
        TextView tvEnd = findViewById(R.id.flight_summary_act_tv_end_point);
        tvEnd.setText(String.format("%s(%s)", end.getName(), end.getID()));
        if (property.isRoundTrip()) {
            ImageView iv = findViewById(R.id.flight_summary_act_iv_indicator);
            iv.setImageResource(R.drawable.ic_swap_horiz_24px);
        }

        LinearLayout llContainer = findViewById(R.id.flight_summary_act_ll_ways);
        addWay(llContainer, trip.getDepartureWay(), trip);

        if (property.isRoundTrip()) {
            View divider = new View(getContext());
            divider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.light_grey));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    getResources().getDimensionPixelSize(R.dimen.divider_size));
            params.topMargin = getResources().getDimensionPixelOffset(R.dimen.dimen_16);
            params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.dimen_16);
            llContainer.addView(divider, params);

            addWay(llContainer, trip.getArrivalWay(), trip);
        }

        showPriceBottomSheet(trip);
    }

    @SuppressLint("DefaultLocale")
    private void addWay(LinearLayout container, final FlightWay way, FlightTrip trip) {
        View item = LayoutInflater.from(getContext()).inflate(
                R.layout.layout_fligt_summary_item, container, false);
        TextView tvDate = item.findViewById(R.id.flight_summary_item_layout_tv_date);
        tvDate.setText(TimeFormatUtil.getDayOfWeekDateFromISO8601UTC(way.getDateInfo().getStartDate()));

        TextView tvTime = item.findViewById(R.id.flight_summary_item_layout_tv_time);
        int[] duration = way.getDuration();
        String durStr = getResources().getString(R.string.air_duration_format_2, duration[0], duration[1]);
        tvTime.setText(String.format("%s - %s (%s)",
                TimeFormatUtil.getDefaultTimeFormatFromISO8601UTC(way.getDateInfo().getStartDate()),
                TimeFormatUtil.getDefaultTimeFormatFromISO8601UTC(way.getDateInfo().getEndDate()),
                durStr));

        TextView tvTransit = item.findViewById(R.id.flight_summary_item_layout_tv_transit);
        String transits = way.getTransit();
        tvTransit.setText(TextUtils.isEmpty(transits) ? getResources().getString(R.string.direct_air) : transits);

        TextView tvAir = item.findViewById(R.id.flight_summary_item_layout_tv_air);
        tvAir.setText(trip.getVendor().getName());

        item.findViewById(R.id.flight_summary_item_layout_tv_detail).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                SeeDetailCmd cmd = new SeeDetailCmd();
                cmd.way = way;
                mPresenter.executeCommand(cmd);
            }
        });
        container.addView(item);
    }

    private void showPriceBottomSheet(FlightTrip trip) {
        PaxInfo[] paxInfos = trip.getPaxInfos();
        LinearLayout llContainer = findViewById(R.id.flight_summary_act_ll_price_containers);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        for (int i = 0; i < paxInfos.length; i++) {
            PaxInfo paxInfo = paxInfos[i];
            RightLeftTextView itemView = (RightLeftTextView)
                    inflater.inflate(R.layout.layout_flight_summary_fare, llContainer, false);
            itemView.setRightText(MoneyFormatter.defaultFormat(paxInfo.getAmount()) + " " + paxInfo.getCurrencyCode());
            String left = "";
            if (paxInfo.getType().equals(PaxInfo.ADULT)) {
                left = getResources().getString(R.string.adult);
            } else if (paxInfo.getType().equals(PaxInfo.CHILD)) {
                left = getResources().getString(R.string.child);
            } else if (paxInfo.getType().equals(PaxInfo.NEW_BORN)) {
                left = getResources().getString(R.string.new_born);
            }
            itemView.setLeftText(left + " (x" + paxInfo.getQuantity() + ")");

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.leftMargin = padding;
            params.rightMargin = padding;
            llContainer.addView(itemView, params);
        }

        TextView tvTotal = findViewById(R.id.flight_summary_act_tv_total);
        tvTotal.setText(String.format("%s %s", MoneyFormatter.defaultFormat(trip.getPrice()),
                trip.getCurrencyUnit()));

        RightLeftTextView needToPay = new RightLeftTextView(getContext());
        float textSize = getResources().getDimension(R.dimen.text_size_12);
        needToPay.setLeftTextSize(textSize);
        needToPay.setRightTextSize(textSize);
        needToPay.setLeftText(getResources().getString(R.string.flight_fare_total));
        needToPay.setRightText(String.format("%s %s", MoneyFormatter.defaultFormat(trip.getPrice()),
                trip.getCurrencyUnit()));
        needToPay.setLeftColor(ContextCompat.getColor(getContext(), R.color.dark));
        needToPay.setRightColor(ContextCompat.getColor(getContext(), R.color.dark));
        needToPay.setPadding(padding, padding, padding, padding);
        needToPay.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.light_periwinkle));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        llContainer.addView(needToPay, params);
    }


    public static class SelectTripCmd implements ICommand {
    }

    public static class SeeDetailCmd implements ICommand {
        public FlightWay way;
    }
}
