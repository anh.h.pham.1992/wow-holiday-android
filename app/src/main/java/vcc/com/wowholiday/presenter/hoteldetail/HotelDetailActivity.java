package vcc.com.wowholiday.presenter.hoteldetail;

import common.presenter.WHActivity;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */

@Layout(R.layout.activity_hotel_detail)
public class HotelDetailActivity extends WHActivity<HotelDetailView> {
    public static final String HOTEL_DETAIL = "HOTEL_DETAIL";
    public static final String SPECIFIC_TAB = "SPECIFIC_TAB";
    public static final String TAB_IMAGES = "TAB_IMAGES";
    public static final String TAB_FACILITIES = "TAB_FACILITIES";
    public static final String TAB_REVIEWS = "TAB_REVIEWS";
    public static final String TAB_DESCRIPTION = "TAB_DESCRIPTION";

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        String specificTab = getIntent().getStringExtra(SPECIFIC_TAB);
        HotelDetail hotelDetail = getIntent().getParcelableExtra(HOTEL_DETAIL);
        mView.setHotelDetail(hotelDetail);
        if (specificTab != null) {
            mView.showSpecificTab(specificTab);
        }
    }
}
