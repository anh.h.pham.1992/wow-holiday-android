package vcc.com.wowholiday.presenter.hoteldetail;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;

import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_DESCRIPTION;
import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_FACILITIES;
import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_IMAGES;
import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_REVIEWS;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelDetailView extends BaseLinearView {
    private TabLayout mTabLayout;
    private TextView mTvActionBarName;
    private HotelVpAdapter mAdapter;

    public HotelDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        ViewPager vp = findViewById(R.id.hotel_detail_act_vp);
        mAdapter = new HotelVpAdapter(((AppCompatActivity) getContext()).getSupportFragmentManager(), getContext());
        vp.setAdapter(mAdapter);

        mTabLayout = findViewById(R.id.hotel_detail_act_tab);
        mTabLayout.setupWithViewPager(vp);
        mTvActionBarName = findViewById(R.id.action_bar_text_left);
    }

    public void setHotelDetail(HotelDetail hotelDetail) {
        mAdapter.setHotelDetail(hotelDetail);
        mTvActionBarName.setText(Html.fromHtml(hotelDetail.getName()));
    }

    public void showSpecificTab(String specificTab) {
        switch (specificTab) {
            case TAB_IMAGES:
                mTabLayout.getTabAt(0).select();
                break;
            case TAB_FACILITIES:
                mTabLayout.getTabAt(1).select();
                break;
            case TAB_REVIEWS:
                mTabLayout.getTabAt(2).select();
                break;
            case TAB_DESCRIPTION:
                mTabLayout.getTabAt(3).select();
                break;
        }
    }
}
