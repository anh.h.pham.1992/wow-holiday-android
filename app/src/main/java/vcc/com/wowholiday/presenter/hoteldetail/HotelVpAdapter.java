package vcc.com.wowholiday.presenter.hoteldetail;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.presenter.hoteldetail.description.HotelDescriptionFragment;
import vcc.com.wowholiday.presenter.hoteldetail.facility.HotelFacilityFragment;
import vcc.com.wowholiday.presenter.hoteldetail.photo.HotelPhotoFragment;
import vcc.com.wowholiday.presenter.hoteldetail.review.HotelReviewFragment;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelVpAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private HotelPhotoFragment mHotelPhotoFragment;
    private HotelFacilityFragment mHotelFacilityFragment;
    private HotelReviewFragment mHotelReviewFragment;
    private HotelDescriptionFragment mHotelDescriptionFragment;

    private HotelDetail mHotelDetail;

    public HotelVpAdapter(@NonNull FragmentManager fm, Context context) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                if (mHotelPhotoFragment == null)
                    mHotelPhotoFragment = new HotelPhotoFragment();
                mHotelPhotoFragment.setHotelDetail(mHotelDetail);
                return mHotelPhotoFragment;
            }
            case 1:
                if (mHotelFacilityFragment == null)
                    mHotelFacilityFragment = new HotelFacilityFragment();
                mHotelFacilityFragment.setHotelDetail(mHotelDetail);
                return mHotelFacilityFragment;
            case 2:
                if (mHotelReviewFragment == null)
                    mHotelReviewFragment = new HotelReviewFragment();
                mHotelReviewFragment.setHotelDetail(mHotelDetail);
                return mHotelReviewFragment;
            case 3:
                if (mHotelDescriptionFragment == null)
                    mHotelDescriptionFragment = new HotelDescriptionFragment();
                mHotelDescriptionFragment.setHotelDetail(mHotelDetail);
                return mHotelDescriptionFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.photo);
            case 1:
                return mContext.getString(R.string.facility_provide);
            case 2:
                return mContext.getString(R.string.review);
            case 3:
                return mContext.getString(R.string.description);
        }
        return null;
    }

    public void setHotelDetail(HotelDetail hotelDetail) {
        mHotelDetail = hotelDetail;
        if (mHotelPhotoFragment != null)
            mHotelPhotoFragment.setHotelDetail(hotelDetail);
        if (mHotelFacilityFragment != null)
            mHotelFacilityFragment.setHotelDetail(hotelDetail);
        if (mHotelReviewFragment != null)
            mHotelReviewFragment.setHotelDetail(hotelDetail);
        if (mHotelDescriptionFragment != null)
            mHotelDescriptionFragment.setHotelDetail(hotelDetail);
    }
}
