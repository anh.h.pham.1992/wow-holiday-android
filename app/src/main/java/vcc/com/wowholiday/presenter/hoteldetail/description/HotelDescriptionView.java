package vcc.com.wowholiday.presenter.hoteldetail.description;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelDescriptionView extends BaseFrameView {
    private TextView mTvDescription;
    public HotelDescriptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        super.onInitView();
        mTvDescription = findViewById(R.id.hotel_overview_act_tv_description_detail);
    }

    public void setHotelDetail(HotelDetail hotelDetail) {
        mTvDescription.setText(Html.fromHtml(hotelDetail.getDescription()));
    }
}
