package vcc.com.wowholiday.presenter.hoteldetail.facility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.Facility;

public class FacilityAdapter extends BaseAdapter {
    private static List<Amenity> mFacilities;

    private Context mContext;

    public FacilityAdapter(Context context) {
        mContext = context;
        mFacilities = new ArrayList<>();
    }


    @Override
    public int getCount() {
        return mFacilities.size();
    }

    @Override
    public Amenity getItem(int position) {
        return mFacilities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_facility, parent, false);
        }

        ((TextView) convertView.findViewById(R.id.facility_itm_tv_name))
                .setText(getItem(position).getName());
        return convertView;
    }

    public void reset(List<Amenity> amenities) {
        mFacilities = amenities;
        notifyDataSetChanged();
    }
}
