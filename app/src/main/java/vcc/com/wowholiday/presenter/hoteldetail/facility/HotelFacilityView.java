package vcc.com.wowholiday.presenter.hoteldetail.facility;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelFacilityView extends BaseFrameView {
    private ListView mListView;
    private FacilityAdapter mAdapter;

    public HotelFacilityView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        super.onInitView();
        mListView = findViewById(R.id.hotel_facilities_frag_lv_facilities);
        mAdapter =new FacilityAdapter(getContext());
        mListView.setAdapter(mAdapter);
    }

    public void setHotelDetail(HotelDetail hotelDetail) {
        mAdapter.reset(hotelDetail.getAmenities());
    }
}
