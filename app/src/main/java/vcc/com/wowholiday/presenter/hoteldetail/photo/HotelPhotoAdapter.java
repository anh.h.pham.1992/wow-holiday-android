package vcc.com.wowholiday.presenter.hoteldetail.photo;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Image;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelPhotoAdapter extends BaseRclvAdapter {

    private IOnPhotoSelectedListener mListener;
    private IImageLoader mImageLoader = GlideImageLoader.getInstance();

    public HotelPhotoAdapter(IOnPhotoSelectedListener listener) {
        mListener = listener;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_hotel_photo;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new ImageVH(itemView);
    }


    private class ImageVH extends BaseRclvHolder<Image> {
        ImageView ivPhoto;

        public ImageVH(@NonNull View itemView) {
            super(itemView);
            ivPhoto = itemView.findViewById(R.id.hotel_photo_itm_iv_photo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onSelected((Image) mDataSet.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }

        @Override
        public void onBind(Image vhData) {
            super.onBind(vhData);
            mImageLoader.loadImage(itemView, vhData.getUrl(), ivPhoto);
        }
    }


    public interface IOnPhotoSelectedListener {
        void onSelected(Image photoUrl, int position);
    }
}
