package vcc.com.wowholiday.presenter.hoteldetail.photo;

import common.presenter.WHTabFragment;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelPhotoFragment extends WHTabFragment<HotelPhotoView> {
    private HotelDetail mHotelDetail;

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_hotel_photo;
    }

    public void setHotelDetail(HotelDetail hotelDetail) {
        mHotelDetail = hotelDetail;
        if (mView != null)
            mView.setHotelDetail(hotelDetail);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mView.setHotelDetail(mHotelDetail);
    }
}
