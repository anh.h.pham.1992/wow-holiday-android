package vcc.com.wowholiday.presenter.hoteldetail.photo;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.TopPaddingDecorator;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Image;
import vcc.com.wowholiday.model.hotel.HotelDetail;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelPhotoView extends BaseFrameView {

    private HotelPhotoAdapter mAdapter;
    private List<Image> mImages;

    public HotelPhotoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        RecyclerView rclv = findViewById(R.id.hotel_photo_frag_rclv_content);
        rclv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        rclv.addItemDecoration(new TopPaddingDecorator(padding));
        rclv.addItemDecoration(new SpaceItemDecoration(padding));

        mAdapter = new HotelPhotoAdapter(new HotelPhotoAdapter.IOnPhotoSelectedListener() {
            @Override
            public void onSelected(Image photo, int position) {
                ViewPhotoDialog dialog = new ViewPhotoDialog(mImages, position);
                dialog.show(((AppCompatActivity)getContext()).getSupportFragmentManager(), "view_photo");
            }
        });
        rclv.setAdapter(mAdapter);

    }

    public void setHotelDetail(HotelDetail hotelDetail) {
        mImages =hotelDetail.getImages();
        mAdapter.reset(mImages);
    }
}
