package vcc.com.wowholiday.presenter.hoteldetail.photo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Image;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;

/**
 * Created by Pham Hai Quang on 10/18/2019.
 */
public class ViewPhotoDialog extends DialogFragment {

    private ViewPager mVpImg;
    private ImageView mIvPrev;
    private ImageView mIvNext;
    private TextView mTvIndicator;

    private List<Image> mImages;
    private int mCurrPosition;

    public ViewPhotoDialog(List<Image> images, int initPosition) {
        mImages = images;
        mCurrPosition = initPosition;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFullScreen);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_view_photo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mIvNext = view.findViewById(R.id.view_photo_dlg_iv_next);
        mIvPrev = view.findViewById(R.id.view_photo_dlg_iv_prev);
        mVpImg = view.findViewById(R.id.view_photo_dlg_vp_img);
        mTvIndicator = view.findViewById(R.id.view_photo_dlg_tv_indicator);
        ImageVpAdapter adapter = new ImageVpAdapter();
        adapter.mParent = this;
        mVpImg.setAdapter(adapter);
        mVpImg.setCurrentItem(mCurrPosition);
        updateIndicator();

        mVpImg.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mCurrPosition = position;
                updateIndicator();
            }
        });

        mIvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVpImg.setCurrentItem(mCurrPosition + 1, true);
            }
        });

        mIvPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVpImg.setCurrentItem(mCurrPosition - 1, true);
            }
        });
    }

    private void updateIndicator() {
        if (mCurrPosition == 0) {
            mIvPrev.setVisibility(View.INVISIBLE);
        } else {
            mIvPrev.setVisibility(View.VISIBLE);
        }

        if (mCurrPosition == mImages.size() - 1) {
            mIvNext.setVisibility(View.INVISIBLE);
        } else {
            mIvNext.setVisibility(View.VISIBLE);
        }
        mTvIndicator.setText((mCurrPosition + 1) + "/" + mImages.size());
    }

    private static class ImageVpAdapter extends PagerAdapter {

        private IImageLoader mImageLoader = GlideImageLoader.getInstance();
        private ViewPhotoDialog mParent;

        @Override
        public int getCount() {
            return mParent.mImages.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NotNull
        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            ImageView iv = new ImageView(container.getContext());
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            ViewPager.LayoutParams params = new ViewPager.LayoutParams();
            iv.setLayoutParams(params);
            container.addView(iv);
            mImageLoader.loadImage(mParent, mParent.mImages.get(position).getUrl(), iv);
            return iv;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
