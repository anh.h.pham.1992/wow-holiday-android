package vcc.com.wowholiday.presenter.hoteldetail.review;

import android.view.View;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.BaseVHData;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.recyclerview.LoadMoreAdapter;

/**
 * Created by Pham Hai Quang on 10/19/2019.
 */
public class CommentAdapter extends LoadMoreAdapter {

    private static final int TOTAL_TYPE = 2;
    private static final int COMMENT_TYPE = 3;

    @Override
    public int getItemViewType(int position) {
        BaseVHData data = (BaseVHData) mDataSet.get(position);
        return data.type;
    }

    @Override
    public int getLayoutResource(int viewType) {
        if (viewType == TOTAL_TYPE) {
            return R.layout.item_hotel_total_review;
        } else if (viewType == COMMENT_TYPE) {
            return R.layout.item_hotel_comment;
        }
        return super.getLayoutResource(viewType);
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        if (viewType == TOTAL_TYPE) {
            return new TotalVH(itemView);
        } else if (viewType == COMMENT_TYPE) {
            return new CommentVH(itemView);
        }
        return super.onCreateVH(itemView, viewType);
    }


    private class TotalVH extends BaseRclvHolder<TotalVHData> {
        public TotalVH(@NonNull View itemView) {
            super(itemView);
        }
    }

    private class CommentVH extends BaseRclvHolder<HotelCommentVHData> {
        public CommentVH(@NonNull View itemView) {
            super(itemView);
        }
    }

    private class TotalVHData extends BaseVHData<String> {
        TotalVHData() {
            type = TOTAL_TYPE;
        }
    }

    private class HotelCommentVHData extends BaseVHData<String> {
        HotelCommentVHData() {
            type = COMMENT_TYPE;
        }
    }
}
