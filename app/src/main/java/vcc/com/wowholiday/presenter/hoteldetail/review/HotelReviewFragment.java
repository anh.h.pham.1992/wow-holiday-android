package vcc.com.wowholiday.presenter.hoteldetail.review;

import common.presenter.WHTabFragment;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelReviewFragment extends WHTabFragment<HotelReviewView> {
    private HotelDetail mHotelDetail;

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_hotel_review;
    }

    public void setHotelDetail(HotelDetail hotelDetail) {
        mHotelDetail = hotelDetail;
        if (mView != null)
            mView.setHotelDetail(hotelDetail);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mView.setHotelDetail(mHotelDetail);
    }
}
