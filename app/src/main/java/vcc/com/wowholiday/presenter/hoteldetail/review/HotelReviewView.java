package vcc.com.wowholiday.presenter.hoteldetail.review;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.presenter.hotelsearch.hoteloverview.ReviewsAdapter;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class HotelReviewView extends BaseLinearView {

    private RecyclerView mWRclvContent;
    private ReviewsAdapter mReviewsAdapter;

    public HotelReviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mWRclvContent = findViewById(R.id.hotel_review_frag_wraprclv);
        mWRclvContent.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        mReviewsAdapter = new ReviewsAdapter();
        mWRclvContent.setAdapter(mReviewsAdapter);
    }

    public void setHotelDetail(HotelDetail hotelDetail) {
    }
}
