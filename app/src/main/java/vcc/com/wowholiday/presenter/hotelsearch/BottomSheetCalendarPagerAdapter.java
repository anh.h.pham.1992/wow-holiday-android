package vcc.com.wowholiday.presenter.hotelsearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.Calendar;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.calendar.CalendarView;
import vcc.com.wowholiday.presenter.widget.calendar.IOnDaySelectedListener;
import vcc.com.wowholiday.util.DateUtil;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public class BottomSheetCalendarPagerAdapter extends PagerAdapter {

    private Context mContext;
    private HotelSearchProperty mHotelSearchProperty;
    private ViewPager mVpContainer;

    public BottomSheetCalendarPagerAdapter(Context context, HotelSearchProperty hotelSearchProperty, ViewPager container) {
        mContext = context;
        mHotelSearchProperty = hotelSearchProperty;
        mVpContainer = container;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        CalendarView calendarView = (CalendarView) inflater.inflate(R.layout.item_calendar, container, false);
        container.addView(calendarView);
        if (position == 0) {
            calendarView.setDate(mHotelSearchProperty.getStartDate());
            calendarView.setMinDate(DateUtil.getCalendar());
        } else {
            calendarView.setDate(mHotelSearchProperty.getEndDate());
            calendarView.setMinDate(mHotelSearchProperty.getStartDate());
        }
        calendarView.addSelectedListener(new IOnDaySelectedListener() {
            @Override
            public void onSelected(Calendar oldSelected, Calendar selectedDay) {
                if (position == 0) {
                    mHotelSearchProperty.setStartDate(selectedDay);
                    mVpContainer.setCurrentItem(1, true);
                } else {
                    mHotelSearchProperty.setEndDate(selectedDay);
                }
            }
        });
        calendarView.setTag(getPageTag(position));
        return calendarView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.start_date);
        }
        return mContext.getString(R.string.end_date);
    }

    public String getPageTag(int position) {
        return String.valueOf(position);
    }
}
