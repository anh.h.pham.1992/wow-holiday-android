package vcc.com.wowholiday.presenter.hotelsearch;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.calendar.CalendarView;

/**
 * Created by Pham Hai Quang on 10/12/2019.
 */
public class CalendarBottomSheet extends WHBottomSheetDialog {
    public static String TAG_START_DATE = "START_DATE ";
    public static String TAG_END_DATE = "END_DATE ";

    private BottomSheetCalendarPagerAdapter mAdapter;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private HotelSearchProperty mHotelSearchProperty;

    public CalendarBottomSheet(HotelSearchProperty hotelSearchProperty) {
        mHotelSearchProperty = hotelSearchProperty;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_hotel_search_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTabLayout = view.findViewById(R.id.hotel_search_dlg_tab);

        mViewPager = view.findViewById(R.id.hotel_booking_dlg_vp);
        mAdapter = new BottomSheetCalendarPagerAdapter(getContext(), mHotelSearchProperty, mViewPager);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 1) {
                    CalendarView calv = mViewPager.findViewWithTag(mAdapter.getPageTag(1));
                    if (calv != null)
                        calv.setDate(mHotelSearchProperty.getEndDate());
                }
            }
        });
        mTabLayout.setupWithViewPager(mViewPager);

        view.findViewById(R.id.hotel_search_dlg_tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mViewPager.setCurrentItem(TAG_START_DATE.equals(getTag()) ? 0 : 1);
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mViewPager.setCurrentItem(TAG_START_DATE.equals(getTag()) ? 0 : 1);
            }
        });
    }

    public void setHotelSearchProperty(HotelSearchProperty hotelSearchProperty) {
        this.mHotelSearchProperty = hotelSearchProperty;
    }
}
