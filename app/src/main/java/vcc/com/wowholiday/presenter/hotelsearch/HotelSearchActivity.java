package vcc.com.wowholiday.presenter.hotelsearch;

import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.presenter.hotelsearch.hotelsearchlocation.HotelSearchLocationActivity;
import vcc.com.wowholiday.presenter.hotelsearch.searchresult.HotelSearchResultActivity;

import static vcc.com.wowholiday.presenter.hotelsearch.searchresult.HotelSearchResultActivity.HOTEL_SEARCH_PROPERTIES;

/**
 * Created by Pham Hai Quang on 10/11/2019.
 */

@Layout(R.layout.activity_hotel_search)
public class HotelSearchActivity extends WHActivity<HotelSearchView> {
    private static final int LOC_SEARCH_REQ = 2345;

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof HotelSearchView.SearchLocCmd) {
            gotoLocationActivity(((HotelSearchView.SearchLocCmd) command).recentSearch);
        } else if (command instanceof HotelSearchView.SearchCmd) {
            gotoSearchResultActivity(((HotelSearchView.SearchCmd) command).hotelSearchProperty);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOC_SEARCH_REQ && resultCode == RESULT_OK) {
            mView.setLocation((HotelSuggestion) data.getParcelableExtra(HotelSearchLocationActivity.SUGGESTION_KEY));
        }
    }

    private void gotoLocationActivity(HotelSuggestion recentSearch) {
        Intent itn = new Intent(this, HotelSearchLocationActivity.class);
        itn.putExtra(HotelSearchLocationActivity.RECENT_SEARCH, recentSearch);
        startActivityForResult(itn, LOC_SEARCH_REQ);
    }

    private void gotoSearchResultActivity(HotelSearchProperty hotelSearchProperty) {
        Intent itn = new Intent(this, HotelSearchResultActivity.class);
        itn.putExtra(HOTEL_SEARCH_PROPERTIES, hotelSearchProperty);
        startActivity(itn);
    }
}
