package vcc.com.wowholiday.presenter.hotelsearch;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

import vcc.com.wowholiday.model.hotel.HotelSuggestion;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public class HotelSearchProperty implements Parcelable {
    private Calendar mStartDate;
    private Calendar mEndDate;

    private int mPersonCount = 0;
    private int mRoomCount = 0;

    private HotelSuggestion mLocation;

    public HotelSearchProperty() {

    }

    protected HotelSearchProperty(Parcel in) {
        mLocation = in.readParcelable(HotelSuggestion.class.getClassLoader());
        mStartDate = Calendar.getInstance();
        mStartDate.setTimeInMillis(in.readLong());
        mEndDate = Calendar.getInstance();
        mEndDate.setTimeInMillis(in.readLong());
        mPersonCount = in.readInt();
        mRoomCount = in.readInt();
    }

    public static final Creator<HotelSearchProperty> CREATOR = new Creator<HotelSearchProperty>() {
        @Override
        public HotelSearchProperty createFromParcel(Parcel in) {
            return new HotelSearchProperty(in);
        }

        @Override
        public HotelSearchProperty[] newArray(int size) {
            return new HotelSearchProperty[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mLocation, flags);
        dest.writeLong(mStartDate.getTimeInMillis());
        dest.writeLong(mEndDate.getTimeInMillis());
        dest.writeInt(mPersonCount);
        dest.writeInt(mRoomCount);
    }

    public void calculateDefaultDates() {
        if (mStartDate == null) {
            mStartDate = Calendar.getInstance();
            mStartDate.add(Calendar.DAY_OF_YEAR, 1);
            mStartDate.set(Calendar.HOUR_OF_DAY, 0);
            mStartDate.set(Calendar.MINUTE, 0);
            mStartDate.set(Calendar.SECOND, 0);
            mStartDate.set(Calendar.MILLISECOND, 0);
            calculateEndDate();
        }
    }

    public void calculateDefaultPersonAndRoomCount() {
        if (mPersonCount == 0)
            mPersonCount = 1;
        if (mRoomCount == 0)
            mRoomCount = 1;
    }

    public void setStartDate(Calendar date) {
        mStartDate = date;
        calculateEndDate();
    }

    public Calendar getStartDate() {
        return mStartDate;
    }

    public Calendar getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Calendar date) {
        mEndDate = date;
    }

    public int getPersonCount() {
        return mPersonCount;
    }

    public void setPersonCount(int count) {
        mPersonCount = count;
    }

    public int getRoomCount() {
        return mRoomCount;
    }

    public void setRoomCount(int count) {
        mRoomCount = count;
    }

    public HotelSuggestion getLocation() {
        return mLocation;
    }

    public void setLocation(HotelSuggestion location) {
        this.mLocation = location;
    }

    public int getDuration() {
        if (mStartDate == null || mEndDate == null) {
            return 0;
        }

        long diff = mEndDate.getTimeInMillis() - mStartDate.getTimeInMillis();
        return (int) (diff / (24 * 60 * 60 * 1000));
    }

    private void calculateEndDate() {
        mEndDate = (Calendar) mStartDate.clone();
        mEndDate.add(Calendar.DAY_OF_YEAR, 2);
    }
}
