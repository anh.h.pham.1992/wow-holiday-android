package vcc.com.wowholiday.presenter.hotelsearch;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import common.presenter.WHBottomSheetDialog;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorBottomSheet;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorItem;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/11/2019.
 */
public class HotelSearchView extends BaseRelativeView {
    private TextViewWithIcon mTvLocation;

    private HotelSearchProperty mHotelSearchProperty = new HotelSearchProperty();
    private CalendarBottomSheet mCalendarBottomSheet;
    private CountSelectorBottomSheet mCountSelectorBottomSheet;

    public HotelSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        final TextViewWithIcon tvStartDate = findViewById(R.id.hotel_search_act_tv_start_date);
        final TextViewWithIcon tvEndDate = findViewById(R.id.hotel_search_act_tv_end_date);
        final TextView tvDur = findViewById(R.id.hotel_search_act_tv_dur);

        mCalendarBottomSheet = new CalendarBottomSheet(mHotelSearchProperty);
        mCalendarBottomSheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
            @Override
            public void onDismiss() {
                tvStartDate.setText(TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getStartDate()));
                tvEndDate.setText(TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getEndDate()));
                tvDur.setText(getResources().getString(R.string.duration_format, mHotelSearchProperty.getDuration()));
            }
        });

        tvStartDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mHotelSearchProperty.calculateDefaultDates();
                mCalendarBottomSheet.setHotelSearchProperty(mHotelSearchProperty);
                if (!mCalendarBottomSheet.isAdded())
                    mCalendarBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), CalendarBottomSheet.TAG_START_DATE);
            }
        });

        tvEndDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mHotelSearchProperty.calculateDefaultDates();
                mCalendarBottomSheet.setHotelSearchProperty(mHotelSearchProperty);
                if (!mCalendarBottomSheet.isAdded())
                    mCalendarBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), CalendarBottomSheet.TAG_END_DATE);
            }
        });

        final TextViewWithIcon tvPersonRoom = findViewById(R.id.hotel_search_act_tv_people_room);

        mCountSelectorBottomSheet = new CountSelectorBottomSheet();
        mCountSelectorBottomSheet.setOnCountSelectedListener(new CountSelectorBottomSheet.IOnCountSelectListener() {
            @Override
            public void onCountChange(TextView tvTitle, int rowIndex, CountSelectorItem item) {
                if (rowIndex == 0) {
                    tvTitle.setText(getResources().getString(R.string.person_count_format, item.count));
                    mHotelSearchProperty.setPersonCount(item.count);
                } else {
                    tvTitle.setText(getResources().getString(R.string.room_count_format, item.count));
                    mHotelSearchProperty.setRoomCount(item.count);
                }
            }
        });
        mCountSelectorBottomSheet.setTitle(R.string.person_room_count);
        mCountSelectorBottomSheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
            @Override
            public void onDismiss() {
                tvPersonRoom.setText(getResources().getString(R.string.person_room_count_format,
                        mHotelSearchProperty.getPersonCount(), mHotelSearchProperty.getRoomCount()));
            }
        });

        tvPersonRoom.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mHotelSearchProperty.calculateDefaultPersonAndRoomCount();
                mCountSelectorBottomSheet.addItems(createSelectorItem());
                if (!mCountSelectorBottomSheet.isAdded())
                    mCountSelectorBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), "person_room");
            }
        });

        mTvLocation = findViewById(R.id.hotel_search_act_tv_loc);
        mTvLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchLocCmd cmd = new SearchLocCmd();
                cmd.recentSearch = mHotelSearchProperty.getLocation();
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.hotel_search_act_bt_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHotelSearchProperty.getLocation() != null
                        && mHotelSearchProperty.getStartDate() != null
                        && mHotelSearchProperty.getEndDate() != null
                        && mHotelSearchProperty.getPersonCount() != 0
                        && mHotelSearchProperty.getRoomCount() != 0) {
                    SearchCmd searchCmd = new SearchCmd();
                    searchCmd.hotelSearchProperty = mHotelSearchProperty;
                    mPresenter.executeCommand(searchCmd);
                } else
                    Toast.makeText(getContext(), "Vui lòng điền đầy đủ thông tin", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setLocation(HotelSuggestion location) {
        if (location != null)
            mTvLocation.setText(location.getName());
        mHotelSearchProperty.setLocation(location);
    }

    private CountSelectorItem[] createSelectorItem() {
        CountSelectorItem[] items = new CountSelectorItem[2];
        CountSelectorItem person = new CountSelectorItem();
        person.max = 30;
        person.min = 1;
        person.count = mHotelSearchProperty.getPersonCount();
        items[0] = person;
        CountSelectorItem room = new CountSelectorItem();
        room.max = 8;
        room.min = 1;
        room.count = mHotelSearchProperty.getRoomCount();
        items[1] = room;
        return items;
    }

    public static class SearchLocCmd implements ICommand {
        public HotelSuggestion recentSearch;
    }

    public static class SearchCmd implements ICommand {
        public HotelSearchProperty hotelSearchProperty;
    }
}
