package vcc.com.wowholiday.presenter.hotelsearch.eticket;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.mainscreen.MainActivity;

@Layout(R.layout.activity_eticket_hotel)
public class HotelTicketActivity extends WHActivity<HotelTicketView> {
    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof HotelTicketView.GoHomeCmd) {
            Intent itn = new Intent(this, MainActivity.class);
            itn.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(itn);
        }
    }
}
