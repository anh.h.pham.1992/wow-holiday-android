package vcc.com.wowholiday.presenter.hotelsearch.eticket;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;

public class HotelTicketView extends BaseRelativeView {
    public HotelTicketView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.hotel_ticket_act_iv_copy).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Đã sao chép mã", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.hotel_ticket_act_img_logo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new GoHomeCmd());
            }
        });

        findViewById(R.id.action_bar_text_right).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new GoHomeCmd());
            }
        });
    }

    public static class GoHomeCmd implements ICommand {}
}
