package vcc.com.wowholiday.presenter.hotelsearch.fillhotelbooking;

import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.contactinfo.ContactInfoActivity;
import vcc.com.wowholiday.presenter.hotelsearch.bookingdetail.BookingDetailActivity;
import vcc.com.wowholiday.presenter.hotelsearch.previewbooking.PreviewBookingActivity;

@Layout(R.layout.activity_fill_hotel_booking)
public class FillHotelBookingActivity extends WHActivity<FillHotelBookingView> {

    private static final int CONTACT_RC = 6390;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CONTACT_RC && resultCode == RESULT_OK) {
            mView.updateUserInfo(data.getStringExtra(ContactInfoActivity.NAME_KEY),
                    data.getStringExtra(ContactInfoActivity.PHONE_KEY),
                    data.getStringExtra(ContactInfoActivity.EMAIL_KEY));
        }
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof FillHotelBookingView.ViewBookingDetailCmd) {
            gotoBookingDetailActivity();
        } else if (command instanceof FillHotelBookingView.EditInfoCmd) {
            gotoEditUserInfoActivity(((FillHotelBookingView.EditInfoCmd) command).name,
                    ((FillHotelBookingView.EditInfoCmd) command).phone,
                    ((FillHotelBookingView.EditInfoCmd) command).email);
        } else if (command instanceof FillHotelBookingView.ContinueCmd) {
            gotoPreviewBookingActivity();
        }
    }

    private void gotoBookingDetailActivity() {
        Intent itn = new Intent(this, BookingDetailActivity.class);
        startActivity(itn);
    }

    private void gotoPreviewBookingActivity() {
        Intent itn = new Intent(this, PreviewBookingActivity.class);
        startActivity(itn);
    }

    private void gotoEditUserInfoActivity(String name, String phone, String email) {
        Intent itn = new Intent(this, ContactInfoActivity.class);
        ContactInfoActivity.createParams(itn, name, phone, email, null, getResources().getString(R.string.who_receive_room_booking_info));
        startActivityForResult(itn, CONTACT_RC);
    }


}
