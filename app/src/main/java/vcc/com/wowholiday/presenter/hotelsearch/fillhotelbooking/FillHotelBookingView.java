package vcc.com.wowholiday.presenter.hotelsearch.fillhotelbooking;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseConstraintView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.CheckboxWithRightText;

public class FillHotelBookingView extends BaseConstraintView {
    private EditText mEdtOtherName;
    private TextView mTvMustFill;
    private CheckboxWithRightText mCbForMe;
    private CheckboxWithRightText mCbForOther;
    private TextView mTvName;
    private TextView mTvPhone;
    private TextView mTvEmail;
    private NestedScrollView nsvWrapper;
    private LinearLayout llAddContactInfo;
    private LinearLayout llContactInfo;
    private TextView tvContactInfoRequired;

    public FillHotelBookingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        super.onInitView();
        mTvName = findViewById(R.id.fill_hotel_booking_act_tv_username);
        mTvPhone = findViewById(R.id.fill_hotel_booking_act_tv_phone);
        mTvEmail = findViewById(R.id.fill_hotel_booking_act_tv_email);
        mCbForMe = findViewById(R.id.fill_hotel_booking_act_cb_for_me);
        mCbForOther = findViewById(R.id.fill_hotel_booking_act_cb_for_other);
        mEdtOtherName = findViewById(R.id.fill_hotel_booking_act_edt_other_user_name);
        mTvMustFill = findViewById(R.id.fill_hotel_booking_act_tv_must_fill);
        nsvWrapper = findViewById(R.id.fill_hotel_booking_act_nsv_wrapper);
        llAddContactInfo = findViewById(R.id.fill_hotel_booking_act_ll_add_contact_info);
        llContactInfo = findViewById(R.id.fill_hotel_booking_act_ll_contact_info);
        tvContactInfoRequired = findViewById(R.id.fill_hotel_booking_act_tv_contact_info_required);

        mCbForMe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCbForOther.isChecked()) {
                    if (!mCbForMe.isChecked()) {
                        mCbForMe.setChecked(true);
                    }
                } else {
                    mCbForOther.setChecked(false);
                    showEditOtherNamePanel(false);
                }
            }
        });

        mCbForOther.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCbForMe.isChecked()) {
                    if (!mCbForOther.isChecked()) {
                        mCbForOther.setChecked(true);
                    }
                } else {
                    mCbForMe.setChecked(false);
                    showEditOtherNamePanel(true);
                }
            }
        });

        mEdtOtherName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mTvMustFill.setVisibility(GONE);
            }
        });

        findViewById(R.id.fill_hotel_booking_act_tv_booking_detail).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewBookingDetailCmd cmd = new ViewBookingDetailCmd();
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.fill_hotel_booking_act_ll_add_contact_info).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditInfoCmd cmd = new EditInfoCmd();
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.fill_hotel_booking_act_tv_edit).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditInfoCmd cmd = new EditInfoCmd();
                cmd.name = mTvName.getText().toString();
                cmd.phone = mTvPhone.getText().toString();
                cmd.email = mTvEmail.getText().toString();
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.fill_hotel_booking_act_btn_continue).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(mTvName.getText().toString())) {
                    tvContactInfoRequired.setVisibility(VISIBLE);
                } else if (mCbForOther.isChecked() && mEdtOtherName.getText().length() == 0) {
                    mTvMustFill.setVisibility(VISIBLE);
                } else {
                    ContinueCmd cmd = new ContinueCmd();
                    mPresenter.executeCommand(cmd);
                }
            }
        });

    }

    public void updateUserInfo(String name, String phone, String email) {
        if (!TextUtils.isEmpty(name) || !TextUtils.isEmpty(name) || !TextUtils.isEmpty(name)) {
            llAddContactInfo.setVisibility(GONE);
            llContactInfo.setVisibility(VISIBLE);
            mTvName.setText(name);
            mTvPhone.setText(phone);
            mTvEmail.setText(email);
        }
    }

    private void showEditOtherNamePanel(boolean isShown) {
        mEdtOtherName.setVisibility(isShown ? VISIBLE : GONE);
        if (isShown) {
            post(new Runnable() {
                @Override
                public void run() {
                    nsvWrapper.fullScroll(View.FOCUS_DOWN);
                }
            });
        }
    }

    public static class ViewBookingDetailCmd implements ICommand {
    }

    public static class EditInfoCmd implements ICommand {
        public String name;
        public String phone;
        public String email;
    }

    public static class ContinueCmd implements ICommand {
    }
}
