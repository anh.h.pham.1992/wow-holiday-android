package vcc.com.wowholiday.presenter.hotelsearch.hoteloverview;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.ActionException;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.hotel.GetHotelDetailsAction;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.model.hotel.HotelInfo;
import vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.presenter.hotelsearch.roomselect.RoomSelectActivity;

import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.SPECIFIC_TAB;
import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_DESCRIPTION;
import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_FACILITIES;
import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_IMAGES;
import static vcc.com.wowholiday.presenter.hoteldetail.HotelDetailActivity.TAB_REVIEWS;

@Layout(R.layout.activity_hotel_overview)
public class HotelOverviewActivity extends WHActivity<HotelOverviewView> {
    public static final String HOTEL_SEARCH_PROPERTIES = "HOTEL_SEARCH_PROPERTIES";
    public static final String HOTEL_INFO = "HOTEL_INFO";

    private HotelSearchProperty mHotelSearchProperty;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView.onInitMap(savedInstanceState);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        HotelInfo hotelInfo = getIntent().getParcelableExtra(HOTEL_INFO);
        mHotelSearchProperty = getIntent().getParcelableExtra(HOTEL_SEARCH_PROPERTIES);
        mView.setHotelInfo(hotelInfo, mHotelSearchProperty);
        getDetail(hotelInfo);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof HotelOverviewView.ViewAppropriateRoomsCmd) {
            goToRoomSelectActivity(((HotelOverviewView.ViewAppropriateRoomsCmd) command).hotelDetail);
        } else if (command instanceof HotelOverviewView.ViewImagesDetailCmd) {
            goToHotelDetailActivity(((HotelOverviewView.ViewImagesDetailCmd) command).hotelDetail, TAB_IMAGES);
        } else if (command instanceof HotelOverviewView.ViewFacilitiesDetailCmd) {
            goToHotelDetailActivity(((HotelOverviewView.ViewFacilitiesDetailCmd) command).hotelDetail, TAB_FACILITIES);
        } else if (command instanceof HotelOverviewView.ViewReviewsDetailCmd) {
            goToHotelDetailActivity(((HotelOverviewView.ViewReviewsDetailCmd) command).hotelDetail, TAB_REVIEWS);
        } else if (command instanceof HotelOverviewView.ViewDescriptionDetailCmd) {
            goToHotelDetailActivity(((HotelOverviewView.ViewDescriptionDetailCmd) command).hotelDetail, TAB_DESCRIPTION);
        }
    }

    private void goToRoomSelectActivity(HotelDetail hotelDetail) {
        Intent itn = new Intent(this, RoomSelectActivity.class);
        itn.putExtra(RoomSelectActivity.HOTEL_DETAIL, hotelDetail);
        itn.putExtra(RoomSelectActivity.HOTEL_SEARCH_PROPERTIES, mHotelSearchProperty);
        startActivity(itn);
    }

    private void goToHotelDetailActivity(HotelDetail hotelDetail, String tab) {
        Intent itn = new Intent(this, HotelDetailActivity.class);
        itn.putExtra(HotelDetailActivity.HOTEL_DETAIL, hotelDetail);
        itn.putExtra(SPECIFIC_TAB, tab);
        startActivity(itn);
    }

    private void getDetail(HotelInfo hotelInfo) {
        GetHotelDetailsAction.RV rq = new GetHotelDetailsAction.RV();
        rq.token = hotelInfo.getSearchToken();
        rq.data = hotelInfo.getID();
        showLoading();
        mActionManager.executeAction(new GetHotelDetailsAction(), rq, new WHApiActionCallback<HotelDetail>(this) {
            @Override
            public void onSuccess(HotelDetail responseValue) {
                super.onSuccess(responseValue);
                hideLoading();
                mView.show(responseValue);
            }

            @Override
            public void onError(ActionException e) {
                super.onError(e);
                hideLoading();
            }
        }, new ThirdPartyScheduler());
    }

}
