package vcc.com.wowholiday.presenter.hotelsearch.hoteloverview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.tabs.TabLayout;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseConstraintView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.model.hotel.HotelInfo;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;
import vcc.com.wowholiday.presenter.widget.GoogleMapView;
import vcc.com.wowholiday.presenter.widget.HashTagsView;
import vcc.com.wowholiday.presenter.widget.ImageThumbLayout;
import vcc.com.wowholiday.presenter.widget.RatioImageView;
import vcc.com.wowholiday.util.TimeFormatUtil;

public class HotelOverviewView extends BaseConstraintView {
    private NestedScrollView mNestedScrollView;
    private RatioImageView mRivFeatureImage;
    private ImageThumbLayout mImageThumbLayout;
    private TextView mTvHotelName;
    private HashTagsView mHashTagsView;
    private RatingBar mRatingBar;
    private TextView mTvRatingCount;
    private TextView mTvAddress;
    private View mVFacilitiesDivider;
    private TextView mTvFacilitiesDetail;
    private View mVMapDivider;
    private GoogleMapView mGmvMap;
    private TextView mTvMapAddress;
    private View mVReviewsDivider;
    private RecyclerView mRclvReviews;
    private TextView mTvDescription;
    private TabLayout mTabLayout;
    private ExpandableLayout mExpandableTabLayout;
    private TextView mTvActionBarName;
    private TextView mTvActionBarDetail;

    private ReviewsAdapter mAdapter;

    private IImageLoader mImageLoader = GlideImageLoader.getInstance();

    private HotelDetail mHotelDetail;
    private HotelSearchProperty mHotelSearchProperty;
    private HotelInfo mHotelInfo;

    public HotelOverviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private int mSelectedTab;

    @Override
    public void onInitView() {
        super.onInitView();
        Button btnViewAppropriateRoom = findViewById(R.id.hotel_overview_act_btn_view_appropriate_rooms);
        mNestedScrollView = findViewById(R.id.hotel_overview_act_nsv_wrapper);
        mRivFeatureImage = findViewById(R.id.hotel_overview_act_riv_feature_image);
        mImageThumbLayout = findViewById(R.id.hotel_overview_act_thumblayout);
        mTvHotelName = findViewById(R.id.hotel_overview_act_tv_hotel_name);
        mHashTagsView = findViewById(R.id.hotel_overview_act_hashtagview);
        mTvRatingCount = findViewById(R.id.hotel_overview_act_tv_rating_count);
        mRatingBar = findViewById(R.id.hotel_overview_act_rb_rating);
        mTvAddress = findViewById(R.id.hotel_overview_act_tv_location);
        mVFacilitiesDivider = findViewById(R.id.hotel_overview_act_v_divider_1);
        mTvFacilitiesDetail = findViewById(R.id.hotel_overview_act_tv_facilities_detail);
        mVMapDivider = findViewById(R.id.hotel_overview_act_v_divider_2);
        mGmvMap = findViewById(R.id.hotel_overview_act_gmv_map);
        mTvMapAddress = findViewById(R.id.hotel_overview_act_tv_map_address);
        mVReviewsDivider = findViewById(R.id.hotel_overview_act_v_divider_3);
        mTabLayout = findViewById(R.id.hotel_overview_act_tab_layout);
        mExpandableTabLayout = findViewById(R.id.hotel_overview_act_epl_tab_layout);
        mTvActionBarName = findViewById(R.id.action_bar_text_left);
        mTvActionBarDetail = findViewById(R.id.action_bar_sub_text_left);
        mTvDescription = findViewById(R.id.hotel_overview_act_tv_description_detail);

        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.photo));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.facility_provide));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.map));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.review));

        mTabLayout.getTabAt(1).select();

        mSelectedTab = 0;

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int selectedTab = mTabLayout.getSelectedTabPosition();
                if (mSelectedTab == selectedTab)
                    return;
                mSelectedTab = selectedTab;
                if (selectedTab == 0) {
                    mNestedScrollView.smoothScrollTo(0, 0);
                } else if (selectedTab == 1) {
                    mNestedScrollView.smoothScrollTo(0, mVFacilitiesDivider.getTop() - mTabLayout.getHeight());
                } else if (selectedTab == 2) {
                    mNestedScrollView.smoothScrollTo(0, mVMapDivider.getTop() - mTabLayout.getHeight());
                } else if (selectedTab == 3) {
                    mNestedScrollView.smoothScrollTo(0, mVReviewsDivider.getTop() - mTabLayout.getHeight());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == 0) {
                    mExpandableTabLayout.collapse();
                } else {
                    mExpandableTabLayout.expand();
                    if (scrollY >= mVReviewsDivider.getTop() - mTabLayout.getHeight()) {
                        selectTab(3);
                    } else if (scrollY >= mVMapDivider.getTop() - mTabLayout.getHeight()) {
                        selectTab(2);
                    } else if (scrollY >= mVFacilitiesDivider.getTop() - mTabLayout.getHeight()) {
                        selectTab(1);
                    } else {
                        selectTab(0);
                    }
                }
            }
        });


        mRivFeatureImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewImagesDetailCmd cmd = new ViewImagesDetailCmd();
                cmd.hotelDetail = mHotelDetail;
                mPresenter.executeCommand(cmd);
            }
        });

        mImageThumbLayout.setMaxDisplayItem(4);

        mImageThumbLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewImagesDetailCmd cmd = new ViewImagesDetailCmd();
                cmd.hotelDetail = mHotelDetail;
                mPresenter.executeCommand(cmd);
            }
        });

        mRclvReviews = findViewById(R.id.hotel_overview_act_rclv_reviews);
        mRclvReviews.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        mAdapter = new ReviewsAdapter();
        mRclvReviews.setAdapter(mAdapter);

        btnViewAppropriateRoom.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAppropriateRoomsCmd cmd = new ViewAppropriateRoomsCmd();
                cmd.hotelDetail = mHotelDetail;
                mPresenter.executeCommand(cmd);
            }
        });

        TextView tvFacilitiesDetail = findViewById(R.id.hotel_overview_act_tv_view_all_facilities);
        tvFacilitiesDetail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewFacilitiesDetailCmd cmd = new ViewFacilitiesDetailCmd();
                cmd.hotelDetail = mHotelDetail;
                mPresenter.executeCommand(cmd);
            }
        });

        TextView tvReviewsDetail = findViewById(R.id.hotel_overview_act_tv_view_all_reviews);
        tvReviewsDetail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewReviewsDetailCmd cmd = new ViewReviewsDetailCmd();
                cmd.hotelDetail = mHotelDetail;
                mPresenter.executeCommand(cmd);
            }
        });

        TextView tvDescriptionDetail = findViewById(R.id.hotel_overview_act_tv_view_all_description);
        tvDescriptionDetail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewDescriptionDetailCmd cmd = new ViewDescriptionDetailCmd();
                cmd.hotelDetail = mHotelDetail;
                mPresenter.executeCommand(cmd);
            }
        });
    }

    public void onInitMap(Bundle savedInstanceState) {
        mGmvMap.onInitView(savedInstanceState);
//        mGmvMap.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap googleMap) {
//                LatLng latLng = new LatLng(16.067033, 108.244460);
//                MarkerOptions marker = new MarkerOptions().position(latLng)
//                        .title("Tháp rùa");
//                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_place_24_px);
//                Bitmap b = bitmapdraw.getBitmap();
//                int markerSize = getResources().getDimensionPixelSize(R.dimen.dimen_24);
//                Bitmap smallMarker = Bitmap.createScaledBitmap(b, markerSize, markerSize, false);
//                marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
//                googleMap.addMarker(marker);
//                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//            }
//        });
    }

    private void selectTab(int position) {
        if (mSelectedTab != position) {
            mSelectedTab = position;
            mTabLayout.getTabAt(position).select();
        }
    }

    public void setHotelInfo(HotelInfo hotelInfo, HotelSearchProperty hotelSearchProperty) {
        mHotelSearchProperty = hotelSearchProperty;
        mHotelInfo = hotelInfo;
        mTvActionBarName.setText(Html.fromHtml(hotelInfo.getName()));
        mTvActionBarDetail.setText(getResources().getString(R.string.search_detail_format, TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getStartDate()), mHotelSearchProperty.getDuration(), mHotelSearchProperty.getRoomCount()));
    }

    public void show(final HotelDetail hotelDetail) {
        mHotelDetail = hotelDetail;
        if (mHotelDetail.getImages().size() > 0) {
            mImageLoader.loadRoundCornerImage(mRivFeatureImage, mHotelDetail.getImages().get(0).getUrl(), mRivFeatureImage, 16);
            List<String> url = new ArrayList<>();
            for (int i = 1; i < mHotelDetail.getImages().size(); i++) {
                url.add(mHotelDetail.getImages().get(i).getUrl());
            }
            mImageThumbLayout.showImage(url);
        }
        mTvHotelName.setText(Html.fromHtml(hotelDetail.getName()));
        mHashTagsView.setHashTags(new String[]{"5 sao", hotelDetail.getCategory()});
        mRatingBar.setRating(hotelDetail.getRating());
        mTvRatingCount.setText(getResources().getString(R.string.rating_format, hotelDetail.getRatingCount()));
        mTvAddress.setText(hotelDetail.getLocation().getAddress());
        mTvFacilitiesDetail.setText(getAmenitiesDetail(hotelDetail));

        mGmvMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng latLng = new LatLng(hotelDetail.getLocation().getLat(), hotelDetail.getLocation().getLong());
                MarkerOptions marker = new MarkerOptions().position(latLng)
                        .title(Html.fromHtml(hotelDetail.getName()).toString());
                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_place_24_px);
                Bitmap b = bitmapdraw.getBitmap();
                int markerSize = getResources().getDimensionPixelSize(R.dimen.dimen_24);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, markerSize, markerSize, false);
                marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                googleMap.addMarker(marker);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        });
        mTvMapAddress.setText(hotelDetail.getLocation().getAddress());
        mTvDescription.setText(Html.fromHtml(hotelDetail.getDescription()));

        mNestedScrollView.setVisibility(VISIBLE);
    }

    private String getAmenitiesDetail(HotelDetail hotelDetail) {
        List<Amenity> amenities = hotelDetail.getAmenities();
        if (amenities != null && amenities.size() > 0) {
            String amenitiesString = amenities.get(0).getName();
            for (int i = 1; i < amenities.size() - 1; i++) {
                amenitiesString += ", " + amenities.get(i).getName();
            }
            return amenitiesString;
        }

        return "";
    }

    public static class ViewAppropriateRoomsCmd implements ICommand {
        public HotelDetail hotelDetail;
    }

    public static class ViewImagesDetailCmd implements ICommand {
        public HotelDetail hotelDetail;
    }

    public static class ViewFacilitiesDetailCmd implements ICommand {
        public HotelDetail hotelDetail;
    }

    public static class ViewReviewsDetailCmd implements ICommand {
        public HotelDetail hotelDetail;
    }

    public static class ViewDescriptionDetailCmd implements ICommand {
        public HotelDetail hotelDetail;
    }
}
