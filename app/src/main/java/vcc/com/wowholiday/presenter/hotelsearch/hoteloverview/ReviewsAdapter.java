package vcc.com.wowholiday.presenter.hotelsearch.hoteloverview;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.BaseVHData;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Review;


public class ReviewsAdapter extends BaseRclvAdapter {
    private static final int REVIEW_PREVIEW_MAX_LINE = 5;

    public ReviewsAdapter() {
        super();
        test();
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_review;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new ReviewVH(itemView);
    }

    void test() {
        for (int i = 0; i < 5; i++) {
            mDataSet.add(new ReviewVHData());
        }
    }


    class ReviewVH extends BaseRclvHolder<ReviewVHData> {

        public ReviewVH(@NonNull View itemView) {
            super(itemView);
            final TextView tvReview = itemView.findViewById(R.id.review_itm_tv_review);
            tvReview.setMaxLines(REVIEW_PREVIEW_MAX_LINE);
            tvReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tvReview.getMaxLines() == REVIEW_PREVIEW_MAX_LINE) {
                        tvReview.setMaxLines(Integer.MAX_VALUE);
                    } else {
                        tvReview.setMaxLines(REVIEW_PREVIEW_MAX_LINE);
                    }
                }
            });
        }

        @Override
        public void onBind(ReviewVHData vhData) {
            super.onBind(vhData);
        }
    }

    class ReviewVHData extends BaseVHData<Review> {

    }
}
