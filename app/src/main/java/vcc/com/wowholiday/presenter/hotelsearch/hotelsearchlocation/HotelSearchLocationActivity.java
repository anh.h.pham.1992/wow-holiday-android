package vcc.com.wowholiday.presenter.hotelsearch.hotelsearchlocation;

import android.content.Intent;

import java.util.List;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.hotel.GetHotelSuggestionAction;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.model.hotel.HotelSuggestionList;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */

@Layout(R.layout.activity_hotel_search_location)
public class HotelSearchLocationActivity extends WHActivity<HotelSearchLocationView> {
    public static final String SUGGESTION_KEY = "SUGGESTION";
    public static final String RECENT_SEARCH = "RECENT_SEARCH";

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof HotelSearchLocationView.SearchCmd) {
            mActionManager.stopAction(GetHotelSuggestionAction.class);
            callSearchSuggestion(((HotelSearchLocationView.SearchCmd) command).keyword);
        } else if (command instanceof HotelSearchLocationView.SuggestionSelectedCmd) {
            selectLocation(((HotelSearchLocationView.SuggestionSelectedCmd) command).suggestion);
        } else if (command instanceof HotelSearchLocationAdapter.SuggestionSelectedCmd) {
            selectLocation(((HotelSearchLocationAdapter.SuggestionSelectedCmd) command).suggestion);
        }
    }

    private void selectLocation(HotelSuggestion suggestion) {
        Intent itn = new Intent();
        itn.putExtra(SUGGESTION_KEY, suggestion);
        setResult(RESULT_OK, itn);
        finish();
    }

    private void callSearchSuggestion(String keyword) {
        GetHotelSuggestionAction.RV rq = new GetHotelSuggestionAction.RV();
        rq.keyword = keyword;
        mActionManager.executeAction(new GetHotelSuggestionAction(), rq, new WHApiActionCallback<List<HotelSuggestionList>>(this){
            @Override
            public void onSuccess(List<HotelSuggestionList> responseValue) {
                super.onSuccess(responseValue);
                mView.showSuggestion(responseValue);
            }
        }, new ThirdPartyScheduler());
    }
}
