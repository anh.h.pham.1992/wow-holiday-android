package vcc.com.wowholiday.presenter.hotelsearch.hotelsearchlocation;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class HotelSearchLocationAdapter extends BaseRclvAdapter {
    private IPresenter mPresenter;

    public HotelSearchLocationAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_hotel_search_location;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new HotelSuggestionVH(itemView);
    }


    private class HotelSuggestionVH extends BaseRclvHolder<HotelSuggestion> {

        ImageView ivIcon;
        TextView tvName;
        TextView tvLocation;

        public HotelSuggestionVH(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.hotel_search_location_itm_iv_icon);
            tvName = itemView.findViewById(R.id.hotel_search_location_itm_tv_name);
            tvLocation = itemView.findViewById(R.id.hotel_search_location_itm_tv_location);
            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    SuggestionSelectedCmd cmd = new SuggestionSelectedCmd();
                    cmd.suggestion = (HotelSuggestion) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(HotelSuggestion vhData) {
            super.onBind(vhData);
            tvName.setText(vhData.getName());
            tvLocation.setText(vhData.getCountryID());
            if (vhData.getType() != null && vhData.getType().equalsIgnoreCase("location")) {
                ivIcon.setImageResource(R.drawable.ic_place_24_px);
            } else {
                ivIcon.setImageResource(R.drawable.ic_hotel_24px);
            }
        }
    }


    public static class SuggestionSelectedCmd implements ICommand {
        public HotelSuggestion suggestion;
    }
}
