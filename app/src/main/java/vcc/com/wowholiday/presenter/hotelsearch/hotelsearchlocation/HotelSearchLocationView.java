package vcc.com.wowholiday.presenter.hotelsearch.hotelsearchlocation;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.model.hotel.HotelSuggestionList;

import static vcc.com.wowholiday.model.hotel.HotelSuggestionList.TYPE_HOTEL;
import static vcc.com.wowholiday.model.hotel.HotelSuggestionList.TYPE_LOCATION;
import static vcc.com.wowholiday.model.hotel.HotelSuggestionList.TYPE_RECENT_SEARCH;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public class HotelSearchLocationView extends BaseLinearView {
    private final int TIME_DELAY_HANDLER_USER_TYPING = 50;

    private EditText mEdtSearch;
    private TextView mTvRecentSearch;
    private TextView mTvSearchSuggestion;
    private HotelSearchLocationAdapter mAdapter;
    private HotelSearchLocationAdapter mRecentSearchAdapter;

    private String mStrLastKeyword;
    private Runnable mSearchRunnable = new Runnable() {
        @Override
        public void run() {
            String keyword = mEdtSearch.getText().toString().trim();
            if (!keyword.equals(mStrLastKeyword)) {
                SearchCmd cmd = new SearchCmd();
                cmd.keyword = keyword;
                mPresenter.executeCommand(cmd);
                mStrLastKeyword = keyword;
            }
        }
    };

    public HotelSearchLocationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mStrLastKeyword = "";
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(mSearchRunnable);
    }

    @Override
    public void onInitView() {
        super.onInitView();
        LinearLayout llNearbyHotel = findViewById(R.id.hotel_search_location_act_ll_nearby_hotel);
        llNearbyHotel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SuggestionSelectedCmd cmd = new SuggestionSelectedCmd();
                HotelSuggestion suggestion = new HotelSuggestion();
                suggestion.setName("Khách sạn gần đây");
                cmd.suggestion = suggestion;
                mPresenter.executeCommand(cmd);
            }
        });

        mEdtSearch = findViewById(R.id.hotel_search_location_act_edt_search);
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                removeCallbacks(mSearchRunnable);
                String key = mEdtSearch.getText().toString().trim();
                if (TextUtils.isEmpty(key) || key.length() < 3) {
                    return;
                }
                postDelayed(mSearchRunnable, TIME_DELAY_HANDLER_USER_TYPING);
            }
        });

        mTvRecentSearch = findViewById(R.id.hotel_search_location_act_tv_recent_search);
        mTvSearchSuggestion = findViewById(R.id.hotel_search_location_act_tv_suggestion);

        RecyclerView rclvSuggestion = findViewById(R.id.hotel_search_location_act_rclv_suggestion);
        rclvSuggestion.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new HotelSearchLocationAdapter(mPresenter);
        rclvSuggestion.setAdapter(mAdapter);

        RecyclerView rclvRecentSearch = findViewById(R.id.hotel_search_location_act_rclv_recent_search);
        rclvRecentSearch.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecentSearchAdapter = new HotelSearchLocationAdapter(mPresenter);
        rclvRecentSearch.setAdapter(mRecentSearchAdapter);

        mEdtSearch.requestFocus();

        SearchCmd cmd = new SearchCmd();
        cmd.keyword = "";
        mPresenter.executeCommand(cmd);
        mStrLastKeyword = "";
    }

    public void showSuggestion(List<HotelSuggestionList> hotelSuggestion) {
        List<HotelSuggestion> hotelSuggestions = new ArrayList<>();
        List<HotelSuggestion> recentSearch = new ArrayList<>();
        for (HotelSuggestionList list :
                hotelSuggestion) {
            if (TYPE_LOCATION.equalsIgnoreCase(list.getType()) || TYPE_HOTEL.equalsIgnoreCase(list.getType())) {
                hotelSuggestions.addAll(list.getList());
            } else if (TYPE_RECENT_SEARCH.equalsIgnoreCase(list.getType())) {
                recentSearch.addAll(list.getList());
            }
        }
//        mTvSearchSuggestion.setVisibility(hotelSuggestions.size() > 0 ? VISIBLE : GONE);
//        mTvRecentSearch.setVisibility(recentSearch.size() > 0 ? VISIBLE : GONE);
        mAdapter.reset(hotelSuggestions);
        mRecentSearchAdapter.reset(recentSearch);
    }

    public static class SuggestionSelectedCmd implements ICommand {
        HotelSuggestion suggestion;
    }

    public static class SearchCmd implements ICommand {
        public String keyword;
    }
}
