package vcc.com.wowholiday.presenter.hotelsearch.previewbooking;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.hotelsearch.eticket.HotelTicketActivity;

@Layout(R.layout.activity_preview_booking)
public class PreviewBookingActivity extends WHActivity<PreviewBookingView> {
    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof PreviewBookingView.ProceedToPaymentCmd) {
            Intent itn = new Intent(this, HotelTicketActivity.class);
            startActivity(itn);
        }
    }
}
