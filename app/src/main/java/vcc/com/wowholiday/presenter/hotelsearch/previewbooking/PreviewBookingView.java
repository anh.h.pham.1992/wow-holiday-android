package vcc.com.wowholiday.presenter.hotelsearch.previewbooking;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;

public class PreviewBookingView extends BaseRelativeView {
    public PreviewBookingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.hotel_overview_act_btn_proceed_to_payment).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new ProceedToPaymentCmd());
            }
        });
    }

    public static class ProceedToPaymentCmd implements ICommand {}
}
