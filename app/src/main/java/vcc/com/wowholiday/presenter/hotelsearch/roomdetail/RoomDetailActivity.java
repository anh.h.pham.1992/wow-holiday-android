package vcc.com.wowholiday.presenter.hotelsearch.roomdetail;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.hotelsearch.fillhotelbooking.FillHotelBookingActivity;
import vcc.com.wowholiday.presenter.hotelsearch.roomdetail.photo.RoomPhotosActivity;

@Layout(R.layout.activity_room_detail)
public class RoomDetailActivity extends WHActivity<RoomDetailView> {

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof RoomDetailView.ViewAllImagesCmd) {
            gotoRoomImagesActivity();
        } else if (command instanceof RoomDetailView.OnRoomBookingCmd) {
            gotoFillHotelBookingActivity();
        }
    }

    private void gotoRoomImagesActivity() {
        Intent itn = new Intent(this, RoomPhotosActivity.class);
        startActivity(itn);
    }

    private void gotoFillHotelBookingActivity() {
        Intent itn = new Intent(this, FillHotelBookingActivity.class);
        startActivity(itn);
    }
}
