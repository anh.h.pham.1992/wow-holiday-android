package vcc.com.wowholiday.presenter.hotelsearch.roomdetail;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseConstraintView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Room;
import vcc.com.wowholiday.presenter.widget.FacilitiesView;
import vcc.com.wowholiday.presenter.widget.ImageCarousel;

public class RoomDetailView extends BaseConstraintView {
    static String[] facilities = new String[]{"Bữa sáng miễn phí", "TV màn hình lớn", "Wifi miễn phí", "Điều hòa 2 chiều"};
    static String[] bathroomFacilities = new String[]{"Máy sấy", "Bình nước nóng", "Bồn tắm", "Vòi sen"};
    static String[] imagesUrl = new String[]{
            "https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768",
            "https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768",
            "https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768"};
    private Room mRoom;

    public RoomDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRoom = new Room();
        mRoom.setName("Deluxe King Room");
        mRoom.setAvatarUrl("https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768");
        mRoom.setFacilities(facilities);
        mRoom.setBathroomFacilities(bathroomFacilities);
        mRoom.setPrice(4799445);
        mRoom.setPriceAfterDiscount(3099445);
        mRoom.setCurrency("VNĐ");
    }

    @Override
    public void onInitView() {
        super.onInitView();
        FacilitiesView fvRoomFacilities = findViewById(R.id.room_detail_act_fv_room_facilities);
        FacilitiesView fvBathroomFacilities = findViewById(R.id.room_detail_act_fv_bathroom_facilities);
        ImageCarousel imageCarousel = findViewById(R.id.room_detail_act_imgc_images);
        TextView tvOriginalPrice = findViewById(R.id.room_detail_act_tv_original_price);
        fvRoomFacilities.setFacilities(mRoom.getFacilities());
        fvBathroomFacilities.setFacilities(mRoom.getBathroomFacilities());
        imageCarousel.setImageUrls(imagesUrl);
        imageCarousel.setOnViewAllClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new ViewAllImagesCmd());
            }
        });

        tvOriginalPrice.setPaintFlags(tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        findViewById(R.id.room_detail_act_btn_book_now).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new OnRoomBookingCmd());
            }
        });
    }

    public static class ViewAllImagesCmd implements ICommand {
    }

    public static class OnRoomBookingCmd implements ICommand {
    }
}
