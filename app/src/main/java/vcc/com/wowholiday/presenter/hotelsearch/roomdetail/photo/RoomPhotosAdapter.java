package vcc.com.wowholiday.presenter.hotelsearch.roomdetail.photo;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;

public class RoomPhotosAdapter extends BaseRclvAdapter {

    private IOnPhotoSelectedListener mListener;
    private IImageLoader mImageLoader = GlideImageLoader.getInstance();

    public RoomPhotosAdapter(IOnPhotoSelectedListener listener) {
        mListener = listener;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_hotel_photo;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new ImageVH(itemView);
    }


    private class ImageVH extends BaseRclvHolder<String> {
        ImageView ivPhoto;

        public ImageVH(@NonNull View itemView) {
            super(itemView);
            ivPhoto = itemView.findViewById(R.id.hotel_photo_itm_iv_photo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onSelected((String) mDataSet.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }

        @Override
        public void onBind(String vhData) {
            super.onBind(vhData);
            mImageLoader.loadImage(itemView, vhData, ivPhoto);
        }
    }


    public interface IOnPhotoSelectedListener {
        void onSelected(String photoUrl, int position);
    }
}
