package vcc.com.wowholiday.presenter.hotelsearch.roomdetail.photo;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.TopPaddingDecorator;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;

public class RoomPhotosView extends BaseLinearView {

    private RoomPhotosAdapter mAdapter;

    public RoomPhotosView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        RecyclerView rclv = findViewById(R.id.room_photo_act_rclv_content);
        rclv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
        rclv.addItemDecoration(new TopPaddingDecorator(padding));
        rclv.addItemDecoration(new SpaceItemDecoration(padding));

        mAdapter = new RoomPhotosAdapter(new RoomPhotosAdapter.IOnPhotoSelectedListener() {
            @Override
            public void onSelected(String photoUrl, int position) {
                RoomPhotosDialog dialog = new RoomPhotosDialog(dummy(), position);
                dialog.show(((AppCompatActivity)getContext()).getSupportFragmentManager(), "view_photo");
            }
        });
        rclv.setAdapter(mAdapter);
        mAdapter.reset(dummy());
    }

    List<String> dummy() {
        List<String> list = new ArrayList<>();;
        list.add("https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768");
        list.add("https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768");
        list.add("https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768");
        return list;
    }
}
