package vcc.com.wowholiday.presenter.hotelsearch.roomselect;


import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.presenter.hotelsearch.fillhotelbooking.FillHotelBookingActivity;
import vcc.com.wowholiday.presenter.hotelsearch.roomdetail.RoomDetailActivity;

@Layout(R.layout.activity_room_select)
public class RoomSelectActivity extends WHActivity<RoomSelectView> {
    public static final String HOTEL_DETAIL = "HOTEL_DETAIL";
    public static final String HOTEL_SEARCH_PROPERTIES = "HOTEL_SEARCH_PROPERTIES";

    private HotelSearchProperty mHotelSearchProperty;

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        HotelDetail hotelDetail = getIntent().getParcelableExtra(HOTEL_DETAIL);
        mHotelSearchProperty = getIntent().getParcelableExtra(HOTEL_SEARCH_PROPERTIES);
        mView.setHotelDetail(hotelDetail, mHotelSearchProperty);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof RoomSelectAdapter.RoomSelectedCmd) {
            goToBookingActivity();
        } else if (command instanceof RoomSelectAdapter.RoomDetailCmd) {
            goToRoomDetailActivity();
        }
    }

    private void goToBookingActivity() {
        Intent itn = new Intent(this, FillHotelBookingActivity.class);
        startActivity(itn);
    }

    private void goToRoomDetailActivity() {
        Intent itn = new Intent(this, RoomDetailActivity.class);
        startActivity(itn);
    }
}
