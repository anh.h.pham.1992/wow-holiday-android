package vcc.com.wowholiday.presenter.hotelsearch.roomselect;

import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.hotel.RoomInfo;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.FacilitiesView;
import vcc.com.wowholiday.presenter.widget.recyclerview.LoadMoreAdapter;

public class RoomSelectAdapter extends LoadMoreAdapter {

    private static final int ROOM_TYPE = 1;
    private IPresenter mPresenter;
    private IImageLoader mImageLoader = GlideImageLoader.getInstance();

    public RoomSelectAdapter(IPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataSet.get(position) instanceof LoadMoreAdapter.LoadingVHData) {
            return LOADING_TYPE;
        } else {
            return ROOM_TYPE;
        }
    }

    @Override
    public int getLayoutResource(int viewType) {
        if (viewType == ROOM_TYPE) {
            return R.layout.item_available_room;
        }
        return super.getLayoutResource(viewType);
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        if (viewType == ROOM_TYPE) {
            return new RoomVH(itemView);
        }
        return super.onCreateVH(itemView, viewType);
    }

    private class RoomVH extends BaseRclvHolder<RoomInfo> {
        private TextView mTvRoomName;
        private ImageView mIvRoomAvatar;
        private FacilitiesView mFacilitiesView;
        private TextView mTvOriginalPrice;
        private TextView mTvPriceAfterDiscount;
        private View mVDivider2;

        public RoomVH(@NonNull View itemView) {
            super(itemView);
            mTvRoomName = itemView.findViewById(R.id.item_available_room_tv_room_name);
            mIvRoomAvatar = itemView.findViewById(R.id.item_available_room_iv_avatar);
            mFacilitiesView = itemView.findViewById(R.id.item_available_room_fv_facilities);
            mVDivider2 = itemView.findViewById(R.id.item_available_room_v_divider_2);
            mTvOriginalPrice = itemView.findViewById(R.id.item_available_room_tv_original_price);
            mTvOriginalPrice.setPaintFlags(mTvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mTvPriceAfterDiscount = itemView.findViewById(R.id.item_available_room_tv_price_after_discount);
            // TODO facilities
            itemView.findViewById(R.id.item_available_room_btn_select).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    RoomSelectedCmd cmd = new RoomSelectedCmd();
                    cmd.room = (RoomInfo) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
            itemView.findViewById(R.id.item_available_room_iv_right_arrow).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    RoomDetailCmd cmd = new RoomDetailCmd();
                    cmd.room = (RoomInfo) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(RoomInfo vhData) {
            super.onBind(vhData);
            List<Amenity> amenities = vhData.getAmenities();
            mFacilitiesView.setFacilities(amenities);
            if (amenities != null && amenities.size() > 0) {
                mFacilitiesView.setVisibility(View.VISIBLE);
                mVDivider2.setVisibility(View.VISIBLE);
            } else {
                mFacilitiesView.setVisibility(View.GONE);
                mVDivider2.setVisibility(View.GONE);
            }

            mTvRoomName.setText(vhData.getName());
            if (vhData.getAvatar() != null)
                mImageLoader.loadRoundCornerImage(mIvRoomAvatar, vhData.getAvatar().getUrl(), mIvRoomAvatar, 16, IImageLoader.CornerType.TOP);
            else
                mImageLoader.loadRoundCornerImage(mIvRoomAvatar, null, mIvRoomAvatar, 16, IImageLoader.CornerType.TOP);

            mTvOriginalPrice.setText(MoneyFormatter.defaultFormat(String.format("%.0f", vhData.getOriginalPrice())) + " " + vhData.getCurrencyUnit());
            mTvPriceAfterDiscount.setText(MoneyFormatter.defaultFormat(String.format("%.0f", vhData.getPrice())) + " " + vhData.getCurrencyUnit());
        }
    }

    public static class RoomDetailCmd implements ICommand {
        public RoomInfo room;
    }

    public static class RoomSelectedCmd implements ICommand {
        public RoomInfo room;
    }

}
