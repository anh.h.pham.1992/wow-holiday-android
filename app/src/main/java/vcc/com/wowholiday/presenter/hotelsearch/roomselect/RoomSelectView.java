package vcc.com.wowholiday.presenter.hotelsearch.roomselect;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.Room;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.presenter.widget.recyclerview.WrapRecyclerView;
import vcc.com.wowholiday.util.TimeFormatUtil;

public class RoomSelectView extends BaseLinearView {
    private TextView mTvActionBarName;
    private TextView mTvActionBarDetail;
    WrapRecyclerView mRclvRooms;
    private RoomSelectAdapter mAdapter;

    public RoomSelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        super.onInitView();
        mRclvRooms = findViewById(R.id.room_select_act_rclv_rooms);
        mTvActionBarName = findViewById(R.id.action_bar_text_left);
        mTvActionBarDetail = findViewById(R.id.action_bar_sub_text_left);
        mRclvRooms.setNoItemImage(R.drawable.ic_domain_24_px);
        mRclvRooms.setNoItemText(R.string.no_room_was_found);

        mAdapter = new RoomSelectAdapter(mPresenter);
        mRclvRooms.setAdapter(mAdapter);
    }

    List<Room> dummy() {
        List<Room> result = new ArrayList<>();
        String[] facilities = new String[]{"Bữa sáng miễn phí", "Không hoàn tiền", "Không thay đổi lịch đã đặt", "Điều hòa 2 chiều"};
        for (int i = 0; i < 20; i++) {
            Room room = new Room();
            room.setName("Deluxe King Room");
            room.setAvatarUrl("https://pix6.agoda.net/hotelImages/984/984078/984078_18092513370068146601.jpg?s=1024x768");
            if (i % 3 != 0)
                room.setFacilities(facilities);
            room.setPrice(4799445);
            room.setPriceAfterDiscount(3099445);
            room.setCurrency("VNĐ");
            result.add(room);
        }
        return result;
    }

    public void setHotelDetail(HotelDetail hotelDetail, HotelSearchProperty mHotelSearchProperty) {
        mTvActionBarName.setText(Html.fromHtml(hotelDetail.getName()));
        mTvActionBarDetail.setText(getResources().getString(R.string.search_detail_format, TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getStartDate()), mHotelSearchProperty.getDuration(), mHotelSearchProperty.getRoomCount()));
        mRclvRooms.reset(hotelDetail.getRooms());
    }
}
