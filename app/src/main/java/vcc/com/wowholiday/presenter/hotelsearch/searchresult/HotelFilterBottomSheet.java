package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import net.cachapa.expandablelayout.ExpandableLayout;

import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.util.NumberFormatTextWatcher;
import vcc.com.wowholiday.presenter.widget.CheckBoxGroupView;
import vcc.com.wowholiday.presenter.widget.EditTextWithLabel;
import vcc.com.wowholiday.presenter.widget.ExpandableLayoutHeader;
import vcc.com.wowholiday.presenter.widget.RangeSeekBarView;
import vcc.com.wowholiday.presenter.widget.RatingCheckBoxGroupView;

public class HotelFilterBottomSheet extends WHBottomSheetDialog {

    private HotelFilterProperty mHotelFilterProperty;
    private EditTextWithLabel mEdtPriceFrom;
    private EditTextWithLabel mEdtPriceTo;
    private RangeSeekBarView mRsbPrice;
    private RatingCheckBoxGroupView mRcbgReview;
    private CheckBoxGroupView mCbgHotelType;
    private CheckBoxGroupView mCbgFacilities;

    public HotelFilterBottomSheet(HotelFilterProperty hotelFilterProperty) {
        mHotelFilterProperty = hotelFilterProperty;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_hotel_filter, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEdtPriceFrom = view.findViewById(R.id.hotel_filter_dlg_edt_price_from);
        mEdtPriceTo = view.findViewById(R.id.hotel_filter_dlg_edt_price_to);
        mRsbPrice = view.findViewById(R.id.hotel_filter_dlg_sb_price);
        final ExpandableLayout eplPrice = view.findViewById(R.id.hotel_filter_dlg_epl_hotel_price);
        final ExpandableLayoutHeader eplhPrice = view.findViewById(R.id.hotel_filter_dlg_eplh_hotel_price);

        mRcbgReview = view.findViewById(R.id.hotel_filter_dlg_rcbg_review);
        final ExpandableLayout eplReview = view.findViewById(R.id.hotel_filter_dlg_epl_review);
        final ExpandableLayoutHeader eplhReview = view.findViewById(R.id.hotel_filter_dlg_eplh_review);

        mCbgHotelType = view.findViewById(R.id.hotel_filter_dlg_cbg_hotel_types);
        final ExpandableLayout eplHotelType = view.findViewById(R.id.hotel_filter_dlg_epl_hotel_types);
        final ExpandableLayoutHeader eplhHotelType = view.findViewById(R.id.hotel_filter_dlg_eplh_hotel_types);

        mCbgFacilities = view.findViewById(R.id.hotel_filter_dlg_cbg_facilities);
        final ExpandableLayout eplFacilities = view.findViewById(R.id.hotel_filter_dlg_epl_facilities);
        final ExpandableLayoutHeader eplhFacilities = view.findViewById(R.id.hotel_filter_dlg_eplh_facilities);

        final TextView tvReset = view.findViewById(R.id.hotel_filter_dlg_tv_reset);
        final TextView tvApply = view.findViewById(R.id.hotel_filter_dlg_tv_apply);

        mEdtPriceFrom.addTextChangedListener(new NumberFormatTextWatcher(mEdtPriceFrom.getEditText()));
        mEdtPriceTo.addTextChangedListener(new NumberFormatTextWatcher(mEdtPriceTo.getEditText()));

        mRsbPrice.setOnRangeSeekBarChangeListener(new RangeSeekBarView.OnRangeSeekBarChangeListener() {
            @Override
            public void onValueChanged(int minThumbValue, int maxThumbValue) {
                mEdtPriceFrom.setText(Integer.toString(minThumbValue));
                mEdtPriceTo.setText(Integer.toString(maxThumbValue));
            }
        });

        eplhPrice.setExpandableLayout(eplPrice);
        eplhReview.setExpandableLayout(eplReview);
        eplhHotelType.setExpandableLayout(eplHotelType);
        eplhFacilities.setExpandableLayout(eplFacilities);

        tvReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHotelFilterProperty.reset();
                initValues();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apply();
                dismiss();
            }
        });

        initValues();
    }

    @Override
    public void onResume() {
        super.onResume();
        initValues();
    }

    public void setHotelFilterProperty(HotelFilterProperty hotelFilterProperty) {
        this.mHotelFilterProperty = hotelFilterProperty;
    }

    void initValues() {
        int priceFrom = mHotelFilterProperty.getPriceFrom();
        int priceTo = mHotelFilterProperty.getPriceTo();
        int priceStep = mHotelFilterProperty.getPriceStep();
        int selectedPriceFrom = mHotelFilterProperty.getSelectedPriceFrom();
        int selectedPriceTo = mHotelFilterProperty.getSelectedPriceTo();
        int maxRating = mHotelFilterProperty.getMaxRating();
        Integer[] selectedRatings = mHotelFilterProperty.getSelectedRatings();
        HotelFilterProperty.Property[] hotelTypes = mHotelFilterProperty.getAvailableHotelTypes();
        HotelFilterProperty.Property[] selectedHotelTypes = mHotelFilterProperty.getSelectedHotelTypes();
        HotelFilterProperty.Property[] facilityTypes = mHotelFilterProperty.getAvailableFacilities();
        HotelFilterProperty.Property[] selectedFacilityTypes = mHotelFilterProperty.getSelectedFacilities();

        if(mEdtPriceFrom !=null) {
            mEdtPriceFrom.setText(Integer.toString(selectedPriceFrom));
            mEdtPriceTo.setText(Integer.toString(selectedPriceTo));
            mRsbPrice.setRange(priceFrom, priceTo);
            mRsbPrice.setMinThumbValue(selectedPriceFrom);
            mRsbPrice.setMaxThumbValue(selectedPriceTo);
            mRsbPrice.setStep(priceStep);
            mRcbgReview.setNumberOfStar(maxRating);
            mRcbgReview.setSelectedStars(selectedRatings);

            CheckBoxGroupView.Value<Integer>[] hotelTypeValues = convertFromPropertyToValue(hotelTypes, false);
            CheckBoxGroupView.Value<Integer>[] hotelTypeCheckedValues = convertFromPropertyToValue(selectedHotelTypes, true);

            mCbgHotelType.setValues(hotelTypeValues);
            Gson g = new Gson();
            mCbgHotelType.setCheckedValues(hotelTypeCheckedValues);


            CheckBoxGroupView.Value<Integer>[] facilityValues = convertFromPropertyToValue(facilityTypes, false);
            CheckBoxGroupView.Value<Integer>[] facilityCheckedValues = convertFromPropertyToValue(selectedFacilityTypes, true);
            mCbgFacilities.setValues(facilityValues);
            mCbgFacilities.setCheckedValues(facilityCheckedValues);
        }
    }

    private void apply() {
        int selectedPriceFrom = mRsbPrice.getMinThumbValue();
        int selectedPriceTo = mRsbPrice.getMaxThumbValue();
        Integer[] selectedRatings = mRcbgReview.getSelectedStars();
        CheckBoxGroupView.Value[] selectedHotelTypes = mCbgHotelType.getCheckedValues();
        CheckBoxGroupView.Value[] selectedFacilities = mCbgFacilities.getCheckedValues();

        mHotelFilterProperty.setSelectedPriceFrom(selectedPriceFrom);
        mHotelFilterProperty.setSelectedPriceTo(selectedPriceTo);
        mHotelFilterProperty.setSelectedRatings(selectedRatings);
        mHotelFilterProperty.clearSelectedHotelType();
        for (CheckBoxGroupView.Value<Integer> value :
                selectedHotelTypes) {
            mHotelFilterProperty.addSelectedHotelType(new HotelFilterProperty.Property<>(value.getLabel(), value.getValue()));
        }
        mHotelFilterProperty.clearSelectedFacilities();
        for (CheckBoxGroupView.Value<Integer> value :
                selectedFacilities) {
            mHotelFilterProperty.addSelectedFacility(new HotelFilterProperty.Property<>(value.getLabel(), value.getValue()));
        }

    }

    private <T> CheckBoxGroupView.Value<T>[] convertFromPropertyToValue(HotelFilterProperty.Property<T>[] properties, boolean checked) {
        if (properties == null)
            return null;
        CheckBoxGroupView.Value<T>[] values = new CheckBoxGroupView.Value[properties.length];
        for (int i = 0; i < properties.length; i++) {
            HotelFilterProperty.Property<T> property = properties[i];
            values[i] = new CheckBoxGroupView.Value<>(property.getName(), property.getValue(), checked);
        }
        return values;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();

                // set bottom sheet height
                int maxHeight = (int) (displayMetrics.heightPixels * 0.9);
                behavior.setPeekHeight(maxHeight);
                bottomSheet.getLayoutParams().height = maxHeight;
                bottomSheet.requestLayout();
            }
        });
        return dialog;
    }
}
