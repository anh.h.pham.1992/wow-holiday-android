package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class HotelFilterProperty {
    static public class Property<T> {
        private String mName;
        private T mValue;

        public Property(String name, T value) {
            this.mName = name;
            this.mValue = value;
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            if (!(obj instanceof Property))
                return false;
            return  mValue != null && mValue.equals(((Property) obj).getValue());
        }

        public String getName() {
            return mName;
        }

        public void setName(String mName) {
            this.mName = mName;
        }

        public T getValue() {
            return mValue;
        }

        public void setValue(T mValue) {
            this.mValue = mValue;
        }
    }

    private int mPriceFrom;
    private int mPriceTo;
    private int mPriceStep;
    private int mMaxRating;
    private Property<Integer>[] mAvailableHotelTypes;
    private Property<Integer>[] mAvailableFacilities;

    private int mSelectedPriceFrom;
    private int mSelectedPriceTo;
    private ArrayList<Integer> mSelectedRatings;
    private ArrayList<Property<Integer>> mSelectedHotelTypes;
    private ArrayList<Property<Integer>> mSelectedFacilities;

    public HotelFilterProperty() {
        mSelectedRatings = new ArrayList<>();
        mSelectedHotelTypes = new ArrayList<>();
        mSelectedFacilities = new ArrayList<>();
    }

    public void reset() {
        mSelectedPriceFrom = mPriceFrom;
        mSelectedPriceTo = mPriceTo;
        mSelectedRatings.clear();
        mSelectedHotelTypes.clear();
        mSelectedFacilities.clear();
    }

    public boolean isHasChanges() {
        return mSelectedPriceFrom != mPriceFrom
                || mSelectedPriceTo != mPriceTo
                || mSelectedRatings.size() != 0
                || mSelectedHotelTypes.size() != 0
                || mSelectedFacilities.size() != 0;
    }

    public int getSelectedPriceFrom() {
        return mSelectedPriceFrom;
    }

    public void setSelectedPriceFrom(int mPriceFrom) {
        this.mSelectedPriceFrom = mPriceFrom;
    }

    public int getSelectedPriceTo() {
        return mSelectedPriceTo;
    }

    public void setSelectedPriceTo(int mPriceTo) {
        this.mSelectedPriceTo = mPriceTo;
    }

    public Integer[] getSelectedRatings() {
        return mSelectedRatings.toArray(new Integer[mSelectedRatings.size()]);
    }

    public void setSelectedRatings(Integer[] ratings) {
        mSelectedRatings.clear();
        for (Integer star :
                ratings) {
            mSelectedRatings.add(star);
        }
    }

    public void addSelectedRating(int rating) {
        if (!this.mSelectedRatings.contains(rating))
            mSelectedRatings.add(rating);
    }

    public void removeSelectedRating(Property<Integer> rating) {
        mSelectedRatings.remove(rating);
    }

    public Property<Integer>[] getSelectedHotelTypes() {
        return mSelectedHotelTypes.toArray(new Property[mSelectedHotelTypes.size()]);
    }

    public void addSelectedHotelType(Property<Integer> hotelType) {
        if (!this.mSelectedHotelTypes.contains(hotelType))
            mSelectedHotelTypes.add(hotelType);
    }

    public void removeSelectedHotelType(Property<Integer> hotelType) {
        mSelectedHotelTypes.remove(hotelType);
    }

    public void clearSelectedHotelType() {
        mSelectedHotelTypes.clear();
    }

    public Property<Integer>[] getSelectedFacilities() {
        return mSelectedFacilities.toArray(new Property[mSelectedFacilities.size()]);
    }

    public void addSelectedFacility(Property<Integer> facility) {
        if (!this.mSelectedFacilities.contains(facility))
            mSelectedFacilities.add(facility);
    }

    public void removeSelectedFacility(Property<Integer> facility) {
        mSelectedFacilities.remove(facility);
    }

    public void clearSelectedFacilities() {
        mSelectedFacilities.clear();
    }

    public int getPriceFrom() {
        return mPriceFrom;
    }

    public void setPriceFrom(int priceFrom) {
        this.mPriceFrom = priceFrom;
    }

    public int getPriceTo() {
        return mPriceTo;
    }

    public void setPriceTo(int mPriceTo) {
        this.mPriceTo = mPriceTo;
    }

    public int getPriceStep() {
        return mPriceStep;
    }

    public void setPriceStep(int priceStep) {
        this.mPriceStep = priceStep;
    }

    public int getMaxRating() {
        return mMaxRating;
    }

    public void setMaxRating(int maxRating) {
        this.mMaxRating = maxRating;
    }

    public Property<Integer>[] getAvailableHotelTypes() {
        return mAvailableHotelTypes;
    }

    public void setAvailableHotelTypes(Property<Integer>[] hotelTypes) {
        this.mAvailableHotelTypes = hotelTypes;
    }

    public Property<Integer>[] getAvailableFacilities() {
        return mAvailableFacilities;
    }

    public void setAvailableFacilities(Property<Integer>[] facilities) {
        this.mAvailableFacilities = facilities;
    }
}
