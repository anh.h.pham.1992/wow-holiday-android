package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.hotel.HotelSearchAction;
import vcc.com.wowholiday.model.hotel.HotelInfo;
import vcc.com.wowholiday.model.hotel.HotelSearchDTO;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.presenter.hotelsearch.hoteloverview.HotelOverviewActivity;
import vcc.com.wowholiday.presenter.hotelsearch.hotelsearchlocation.HotelSearchLocationActivity;

import static vcc.com.wowholiday.presenter.hotelsearch.hoteloverview.HotelOverviewActivity.HOTEL_INFO;

@Layout(R.layout.activity_hotel_search_result)
public class HotelSearchResultActivity extends WHActivity<HotelSearchResultView> {
    public static final String HOTEL_SEARCH_PROPERTIES = "HOTEL_SEARCH_PROPERTIES";
    private static final int LOC_SEARCH_REQ = 2345;

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof HotelSearchResultAdapter.HotelSelectedCmd) {
            gotoHotelOverviewActivity(((HotelSearchResultAdapter.HotelSelectedCmd) command).hotelInfo, ((HotelSearchResultAdapter.HotelSelectedCmd) command).hotelSearchProperty);
        } else if (command instanceof HotelSearchResultView.SearchLocCmd) {
            gotoLocationActivity(((HotelSearchResultView.SearchLocCmd) command).recentSearch);
        } else if (command instanceof HotelSearchResultView.SearchCmd) {
            doSearch(((HotelSearchResultView.SearchCmd) command).hotelSearchProperty);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOC_SEARCH_REQ && resultCode == RESULT_OK) {
            mView.setLocation((HotelSuggestion) data.getParcelableExtra(HotelSearchLocationActivity.SUGGESTION_KEY));
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        HotelSearchProperty hotelSearchProperty = getIntent().getParcelableExtra(HOTEL_SEARCH_PROPERTIES);
        if (hotelSearchProperty != null)
            mView.setHotelSearchProperty(hotelSearchProperty);
    }

    private void gotoLocationActivity(HotelSuggestion recentSearch) {
        Intent itn = new Intent(this, HotelSearchLocationActivity.class);
        itn.putExtra(HotelSearchLocationActivity.RECENT_SEARCH, recentSearch);
        startActivityForResult(itn, LOC_SEARCH_REQ);
    }

    private void gotoHotelOverviewActivity(HotelInfo hotelInfo, HotelSearchProperty hotelSearchProperty) {
        Intent itn = new Intent(this, HotelOverviewActivity.class);
        itn.putExtra(HOTEL_INFO, hotelInfo);
        itn.putExtra(HotelOverviewActivity.HOTEL_SEARCH_PROPERTIES, hotelSearchProperty);
        startActivity(itn);
    }

    private void doSearch(final HotelSearchProperty property) {
        mActionManager.stopAction(HotelSearchAction.class);
        HotelSearchAction.HotelSearchRV rv = new HotelSearchAction.HotelSearchRV();
        rv.property = property;
        mActionManager.executeAction(new HotelSearchAction(), rv, new WHApiActionCallback<HotelSearchDTO>(this) {

            @Override
            public void onStart() {
                super.onStart();
                showLoading();
            }

            @Override
            public void onSuccess(HotelSearchDTO responseValue) {
                super.onSuccess(responseValue);
                mView.showHotels(responseValue, property);
            }
        }, new ThirdPartyScheduler());
    }
}
