package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.DecimalFormat;

import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelInfo;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;
import vcc.com.wowholiday.presenter.widget.RatingView;
import vcc.com.wowholiday.presenter.widget.recyclerview.LoadMoreAdapter;

public class HotelSearchResultAdapter extends LoadMoreAdapter {
    private IPresenter mPresenter;
    private IImageLoader mImageLoader = GlideImageLoader.getInstance();
    private HotelSearchProperty hotelSearchProperty;

    public HotelSearchResultAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_hotel;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new HotelVH(itemView);
    }

    public void setHotelSearchProperty(HotelSearchProperty hotelSearchProperty) {
        this.hotelSearchProperty = hotelSearchProperty;
    }

    class HotelVH extends BaseRclvHolder<HotelInfo> {
        private ImageView ivAvatar;
        private TextView tvStar;
        private TextView tvName;
        private RatingView rvRating;
        private TextView tvRatingCount;
        private TextView tvLocation;
        private TextView tvPrice;
        private TextView tvOriginalPrice;

        public HotelVH(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.item_hotel_iv_avatar);
            tvStar = itemView.findViewById(R.id.item_hotel_tv_star);
            tvName = itemView.findViewById(R.id.item_hotel_tv_name);
            rvRating = itemView.findViewById(R.id.item_hotel_view_rating);
            tvRatingCount = itemView.findViewById(R.id.item_hotel_tv_review_count);
            tvLocation = itemView.findViewById(R.id.item_hotel_tv_location);
            tvPrice = itemView.findViewById(R.id.item_hotel_tv_price);
            tvOriginalPrice = itemView.findViewById(R.id.item_hotel_tv_original_price);
        }

        @Override
        public void onBind(final HotelInfo vhData) {
            super.onBind(vhData);
            mImageLoader.loadRoundCornerImage(ivAvatar, vhData.getAvatarUrl(), ivAvatar, 16, IImageLoader.CornerType.LEFT);
            tvName.setText(Html.fromHtml(vhData.getName()));
            rvRating.setRating(vhData.getRating());
            tvRatingCount.setText(itemView.getResources().getString(R.string.rating_format, vhData.getRatingCount()));
            tvLocation.setText(vhData.getLocation().getAddress());
            tvPrice.setText(String.format("%s VNĐ", MoneyFormatter.defaultFormat(new DecimalFormat("#.##").format(vhData.getPrice()))));
            tvOriginalPrice.setText(String.format("%s VNĐ", MoneyFormatter.defaultFormat(new DecimalFormat("#.##").format(vhData.getOriginalPrice()))));


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HotelSelectedCmd cmd = new HotelSelectedCmd();
                    cmd.hotelInfo = vhData;
                    cmd.hotelSearchProperty = hotelSearchProperty;
                    mPresenter.executeCommand(cmd);
                }
            });

        }
    }

    public static class HotelSelectedCmd implements ICommand {
        public HotelSearchProperty hotelSearchProperty;
        public HotelInfo hotelInfo;
    }
}
