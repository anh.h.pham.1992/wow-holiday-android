package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import net.cachapa.expandablelayout.ExpandableLayout;

import common.presenter.WHBottomSheetDialog;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.hotel.HotelSearchDTO;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorBottomSheet;
import vcc.com.wowholiday.presenter.dialog.countselector.CountSelectorItem;
import vcc.com.wowholiday.presenter.hotelsearch.CalendarBottomSheet;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;
import vcc.com.wowholiday.presenter.widget.recyclerview.WrapRecyclerView;
import vcc.com.wowholiday.util.TimeFormatUtil;

public class HotelSearchResultView extends BaseRelativeView {
    private static HotelFilterProperty.Property<Integer>[] hotelTypes = new HotelFilterProperty.Property[]{
            new HotelFilterProperty.Property("Chỗ ở gia đình", 0),
            new HotelFilterProperty.Property("Căn hộ", 1),
            new HotelFilterProperty.Property("Villa", 2),
            new HotelFilterProperty.Property("Sang trọng", 3),
            new HotelFilterProperty.Property("Bờ sông", 4),
    };

    private HotelFilterProperty.Property<Integer>[] hotelFacilities = new HotelFilterProperty.Property[]{
            new HotelFilterProperty.Property("Trả tiền tại khách sạn", 0),
            new HotelFilterProperty.Property("Ăn sáng miễn phí", 1),
            new HotelFilterProperty.Property("Wifi miễn phí", 2),
            new HotelFilterProperty.Property("Có bãi đậu xe", 3),
            new HotelFilterProperty.Property("Có nhà hàng", 4),
            new HotelFilterProperty.Property("Lễ tân 24/7", 5),
            new HotelFilterProperty.Property("Có thang máy", 6),
            new HotelFilterProperty.Property("Đưa đón ra sân bay", 7),
    };

    private HotelSortProperty.SortType[] sortTypes = new HotelSortProperty.SortType[]{
            new HotelSortProperty.SortType("Phổ biến nhất", 0),
            new HotelSortProperty.SortType("Giá thấp nhất", 1),
            new HotelSortProperty.SortType("Giá cao nhất", 2),
            new HotelSortProperty.SortType("Được đánh giá cao nhất", 3),
    };

    private HotelSearchResultAdapter mAdapter;
    private HotelFilterProperty mHotelFilterProperty = new HotelFilterProperty();
    private HotelSortProperty mHotelSortProperty = new HotelSortProperty();
    private HotelSearchProperty mHotelSearchProperty = new HotelSearchProperty();
    private ImageView mIvFilterAnnotation;
    private ExpandableLayout mEplSearchPanel;
    private TextViewWithIcon mTvLocation;
    private TextViewWithIcon mTvStartDate;
    private TextViewWithIcon mTvEndDate;
    private TextView mTvDuration;
    private TextViewWithIcon mTvPersonRoom;
    private CalendarBottomSheet mCalendarBottomSheet;
    private CountSelectorBottomSheet mCountSelectorBottomSheet;
    private HotelFilterBottomSheet mHotelFilterBottomSheet;
    private HotelSortBottomSheet mHotelSortBottomSheet;
    private TextView mTvSearchLocation;
    private TextView mTvSearchDetail;
    private WrapRecyclerView mRclvHotels;


    public HotelSearchResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHotelFilterProperty.setAvailableHotelTypes(hotelTypes);
        mHotelFilterProperty.setAvailableFacilities(hotelFacilities);
        mHotelFilterProperty.setMaxRating(5);
        mHotelFilterProperty.setPriceFrom(10000000);
        mHotelFilterProperty.setPriceTo(15000000);
        mHotelFilterProperty.setPriceStep(1000);
        mHotelFilterProperty.reset();
        mHotelSortProperty.setAvailableSortTypes(sortTypes);
        mHotelSortProperty.setSelectedSortType(sortTypes[0]);
    }

    @Override
    public void onInitView() {
        super.onInitView();
        mRclvHotels = findViewById(R.id.hotel_search_result_act_rclv_result);
        final ViewGroup btnFilter = findViewById(R.id.hotel_search_result_act_rl_filter);
        final ViewGroup btnSort = findViewById(R.id.hotel_search_result_act_rl_sort);
        final TextView btnSearchToggle = findViewById(R.id.action_bar_text_right);
        final TextViewWithIcon tvStartDate = findViewById(R.id.hotel_search_result_act_tv_start_date);
        final TextViewWithIcon tvEndDate = findViewById(R.id.hotel_search_result_act_tv_end_date);
        final TextView tvDur = findViewById(R.id.hotel_search_result_act_tv_dur);
        mIvFilterAnnotation = findViewById(R.id.hotel_search_result_act_iv_filter_annotation);
        mEplSearchPanel = findViewById(R.id.hotel_search_result_act_epl_search_panel);
        mTvLocation = findViewById(R.id.hotel_search_result_act_tv_loc);
        mTvStartDate = findViewById(R.id.hotel_search_result_act_tv_start_date);
        mTvEndDate = findViewById(R.id.hotel_search_result_act_tv_end_date);
        mTvDuration = findViewById(R.id.hotel_search_result_act_tv_dur);
        mTvDuration = findViewById(R.id.hotel_search_result_act_tv_dur);
        mTvPersonRoom = findViewById(R.id.hotel_search_result_act_tv_people_room);
        mTvSearchLocation = findViewById(R.id.action_bar_text_left);
        mTvSearchDetail = findViewById(R.id.action_bar_sub_text_left);

        mRclvHotels.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        mAdapter = new HotelSearchResultAdapter(mPresenter);
        mRclvHotels.setAdapter(mAdapter);

        mIvFilterAnnotation.setVisibility(mHotelFilterProperty.isHasChanges() ? VISIBLE : GONE);

        mHotelFilterBottomSheet = new HotelFilterBottomSheet(mHotelFilterProperty);
        mHotelFilterBottomSheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
            @Override
            public void onDismiss() {
                mIvFilterAnnotation.setVisibility(mHotelFilterProperty.isHasChanges() ? VISIBLE : GONE);
            }
        });
        btnFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mHotelFilterBottomSheet.setHotelFilterProperty(mHotelFilterProperty);
                if (!mHotelFilterBottomSheet.isAdded())
                    mHotelFilterBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), "filter");
            }
        });

        mHotelSortBottomSheet = new HotelSortBottomSheet(mHotelSortProperty);
        mHotelSortBottomSheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });
        btnSort.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mHotelSortBottomSheet.setHotelSortProperty(mHotelSortProperty);
                if (!mHotelSortBottomSheet.isAdded())
                    mHotelSortBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), "sort");
            }
        });

        btnSearchToggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mEplSearchPanel.toggle();
            }
        });

        // search function
        mCalendarBottomSheet = new CalendarBottomSheet(mHotelSearchProperty);
        mCalendarBottomSheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
            @Override
            public void onDismiss() {
                tvStartDate.setText(TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getStartDate()));
                tvEndDate.setText(TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getEndDate()));
                tvDur.setText(getResources().getString(R.string.duration_format, mHotelSearchProperty.getDuration()));
            }
        });

        tvStartDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mHotelSearchProperty.calculateDefaultDates();
                mCalendarBottomSheet.setHotelSearchProperty(mHotelSearchProperty);
                if (!mCalendarBottomSheet.isAdded())
                    mCalendarBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), CalendarBottomSheet.TAG_START_DATE);
            }
        });

        tvEndDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mHotelSearchProperty.calculateDefaultDates();
                mCalendarBottomSheet.setHotelSearchProperty(mHotelSearchProperty);
                if (!mCalendarBottomSheet.isAdded())
                    mCalendarBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), CalendarBottomSheet.TAG_END_DATE);
            }
        });

        final TextViewWithIcon tvPersonRoom = findViewById(R.id.hotel_search_result_act_tv_people_room);
        mCountSelectorBottomSheet = new CountSelectorBottomSheet();
        mCountSelectorBottomSheet.setOnCountSelectedListener(new CountSelectorBottomSheet.IOnCountSelectListener() {
            @Override
            public void onCountChange(TextView tvTitle, int rowIndex, CountSelectorItem item) {
                if (rowIndex == 0) {
                    tvTitle.setText(getResources().getString(R.string.person_count_format, item.count));
                    mHotelSearchProperty.setPersonCount(item.count);
                } else {
                    tvTitle.setText(getResources().getString(R.string.room_count_format, item.count));
                    mHotelSearchProperty.setRoomCount(item.count);
                }
            }
        });
        mCountSelectorBottomSheet.setTitle(R.string.person_room_count);
        mCountSelectorBottomSheet.setOnDismissListener(new WHBottomSheetDialog.IOnDismissListener() {
            @Override
            public void onDismiss() {
                tvPersonRoom.setText(getResources().getString(R.string.person_room_count_format,
                        mHotelSearchProperty.getPersonCount(), mHotelSearchProperty.getRoomCount()));
            }
        });
        tvPersonRoom.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountSelectorBottomSheet.addItems(createSelectorItem());
                if (!mCountSelectorBottomSheet.isAdded())
                    mCountSelectorBottomSheet.show(((AppCompatActivity) mPresenter).getSupportFragmentManager(), "person_room");
            }
        });

        mTvLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchLocCmd cmd = new SearchLocCmd();
                cmd.recentSearch = mHotelSearchProperty.getLocation();
                mPresenter.executeCommand(cmd);
            }
        });

        findViewById(R.id.hotel_search_result_act_bt_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mEplSearchPanel.toggle();
                mTvSearchLocation.setText(mHotelSearchProperty.getLocation().getName());
                mTvSearchDetail.setText(getResources().getString(R.string.search_detail_format, TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getStartDate()), mHotelSearchProperty.getDuration(), mHotelSearchProperty.getRoomCount()));
                mAdapter.setHotelSearchProperty(mHotelSearchProperty);
                SearchCmd cmd = new SearchCmd();
                cmd.hotelSearchProperty = mHotelSearchProperty;
                mPresenter.executeCommand(cmd);
            }
        });
    }

    public void setLocation(HotelSuggestion location) {
        if (location != null) {
            mTvLocation.setText(location.getName());
        }
        mHotelSearchProperty.setLocation(location);
    }

    public void setHotelSearchProperty(HotelSearchProperty hotelSearchProperty) {
        mHotelSearchProperty = hotelSearchProperty;
        if (mHotelSearchProperty.getLocation() != null) {
            mTvLocation.setText(mHotelSearchProperty.getLocation().getName());
            mTvSearchLocation.setText(mHotelSearchProperty.getLocation().getName());
        }

        mTvSearchDetail.setText(getResources().getString(R.string.search_detail_format, TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getStartDate()), mHotelSearchProperty.getDuration(), mHotelSearchProperty.getRoomCount()));

        mTvStartDate.setText(TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getStartDate()));
        mTvEndDate.setText(TimeFormatUtil.getDefaultDateFormat(mHotelSearchProperty.getEndDate()));
        mTvDuration.setText(getResources().getString(R.string.duration_format, mHotelSearchProperty.getDuration()));
        mTvPersonRoom.setText(getResources().getString(R.string.person_room_count_format,
                mHotelSearchProperty.getPersonCount(), mHotelSearchProperty.getRoomCount()));

        mAdapter.setHotelSearchProperty(mHotelSearchProperty);

        SearchCmd cmd = new SearchCmd();
        cmd.hotelSearchProperty = mHotelSearchProperty;
        mPresenter.executeCommand(cmd);
    }

    public void showHotels(HotelSearchDTO responseValue, HotelSearchProperty property) {
        mRclvHotels.reset(responseValue.hotelInfoList);
    }

    private CountSelectorItem[] createSelectorItem() {
        CountSelectorItem[] items = new CountSelectorItem[2];
        CountSelectorItem person = new CountSelectorItem();
        person.max = 30;
        person.min = 1;
        person.count = mHotelSearchProperty.getPersonCount();
        items[0] = person;
        CountSelectorItem room = new CountSelectorItem();
        room.max = 8;
        room.min = 1;
        room.count = mHotelSearchProperty.getRoomCount();
        items[1] = room;
        return items;
    }

    public static class SearchLocCmd implements ICommand {
        public HotelSuggestion recentSearch;
    }

    public static class SearchCmd implements ICommand {
        public HotelSearchProperty hotelSearchProperty;
    }
}
