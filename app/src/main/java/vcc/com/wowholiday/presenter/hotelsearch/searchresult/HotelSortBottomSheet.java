package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

import common.presenter.WHBottomSheetDialog;
import vcc.com.wowholiday.R;

public class HotelSortBottomSheet extends WHBottomSheetDialog {
    private HotelSortProperty mHotelSortProperty;
    private RadioGroup mRgSortType;
    private HashMap<Integer, HotelSortProperty.SortType> mSortTypeByViewId;

    public HotelSortBottomSheet(HotelSortProperty hotelSortProperty) {
        mHotelSortProperty = hotelSortProperty;
        mSortTypeByViewId = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_hotel_sort, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRgSortType = view.findViewById(R.id.hotel_sort_dlg_rg_sort_types);
        mRgSortType.setGravity(Gravity.CENTER_HORIZONTAL);
//        mRgSortType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//
//            }
//        });
        initValues();
    }

    @Override
    public void onResume() {
        super.onResume();
        initValues();
    }

    void initValues() {
        HotelSortProperty.SortType[] sortTypes = mHotelSortProperty.getAvailableSortTypes();
        if (mRgSortType != null) {
            mRgSortType.removeAllViews();
            mSortTypeByViewId.clear();
            if (sortTypes != null) {
                for (int i = 0; i < sortTypes.length; i++) {
                    final HotelSortProperty.SortType sortType = sortTypes[i];
                    int id = View.generateViewId();
                    RadioButton radioButton = new RadioButton(this.getContext());
                    radioButton.setId(id);
                    radioButton.setButtonDrawable(R.drawable.selector_check_mark);
                    radioButton.setBackground(null);
                    radioButton.setTextColor(getResources().getColorStateList(R.color.color_dark_turquoise_blue_selector));
                    radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_size_14));
                    radioButton.setText(sortType.getName());
                    radioButton.setChecked(sortType.equals(mHotelSortProperty.getSelectedSortType()));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    int padding = getResources().getDimensionPixelSize(R.dimen.dimen_16);
                    radioButton.setPadding(0, padding, 0, padding);
                    radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                apply(sortType);
                                dismiss();
                            }
                        }
                    });
                    mRgSortType.addView(radioButton, layoutParams);

                    if (i < sortTypes.length - 1) {
                        View divider = new View(getContext());
                        divider.setBackgroundColor(getResources().getColor(R.color.pale_grey));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                getResources().getDimensionPixelSize(R.dimen.dimen_1));
                        mRgSortType.addView(divider, params);
                    }

                    mSortTypeByViewId.put(id, sortType);
                }
            }
        }
    }

    private void apply(HotelSortProperty.SortType sortType) {
        mHotelSortProperty.setSelectedSortType(sortType);
    }

    public void setHotelSortProperty(HotelSortProperty hotelSortProperty) {
        this.mHotelSortProperty = hotelSortProperty;
    }
}
