package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class HotelSortProperty {
    private SortType<Integer>[] mAvailableSortTypes;
    private SortType<Integer> mSelectedSortType;

    public HotelSortProperty() {
    }

    public void reset() {
        mSelectedSortType = (mAvailableSortTypes != null && mAvailableSortTypes.length > 0) ? mAvailableSortTypes[0] : null;
    }

    public SortType<Integer>[] getAvailableSortTypes() {
        return mAvailableSortTypes;
    }

    public void setAvailableSortTypes(SortType<Integer>[] availableSortTypes) {
        this.mAvailableSortTypes = availableSortTypes;
        reset();
    }

    public SortType<Integer> getSelectedSortType() {
        return mSelectedSortType;
    }

    public void setSelectedSortType(SortType<Integer> selectedSortType) {
        this.mSelectedSortType = selectedSortType;
    }

    static public class SortType<T> {
        private String mName;
        private T mValue;

        public SortType(String name, T value) {
            this.mName = name;
            this.mValue = value;
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            if (!(obj instanceof SortType))
                return false;
            return mValue != null && mValue.equals(((SortType) obj).getValue());
        }

        public String getName() {
            return mName;
        }

        public void setName(String mName) {
            this.mName = mName;
        }

        public T getValue() {
            return mValue;
        }

        public void setValue(T mValue) {
            this.mValue = mValue;
        }

        @NonNull
        @Override
        public String toString() {
            return mName;
        }
    }
}
