package vcc.com.wowholiday.presenter.hotelsearch.searchresult;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import vcc.com.wowholiday.R;

public class ItemHotelView extends ConstraintLayout {

    private TextView mTvOriginalPrice;

    public ItemHotelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTvOriginalPrice = findViewById(R.id.item_hotel_tv_original_price);
        mTvOriginalPrice.setPaintFlags(mTvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.ItemHotelView, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.ItemHotelView_itm_hotel_compound_layout)) {
                int layoutRes = ta.getResourceId(R.styleable.ItemHotelView_itm_hotel_compound_layout, -1);
                if (layoutRes != -1) {
                    compound(layoutRes);
                }
            }
            ta.recycle();
        }
    }

    private void compound(int layoutRes) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(layoutRes, this, true);
    }
}
