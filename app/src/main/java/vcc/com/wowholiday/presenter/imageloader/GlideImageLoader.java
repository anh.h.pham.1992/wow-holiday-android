package vcc.com.wowholiday.presenter.imageloader;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by Pham Hai Quang on 11/10/2019.
 */
public class GlideImageLoader implements IImageLoader {

    private static GlideImageLoader sInstance;

    public static GlideImageLoader getInstance() {
        if (sInstance == null) {
            sInstance = new GlideImageLoader();
        }
        return sInstance;
    }

    private GlideImageLoader(){}

    public void loadImage(Activity activity, String url, ImageView view) {
        Glide.with(activity).load(url).into(view);
    }

    public void loadImage(Fragment fragment, String url, ImageView view) {
        Glide.with(fragment).load(url).into(view);
    }

    @Override
    public void loadImage(View withView, String url, ImageView view) {
        Glide.with(withView).load(url).into(view);
    }

    @Override
    public void loadRoundCornerImage(View withView, String url, ImageView view, int corner) {
        Glide.with(withView).load(url).apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(corner))).into(view);
    }

    @Override
    public void loadRoundCornerImage(View withView, String url, ImageView view, int corner, CornerType cornerType) {
        Glide.with(withView).load(url).apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCornersTransformation(corner, 0, RoundedCornersTransformation.CornerType.valueOf(cornerType.toString())))).into(view);
    }

    @Override
    public void loadCircleImage(View withView, String url, ImageView view) {
        Glide.with(withView).load(url).
                apply(RequestOptions.circleCropTransform()).into(view);
    }

    @Override
    public void loadCircleImage(View withView, File file, ImageView view) {
        Glide.with(withView).load(file).
                apply(RequestOptions.circleCropTransform()).into(view);
    }

    @Override
    public void loadCircleImage(View withView, Bitmap bitmap, ImageView view) {
        Glide.with(withView).load(bitmap).
                apply(RequestOptions.circleCropTransform()).into(view);
    }
}
