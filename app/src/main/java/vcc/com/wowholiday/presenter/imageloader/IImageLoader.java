package vcc.com.wowholiday.presenter.imageloader;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import java.io.File;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by Pham Hai Quang on 11/10/2019.
 */
public interface IImageLoader {
    public static enum CornerType {
        ALL,
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        TOP,
        BOTTOM,
        LEFT,
        RIGHT,
        OTHER_TOP_LEFT,
        OTHER_TOP_RIGHT,
        OTHER_BOTTOM_LEFT,
        OTHER_BOTTOM_RIGHT,
        DIAGONAL_FROM_TOP_LEFT,
        DIAGONAL_FROM_TOP_RIGHT
    }

    void loadImage(Activity activity, String url, ImageView view);
    void loadImage(Fragment fragment, String url, ImageView view);
    void loadImage(View withView, String url, ImageView view);
    void loadRoundCornerImage(View withView, String url, ImageView view, int corner);
    void loadRoundCornerImage(View withView, String url, ImageView view, int corner, CornerType cornerType);
    void loadCircleImage(View withView, String url, ImageView view);
    void loadCircleImage(View withView, File file, ImageView view);
    void loadCircleImage(View withView, Bitmap bitmap, ImageView view);
}
