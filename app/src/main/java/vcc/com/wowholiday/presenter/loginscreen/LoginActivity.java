package vcc.com.wowholiday.presenter.loginscreen;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.ActionException;
import quangph.com.mvp.mvp.action.scheduler.AsyncTaskScheduler;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.domain.usecase.LoginEmailOrPhoneAction;
import vcc.com.wowholiday.domain.usecase.SaveUserAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidEmailOrPhoneNumberAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidPasswordAction;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.register.RegisterActivity;
import vcc.com.wowholiday.repository.api.APIException;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */

@Layout(R.layout.activity_login)
public class LoginActivity extends WHActivity<LoginView> {

    public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";
    public static final int REGISTER_SUCCESS = 302;

    @Override
    protected void onViewWillCreate(Bundle savedState) {
        super.onViewWillCreate(savedState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 980 && resultCode == RESULT_OK) {
            setResult(REGISTER_SUCCESS, data);
            finish();
        }
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof LoginView.LoginCmd) {
            doLogin(((LoginView.LoginCmd) command).name, ((LoginView.LoginCmd) command).passwords);
        } else if (command instanceof LoginView.CreateNewAccountCmd) {
            gotoRegisterScreen();
        }
    }

    private void doLogin(String emailOrPhone, String pass) {
        if (validEmailOrPhone(emailOrPhone) && validPasswords(pass)) {
            LoginEmailOrPhoneAction.LoginEmailRV rv = new LoginEmailOrPhoneAction.LoginEmailRV();
            rv.emailOrPhone = emailOrPhone;
            rv.password = pass;
            showLoading();
            mActionManager.executeAction(new LoginEmailOrPhoneAction(), rv, new WHApiActionCallback<User>(this){
                @Override
                public void onSuccess(User responseValue) {
                    super.onSuccess(responseValue);
                    saveUser(responseValue);
                    Intent itn = new Intent();
                    itn.putExtra(LOGIN_SUCCESS, responseValue);
                    setResult(RESULT_OK, itn);
                    finish();
                }

                @Override
                protected void onException(Context context, APIException e) {
                    super.onException(context, e);
                    showAlertError();
                }
            }, new ThirdPartyScheduler());
        }
    }

    private boolean validEmailOrPhone(String emailOrPhone) {
        ValidEmailOrPhoneNumberAction.ValidEmailOrPhoneReq req
                = new ValidEmailOrPhoneNumberAction.ValidEmailOrPhoneReq();
        req.emailOrPhone = emailOrPhone;

        ValidEmailOrPhoneNumberAction action = new ValidEmailOrPhoneNumberAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            mView.showPhoneOrEmailError(getResources().getString(R.string.phone_or_email_format_error));
            return false;
        }
    }

    private boolean validPasswords(String passwords) {
        ValidPasswordAction.ValidPasswordReq req = new ValidPasswordAction.ValidPasswordReq();
        req.password = passwords;
        ValidPasswordAction action = new ValidPasswordAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            mView.showPasswordError(getResources().getString(R.string.password_length_error,
                    AppConfig.PASSWORD_LENGTH_MIN));
            return false;
        }
    }

    private void gotoRegisterScreen() {
        Intent itn = new Intent(this, RegisterActivity.class);
        startActivityForResult(itn, 980);
    }

    private void saveUser(User user) {
        SaveUserAction.SaveUserRv rv = new SaveUserAction.SaveUserRv();
        rv.user = user;
        mActionManager.executeAction(new SaveUserAction(), rv, null, new AsyncTaskScheduler());
    }

    private void showAlertError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Email/Số điện thoại hoặc mật khẩu chưa chính xác");
        builder.setCancelable(true);

        builder.setPositiveButton(
                "Thử lại",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
