package vcc.com.wowholiday.presenter.loginscreen;

import android.content.Context;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputLayout;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.presenter.util.SpannableBuilder;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class LoginView extends BaseLinearView {

    private EditText mEdtPass;
    private EditText mEdtPhoneOrEmail;
    private TextInputLayout mTilName;
    private TextInputLayout mTilPass;

    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        initHasNotAccount();
        mEdtPass = findViewById(R.id.login_act_edt_pass);
        mEdtPhoneOrEmail = findViewById(R.id.login_act_edt_name);
        mTilName = findViewById(R.id.login_act_til_name);
        mTilPass = findViewById(R.id.login_act_til_pass);

        findViewById(R.id.login_act_iv_visibility).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                boolean isSelected = v.isSelected();
                ImageView iv = (ImageView) v;
                iv.setColorFilter(isSelected ? ContextCompat.getColor(getContext(), R.color.light_burgundy)
                        : ContextCompat.getColor(getContext(), R.color.light_grey));
                mEdtPass.setTransformationMethod(isSelected ? null : (new PasswordTransformationMethod()));
            }
        });

        findViewById(R.id.login_act_btn_login).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validInfo()) {
                    LoginCmd cmd = new LoginCmd();
                    cmd.name = mEdtPhoneOrEmail.getText().toString();
                    cmd.passwords = mEdtPass.getText().toString();
                    mPresenter.executeCommand(cmd);
                }
            }
        });
    }

    public void showPasswordError(String error) {
        mTilPass.setError(error);
    }

    public void showPhoneOrEmailError(String error) {
        mTilName.setError(error);
    }

    private void initHasNotAccount() {
        TextView tvHasNotAccount = findViewById(R.id.login_act_tv_has_not_account);
        SpannableBuilder builder = new SpannableBuilder(getResources().getString(R.string.has_not_account_yet));
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                mPresenter.executeCommand(new CreateNewAccountCmd());
            }
        };
        builder.appendText(getResources().getString(R.string.create_new))
                .withSpan(new UnderlineSpan())
                .withSpan(clickable);
        tvHasNotAccount.setText(builder.getSpannedText());
        tvHasNotAccount.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private boolean validInfo() {
        boolean valid = true;
        String name = mEdtPhoneOrEmail.getText().toString();
        if (TextUtils.isEmpty(name)) {
            valid = false;
            showPhoneOrEmailError(getResources().getString(R.string.empty_error));
        }

        if (TextUtils.isEmpty(mEdtPass.getText().toString())) {
            valid = false;
            showPasswordError(getResources().getString(R.string.password_length_error, AppConfig.PASSWORD_LENGTH_MIN));
        }
        return valid;
    }

    public static class LoginCmd implements ICommand {
        public String name;
        public String passwords;
    }

    public static class CreateNewAccountCmd implements ICommand{}
}
