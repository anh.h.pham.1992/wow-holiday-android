package vcc.com.wowholiday.presenter.mainscreen;

import androidx.lifecycle.ViewModelProviders;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.BuildUserFromLocalAction;
import vcc.com.wowholiday.model.User;

@Layout(R.layout.activity_main)
public class MainActivity extends WHActivity<MainView> {

    private MainViewModel mViewModel;

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        getSavedUser();
        /*//Test
        UserData user = new UserData();
        user.setFullName("Pham Hai Quang");
        mViewModel.user.setValue(user);*/
    }

    @Override
    protected void onPostInit() {
        super.onPostInit();
        mView.doLoadViewStub();
    }

    private void getSavedUser() {
        mActionManager.executeAction(new BuildUserFromLocalAction(), new WHApiActionCallback<User>(this) {
            @Override
            public void onSuccess(User responseValue) {
                super.onSuccess(responseValue);
                mViewModel.user.setValue(responseValue);
            }
        });
    }
}
