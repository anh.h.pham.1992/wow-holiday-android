package vcc.com.wowholiday.presenter.mainscreen;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

import vcc.com.wowholiday.presenter.mainscreen.accountscreen.AccountFragment;
import vcc.com.wowholiday.presenter.mainscreen.bookingscreen.BookingFragment;
import vcc.com.wowholiday.presenter.mainscreen.homescreen.HomeFragment;
import vcc.com.wowholiday.presenter.mainscreen.inboxscreen.InboxFragment;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class MainContentVPAdapter extends FragmentPagerAdapter {

    public MainContentVPAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeFragment();
            case 1:
                return new BookingFragment();
            case 2:
                return new AccountFragment();
        }
        throw new IllegalArgumentException("Cannot create fragment for position: " + position);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
