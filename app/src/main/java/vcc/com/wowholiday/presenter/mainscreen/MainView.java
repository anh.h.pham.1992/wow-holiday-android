package vcc.com.wowholiday.presenter.mainscreen;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.NonSwipeViewpager;

/**
 * Created by Pham Hai Quang on 10/8/2019.
 */
public class MainView extends BaseFrameView {

    private BottomNavigationView mBnvNaviagation;
    private NonSwipeViewpager mVpContent;

    private int mCurrTabIndex;

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void doLoadViewStub() {
        ViewStub stub = findViewById(R.id.main_act_vstub);
        View vs = stub.inflate();
        mVpContent = vs.findViewById(R.id.main_act_home_vp_content);
        mVpContent.setOffscreenPageLimit(4);
        mVpContent.setAdapter(new MainContentVPAdapter(((AppCompatActivity)mPresenter)
                .getSupportFragmentManager()));

        mBnvNaviagation = vs.findViewById(R.id.main_act_bottom_navigation);
        mBnvNaviagation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                mCurrTabIndex = getIndexFromMenuItem(menuItem.getItemId());
                mVpContent.setCurrentItem(mCurrTabIndex, true);
                return true;
            }
        });
    }

    private int getIndexFromMenuItem(int menuID) {
        int index = -1;
        switch (menuID) {
            case R.id.bottom_nav_home:
                index = 0;
                break;
            case R.id.bottom_nav_booking:
                index = 1;
                break;
            case R.id.bottom_nav_account:
                index = 2;
                break;
        }
        return index;
    }
}
