package vcc.com.wowholiday.presenter.mainscreen;

import androidx.lifecycle.MutableLiveData;

import quangph.com.mvp.viewmodel.NotifyViewModel;
import vcc.com.wowholiday.model.User;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public class MainViewModel extends NotifyViewModel {
    public MutableLiveData<User> user = new MutableLiveData<>();
}
