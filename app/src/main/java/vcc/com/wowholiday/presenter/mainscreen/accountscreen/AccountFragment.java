package vcc.com.wowholiday.presenter.mainscreen.accountscreen;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import common.presenter.WHTabFragment;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.Action;
import quangph.com.mvp.mvp.action.scheduler.AsyncTaskScheduler;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.LogoutAction;
import vcc.com.wowholiday.model.ContactInfo;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.webview.WebViewActivity;
import vcc.com.wowholiday.presenter.loginscreen.LoginActivity;
import vcc.com.wowholiday.presenter.mainscreen.MainViewModel;
import vcc.com.wowholiday.presenter.profileinfo.ProfileInfoActivity;
import vcc.com.wowholiday.presenter.register.RegisterActivity;
import vcc.com.wowholiday.presenter.mycoupon.MyCouponActivity;
import vcc.com.wowholiday.presenter.updatedetail.ProfileDetailActivity;

/**
 * Created by Pham Hai Quang on 10/10/2019.
 */
public class AccountFragment extends WHTabFragment<AccountView> {

    private static final int LOGIN_RQ = 3456;
    private static final int PROFILE_DETAIL_RQ = 2356;

    private MainViewModel mViewModel;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_RQ) {
            if (resultCode == Activity.RESULT_OK) {
                mViewModel.user.setValue((User) data.getParcelableExtra(LoginActivity.LOGIN_SUCCESS));
            } else if (resultCode == LoginActivity.REGISTER_SUCCESS) {
                mViewModel.user.setValue((User) data.getParcelableExtra(ProfileInfoActivity.REGISTER_SUCCESS));
                Toast.makeText(getActivity(), "Đăng kí thành công", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                mViewModel.user.setValue((User) data.getParcelableExtra(ProfileInfoActivity.REGISTER_SUCCESS));
                Toast.makeText(getActivity(), "Đăng kí thành công", Toast.LENGTH_SHORT).show();
            } else if (resultCode == Activity.RESULT_FIRST_USER) {
                gotoLogin();
            }
        } else if (requestCode == PROFILE_DETAIL_RQ && resultCode == Activity.RESULT_OK) {
            mViewModel.user.setValue((User) data.getParcelableExtra(ProfileDetailActivity.USER));
            Toast.makeText(getActivity(), "Cập nhật thông tin thành công", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_account;
    }

    @Override
    protected void onShow(boolean isShowing, int flag) {
        super.onShow(isShowing, flag);

        mViewModel.notifyActive(this, isShowing);
        //dummy();
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        mViewModel.bind(this).register(mViewModel.user, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                mView.userUI(user);
            }
        });

        mView.userUI(null);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof AccountView.EditInfoCmd) {
            gotoProfileDetail();
        } else if (command instanceof AccountView.LoginCmd) {
            gotoLogin();
        } else if (command instanceof AccountView.RegisterCmd) {
            gotoRegister();
        } else if (command instanceof AccountView.MyCouponCmd) {
            Intent itn = new Intent(getContext(), MyCouponActivity.class);
            startActivity(itn);
        } else if (command instanceof AccountView.HelpCenterCmd) {
            Intent itn = new Intent(getContext(), WebViewActivity.class);
            WebViewActivity.createParams(itn, getString(R.string.help_center));
            startActivity(itn);
        } else if (command instanceof AccountView.LogoutCmd) {
            logout();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewModel.unbind(this);
    }

    private void gotoProfileDetail() {
        Intent itn = new Intent(getContext(), ProfileDetailActivity.class);
        ProfileDetailActivity.buildParam(itn, mViewModel.user.getValue());
        startActivityForResult(itn, PROFILE_DETAIL_RQ);
    }

    private void gotoLogin() {
        Intent itn = new Intent(getContext(), LoginActivity.class);
        startActivityForResult(itn, LOGIN_RQ);
    }

    private void gotoRegister() {
        Intent itn = new Intent(getContext(), RegisterActivity.class);
        startActivityForResult(itn, 1000);
    }

    private void logout() {
        showLoading();
        mActionManager.executeAction(new LogoutAction(), new Action.VoidRequest(), new Action.SimpleActionCallback<Boolean>(){
            @Override
            public void onSuccess(Boolean responseValue) {
                super.onSuccess(responseValue);
                hideLoading();
                mViewModel.user.setValue(null);
            }
        }, new AsyncTaskScheduler());
    }

    void dummy() {
        User user = new User();
        user.setLastName("ABC");
        user.setFirstName("def");

        ContactInfo contactInfo = new ContactInfo();
        contactInfo.setEmail("sad@gmail.com");
        user.setContactInfo(contactInfo);
        user.setGender(User.MALE);
        user.setBirthday("1990-09-01T00:00:00");
        mViewModel.user.setValue(user);
    }
}
