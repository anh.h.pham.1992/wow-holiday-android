package vcc.com.wowholiday.presenter.mainscreen.accountscreen;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.widget.TextViewWithIcon;

/**
 * Created by Pham Hai Quang on 10/10/2019.
 */
public class AccountView extends BaseRelativeView {

    private TextView mTvRegisterWelcome;
    private LinearLayout mLlNotLogin;
    private ConstraintLayout mConstlInfoPanel;
    private TextView mTvUsd;
    private TextView mTvVnd;
    private TextViewWithIcon mTvicLogout;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mTvRegisterWelcome = findViewById(R.id.account_frag_tv_register_welcome);
        mLlNotLogin = findViewById(R.id.account_frag_ll_not_login);
        mConstlInfoPanel = findViewById(R.id.account_frag_constl_info_panel);

        mTvVnd = findViewById(R.id.account_frag_tv_vnd);
        mTvUsd = findViewById(R.id.account_frag_tv_usd);
        mTvVnd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvVnd.setSelected(true);
                mTvUsd.setSelected(false);
            }
        });
        mTvUsd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvVnd.setSelected(false);
                mTvUsd.setSelected(true);
            }
        });
        mTvVnd.setSelected(true);

        findViewById(R.id.account_frag_btn_update_info).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new EditInfoCmd());
            }
        });

        findViewById(R.id.account_frag_btn_login).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new LoginCmd());
            }
        });

        findViewById(R.id.account_frag_btn_register).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new RegisterCmd());
            }
        });

        findViewById(R.id.account_frag_tvic_my_coupon).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new MyCouponCmd());
            }
        });

        findViewById(R.id.account_frag_tvic_help_center).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new HelpCenterCmd());
            }
        });

        mTvicLogout = findViewById(R.id.account_frag_tvic_logout);
        mTvicLogout.setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new LogoutCmd());
            }
        });
    }

    public void userUI(User user) {
        if (user != null) {
            mTvRegisterWelcome.setVisibility(INVISIBLE);
            mLlNotLogin.setVisibility(INVISIBLE);
            mConstlInfoPanel.setVisibility(VISIBLE);
            mTvicLogout.setVisibility(VISIBLE);

            String fullName = user.getLastName() + " " + user.getFirstName();
            TextView tvAvatar = findViewById(R.id.account_frag_tv_avatar);
            tvAvatar.setText(extractTextAvatar(fullName));

            TextView tvName = findViewById(R.id.account_frag_tv_name);
            tvName.setText(fullName);

            TextView tvContact = findViewById(R.id.account_frag_tv_email);
            if (TextUtils.isEmpty(user.getContactInfo().getPhoneNumber())) {
                tvContact.setText(user.getContactInfo().getEmail());
            } else {
                tvContact.setText(user.getContactInfo().getPhoneNumber());
            }

        } else {
            mTvRegisterWelcome.setVisibility(VISIBLE);
            mLlNotLogin.setVisibility(VISIBLE);
            mConstlInfoPanel.setVisibility(INVISIBLE);
            mTvicLogout.setVisibility(INVISIBLE);
        }
    }

    private String extractTextAvatar(String name) {
        if (TextUtils.isEmpty(name)) {
            return "";
        }

        String avatar = "";
        int firstSpaceIndex = name.trim().indexOf(' ');
        if (firstSpaceIndex == -1) {
            avatar = name.trim().substring(0, 1);
        } else {
            String firstWord = name.trim().substring(0, firstSpaceIndex);
            String firstChar = firstWord.substring(0, 1);
            int lastSpaceIndex = name.trim().lastIndexOf(' ');
            String lastWord = name.trim().substring(lastSpaceIndex + 1);
            avatar = firstChar + lastWord.substring(0, 1);
        }

        return avatar;
    }


    public class RegisterCmd implements ICommand {}
    public class LoginCmd implements ICommand {}
    public class EditInfoCmd implements ICommand {}
    public class MyCouponCmd implements ICommand{}
    public class HelpCenterCmd implements ICommand {}
    public class LogoutCmd implements ICommand{}
}
