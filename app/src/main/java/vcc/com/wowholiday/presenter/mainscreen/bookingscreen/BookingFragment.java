package vcc.com.wowholiday.presenter.mainscreen.bookingscreen;

import common.presenter.WHTabFragment;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/10/2019.
 */

public class BookingFragment extends WHTabFragment<BookingView> {
    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_booking;
    }
}
