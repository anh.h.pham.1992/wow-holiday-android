package vcc.com.wowholiday.presenter.mainscreen.bookingscreen;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import common.presenter.WHFragment;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/10/2019.
 */
public class BookingView extends BaseLinearView {
    public BookingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        ViewPager vp = findViewById(R.id.my_booking_frag_vp_content);
        vp.setAdapter(new MyBookingVpAdapter(((WHFragment)mPresenter).getChildFragmentManager()));
        TabLayout tab = findViewById(R.id.my_booking_frag_tab);
        tab.setupWithViewPager(vp);
    }
}
