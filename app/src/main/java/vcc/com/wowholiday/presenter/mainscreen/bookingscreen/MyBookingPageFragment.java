package vcc.com.wowholiday.presenter.mainscreen.bookingscreen;

import android.content.Intent;

import common.presenter.WHFragment;
import quangph.com.mvp.mvp.ICommand;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.model.BOOKING_STATUS;
import vcc.com.wowholiday.presenter.activitiesscreen.ticket.ActivitiesTicketActivity;

/**
 * Created by QuangPH on 2/8/2020.
 */
public class MyBookingPageFragment extends WHFragment<MyBookingPageView> {

    //For dummy data
    public BOOKING_STATUS bookingStatus;

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_my_booking_page;
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mView.showMyBookingDummy(bookingStatus);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof MyBookingPageRclvAdapter.SelectMyBooking) {
            gotoActivitiesTicket(((MyBookingPageRclvAdapter.SelectMyBooking) command).booking);
        }
    }

    private void gotoActivitiesTicket(ActivitiesTicket ticket) {
        Intent itn = new Intent(getContext(), ActivitiesTicketActivity.class);
        ActivitiesTicketActivity.createParams(itn, ticket);
        startActivity(itn);
    }
}
