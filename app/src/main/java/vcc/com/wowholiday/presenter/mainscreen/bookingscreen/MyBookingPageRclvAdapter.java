package vcc.com.wowholiday.presenter.mainscreen.bookingscreen;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.presenter.util.MoneyFormatter;

/**
 * Created by QuangPH on 2/8/2020.
 */
public class MyBookingPageRclvAdapter extends BaseRclvAdapter {

    private IPresenter mPresenter;

    public MyBookingPageRclvAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_my_booking_page;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new BookingItemVH(itemView);
    }


    class BookingItemVH extends BaseRclvHolder<ActivitiesTicket> {

        ImageView ivIcon;
        TextView tvName;
        TextView tvCode;
        TextView tvTime;
        TextView tvPrice;

        public BookingItemVH(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.my_booking_page_itm_iv_icon);
            tvName = itemView.findViewById(R.id.my_booking_page_itm_tv_name);
            tvCode = itemView.findViewById(R.id.my_booking_page_itm_tv_booking_code);
            tvTime = itemView.findViewById(R.id.my_booking_page_itm_tv_booking_time);
            tvPrice = itemView.findViewById(R.id.my_booking_page_itm_tv_booking_price);
            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    SelectMyBooking cmd = new SelectMyBooking();
                    cmd.booking = (ActivitiesTicket) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(ActivitiesTicket vhData) {
            super.onBind(vhData);
            tvName.setText(vhData.getName());
            tvCode.setText("Mã đặt chỗ: " + vhData.getCode());
            tvTime.setText(vhData.getStartTime());
            tvPrice.setText(MoneyFormatter.defaultFormat(String.valueOf(vhData.getPrice())) + " VND");
        }
    }

    public static class SelectMyBooking implements ICommand {
        public ActivitiesTicket booking;
    }
}
