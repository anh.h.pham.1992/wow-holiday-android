package vcc.com.wowholiday.presenter.mainscreen.bookingscreen;

import android.content.Context;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import common.presenter.adapter.decor.SpaceItemDecoration;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.ActivitiesTicket;
import vcc.com.wowholiday.model.BOOKING_STATUS;
import vcc.com.wowholiday.model.TicketContactInfo;
import vcc.com.wowholiday.model.air.PaxInfo;

/**
 * Created by QuangPH on 2/8/2020.
 */
public class MyBookingPageView extends BaseFrameView {

    private MyBookingPageRclvAdapter mAdapter;
    public MyBookingPageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        RecyclerView rclv = findViewById(R.id.my_booking_page_frag_rclv_content);
        rclv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rclv.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        mAdapter = new MyBookingPageRclvAdapter(mPresenter);
        rclv.setAdapter(mAdapter);

    }

    public void showMyBookingDummy(BOOKING_STATUS status) {
        mAdapter.reset(dummy(status));
    }

    private List<ActivitiesTicket> dummy(BOOKING_STATUS status) {
        List<ActivitiesTicket> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ActivitiesTicket booking = new ActivitiesTicket();
            booking.setName("Vé Thăm Quan Bà Nà Hills");
            booking.setCode("3V5PKA");
            booking.setStartTime("12:00");
            booking.setEndTime("18:00");
            booking.setDate("16/10/2019");
            booking.setPrice(1660000);
            booking.setStatus(status);
            booking.setAdultCount(1);
            booking.setChildCount(1);
            booking.setActiviesID("VIE-DADU1");
            booking.setActivitiesToken("54e997a5-8b68-47e7-a056-cdc1e1297414");

            TicketContactInfo info = new TicketContactInfo();
            info.setName("John Evan");
            info.setPhone("0987654321");
            info.setEmail("johnevan@gmail.com");
            booking.setContact(info);

            List<PaxInfo> paxInfoList = new ArrayList<>();
            PaxInfo adult = new PaxInfo();
            adult.setType(PaxInfo.ADULT);
            adult.setQuantity(2);
            adult.setAmount("599000");
            paxInfoList.add(adult);

            PaxInfo child = new PaxInfo();
            adult.setType(PaxInfo.CHILD);
            adult.setQuantity(1);
            adult.setAmount("399000");
            paxInfoList.add(child);
            booking.setPaxInfoList(paxInfoList);
            result.add(booking);
        }
        return result;
    }
}
