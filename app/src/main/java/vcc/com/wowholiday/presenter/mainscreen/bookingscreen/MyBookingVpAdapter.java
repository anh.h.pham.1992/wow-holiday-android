package vcc.com.wowholiday.presenter.mainscreen.bookingscreen;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import vcc.com.wowholiday.model.BOOKING_STATUS;

/**
 * Created by QuangPH on 2/8/2020.
 */
public class MyBookingVpAdapter extends FragmentPagerAdapter {

    public MyBookingVpAdapter(@NonNull FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        MyBookingPageFragment fragment = new MyBookingPageFragment();
        if (position == 0) {
            fragment.bookingStatus = BOOKING_STATUS.WAITING_PAY;
        } else if (position == 1) {
            fragment.bookingStatus = BOOKING_STATUS.CONFIRMED;
        } else if (position == 2) {
            fragment.bookingStatus = BOOKING_STATUS.USED;
        } else if (position == 3) {
            fragment.bookingStatus = BOOKING_STATUS.CANCELED;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Chờ thanh toán";
        }
        if (position == 1) {
            return "Đã xác nhận";
        }

        if (position == 2) {
            return "Đã sử dụng";
        }

        if (position == 3) {
            return "Đã huỷ";
        }
        return super.getPageTitle(position);
    }
}
