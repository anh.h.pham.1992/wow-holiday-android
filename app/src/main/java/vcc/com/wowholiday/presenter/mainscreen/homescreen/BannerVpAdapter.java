package vcc.com.wowholiday.presenter.mainscreen.homescreen;

import android.graphics.Outline;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;

/**
 * Created by Pham Hai Quang on 10/11/2019.
 */
public class BannerVpAdapter extends PagerAdapter {

    private List<String> mUrlList;
    private IImageLoader mImageLoader = GlideImageLoader.getInstance();
    private int mCorner;
    private int mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom;

    public BannerVpAdapter(List<String> urlList) {
        mUrlList = urlList;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        final ImageView v = (ImageView) inflater.inflate(R.layout.item_home_banner, container, false);
        v.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
        container.addView(v);
        if (mCorner > 0) {
            mImageLoader.loadRoundCornerImage(v, mUrlList.get(position % mUrlList.size()), (ImageView) v, mCorner);
        } else {
            mImageLoader.loadImage(v, mUrlList.get(position % mUrlList.size()), (ImageView) v);
        }

        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    public void setUrls(List<String> urlList) {
        mUrlList.clear();
        mUrlList.addAll(urlList);
        notifyDataSetChanged();
    }

    public int getInitPagePosition() {
        int centerPos = Integer.MAX_VALUE / 2;
        int realCenterPos = centerPos % mUrlList.size();
        if (realCenterPos > 0) {
            centerPos -= realCenterPos;
        }
        return centerPos;
    }

    public void setCorner(int corner) {
        mCorner = corner;
    }

    public void setPadding(int left, int top, int right, int bottom) {
        mPaddingLeft = left;
        mPaddingTop = top;
        mPaddingRight = right;
        mPaddingBottom = bottom;
    }
}
