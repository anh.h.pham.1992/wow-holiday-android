package vcc.com.wowholiday.presenter.mainscreen.homescreen;

import android.view.View;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.ItemDescriptionView;

/**
 * Created by QuangPH on 2020-02-05.
 */
public class DomesticItemAdapter extends BaseRclvAdapter {

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_home_domestic_item;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new DomestticVH(itemView);
    }

    private class DomestticVH extends BaseRclvHolder<SaleOff> {
        ItemDescriptionView domesticView;

        public DomestticVH(@NonNull View itemView) {
            super(itemView);
            domesticView = (ItemDescriptionView) itemView;
        }

        @Override
        public void onBind(SaleOff vhData) {
            super.onBind(vhData);
            domesticView.setAvatar(vhData.getAvatarUrl());
            domesticView.setDescription(vhData.getDescription());
        }
    }
}
