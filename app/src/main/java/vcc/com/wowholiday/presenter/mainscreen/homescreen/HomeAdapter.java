package vcc.com.wowholiday.presenter.mainscreen.homescreen;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.BaseVHData;
import common.presenter.adapter.decor.EndPaddingDecorator;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.StartPaddingDecorator;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.WHApplication;
import vcc.com.wowholiday.model.PopularActivity;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class HomeAdapter extends BaseRclvAdapter {

    private IPresenter mPresenter;

    public HomeAdapter(IPresenter presenter) {
        mPresenter = presenter;
        mDataSet.add(new QuickSelectVHData());
        test();
    }

    @Override
    public int getItemViewType(int position) {
        return ((BaseVHData) mDataSet.get(position)).type;
    }

    @Override
    public int getLayoutResource(int viewType) {
        int layout = 0;
        switch (viewType) {
            case IHomeType.SALE_OFF_HOTEL:
                layout = R.layout.item_home_sale_hotel;
                break;
            case IHomeType.SALE_OFF_FLIGHT:
                layout = R.layout.item_home_sale_flight;
                break;
            case IHomeType.QUICK_SELECT:
                layout = R.layout.item_home_quick_select;
                break;
            case IHomeType.RECOMMEND_HOTEL:
                layout = R.layout.item_home_recommend_hotel;
                break;
            case IHomeType.DOMESTIC_SPECIAL_FARE:
            case IHomeType.POPULAR_ACTIVITIES:
                layout = R.layout.item_home_domestic;
                break;
        }
        return layout;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        switch (viewType) {
            case IHomeType.SALE_OFF_HOTEL:
            case IHomeType.SALE_OFF_FLIGHT:
                return new SaleOffVH(itemView);
            case IHomeType.QUICK_SELECT:
                return new QuickSelectVH(itemView);
            case IHomeType.RECOMMEND_HOTEL:
                return new RecommendHotelVH(itemView);
            case IHomeType.DOMESTIC_SPECIAL_FARE:
            case IHomeType.POPULAR_ACTIVITIES:
                return new DomesticVH(itemView);
        }
        return null;
    }

    void test() {
        List<SaleOff> saleOffs = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SaleOff sl = new SaleOff();
            sl.setAvatarUrl("https://media.activitiesbank.com/46102/ENG/L/PHOTO3.jpg");
            sl.setDescription("Giảm 50% khi đặt phòng và căn hộ cho gia đình");
            saleOffs.add(sl);
        }
        SaleOffHotelVHData data = new SaleOffHotelVHData(saleOffs);
        mDataSet.add(data);

        List<SaleOff> air = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SaleOff sl = new SaleOff();
            sl.setAvatarUrl("https://media.sandhills.com/img.axd?id=4032323781&wid=&p=&ext=&w=0&h=0&t=&lp=7&c=True&wt=False&sz=Max&rt=0&checksum=RE9bPoQU13LnZd4QrywX6fZ4UyyVppqu");
            sl.setDescription("Khuyến mãi lên đến 50% cho các chuyến bay trong tháng 10");
            air.add(sl);
        }
        SaleOffFlightVHData airData = new SaleOffFlightVHData(air);
        mDataSet.add(airData);

        List<String> recommendHotel = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            recommendHotel.add("https://upload.wikimedia.org/wikipedia/commons/f/fe/Abbasi_Hotel.jpg");
            recommendHotel.add("https://upload.wikimedia.org/wikipedia/commons/f/fe/Abbasi_Hotel.jpg");
            recommendHotel.add("https://upload.wikimedia.org/wikipedia/commons/f/fe/Abbasi_Hotel.jpg");
        }

        mDataSet.add(new RecommendHotelVHData(recommendHotel));

        List<SaleOff> domestic = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SaleOff sl = new SaleOff();
            sl.setAvatarUrl("https://upload.wikimedia.org/wikipedia/commons/3/3d/DirkvdM_natural_spiral.jpg");
            sl.setDescription("What to do in Phu Quoc for your 3-day ...");
            domestic.add(sl);
        }
        DomesticSpecialFareVHData domesticData = new DomesticSpecialFareVHData(domestic);
        domesticData.type = IHomeType.DOMESTIC_SPECIAL_FARE;
        domesticData.title = WHApplication.getInstance().getString(R.string.domestic_special_fares);
        mDataSet.add(domesticData);

        List<SaleOff> activities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SaleOff sl = new SaleOff();
            sl.setAvatarUrl("https://www.ttrweekly.com/site/wp-content/uploads/2017/08/inside-no-7.jpg");
            sl.setDescription("Lễ hội pháo hoa rực rỡ chào đón mùa Thu");
            activities.add(sl);
        }
        DomesticSpecialFareVHData actData = new DomesticSpecialFareVHData(activities);
        actData.type = IHomeType.POPULAR_ACTIVITIES;
        actData.title = WHApplication.getInstance().getString(R.string.popular_activities);
        mDataSet.add(actData);
    }

    private class QuickSelectVH extends BaseRclvHolder<QuickSelectVHData> {
        public QuickSelectVH(@NonNull View itemView) {
            super(itemView);
            itemView.findViewById(R.id.home_quick_select_itm_iv_hotel).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    mPresenter.executeCommand(new HotelSearchCmd());
                }
            });
            itemView.findViewById(R.id.home_quick_select_itm_iv_flight).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    mPresenter.executeCommand(new FlightSearchCmd());
                }
            });
            itemView.findViewById(R.id.home_quick_select_itm_iv_activities).setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    mPresenter.executeCommand(new ActivitesSearchCmd());
                }
            });
        }
    }

    private class SaleOffVH extends BaseRclvHolder<SaleOffVHData> {
        RecyclerView rclvContent;
        SaleOffAdapter adapter;

        public SaleOffVH(@NonNull View itemView) {
            super(itemView);
            rclvContent = itemView.findViewById(R.id.home_sale_off_content);
            rclvContent.setLayoutManager(new LinearLayoutManager(itemView.getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            int padding = itemView.getResources().getDimensionPixelSize(R.dimen.dimen_16);
            rclvContent.addItemDecoration(new StartPaddingDecorator(padding));
            rclvContent.addItemDecoration(new EndPaddingDecorator(padding));
            rclvContent.addItemDecoration(new SpaceItemDecoration(padding, LinearLayoutManager.HORIZONTAL));
            adapter = new SaleOffAdapter();
            rclvContent.setAdapter(adapter);
        }

        @Override
        public void onBind(SaleOffVHData vhData) {
            super.onBind(vhData);
            adapter.reset(vhData.realData);
        }
    }

    private class RecommendHotelVH extends BaseRclvHolder<RecommendHotelVHData> {

        ViewPager vpAvatar;
        RecyclerView rclvIndicator;
        private int mCurrPos = 0;
        private int mBannerQuantity;

        private Handler mHandler = new Handler();
        private Runnable mRunnable = new Runnable() {
            @Override
            public void run() {
                if (itemView.isAttachedToWindow()) {
                    mCurrPos++;
                    vpAvatar.setCurrentItem(mCurrPos);
                }
            }
        };
        private IndicatorBannerAdapter mIndicatorAdapter;

        public RecommendHotelVH(@NonNull final View itemView) {
            super(itemView);
            vpAvatar = itemView.findViewById(R.id.home_recommend_hotel_itm_vp);
            vpAvatar.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    mHandler.removeCallbacks(mRunnable);
                    mCurrPos = i;
                    mIndicatorAdapter.setSelect(mCurrPos % mBannerQuantity);
                    mHandler.postDelayed(mRunnable, 5000);
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });

            rclvIndicator = itemView.findViewById(R.id.home_recommend_hotel_itm_rcvl_indicator);
            rclvIndicator.setLayoutManager(new LinearLayoutManager(itemView.getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            mIndicatorAdapter = new IndicatorBannerAdapter();
            rclvIndicator.setAdapter(mIndicatorAdapter);
        }

        @Override
        public void onBind(RecommendHotelVHData vhData) {
            super.onBind(vhData);
            if (!vhData.isBinded) {
                vhData.isBinded = true;
                mBannerQuantity = vhData.realData.size();
                List<Boolean> listIndicator = new ArrayList<>();
                for (int i = 0; i < vhData.realData.size(); i++) {
                    listIndicator.add(false);
                }
                mIndicatorAdapter.reset(listIndicator);
                BannerVpAdapter adapter = new BannerVpAdapter(vhData.realData);
                adapter.setCorner(itemView.getResources().getDimensionPixelOffset(R.dimen.dimen_8));
                int padding = itemView.getResources().getDimensionPixelOffset(R.dimen.dimen_16);
                adapter.setPadding(padding, 0, padding, 0);
                vpAvatar.setAdapter(adapter);
                vpAvatar.setCurrentItem(adapter.getInitPagePosition());
                mHandler.post(mRunnable);
            }
        }
    }

    private class DomesticVH extends BaseRclvHolder<DomesticSpecialFareVHData> {
        TextView tvTitle;
        RecyclerView rclvContent;
        DomesticItemAdapter adapter;

        public DomesticVH(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.home_domestic_title);
            rclvContent = itemView.findViewById(R.id.home_domestic_content);
            rclvContent.setLayoutManager(new LinearLayoutManager(itemView.getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            int padding = itemView.getResources().getDimensionPixelSize(R.dimen.dimen_16);
            rclvContent.addItemDecoration(new StartPaddingDecorator(padding));
            rclvContent.addItemDecoration(new EndPaddingDecorator(padding));
            rclvContent.addItemDecoration(new SpaceItemDecoration(padding, LinearLayoutManager.HORIZONTAL));
            adapter = new DomesticItemAdapter();
            rclvContent.setAdapter(adapter);
        }

        @Override
        public void onBind(DomesticSpecialFareVHData vhData) {
            super.onBind(vhData);
            tvTitle.setText(vhData.title);
            adapter.reset(vhData.realData);
        }
    }


    private static class SaleOffVHData extends BaseVHData<List<SaleOff>> {
        SaleOffVHData(List<SaleOff> data) {
            super(data);
        }
    }

    private static class SaleOffHotelVHData extends SaleOffVHData {
        SaleOffHotelVHData(List<SaleOff> data) {
            super(data);
            type = IHomeType.SALE_OFF_HOTEL;
        }
    }

    private static class SaleOffFlightVHData extends SaleOffVHData {
        SaleOffFlightVHData(List<SaleOff> data) {
            super(data);
            type = IHomeType.SALE_OFF_FLIGHT;
        }
    }

    private static class RecommendHotelVHData extends BaseVHData<List<String>> {
        boolean isBinded = false;

        RecommendHotelVHData(List<String> url) {
            super(url);
            type = IHomeType.RECOMMEND_HOTEL;
        }
    }

    private static class DomesticSpecialFareVHData extends BaseVHData<List<SaleOff>> {
        String title;

        DomesticSpecialFareVHData(List<SaleOff> data) {
            super(data);
            type = IHomeType.DOMESTIC_SPECIAL_FARE;
        }
    }

    private static class PopularActivityVHData extends BaseVHData<PopularActivity> {
        PopularActivityVHData(PopularActivity data) {
            super(data);
            type = IHomeType.POPULAR_ACTIVITIES;
        }
    }

    private static class QuickSelectVHData extends BaseVHData<Void> {
        QuickSelectVHData() {
            type = IHomeType.QUICK_SELECT;
        }
    }


    class TestVH extends BaseRclvHolder<TestVHData> {

        public TestVH(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void onBind(TestVHData vhData) {
            super.onBind(vhData);
            TextView tv = itemView.findViewById(R.id.tv);
            tv.setText(vhData.realData);
        }
    }

    class TestVHData extends BaseVHData<String> {
        TestVHData(String str) {
            super(str);
            type = 100;
        }
    }

    public static class HotelSearchCmd implements ICommand {
    }

    public static class FlightSearchCmd implements ICommand {
    }

    public static class ActivitesSearchCmd implements ICommand {
    }
}
