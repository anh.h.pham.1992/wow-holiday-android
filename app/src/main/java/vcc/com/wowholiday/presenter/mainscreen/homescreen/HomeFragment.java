package vcc.com.wowholiday.presenter.mainscreen.homescreen;

import android.content.Intent;

import common.presenter.WHTabFragment;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.activitiesscreen.ActivitiesActivity;
import vcc.com.wowholiday.presenter.flight.flightbooking.FlightBookingActivity;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchActivity;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */

@Layout(R.layout.fragment_home)
public class HomeFragment extends WHTabFragment<HomeView> {
    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if ((command instanceof HomeAdapter.HotelSearchCmd) || (command instanceof HomeView.HotelSearchCmd)) {
            gotoHotelSearch();
        } else if ((command instanceof HomeAdapter.FlightSearchCmd) || (command instanceof HomeView.AirSearchCmd)) {
            gotoFlightSearch();
        } else if ((command instanceof HomeAdapter.ActivitesSearchCmd) || (command instanceof HomeView.ActivitiesSearchCmd)) {
            gotoActivitiesScreen();
        }
    }

    private void gotoHotelSearch() {
        Intent itn = new Intent(getContext(), HotelSearchActivity.class);
        startActivity(itn);
    }

    private void gotoFlightSearch() {
        Intent itn = new Intent(getContext(), FlightBookingActivity.class);
        startActivity(itn);
    }

    private void gotoActivitiesScreen() {
        startActivity(new Intent(getContext(), ActivitiesActivity.class));
    }
}
