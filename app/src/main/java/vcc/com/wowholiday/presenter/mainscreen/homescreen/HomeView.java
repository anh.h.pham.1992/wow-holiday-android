package vcc.com.wowholiday.presenter.mainscreen.homescreen;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseFrameView;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class HomeView extends BaseFrameView {

    private static final int BANNER_DELAY = 5000;

    private RelativeLayout mRlStickyPanel;
    private ViewPager mVpBanner;
    private HomeAdapter mAdapter;
    private IndicatorBannerAdapter mIndicatorAdapter;
    private int mCurrBannerPos;
    private int mBannerQuantity;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mCurrBannerPos++;
            mVpBanner.setCurrentItem(mCurrBannerPos, true);
        }
    };

    public HomeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        super.onInitView();
        mRlStickyPanel = findViewById(R.id.home_frag_rl_sticky_panel);
        RecyclerView rclv = findViewById(R.id.home_frag_rclv_content);
        rclv.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        mAdapter = new HomeAdapter(mPresenter);
        rclv.setAdapter(mAdapter);
        rclv.addOnScrollListener(new RclvOnScrollListener());

        initBanner();

        findViewById(R.id.home_frag_rl_hotel).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new HotelSearchCmd());
            }
        });

        findViewById(R.id.home_frag_rl_air).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new AirSearchCmd());
            }
        });

        findViewById(R.id.home_frag_rl_activities).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                mPresenter.executeCommand(new ActivitiesSearchCmd());
            }
        });
    }

    private void initBanner() {
        RecyclerView rclIndicator = findViewById(R.id.home_frag_rclv_banner_indicator);
        rclIndicator.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        mIndicatorAdapter = new IndicatorBannerAdapter();
        rclIndicator.setAdapter(mIndicatorAdapter);

        mVpBanner = findViewById(R.id.home_frag_vp_banner);
        BannerVpAdapter bannerAdapter = new BannerVpAdapter(dummyBanner());
        mVpBanner.setAdapter(bannerAdapter);
        mVpBanner.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mCurrBannerPos = position;
                mIndicatorAdapter.setSelect(mCurrBannerPos % mBannerQuantity);
                removeCallbacks(mRunnable);
                postDelayed(mRunnable, BANNER_DELAY);
            }
        });


        dummy();
        mVpBanner.setCurrentItem(bannerAdapter.getInitPagePosition());
        postDelayed(mRunnable, BANNER_DELAY);
    }

    private void dummy() {
        List<Boolean> indicators = new ArrayList<>();
        for (int i = 0; i < mBannerQuantity; i++) {
            indicators.add(false);
        }
        indicators.set(0, true);
        mIndicatorAdapter.reset(indicators);
        mIndicatorAdapter.setSelect(0);
    }

    List<String> dummyBanner() {
        mBannerQuantity = 2;
        List<String> banner = new ArrayList<>();
        banner.add("https://media.activitiesbank.com/46102/ENG/L/PHOTO3.jpg");
        banner.add("https://media.activitiesbank.com/46102/ENG/L/PHOTO3.jpg");
        return banner;
    }


    private class RclvOnScrollListener extends RecyclerView.OnScrollListener {
        int SHOW = 1;
        int HIDE = 2;
        int status = HIDE;
        int totalScrolledY;
        int quickSelectHeight = getResources().getDimensionPixelSize(R.dimen.home_quick_select_height);
        int marginTop = getResources().getDimensionPixelSize(R.dimen.item_height_40);
        boolean isFirstTime = true;

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            totalScrolledY += dy;
            Log.e("HomeView", "dy: " + dy + " " + totalScrolledY);
            if (totalScrolledY > quickSelectHeight) {
                if (status == HIDE) {
                    show();
                }
            } else {
                if (status == SHOW) {
                    hide();
                }
            }
        }

        void show() {
            status = SHOW;
            if (isFirstTime) {
                mRlStickyPanel.setTranslationY(-mRlStickyPanel.getHeight());
                isFirstTime = false;
            }

            mRlStickyPanel.setVisibility(VISIBLE);
            mRlStickyPanel.animate().translationYBy(mRlStickyPanel.getHeight());
        }

        void hide() {
            status = HIDE;
            mRlStickyPanel.setVisibility(VISIBLE);
            mRlStickyPanel.animate().translationYBy(-mRlStickyPanel.getHeight());
        }
    }

    public static class HotelSearchCmd implements ICommand{}
    public static class AirSearchCmd implements ICommand{}
    public static class ActivitiesSearchCmd implements ICommand{}
}
