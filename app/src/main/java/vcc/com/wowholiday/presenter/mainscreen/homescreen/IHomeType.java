package vcc.com.wowholiday.presenter.mainscreen.homescreen;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public interface IHomeType {
    int SALE_OFF_HOTEL = 1;
    int SALE_OFF_FLIGHT = 2;
    int DOMESTIC_SPECIAL_FARE = 3;
    int POPULAR_ACTIVITIES = 4;
    int RECOMMEND_HOTEL = 5;
    int QUICK_SELECT = 6;
}
