package vcc.com.wowholiday.presenter.mainscreen.homescreen;

import android.view.View;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;

public class IndicatorBannerAdapter extends BaseRclvAdapter {

    private int mLastSelectedPosition = 0;

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_banner_indicator;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new IndicatorViewholder(itemView);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void setSelect(int position) {
        if (position == mLastSelectedPosition) {
            return;
        }

        mDataSet.set(mLastSelectedPosition, false);
        notifyItemChanged(mLastSelectedPosition);
        mDataSet.set(position, true);
        notifyItemChanged(position);
        mLastSelectedPosition = position;
    }

    class IndicatorViewholder extends BaseRclvHolder<Boolean> {

        View vIndicator;

        public IndicatorViewholder(@NonNull View itemView) {
            super(itemView);
            vIndicator = itemView.findViewById(R.id.banner_indicator_itm_iv_indicator);
        }

        @Override
        public void onBind(Boolean vhData) {
            super.onBind(vhData);
            vIndicator.setSelected(vhData);
        }
    }
}
