package vcc.com.wowholiday.presenter.mainscreen.homescreen;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class SaleOff {

    private String mDescription;
    private String mAvatarUrl;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }
}
