package vcc.com.wowholiday.presenter.mainscreen.homescreen;

import android.view.View;

import androidx.annotation.NonNull;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.ItemDescriptionView;

/**
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class SaleOffAdapter extends BaseRclvAdapter {
    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_sale_home_item;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new SaleOffVH(itemView);
    }

    private class SaleOffVH extends BaseRclvHolder<SaleOff> {
        ItemDescriptionView saleView;

        public SaleOffVH(@NonNull View itemView) {
            super(itemView);
            saleView = (ItemDescriptionView) itemView;
        }

        @Override
        public void onBind(SaleOff vhData) {
            super.onBind(vhData);
            saleView.setAvatar(vhData.getAvatarUrl());
            saleView.setDescription(vhData.getDescription());
        }
    }
}
