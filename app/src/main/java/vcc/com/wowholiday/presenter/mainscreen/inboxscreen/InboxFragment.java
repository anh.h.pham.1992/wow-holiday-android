package vcc.com.wowholiday.presenter.mainscreen.inboxscreen;

import common.presenter.WHTabFragment;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/10/2019.
 */
public class InboxFragment extends WHTabFragment<InboxView> {
    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_inbox;
    }
}
