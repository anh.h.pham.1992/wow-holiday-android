package vcc.com.wowholiday.presenter.mycoupon;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;

/**
 * Created by QuangPH on 2/10/2020.
 */

@Layout(R.layout.activity_my_coupon)
public class MyCouponActivity extends WHActivity<MyCouponVIew> {

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof MyCouponAdapter.SelectedMyCouponCmd) {
            Intent itn = new Intent(this, MyCouponDetailActivity.class);
            MyCouponDetailActivity.createParams(itn, ((MyCouponAdapter.SelectedMyCouponCmd) command).coupon);
            startActivity(itn);
        }
    }
}
