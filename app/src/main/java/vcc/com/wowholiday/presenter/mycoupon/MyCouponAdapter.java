package vcc.com.wowholiday.presenter.mycoupon;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.TextView;

import androidx.annotation.NonNull;
import common.presenter.SafeClicked;
import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.IPresenter;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.MyCoupon;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;
import vcc.com.wowholiday.presenter.widget.RatioImageView;

/**
 * Created by QuangPH on 2/10/2020.
 */
public class MyCouponAdapter extends BaseRclvAdapter {
    private IPresenter mPresenter;
    private IImageLoader mLoader = GlideImageLoader.getInstance();

    public MyCouponAdapter(IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public int getLayoutResource(int viewType) {
        return R.layout.item_my_coupon;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        return new MyCouponVH(itemView);
    }

    private class MyCouponVH extends BaseRclvHolder<MyCoupon> {
        RatioImageView rivAvatar;
        TextView tvTime;
        public MyCouponVH(@NonNull View itemView) {
            super(itemView);
            rivAvatar = itemView.findViewById(R.id.my_coupon_itm_riv);
            tvTime = itemView.findViewById(R.id.my_coupon_itm_tv_time);
            rivAvatar.setClipToOutline(true);
            rivAvatar.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    int corner = view.getResources().getDimensionPixelOffset(R.dimen.corner_8);
                    outline.setRoundRect(0, 0, view.getWidth(), view.getHeight() + corner, corner);
                }
            });
            itemView.setOnClickListener(new SafeClicked() {
                @Override
                public void onSafeClicked(View view) {
                    SelectedMyCouponCmd cmd = new SelectedMyCouponCmd();
                    cmd.coupon = (MyCoupon) mDataSet.get(getAdapterPosition());
                    mPresenter.executeCommand(cmd);
                }
            });
        }

        @Override
        public void onBind(MyCoupon vhData) {
            super.onBind(vhData);
            mLoader.loadImage(itemView, vhData.getUrl(), rivAvatar);
            tvTime.setText("Thời gian: " + vhData.getStartDate() + " - " + vhData.getEndDate());
        }
    }

    public static class SelectedMyCouponCmd implements ICommand {
        public MyCoupon coupon;
    }
}
