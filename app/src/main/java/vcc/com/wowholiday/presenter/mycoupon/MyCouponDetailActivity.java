package vcc.com.wowholiday.presenter.mycoupon;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.MyCoupon;

/**
 * Created by QuangPH on 2/10/2020.
 */

@Layout(R.layout.activity_my_coupon_detail)
public class MyCouponDetailActivity extends WHActivity<MyCouponDetailView> {

    public static void createParams(Intent itn, MyCoupon coupon) {
        itn.putExtra("coupon", coupon);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        MyCoupon coupon = getIntent().getParcelableExtra("coupon");
        mView.showCoupon(coupon);
    }
}
