package vcc.com.wowholiday.presenter.mycoupon;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import common.presenter.SafeClicked;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.MyCoupon;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.widget.RatioImageView;

/**
 * Created by QuangPH on 2/10/2020.
 */
public class MyCouponDetailView extends BaseLinearView {

    public MyCouponDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.my_coupon_detail_act_iv_copy).setOnClickListener(new SafeClicked() {
            @Override
            public void onSafeClicked(View view) {
                Toast.makeText(getContext(), "Đã sao chép mã", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showCoupon(MyCoupon coupon) {
        RatioImageView iv = findViewById(R.id.my_coupon_detail_act_riv_avatar);
        GlideImageLoader.getInstance().loadImage(this, coupon.getUrl(), iv);

        TextView tvName = findViewById(R.id.my_coupon_detail_act_tv_name);
        tvName.setText(coupon.getName());

        TextView tvDes = findViewById(R.id.my_coupon_detail_act_tv_des);
        tvDes.setText(coupon.getDescription());

        TextView tvTime = findViewById(R.id.my_coupon_detail_act_tv_time);
        tvTime.setText(coupon.getStartDate() + " - " + coupon.getEndDate());

        TextView tvCode = findViewById(R.id.my_coupon_detail_act_tv_code);
        tvCode.setText(coupon.getCode());

        TextView tvPolicy = findViewById(R.id.my_coupon_detail_act_tv_policy);
        StringBuilder builder = new StringBuilder();
        for (String str : coupon.getPolicyList()) {
            builder.append(str).append("\n");
        }
        tvPolicy.setText(builder.toString());
    }
}
