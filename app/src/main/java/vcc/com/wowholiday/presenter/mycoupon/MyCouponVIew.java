package vcc.com.wowholiday.presenter.mycoupon;

import android.content.Context;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import common.presenter.adapter.decor.BottomPaddingDecorator;
import common.presenter.adapter.decor.SpaceItemDecoration;
import common.presenter.adapter.decor.TopPaddingDecorator;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.MyCoupon;

/**
 * Created by QuangPH on 2/10/2020.
 */
public class MyCouponVIew extends BaseLinearView {
    public MyCouponVIew(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        RecyclerView rclvContent = findViewById(R.id.my_coupon_act_rclv_content);
        rclvContent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rclvContent.addItemDecoration(new TopPaddingDecorator(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        rclvContent.addItemDecoration(new BottomPaddingDecorator(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        rclvContent.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelOffset(R.dimen.dimen_16)));
        MyCouponAdapter adapter = new MyCouponAdapter(mPresenter);
        rclvContent.setAdapter(adapter);
        adapter.reset(dummy());
    }

    List<MyCoupon> dummy() {
        List<MyCoupon> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            MyCoupon coupon = new MyCoupon();
            coupon.setUrl("https://media.activitiesbank.com/46102/ENG/L/PHOTO3.jpg");
            coupon.setCode("31KHNT");
            coupon.setName("10 vé đặt đầu tiên giá chỉ 31,000 VNĐ");
            coupon.setDescription("Cùng săn vé siêu rẻ với chặng bay mới Huế -Nha Trang.\n" +
                    "Nhập mã 31KHNT để mua vé bay một chiều với giá chỉ còn 31,000 VNĐ.");
            coupon.setStartDate("01/10/2019");
            coupon.setEndDate("31/10/2019");
            List<String> policies = new ArrayList<>();
            policies.add("- Số lượng có hạn, mã giảm giá sẽ không được áp dụng khi đạt số lượng");
            coupon.setPolicyList(policies);
            result.add(coupon);
        }
        return result;
    }
}
