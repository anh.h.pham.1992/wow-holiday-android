package vcc.com.wowholiday.presenter.profileinfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import common.presenter.WHActivity;
import common.presenter.WHApiActionCallback;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.ActionException;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.domain.usecase.user.RegisterAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidFullNameAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidPasswordAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidTextException;
import vcc.com.wowholiday.domain.usecase.valid.ValidTextType;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.RegisterType;
import vcc.com.wowholiday.presenter.confirmaccount.OtpConfirmActivity;
import vcc.com.wowholiday.presenter.loginscreen.LoginActivity;
import vcc.com.wowholiday.presenter.mainscreen.MainActivity;
import vcc.com.wowholiday.repository.api.APIException;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */

@Layout(R.layout.activity_profile_info)
public class ProfileInfoActivity extends WHActivity<ProfileInfoView> {
    public static final String REGISTER_SUCCESS = "REGISTER_SUCCESS";

    private String mEmailOrPhone;
    private int mType;

    public static void createParams(Intent itn, String emailOrPhone, int type) {
        itn.putExtra("email_phone", emailOrPhone);
        itn.putExtra("type", type);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mEmailOrPhone = getIntent().getStringExtra("email_phone");
        mType = getIntent().getIntExtra("type", RegisterType.PHONE);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ProfileInfoView.CompleteCmd) {
            doRegister(((ProfileInfoView.CompleteCmd) command).firstName,
                    ((ProfileInfoView.CompleteCmd) command).name,
                    ((ProfileInfoView.CompleteCmd) command).password,
                    ((ProfileInfoView.CompleteCmd) command).confirmPassword,
                    ((ProfileInfoView.CompleteCmd) command).gender);
        } else if (command instanceof ProfileInfoView.DoneKeyboardCmd) {
            hideKeyBoard();
        }
        /*if (command instanceof ProfileInfoView.CompleteCmd) {
            finish();
        }*/
    }

    private void doRegister(String firstName, String name, String password, String confirmPass, String gender) {
        boolean confirPassValid = validPasswords(confirmPass, true);
        boolean passValid = validPasswords(password, false);
        boolean nameValid = validName(name, true);
        boolean firstNameValid = validName(firstName, false);
        if (firstNameValid
                && nameValid
                && passValid
                && confirPassValid) {
            if (!password.equals(confirmPass)) {
                mView.showConfirmPasswordError(getResources().getString(R.string.password_not_the_same));
            } else {
                //finish();
                callRegister(name, firstName, password,gender);
            }
        }
    }

    private boolean validName(String name, boolean isName) {
        ValidFullNameAction.ValidFullNameReq req = new ValidFullNameAction.ValidFullNameReq();
        req.name = name;
        ValidFullNameAction action = new ValidFullNameAction();
        action.setRequestValue(req);
        try {
            boolean valid = action.execute();
            if (isName) {
                mView.showNameError(null);
            } else {
                mView.showFirstNameError(null);
            }
            return valid;
        } catch (ActionException e) {
            ValidTextException validEx = (ValidTextException) e;
            if(validEx.getValidTextType() == ValidTextType.EMPTY) {
                if (isName) {
                    mView.showNameError(getString(R.string.empty_error));
                } else {
                    mView.showFirstNameError(getString(R.string.empty_error));
                }
            } else {
                if (isName) {
                    mView.showNameError(getString(R.string.error_format_world));
                } else {
                    mView.showFirstNameError(getString(R.string.error_format_world));
                }
            }
            return false;
        }
    }

    /*private boolean validName(String lastName, String firstName) {
        ValidUserNameAction.ValidUserNameReq req = new ValidUserNameAction.ValidUserNameReq();
        req.lastName = lastName;
        req.firstName = firstName;
        ValidUserNameAction action = new ValidUserNameAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            ValidTextException validEx = (ValidTextException) e;
            if(validEx.getValidTextType() == ValidTextType.EMPTY) {
                mView.showUserNameError(getString(R.string.empty_error));
            } else {
                mView.showUserNameError(getString(R.string.error_format_world));
            }
            return false;
        }
    }*/

    private boolean validPasswords(String passwords, boolean isConfirm) {
        ValidPasswordAction.ValidPasswordReq req = new ValidPasswordAction.ValidPasswordReq();
        req.password = passwords;
        ValidPasswordAction action = new ValidPasswordAction();
        action.setRequestValue(req);
        try {
            boolean valid = action.execute();
            if (isConfirm) {
                mView.showConfirmPasswordError(null);
            } else {
                mView.showPasswordError(null);
            }
            return valid;
        } catch (ActionException e) {
            e.printStackTrace();
            ValidTextException validEx = (ValidTextException) e;
            if (validEx.getValidTextType() == ValidTextType.EMPTY) {
                if (isConfirm) {
                    mView.showConfirmPasswordError(getResources().getString(R.string.empty_error));
                } else {
                    mView.showPasswordError(getResources().getString(R.string.empty_error));
                }
            } else {
                if (isConfirm) {
                    mView.showConfirmPasswordError(getResources().getString(R.string.password_length_error,
                            AppConfig.PASSWORD_LENGTH_MIN));
                } else {
                    mView.showPasswordError(getResources().getString(R.string.password_length_error,
                            AppConfig.PASSWORD_LENGTH_MIN));
                }
            }

            return false;
        }
    }

    private void callRegister(String firstName, String lastName, String password, String gender) {
        RegisterAction.RegisterReq req = new RegisterAction.RegisterReq();
        req.emailOrPhone = mEmailOrPhone;
        req.firstName = firstName;
        req.lastName = lastName;
        req.password = password;
        req.gender = gender;

        showLoading();
        mActionManager.executeAction(new RegisterAction(), req, new WHApiActionCallback<User>(this){
            @Override
            public void onSuccess(User responseValue) {
                super.onSuccess(responseValue);
                hideLoading();
                Intent itn = new Intent();
                itn.putExtra(REGISTER_SUCCESS, responseValue);
                setResult(RESULT_OK, itn);
                finish();
            }

            @Override
            protected void onException(Context context, APIException e) {
                super.onException(context, e);
                if (e.getCode() == 260031 || e.getCode() == 260029) {
                    showAlerError();
                } else {
                    Toast.makeText(ProfileInfoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        }, new ThirdPartyScheduler());
    }

    private void showAlerError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Nếu đây là tài khoản của bạn, xin vui lòng đăng nhập");
        String title = mType == RegisterType.EMAIL ? "Địa chỉ email này đã được sử dụng"
                : "Số điện thoại này đã được sử dụng";
        builder.setTitle(title);
        builder.setCancelable(false);

        builder.setNegativeButton(
                "Quay lại",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.setPositiveButton(
                "Đăng nhập",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        gotoLogin();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void gotoLogin() {
        //Intent itn = new Intent(this, LoginActivity.class);
        //startActivity(itn);
        setResult(RESULT_FIRST_USER);
        finish();
    }
}
