package vcc.com.wowholiday.presenter.profileinfo;

import android.content.Context;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputLayout;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseRelativeView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.widget.RadioRightText;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class ProfileInfoView extends BaseRelativeView {

    private EditText mEdtName;
    private EditText mEdtFirstName;
    private EditText mEdtPass;
    private EditText mEdtConfirmPass;

    private TextInputLayout mTilName;
    private TextInputLayout mTilFirstName;
    private TextInputLayout mTilError;
    private TextInputLayout mTilConfirmError;
    private RadioRightText mRrTMale;
    private RadioRightText mRrFemale;

    public ProfileInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mEdtName = findViewById(R.id.profile_info_act_edt_name);
        mEdtFirstName = findViewById(R.id.profile_info_act_edt_first_name);
        mEdtPass = findViewById(R.id.profile_info_act_edt_pass);
        mEdtConfirmPass = findViewById(R.id.profile_info_act_edt_confirm_pass);

        mTilName = findViewById(R.id.profile_info_act_til_name);
        mTilFirstName = findViewById(R.id.profile_info_act_til_first_name);
        mTilError = findViewById(R.id.profile_info_act_til_pass);
        mTilConfirmError = findViewById(R.id.profile_info_act_til_confirm_pass);

        findViewById(R.id.profile_info_act_iv_visibility).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                changeVisibilityColor((ImageView) v);
                mEdtPass.setTransformationMethod(v.isSelected() ? null : (new PasswordTransformationMethod()));
            }
        });

        findViewById(R.id.profile_info_act_iv_confirm_visibility).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                changeVisibilityColor((ImageView) v);
                mEdtConfirmPass.setTransformationMethod(v.isSelected() ? null : (new PasswordTransformationMethod()));
            }
        });

        findViewById(R.id.profile_info_act_btn_complete).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doneClicked();
            }
        });

        mEdtConfirmPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //doneClicked();
                    mPresenter.executeCommand(new DoneKeyboardCmd());
                }
                return false;
            }
        });

        mRrTMale = findViewById(R.id.profile_info_act_rrt_male);
        mRrFemale = findViewById(R.id.profile_info_act_rrt_female);
        mRrTMale.setChecked(true);
        mRrTMale.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mRrTMale.setChecked(true);
                mRrFemale.setChecked(false);
            }
        });
        mRrFemale.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mRrTMale.setChecked(false);
                mRrFemale.setChecked(true);
            }
        });

        changeVisibilityColor((ImageView) findViewById(R.id.profile_info_act_iv_confirm_visibility));
        changeVisibilityColor((ImageView) findViewById(R.id.profile_info_act_iv_visibility));
    }

    private void changeVisibilityColor(ImageView ivVisibility) {
        boolean isSelected = ivVisibility.isSelected();
        ivVisibility.setColorFilter(isSelected ? ContextCompat.getColor(getContext(), R.color.light_burgundy)
                : ContextCompat.getColor(getContext(), R.color.light_grey));
    }

    public void showNameError(String error) {
        if (error == null) {
            mTilName.setErrorEnabled(false);
        } else {
            mTilName.setError(error);
            mEdtName.requestFocus();
        }
    }

    public void showFirstNameError(String error) {
        if (error == null) {
            mTilFirstName.setErrorEnabled(false);
        } else {
            mTilFirstName.setError(error);
            mEdtFirstName.requestFocus();
        }
    }

    public void showPasswordError(String error) {
        if (error == null) {
            mTilError.setErrorEnabled(false);
        } else {
            mTilError.setError(error);
            mEdtPass.requestFocus();
        }
    }

    public void showConfirmPasswordError(String error) {
        if (error == null) {
            mTilConfirmError.setErrorEnabled(false);
        } else {
            mTilConfirmError.setError(error);
            mEdtConfirmPass.requestFocus();
        }
    }

    private void doneClicked() {
        CompleteCmd cmd = new CompleteCmd();
        cmd.name = mEdtName.getText().toString().trim();
        cmd.firstName = mEdtFirstName.getText().toString().trim();
        cmd.password = mEdtPass.getText().toString().trim();
        cmd.confirmPassword = mEdtConfirmPass.getText().toString().trim();
        if (mRrTMale.isChecked()) {
            cmd.gender = User.MALE;
        } else {
            cmd.gender = User.FEMALE;
        }
        mPresenter.executeCommand(cmd);
    }


    public static class CompleteCmd implements ICommand {
        public String name;
        public String firstName;
        public String password;
        public String confirmPassword;
        public String gender;
    }

    public static class DoneKeyboardCmd implements ICommand{}
}
