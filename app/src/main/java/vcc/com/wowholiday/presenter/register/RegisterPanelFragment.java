package vcc.com.wowholiday.presenter.register;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHApiActionCallback;
import common.presenter.WHFragment;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.action.ActionException;
import quangph.com.mvp.mvp.action.scheduler.thirdparty.ThirdPartyScheduler;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.domain.usecase.user.RegisterAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidEmailAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidPhoneNumberAction;
import vcc.com.wowholiday.domain.usecase.valid.ValidTextException;
import vcc.com.wowholiday.domain.usecase.valid.ValidTextType;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.RegisterType;
import vcc.com.wowholiday.presenter.confirmaccount.OtpConfirmActivity;
import vcc.com.wowholiday.presenter.webview.WebViewActivity;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */
public class RegisterPanelFragment extends WHFragment<RegisterPanelView> {

    private int mMode;

    public RegisterPanelFragment(int mode) {
        mMode = mode;
    }

    @Override
    protected int onGetLayoutId() {
        return R.layout.fragment_register_panel;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            getActivity().setResult(resultCode);
            getActivity().finish();
        } else {
            getActivity().setResult(Activity.RESULT_OK, data);
            getActivity().finish();
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        if (mMode == RegisterType.PHONE) {
            mView.setHint(getString(R.string.phone_number));
        } else if (mMode == RegisterType.EMAIL) {
            mView.setHint(getString(R.string.email));
            mView.setEmailMode();
        }
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof RegisterPanelView.RegisterCmd) {
            doRegister(((RegisterPanelView.RegisterCmd) command).name);
        } else if (command instanceof RegisterPanelView.RuleClickCmd) {
            Intent itn = new Intent(getContext(), WebViewActivity.class);
            WebViewActivity.createParams(itn, getString(R.string.rule_holiday));
            startActivity(itn);
        } else if (command instanceof RegisterPanelView.PolicyClickCmd) {
            Intent itn = new Intent(getContext(), WebViewActivity.class);
            WebViewActivity.createParams(itn, getString(R.string.policy_3));
            startActivity(itn);
        }
    }

    private void doRegister(String name) {
        if (mMode == RegisterType.PHONE) {
            if (validPhone(name)) {
                callOTP(name);
            }
        } else {
            if (validEmail(name)) {
                callOTP(name);
            }
        }
    }

    private boolean validPhone(String phone) {
        ValidPhoneNumberAction.ValidPhoneNumberReq req = new ValidPhoneNumberAction.ValidPhoneNumberReq();
        req.phoneNumber = phone;
        ValidPhoneNumberAction action = new ValidPhoneNumberAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            ValidTextException exception = (ValidTextException) e;
            if (exception.getValidTextType() == ValidTextType.EMPTY) {
                mView.showError(getString(R.string.empty_error));
            } else if (exception.getValidTextType() == ValidTextType.FORMAT) {
                mView.showError(getString(R.string.phone_format_error));
            }
            return false;
        }
    }

    private boolean validEmail(String email) {
        ValidEmailAction.ValidEmailReq req = new ValidEmailAction.ValidEmailReq();
        req.email = email;
        ValidEmailAction action = new ValidEmailAction();
        action.setRequestValue(req);
        try {
            return action.execute();
        } catch (ActionException e) {
            e.printStackTrace();
            ValidTextException exception = (ValidTextException) e;
            if (exception.getValidTextType() == ValidTextType.EMPTY) {
                mView.showError(getString(R.string.empty_error));
            } else if (exception.getValidTextType() == ValidTextType.FORMAT) {
                mView.showError(getString(R.string.email_format_error));
            }
            return false;
        }
    }


    private void callOTP(String emailOrPhone) {
        Intent itn = new Intent(getContext(), OtpConfirmActivity.class);
        OtpConfirmActivity.createParams(itn, emailOrPhone, mMode);
        startActivityForResult(itn, 1235);
        //getActivity().finish();
    }
}
