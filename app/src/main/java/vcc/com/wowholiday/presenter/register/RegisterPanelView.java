package vcc.com.wowholiday.presenter.register;

import android.content.Context;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.util.SpannableBuilder;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */
public class RegisterPanelView extends BaseLinearView {

    private EditText mEdtName;
    private TextInputLayout mTilName;

    public RegisterPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        mEdtName = findViewById(R.id.register_panel_act_edt_name);
        mTilName = findViewById(R.id.register_panel_act_til_name);

        initPolicy();

        findViewById(R.id.register_act_btn_register).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterCmd cmd = new RegisterCmd();
                cmd.name = mEdtName.getText().toString().trim();
                mPresenter.executeCommand(cmd);
            }
        });
    }

    public void setHint(String hint) {
        mTilName.setHint(hint);
    }

    public void setEmailMode() {
        mEdtName.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    public void showError(String error) {
        mTilName.setError(error);
    }

    private void initPolicy() {
        TextView tv = findViewById(R.id.register_panel_act_tv_policy);
        ClickableSpan ruleSpan = new ClickableSpan() {
            public void onClick(View view) {
                mPresenter.executeCommand(new RuleClickCmd());
            }
        };

        ClickableSpan policySpan = new ClickableSpan() {
            public void onClick(View view) {
                mPresenter.executeCommand(new PolicyClickCmd());
            }
        };

        SpannableBuilder builder = new SpannableBuilder();
        builder.appendText(getResources().getString(R.string.policy_1) + " ")
                .appendText(getResources().getString(R.string.policy_2))
                //.withSpan(new UnderlineSpan())
                .withSpan(ruleSpan)
                .appendText(" ")
                .appendText(getResources().getString(R.string.and))
                .appendText(" ")
                .appendText(getResources().getString(R.string.policy_3))
                //.withSpan(new UnderlineSpan())
                .withSpan(policySpan);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(builder.getSpannedText(), TextView.BufferType.SPANNABLE);
    }


    public class RegisterCmd implements ICommand {
        String name;
    }

    public class RuleClickCmd implements ICommand{}
    public class PolicyClickCmd implements ICommand{}
}
