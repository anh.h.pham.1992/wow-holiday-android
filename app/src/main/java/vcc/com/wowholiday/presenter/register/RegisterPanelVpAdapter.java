package vcc.com.wowholiday.presenter.register;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.RegisterType;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */
public class RegisterPanelVpAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public RegisterPanelVpAdapter(@NonNull FragmentManager fm, Context context) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return new RegisterPanelFragment(position == 0 ? RegisterType.PHONE : RegisterType.EMAIL);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.phone_number);
        } else {
            return mContext.getString(R.string.email);
        }
    }
}
