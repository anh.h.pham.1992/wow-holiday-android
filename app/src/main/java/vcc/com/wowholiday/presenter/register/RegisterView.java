package vcc.com.wowholiday.presenter.register;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/15/2019.
 */
public class RegisterView extends BaseLinearView {
    public RegisterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        ViewPager vp = findViewById(R.id.register_act_vp_panel);
        vp.setAdapter(new RegisterPanelVpAdapter(((AppCompatActivity)getContext()).getSupportFragmentManager(),
                getContext()));

        TabLayout tab = findViewById(R.id.register_act_tab);
        tab.setupWithViewPager(vp);
    }
}
