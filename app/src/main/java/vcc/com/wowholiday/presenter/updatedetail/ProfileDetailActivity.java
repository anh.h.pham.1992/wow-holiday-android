package vcc.com.wowholiday.presenter.updatedetail;

import android.content.Intent;

import androidx.annotation.Nullable;

import common.presenter.WHActivity;
import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.presenter.editprofile.EditProfileActivity;

/**
 * Created by Pham Hai Quang on 10/14/2019.
 */

@Layout(R.layout.activity_profile_detail)
public class ProfileDetailActivity extends WHActivity<ProfileDetailView> {

    public static final String USER = "user";

    private User mUser;

    public static void buildParam(Intent itn, User user) {
        itn.putExtra(USER, user);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && resultCode == RESULT_OK) {
            Intent itn = new Intent();
            itn.putExtra(USER, data.getParcelableExtra(EditProfileActivity.USER));
            setResult(RESULT_OK, itn);
            finish();
        }
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        mUser = getIntent().getParcelableExtra(USER);
        mView.showUserInfo(mUser);
    }

    @Override
    public void onExecuteCommand(ICommand command) {
        super.onExecuteCommand(command);
        if (command instanceof ProfileDetailView.EditProfileCmd) {
            gotoEditScreen();
        }
    }

    private void gotoEditScreen() {
        Intent itn = new Intent(this, EditProfileActivity.class);
        EditProfileActivity.buildParam(itn, mUser);
        startActivityForResult(itn, 1234);
    }
}
