package vcc.com.wowholiday.presenter.updatedetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import quangph.com.mvp.mvp.ICommand;
import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 10/14/2019.
 */
public class ProfileDetailView extends BaseLinearView {
    public ProfileDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onInitView() {
        findViewById(R.id.profile_detail_act_btn_edit).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.executeCommand(new EditProfileCmd());
            }
        });
    }

    public void showUserInfo(User user) {
        TextView tvLastName = findViewById(R.id.profile_detail_act_tv_last_name);
        tvLastName.setText(user.getLastName());

        TextView tvFirstName = findViewById(R.id.profile_detail_act_tv_first_name);
        tvFirstName.setText(user.getFirstName());

        TextView tvGender = findViewById(R.id.profile_detail_act_tv_gender);
        if (user.getGender().equals(User.MALE)) {
            tvGender.setText(R.string.male);
        } else {
            tvGender.setText(R.string.female);
        }

        if (!TextUtils.isEmpty(user.getBirthday())) {
            TextView tvBir = findViewById(R.id.profile_detail_act_tv_birthday);
            tvBir.setText(TimeFormatUtil.getDefaultDateFormatFromISO8601UTC(user.getBirthday()));
        }
    }


    public static class EditProfileCmd implements ICommand{}
}
