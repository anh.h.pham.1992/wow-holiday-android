package vcc.com.wowholiday.presenter.util;

/**
 * Created by Pham Hai Quang on 10/22/2019.
 */
public class MoneyFormatter {
    public static String defaultFormat(String price) {
        String priceStr = price;
        int dotIndex = priceStr.indexOf(".");
        String afterDot = null;
        if (dotIndex > -1) {
            String beforeDot = priceStr.substring(0, dotIndex);
            afterDot = priceStr.substring(dotIndex);
            priceStr = beforeDot;
        }

        StringBuilder str = new StringBuilder(priceStr);
        String strReveser = str.reverse().toString();
        int dotCount;
        if (priceStr.length() % 3 == 0) {
            dotCount = priceStr.length() / 3 - 1;
        } else {
            dotCount = priceStr.length() / 3;
        }
        int dem = 0;
        for (int i = 1; i <= dotCount; i++) {
            strReveser = strReveser.substring(0, i * 3 + dem) + "," + strReveser.substring(i * 3 + dem);
            dem++;
        }
        StringBuilder strReturn = new StringBuilder(strReveser);
        String val = strReturn.reverse().toString();
        if (afterDot != null) {
            return val + afterDot;
        }
        return val;
    }
}
