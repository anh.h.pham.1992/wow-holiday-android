package vcc.com.wowholiday.presenter.webview;

import android.content.Intent;

import common.presenter.WHActivity;
import quangph.com.mvp.viewbinder.Layout;
import vcc.com.wowholiday.R;

/**
 * Created by QuangPH on 2020-02-17.
 */

@Layout(R.layout.activity_web_view)
public class WebViewActivity extends WHActivity<WebViewView> {

    public static void createParams(Intent itn, String title) {
        itn.putExtra("title", title);
    }

    @Override
    public void onPresenterReady() {
        super.onPresenterReady();
        String title = getIntent().getStringExtra("title");
        mView.showTitle(title);
    }
}
