package vcc.com.wowholiday.presenter.webview;

import android.content.Context;
import android.util.AttributeSet;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.ActionBarView;

/**
 * Created by QuangPH on 2020-02-17.
 */
public class WebViewView extends BaseLinearView {

    public WebViewView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void showTitle(String title) {
        ActionBarView actionBarView = findViewById(R.id.web_view_act_actionbar);
        actionBarView.setLeftLabel(title);
    }
}
