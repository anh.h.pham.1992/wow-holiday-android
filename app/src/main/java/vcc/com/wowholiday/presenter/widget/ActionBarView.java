package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 11/10/2019.
 */
public class ActionBarView extends RelativeLayout {

    private Drawable mLeftIcon;
    private String mLeftLabel;
    private String mLeftSubLabel;
    private String mRightLabel;
    private Drawable mRightIcon;
    private Drawable mSecondRightIcon;
    private int mTextColor;
    private int mSubTextColor;
    private int mRightTextColor;
    private int mIconColor;

    public ActionBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        compound();
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ImageView ivLeft = findViewById(R.id.action_bar_ic_back);
        TextView left = findViewById(R.id.action_bar_text_left);
        TextView subLeft = findViewById(R.id.action_bar_sub_text_left);
        if (mLeftIcon != null) {
            ivLeft.setImageDrawable(mLeftIcon);
            ivLeft.setColorFilter(mIconColor);
        } else {
            left.setPadding(getResources().getDimensionPixelSize(R.dimen.dimen_16), 0,
                    left.getPaddingRight(), 0);
        }

        if (!TextUtils.isEmpty(mLeftLabel)) {
            left.setText(mLeftLabel);
        }
        left.setTextColor(mTextColor);

        if (!TextUtils.isEmpty(mLeftSubLabel)) {
            subLeft.setText(mLeftSubLabel);
            subLeft.setTextColor(mSubTextColor);
            float leftTextSize = getResources().getDimension(R.dimen.text_size_14);
            left.setTextSize(TypedValue.COMPLEX_UNIT_PX, leftTextSize);
            subLeft.setVisibility(VISIBLE);
        } else {
            float leftTextSize = getResources().getDimension(R.dimen.text_size_16);
            left.setTextSize(TypedValue.COMPLEX_UNIT_PX, leftTextSize);
            subLeft.setVisibility(GONE);
        }

        if (!TextUtils.isEmpty(mRightLabel)) {
            TextView right = findViewById(R.id.action_bar_text_right);
            if (mRightIcon == null && mSecondRightIcon == null) {
                right.setPadding(0, 0, getResources().getDimensionPixelSize(R.dimen.dimen_16), 0);
            }
            right.setText(mRightLabel);
            right.setTextColor(mRightTextColor);
        }
        ImageView right = findViewById(R.id.action_bar_ic_right);
        if (mRightIcon != null) {
            right.setColorFilter(mIconColor);
            right.setImageDrawable(mRightIcon);
            right.setVisibility(VISIBLE);
        } else {
            right.setVisibility(GONE);
        }

        ImageView secondRight = findViewById(R.id.action_bar_second_ic_right);
        if (mSecondRightIcon != null) {
            TextView tvRight = findViewById(R.id.action_bar_text_right);
            tvRight.setPadding(0, 0, getResources().getDimensionPixelSize(R.dimen.item_height) * 2, 0);
            secondRight.setColorFilter(mIconColor);
            secondRight.setImageDrawable(mSecondRightIcon);
            secondRight.setVisibility(VISIBLE);
        } else {
            secondRight.setVisibility(GONE);
        }
    }

    public void setLeftLabel(String label) {
        TextView left = findViewById(R.id.action_bar_text_left);
        left.setText(label);
    }

    public void setLeftSubLabel(String label) {
        TextView subLeft = findViewById(R.id.action_bar_sub_text_left);
        subLeft.setText(label);
    }

    public void setRightTextOnClickListener(OnClickListener onClickListener) {
        TextView right = findViewById(R.id.action_bar_text_right);
        right.setOnClickListener(onClickListener);
    }

    private void compound() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_action_bar, this, true);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.ActionBarView, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.ActionBarView_action_bar_left_icon)) {
                mLeftIcon = ta.getDrawable(R.styleable.ActionBarView_action_bar_left_icon);
            }

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_left_label)) {
                mLeftLabel = ta.getString(R.styleable.ActionBarView_action_bar_left_label);
            }

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_left_sub_label)) {
                mLeftSubLabel = ta.getString(R.styleable.ActionBarView_action_bar_left_sub_label);
            }

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_right_label)) {
                mRightLabel = ta.getString(R.styleable.ActionBarView_action_bar_right_label);
            }

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_right_icon)) {
                mRightIcon = ta.getDrawable(R.styleable.ActionBarView_action_bar_right_icon);
            }

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_second_right_icon)) {
                mSecondRightIcon = ta.getDrawable(R.styleable.ActionBarView_action_bar_second_right_icon);
            }

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_text_color)) {
                mTextColor = ta.getColor(R.styleable.ActionBarView_action_bar_text_color,
                        getResources().getColor(R.color.colorPrimary));
            }

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_icon_tint)) {
                mIconColor = ta.getColor(R.styleable.ActionBarView_action_bar_icon_tint, Color.WHITE);
            }

            mRightTextColor = mTextColor;

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_right_text_color)) {
                mRightTextColor = ta.getColor(R.styleable.ActionBarView_action_bar_right_text_color, mTextColor);
            }

            mSubTextColor = mTextColor;

            if (ta.hasValue(R.styleable.ActionBarView_action_bar_sub_text_color)) {
                mSubTextColor = ta.getColor(R.styleable.ActionBarView_action_bar_sub_text_color, mTextColor);
            }

            ta.recycle();
        }
    }
}
