package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Pham Hai Quang on 10/10/2019.
 */
public class CenterHorizontalContentView extends FrameLayout {

    public CenterHorizontalContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int totalChildrenWidth = calculateTotalChildrenWidth();
        int space = calculateSpaceBetweenChildren(totalChildrenWidth);
        int childCount = getChildCount();
        int totalLeft = l;
        View child;
        for (int i = 0; i < childCount; i++) {
            child = getChildAt(i);
            int left = totalLeft + space;
            int top = t + (getMeasuredHeight() - child.getMeasuredHeight()) / 2;
            int right = left + child.getMeasuredWidth();
            int bottom = top + child.getMeasuredHeight();
            child.layout(left, top, right, bottom);
            totalLeft = right;
        }
    }

    private int calculateTotalChildrenWidth() {
        int totalWidth = 0;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            totalWidth += child.getMeasuredWidth();
        }
        return totalWidth;
    }

    private int calculateSpaceBetweenChildren(int totalChildrenWidth) {
        return (getMeasuredWidth() - totalChildrenWidth) / (getChildCount() + 1);
    }
}
