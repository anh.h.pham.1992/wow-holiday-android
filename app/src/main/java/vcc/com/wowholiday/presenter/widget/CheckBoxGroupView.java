package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import vcc.com.wowholiday.R;

public class CheckBoxGroupView<T> extends LinearLayout {
    private Value<T>[] mValues;
    private HashMap<Value<T>, CheckBox> mCheckBoxes;

    public CheckBoxGroupView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mCheckBoxes = new HashMap<>();
        setOrientation(LinearLayout.VERTICAL);
    }

    public void setValues(Value<T>[] mValues) {
        this.mValues = mValues;
        initViews();
    }

    public void setCheckedValues(Value[] checkedValues) {
        for (Map.Entry<Value<T>, CheckBox> entry : mCheckBoxes.entrySet()) {
            CheckBox checkBox = entry.getValue();
            checkBox.setChecked(false);
        }
        if (checkedValues == null)
            return;
        for (Value<T> value :
                checkedValues) {
            CheckBox checkbox = mCheckBoxes.get(value);
            if (checkbox != null)
                checkbox.setChecked(true);
        }
    }

    public Value<T>[] getCheckedValues() {
        ArrayList<Value> checkedValues = new ArrayList<>();
        for (Map.Entry<Value<T>, CheckBox> entry : mCheckBoxes.entrySet()) {
            CheckBox checkBox = entry.getValue();
            Value<T> value = entry.getKey();
            if(checkBox.isChecked())
                checkedValues.add(value);
        }
        return checkedValues.toArray(new Value[checkedValues.size()]);
    }

    private void initViews() {
        removeAllViews();
        mCheckBoxes.clear();
        if (mValues != null)
            for (final Value<T> v : mValues) {
                AppCompatCheckBox checkBox = new AppCompatCheckBox(this.getContext());
                checkBox.setButtonDrawable(R.drawable.selector_burgundy_checkbox);
                checkBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_14));
                checkBox.setTextColor(getResources().getColor(R.color.dark));
                int paddingVertical = getResources().getDimensionPixelSize(R.dimen.dimen_4);
                int paddingLeft = getResources().getDimensionPixelSize(R.dimen.dimen_8);
                checkBox.setPadding(paddingLeft, paddingVertical, 0, paddingVertical);
                checkBox.setText(v.getLabel());
                checkBox.setChecked(v.isChecked());
                checkBox.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        v.setChecked(!v.isChecked());
                    }
                });
                addView(checkBox);
                mCheckBoxes.put(v, checkBox);
            }
    }

    static public class Value<T> {
        private String mLabel;
        private T mValue;
        private Boolean mChecked = false;

        public Value(String mLabel, T mValue, Boolean mChecked) {
            this.mLabel = mLabel;
            this.mValue = mValue;
            this.mChecked = mChecked;
        }


        @Override
        public boolean equals(@Nullable Object obj) {
            if (! (obj instanceof Value))
                return false;
            return mValue != null && mValue.equals(((Value) obj).getValue());
        }

        @Override
        public int hashCode() {
            return mValue.hashCode();
        }

        public String getLabel() {
            return mLabel;
        }

        public void setLabel(String mLabel) {
            this.mLabel = mLabel;
        }

        public T getValue() {
            return mValue;
        }

        public void setValue(T mValue) {
            this.mValue = mValue;
        }

        public Boolean isChecked() {
            return mChecked;
        }

        public void setChecked(Boolean mChecked) {
            this.mChecked = mChecked;
        }
    }
}
