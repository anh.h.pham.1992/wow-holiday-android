package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import vcc.com.wowholiday.R;

public class CheckableStar extends LinearLayout implements Checkable {
    private static final int[] CheckedStateSet = {
            android.R.attr.state_checked,
    };

    private boolean mIsChecked = false;

    private int mNumber;
    private TextView mTitleTextView;
    private ImageView mStar;

    public CheckableStar(Context context) {
        super(context);
        compound();
        init();
    }

    public CheckableStar(Context context, AttributeSet attrs) {
        super(context, attrs);
        compound();
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);
        setBackgroundResource(R.drawable.selector_light_burgundy_rounded_corner);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckableStar.this.toggle();
            }
        });
        setClickable(true);
        mTitleTextView = findViewById(R.id.star_checkbox_layout_tv_number);
        mStar = findViewById(R.id.star_checkbox_layout_iv_star);
        mTitleTextView.setText(Integer.toString(mNumber));
    }

    private void compound() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_checkable_star, this, true);
    }


    public int getNumber() {
        return mNumber;
    }

    public void setNumber(int number) {
        mNumber = number;
        if (mTitleTextView != null)
            mTitleTextView.setText(Integer.toString(mNumber));
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CheckedStateSet);
        }
        return drawableState;
    }

    @Override
    public void setChecked(boolean b) {
        mIsChecked = b;
        refreshDrawableState();
        if (mTitleTextView != null)
            mTitleTextView.refreshDrawableState();
        if (mStar != null)
            mStar.refreshDrawableState();
    }

    @Override
    public boolean isChecked() {
        return mIsChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mIsChecked);
    }
}
