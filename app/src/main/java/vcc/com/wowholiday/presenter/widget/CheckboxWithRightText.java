package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/25/2019.
 */
public class CheckboxWithRightText extends LinearLayout implements Checkable {

    private ImageView mCb;
    private TextView mTvTitle;
    private String mTitle;
    private boolean isChecked;
    private OnClickListener mListener;

    public CheckboxWithRightText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        compound();
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mCb = findViewById(R.id.checkbox_with_right_text_layout_iv_checkbox);
        mTvTitle = findViewById(R.id.checkbox_with_right_text_layout_tv_title);
        mTvTitle.setText(mTitle);
        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
                if (mListener != null) {
                    mListener.onClick(v);
                }
            }
        });

        refreshCheckbox();
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        mListener = l;
    }

    @Override
    public void setChecked(boolean checked) {
        isChecked = checked;
        refreshCheckbox();
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void toggle() {
        isChecked = !isChecked;
        refreshCheckbox();
    }

    private void compound() {
        setOrientation(HORIZONTAL);
        LayoutInflater.from(getContext()).inflate(R.layout.layout_checkbox_with_right_text, this, true);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.CheckboxWithRightText, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.CheckboxWithRightText_check_box_text)) {
                mTitle = ta.getString(R.styleable.CheckboxWithRightText_check_box_text);
            }

            if (ta.hasValue(R.styleable.CheckboxWithRightText_check_box_checked)) {
                isChecked = ta.getBoolean(R.styleable.CheckboxWithRightText_check_box_checked, false);
            }

            ta.recycle();
        }
    }

    private void refreshCheckbox() {
        if (isChecked) {
            mCb.setImageResource(R.drawable.ic_radio_check_24px);
            mCb.setColorFilter(ContextCompat.getColor(getContext(), R.color.light_burgundy));
        } else {
            mCb.setImageResource(R.drawable.ic_radio_uncheck_24px);
            mCb.setColorFilter(ContextCompat.getColor(getContext(), R.color.dark));
        }
    }
}
