package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import vcc.com.wowholiday.R;

/**
 * Created by QuangPH on 2020-01-15.
 */
public class DashgapDividerView extends View {

    static public int ORIENTATION_HORIZONTAL = 0;
    static public int ORIENTATION_VERTICAL = 1;

    private Paint mPaint;
    private int mOrientation;

    public DashgapDividerView(Context context) {
        super(context);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
    }

    public DashgapDividerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        int dashGap, dashLength, dashThickness;
        int color;

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DashgapDividerView, 0, 0);

        try {
            dashGap = a.getDimensionPixelSize(R.styleable.DashgapDividerView_dashGap, 5);
            dashLength = a.getDimensionPixelSize(R.styleable.DashgapDividerView_dashLength, 5);
            dashThickness = a.getDimensionPixelSize(R.styleable.DashgapDividerView_dashThickness, 3);
            color = a.getColor(R.styleable.DashgapDividerView_color, 0xff000000);
            mOrientation = a.getInt(R.styleable.DashgapDividerView_orientation, ORIENTATION_HORIZONTAL);
        } finally {
            a.recycle();
        }

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(color);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(dashThickness);
        mPaint.setPathEffect(new DashPathEffect(new float[] { dashLength, dashGap, }, 0));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mOrientation == ORIENTATION_HORIZONTAL) {
            float center = getHeight() * .5f;
            canvas.drawLine(0, center, getWidth(), center, mPaint);
        } else {
            float center = getWidth() * .5f;
            canvas.drawLine(center, 0, center, getHeight(), mPaint);
        }
    }

    public void setOrientation(int orientation) {
        mOrientation = orientation;
    }

    public void setStrokeWidth(int width) {
        mPaint.setStrokeWidth(width);
    }

    public void setColor(int color) {
        mPaint.setColor(color);
    }

    public void setDashGap(int dashLength, int dashGap) {
        mPaint.setPathEffect(new DashPathEffect(new float[] { dashLength, dashGap, }, 0));
    }
}
