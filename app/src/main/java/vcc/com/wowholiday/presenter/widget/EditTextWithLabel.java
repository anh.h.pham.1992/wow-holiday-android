package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 2/13/2019.
 */

public class EditTextWithLabel extends LinearLayout {

    private TextView mTvLeftLabel;
    private TextView mTvRightLabel;
    private EditText mEditText;

    private String mText;
    private int mTextColor;
    private float mTextSize;
    private String mStrLeftLabel;
    private String mStrRightLabel;
    private int mLeftLabelColor;
    private int mRightLabelColor;
    private String mHint;
    private int mInputType;


    public EditTextWithLabel(Context context) {
        super(context);
    }

    public EditTextWithLabel(Context context, AttributeSet attrs) {
        super(context, attrs);
        compoundView();
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTvLeftLabel = findViewById(R.id.edt_with_label_layout_tv_left_label);
        mTvRightLabel = findViewById(R.id.edt_with_label_layout_tv_right_label);
        mEditText = findViewById(R.id.edt_with_label_layout_edt_content);

        mTvLeftLabel.setText(mStrLeftLabel);
        if (mLeftLabelColor != 0) {
            mTvLeftLabel.setTextColor(mLeftLabelColor);
        }

        mTvRightLabel.setText(mStrRightLabel);
        if (mRightLabelColor != 0) {
            mTvRightLabel.setTextColor(mRightLabelColor);
        }

        mEditText.setHint(mHint);
        mEditText.setText(mText);

        if (mTextColor != 0) {
            mEditText.setTextColor(mTextColor);
        }

        if (mTextSize > 0) {
            mEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX,mTextSize);
            mTvLeftLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX,mTextSize);
            mTvRightLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX,mTextSize);
        }

        if (mInputType > 0) {
            mEditText.setInputType(mInputType);
        }
    }

    public void setText(String text) {
        mEditText.setText(text);
    }

    private void compoundView() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_edittext_with_label, this, true);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.EditTextWithLabel, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.EditTextWithLabel_android_text)) {
                mText = ta.getString(R.styleable.EditTextWithLabel_android_text);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_edt_with_label_left_label)) {
                mStrLeftLabel = ta.getString(R.styleable.EditTextWithLabel_edt_with_label_left_label);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_edt_with_label_right_label)) {
                mStrRightLabel = ta.getString(R.styleable.EditTextWithLabel_edt_with_label_right_label);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_android_textColor)) {
                mTextColor = ta.getColor(R.styleable.EditTextWithLabel_android_textColor, 0);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_edt_with_label_left_label_color)) {
                mLeftLabelColor = ta.getColor(R.styleable.EditTextWithLabel_edt_with_label_left_label_color, 0);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_edt_with_label_right_label_color)) {
                mRightLabelColor = ta.getColor(R.styleable.EditTextWithLabel_edt_with_label_right_label_color, 0);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_android_hint)) {
                mHint = ta.getString(R.styleable.EditTextWithLabel_android_hint);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_android_textSize)) {
                mTextSize = ta.getDimensionPixelSize(R.styleable.EditTextWithLabel_android_textSize, 0);
            }

            if (ta.hasValue(R.styleable.EditTextWithLabel_android_inputType)) {
                mInputType = ta.getInt(R.styleable.EditTextWithLabel_android_inputType, 0);
            }
            ta.recycle();
        }
    }

    public void addTextChangedListener(TextWatcher watcher) {
        mEditText.addTextChangedListener(watcher);
    }

    public EditText getEditText() {
        return mEditText;
    }

    public String getText() {
        return mEditText.getText().toString();
    }
}
