package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaTimestamp;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import net.cachapa.expandablelayout.ExpandableLayout;

import vcc.com.wowholiday.R;

public class ExpandableLayoutHeader extends LinearLayout {
    private ExpandableLayout mExpandableLayout;
    private Drawable mIndictorIcon;
    private String mTitle;
    private int mTitleColor;
    private int mIconColor;
    private ImageView mIndictorImageView;

    public ExpandableLayoutHeader(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        compound();
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!TextUtils.isEmpty(mTitle)) {
            TextView title = findViewById(R.id.expandable_layout_header_layout_tv_title);
            title.setText(mTitle);
            title.setTextColor(mTitleColor);
        }

        mIndictorImageView = findViewById(R.id.expandable_layout_header_layout_iv_indicator);
        if (mIndictorIcon != null) {
            mIndictorImageView.setColorFilter(mIconColor);
            mIndictorImageView.setImageDrawable(mIndictorIcon);
            mIndictorImageView.setVisibility(VISIBLE);
        } else {
            mIndictorImageView.setVisibility(GONE);
        }

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mExpandableLayout != null) mExpandableLayout.toggle();
            }
        });
    }

    public void setExpandableLayout(ExpandableLayout expandableLayout) {
        this.mExpandableLayout = expandableLayout;
        mExpandableLayout.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
            @Override
            public void onExpansionUpdate(float expansionFraction, int state) {
                if (mIndictorImageView != null)
                    mIndictorImageView.setRotation((1 - expansionFraction) * 180);
            }
        });
    }

    private void compound() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_expanable_layout_header, this, true);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.ExpandableLayoutHeader, 0, 0);
        if (ta != null) {


            if (ta.hasValue(R.styleable.ExpandableLayoutHeader_title)) {
                mTitle = ta.getString(R.styleable.ExpandableLayoutHeader_title);
            }

            if (ta.hasValue(R.styleable.ExpandableLayoutHeader_indicator_icon)) {
                mIndictorIcon = ta.getDrawable(R.styleable.ExpandableLayoutHeader_indicator_icon);
            }

            if (ta.hasValue(R.styleable.ExpandableLayoutHeader_title_color)) {
                mTitleColor = ta.getColor(R.styleable.ExpandableLayoutHeader_title_color,
                        getResources().getColor(R.color.dark));
            }

            if (ta.hasValue(R.styleable.ExpandableLayoutHeader_indicator_icon_tint)) {
                mIconColor = ta.getColor(R.styleable.ExpandableLayoutHeader_indicator_icon_tint, getResources().getColor(R.color.turquoise_blue));
            }
            ta.recycle();
        }
    }

}
