package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import java.util.List;

import vcc.com.wowholiday.model.Amenity;

public class FacilitiesView extends AppCompatTextView {
    public FacilitiesView(Context context) {
        super(context);
    }

    public FacilitiesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setFacilities(String[] facilities) {
        if (facilities != null && facilities.length > 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < facilities.length; i++) {
                sb.append("\u2022 ");
                sb.append(facilities[i]);
                if (i < facilities.length - 1)
                    sb.append("\n");
            }
            setText(sb);
        } else {
            setText(null);
        }
    }

    public void setFacilities(List<Amenity> facilities) {
        if (facilities != null && facilities.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < facilities.size(); i++) {
                sb.append("\u2022 ");
                sb.append(facilities.get(i).getName());
                if (i < facilities.size() - 1)
                    sb.append("\n");
            }
            setText(sb);
        } else {
            setText(null);
        }
    }
}
