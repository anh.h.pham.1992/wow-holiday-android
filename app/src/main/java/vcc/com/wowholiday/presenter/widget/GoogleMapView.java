package vcc.com.wowholiday.presenter.widget;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;

import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;

import common.presenter.WHActivity;
import vcc.com.wowholiday.WHApplication;

/**
 * Created by Pham Hai Quang on 10/24/2019.
 */
public class GoogleMapView extends MapView {

    public GoogleMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onInitView(Bundle savedInstanceState) {
        MapsInitializer.initialize(getContext());
        onCreate(savedInstanceState);

        final WHApplication application = (WHApplication) ((WHActivity)getContext()).getApplication();
        final ComponentCallbacks2 callbacks2 = new ComponentCallbacks2() {
            @Override
            public void onTrimMemory(int level) {
            }

            @Override
            public void onConfigurationChanged(Configuration newConfig) {

            }

            @Override
            public void onLowMemory() {
                GoogleMapView.this.onLowMemory();
            }
        };

        application.registerComponentCallbacks(callbacks2);

        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            }

            @Override
            public void onActivityStarted(Activity activity) {
            }

            @Override
            public void onActivityResumed(Activity activity) {
                if (activity == getContext()) {
                    onResume();
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {
                if (activity == getContext()) {
                    onPause();
                }
            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                if (activity == getContext()) {
                    onSaveInstanceState(outState);
                }
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                if (activity == getContext()) {
                    application.unregisterComponentCallbacks(callbacks2);
                    application.unregisterActivityLifecycleCallbacks(this);
                    onDestroy();
                }
            }
        });
    }

}
