package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseLinearView;
import vcc.com.wowholiday.R;

public class HashTagsView extends BaseLinearView {
    public HashTagsView(Context context) {
        super(context);
        init();
    }

    public HashTagsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setHashTags(String[] hashTags) {
        removeAllViews();
        if(hashTags == null)
            return;
        for (String hashTag :
                hashTags) {
            TextView tvHashTag = new TextView(getContext());
            tvHashTag.setText(hashTag);
            tvHashTag.setTextColor(getResources().getColor(R.color.white));
            tvHashTag.setBackground(getResources().getDrawable(R.drawable.shape_light_grey_blue_bg_corner_tiny));
            int paddingHorizontal = getResources().getDimensionPixelSize(R.dimen.dimen_8);
            int paddingVertical = getResources().getDimensionPixelSize(R.dimen.dimen_4);
            tvHashTag.setPadding(paddingHorizontal,paddingVertical,paddingHorizontal,paddingVertical);
            LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
            layoutParams.setMarginEnd(paddingHorizontal);
            this.addView(tvHashTag,layoutParams);
        }
    }

    private void init(){
        setOrientation(HORIZONTAL);
    }
}
