package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import quangph.com.mvp.mvp.mvpcomponent.view.BaseConstraintView;
import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;

public class ImageCarousel extends BaseConstraintView {
    private String[] mImageUrls = new String[]{};
    private ViewPager mViewPager;
    private TextView mTvIndicator;
    private TextView mTvViewAll;
    private ImageView mImvPrevious;
    private ImageView mImvNext;

    ImagePagerAdapter mAdapter;

    public ImageCarousel(Context context) {
        super(context);
        compoundView();
    }

    public ImageCarousel(Context context, AttributeSet attrs) {
        super(context, attrs);
        compoundView();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mViewPager = findViewById(R.id.image_carousel_layout_vp_images);
        mTvIndicator = findViewById(R.id.image_carousel_layout_tv_indicator);
        mTvViewAll = findViewById(R.id.image_carousel_layout_tv_view_all);
        mImvPrevious = findViewById(R.id.image_carousel_layout_imv_previous);
        mImvNext = findViewById(R.id.image_carousel_layout_imv_next);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTvIndicator.setText((position + 1) + "/" + mAdapter.getCount());
                if (position == 0) {
                    mImvPrevious.setVisibility(GONE);
                } else {
                    mImvPrevious.setVisibility(VISIBLE);
                    if (position == mAdapter.getCount() - 1)
                        mImvNext.setVisibility(GONE);
                    else
                        mImvNext.setVisibility(VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mImvPrevious.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
            }
        });

        mImvNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
            }
        });

        setImageUrls(new String[]{});
    }

    public void setImageUrls(String[] imageUrls) {
        this.mImageUrls = imageUrls;
        mAdapter = new ImagePagerAdapter(getContext(), mImageUrls);
        mViewPager.setAdapter(mAdapter);
        mTvIndicator.setText((mAdapter.getCount() == 0 ? 0 : mViewPager.getCurrentItem() + 1) + "/" + mAdapter.getCount());
        mImvPrevious.setVisibility(VISIBLE);
        mImvNext.setVisibility(VISIBLE);
        if (mViewPager.getCurrentItem() == 0)
            mImvPrevious.setVisibility(GONE);
        if (mAdapter.getCount() == 0 || mViewPager.getCurrentItem() == mAdapter.getCount() - 1)
            mImvNext.setVisibility(GONE);
    }

    public void setOnViewAllClickListener(OnClickListener onClickListener) {
        mTvViewAll.setOnClickListener(onClickListener);
    }

    private void compoundView() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_image_carousel, this, true);
    }

    private static class ImagePagerAdapter extends PagerAdapter {
        private GlideImageLoader imageLoader = GlideImageLoader.getInstance();
        private String[] mImageUrls;
        private Context mContext;

        public ImagePagerAdapter(Context context, String[] imageUrls) {
            this.mContext = context;
            this.mImageUrls = imageUrls;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            ImageView imageView = new ImageView(mContext);
            container.addView(imageView);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageLoader.loadImage(imageView, mImageUrls[position], imageView);
            return imageView;
        }


        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mImageUrls == null ? 0 : mImageUrls.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}
