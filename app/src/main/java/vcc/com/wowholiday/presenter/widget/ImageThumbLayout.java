package vcc.com.wowholiday.presenter.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Outline;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.List;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;

/**
 * Created by Pham Hai Quang on 10/23/2019.
 */
public class ImageThumbLayout extends LinearLayout {

    private ImageView[] mIvThumbs;
    private TextView mTvMore;

    private IImageLoader mImageLoader = GlideImageLoader.getInstance();

    public ImageThumbLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setMaxDisplayItem(int maxDisplay) {
        setWeightSum(maxDisplay);
        mIvThumbs = new ImageView[maxDisplay];
        for (int i = 0; i < maxDisplay - 1; i++) {
            addImageViewItem(i);
        }
        addLastImageItem();
    }

    @SuppressLint("DefaultLocale")
    public void showImage(List<String> urlList) {
        for (int i = 0; i < mIvThumbs.length; i++) {
            if (i >= urlList.size()) {
                mIvThumbs[i].setVisibility(INVISIBLE);

            } else {
                mImageLoader.loadImage(this, urlList.get(i), mIvThumbs[i]);
            }
        }

        int remain = urlList.size() - mIvThumbs.length;
        if (remain > 0) {
            mTvMore.setVisibility(View.VISIBLE);
            mTvMore.setText(String.format("+%d", remain));
        } else {
            mTvMore.setVisibility(INVISIBLE);
        }
    }

    private void addImageViewItem(int index) {
        RatioImageView iv = new RatioImageView(getContext());
        iv.setRatio("1:1");
        iv.setScaleType(ScaleType.CENTER_CROP);
        iv.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                int corner = (int) view.getResources().getDimension(R.dimen.corner_8);
                outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), corner);
            }
        });
        iv.setClipToOutline(true);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, 0, 1);
        addView(iv, params);
        mIvThumbs[index] = iv;
    }

    private void addLastImageItem() {
        RatioFrameLayout container = new RatioFrameLayout(getContext());
        container.setRatio("1:1");
        ImageView iv = new ImageView(getContext());
        iv.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                int corner = (int) view.getResources().getDimension(R.dimen.corner_8);
                outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), corner);
            }
        });
        iv.setClipToOutline(true);
        mIvThumbs[mIvThumbs.length - 1] = iv;
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        container.addView(iv, params);

        mTvMore = new TextView(getContext());
        mTvMore.setBackgroundResource(R.drawable.selector_dark2_opacity_70_bg_corner);
        mTvMore.setTextColor(Color.WHITE);
        mTvMore.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        mTvMore.setGravity(Gravity.CENTER);
        FrameLayout.LayoutParams tvParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        container.addView(mTvMore, tvParams);

        LinearLayout.LayoutParams containerParams = new LinearLayout.LayoutParams(0, 0, 1);
        addView(container, containerParams);
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setShowDividers(SHOW_DIVIDER_MIDDLE);
        setDividerDrawable(ContextCompat.getDrawable(getContext(), R.drawable.shape_empty_divider_hort_8dp));
    }
}
