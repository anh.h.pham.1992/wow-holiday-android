package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Outline;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.imageloader.GlideImageLoader;
import vcc.com.wowholiday.presenter.imageloader.IImageLoader;

/**
 * View with an image and a description
 * Created by Pham Hai Quang on 10/9/2019.
 */
public class ItemDescriptionView extends ConstraintLayout {

    private ImageView mIvAvatar;
    private TextView mTvDescription;

    private IImageLoader mLoader = GlideImageLoader.getInstance();

    public ItemDescriptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mIvAvatar = findViewById(R.id.item_description_iv_avatar);
        mTvDescription = findViewById(R.id.item_description_tv_des);
        mIvAvatar.setClipToOutline(true);
        mIvAvatar.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setRoundRect(0, 0, view.getRight(),
                        view.getBottom(), getResources().getDimensionPixelOffset(R.dimen.corner_8));
            }
        });
    }

    public void setAvatar(String url) {
        mLoader.loadImage(this, url, mIvAvatar);
    }

    public void setDescription(String des) {
        mTvDescription.setText(des);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.ItemDescriptionView, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.ItemDescriptionView_itm_description_compound_layout)) {
                int layoutRes = ta.getResourceId(R.styleable.ItemDescriptionView_itm_description_compound_layout, -1);
                if (layoutRes != -1) {
                    compound(layoutRes);
                }
            }
            ta.recycle();
        }
    }

    private void compound(int layoutRes) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(layoutRes, this, true);
    }
}
