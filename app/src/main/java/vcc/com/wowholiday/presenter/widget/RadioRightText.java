package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class RadioRightText extends LinearLayout implements Checkable {

    private View mVRadio;
    private TextView mTvLabel;

    private String mLabel;

    public RadioRightText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
        compound();
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mVRadio = findViewById(R.id.radio_right_text_layout_v_radio);
        mTvLabel = findViewById(R.id.radio_right_text_layout_tv_label);
        if (mLabel != null) {
            mTvLabel.setText(mLabel);
        }
    }

    @Override
    public void setChecked(boolean checked) {
        mVRadio.setSelected(checked);
    }

    @Override
    public boolean isChecked() {
        return mVRadio.isSelected();
    }

    @Override
    public void toggle() {
        mVRadio.setSelected(!mVRadio.isSelected());
    }

    private void compound() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_radio_right_text, this, true);
    }

    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs,
                R.styleable.RadioRightText);
        mLabel = typedArray.getString(R.styleable.RadioRightText_radio_right_label);
        typedArray.recycle();
    }
}
