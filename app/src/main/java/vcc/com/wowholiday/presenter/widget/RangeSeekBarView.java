package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.innovattic.rangeseekbar.RangeSeekBar;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import vcc.com.wowholiday.R;

public class RangeSeekBarView extends LinearLayout {
    private int mMinThumbValue;
    private int mMaxThumbValue;
    private int mMinValue;
    private int mMaxValue;
    private int mRange;
    private int mStep;
    private RangeSeekBar mRangeSeekBar;
    private OnRangeSeekBarChangeListener mOnRangeSeekBarChangeListener;

    public RangeSeekBarView(@NotNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        compoundView();
        init(attrs);
    }

    public RangeSeekBarView(@NotNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        compoundView();
        init(attrs);
    }

    public RangeSeekBarView(@NotNull Context context) {
        super(context);
        compoundView();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mRangeSeekBar = findViewById(R.id.range_seek_bar_layout_seek_bar);
        mRangeSeekBar.setSeekBarChangeListener(new RangeSeekBar.SeekBarChangeListener() {
            @Override
            public void onStartedSeeking() {

            }

            @Override
            public void onStoppedSeeking() {

            }

            @Override
            public void onValueChanged(int i, int i1) {
                if (mOnRangeSeekBarChangeListener != null) {
                    int min = Math.round((float)i / mStep) * mStep;
                    int max = Math.round((float)i1 / mStep) * mStep;
                    mOnRangeSeekBarChangeListener.onValueChanged(min + mMinValue, max + mMinValue);
                }
            }
        });
    }

    private void compoundView() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_range_seek_bar, this, true);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.TextViewWithIcon, 0, 0);
        if (ta != null) {
        }
    }

    private void updateSeekBar() {
        mRange = mMaxValue - mMinValue;
        if (mRangeSeekBar != null) {
            mRangeSeekBar.setMax(mRange);
            mRangeSeekBar.setMinThumbValue(mMinThumbValue - mMinValue);
            mRangeSeekBar.setMaxThumbValue(mMaxThumbValue - mMinValue);
        }
    }

    public void setRange(int minValue, int maxValue) {
        mMinValue = minValue;
        mMaxValue = maxValue;
        updateSeekBar();
    }

    public int getMinValue() {
        return mMinValue;
    }

    public void setMinValue(int minValue) {
        this.mMinValue = minValue;
        updateSeekBar();
    }

    public int getMaxValue() {
        return mMaxValue;
    }

    public void setMaxValue(int maxValue) {
        this.mMaxValue = maxValue;
        updateSeekBar();
    }

    public int getMinThumbValue() {
        return mRangeSeekBar.getMinThumbValue() + mMinValue;
    }

    public void setMinThumbValue(int minThumbValue) {
        this.mMinThumbValue = minThumbValue;
        updateSeekBar();
    }

    public int getMaxThumbValue() {
        return mRangeSeekBar.getMaxThumbValue() + mMinValue;
    }

    public void setMaxThumbValue(int maxThumbValue) {
        this.mMaxThumbValue = maxThumbValue;
        updateSeekBar();
    }

    public int getStep() {
        return mStep;
    }

    public void setStep(int step) {
        this.mStep = step;
    }

    public OnRangeSeekBarChangeListener getOnRangeSeekBarChangeListener() {
        return mOnRangeSeekBarChangeListener;
    }

    public void setOnRangeSeekBarChangeListener(OnRangeSeekBarChangeListener onRangeSeekBarChangeListener) {
        this.mOnRangeSeekBarChangeListener = onRangeSeekBarChangeListener;
    }

    public static interface OnRangeSeekBarChangeListener {
        /**
         * Gets called during the dragging of min or max value
         *
         * @param minThumbValue the current minimum value of selected range
         * @param maxThumbValue the current maximum value of selected range
         */
        void onValueChanged(int minThumbValue, int maxThumbValue);
    }
}
