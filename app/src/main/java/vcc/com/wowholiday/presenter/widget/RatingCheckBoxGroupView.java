package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import java.util.ArrayList;
import java.util.Arrays;

import vcc.com.wowholiday.R;

public class RatingCheckBoxGroupView extends ConstraintLayout {
    private int mNumberOfStar = 5;
    private ArrayList<CheckableStar> mCheckBoxes;

    public RatingCheckBoxGroupView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void initViews() {
        removeAllViews();
        mCheckBoxes.clear();
        int[] ids = new int[mNumberOfStar];
        int checkboxSize = getResources().getDimensionPixelSize(R.dimen.dimen_56);

        for (int i = 0; i < mNumberOfStar; i++) {
            CheckableStar checkBox = new CheckableStar(this.getContext());
            int id = generateViewId();
            ids[i] = id;
            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(checkboxSize,checkboxSize);
            checkBox.setLayoutParams(layoutParams);
            checkBox.setId(id);
            checkBox.setNumber(i + 1);
            checkBox.setChecked(false);
            addView(checkBox);
            mCheckBoxes.add(checkBox);
        }

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);
        View previousItem = null;
        for (CheckableStar checkBox : mCheckBoxes) {
            boolean lastItem = mCheckBoxes.indexOf(checkBox) == mCheckBoxes.size() - 1;
            if (previousItem == null) {
                constraintSet.connect(checkBox.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
            } else {
                constraintSet.connect(checkBox.getId(), ConstraintSet.LEFT, previousItem.getId(), ConstraintSet.RIGHT);
                if (lastItem) {
                    constraintSet.connect(checkBox.getId(), ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT);
                }
            }
            previousItem = checkBox;
        }

        constraintSet.createHorizontalChain(ConstraintSet.PARENT_ID, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, ids, null, ConstraintSet.CHAIN_SPREAD_INSIDE);
        constraintSet.applyTo(this);

    }

    private void init() {
        mCheckBoxes = new ArrayList<>();
        initViews();
    }

    private void updateCheckedState(Integer[] selectedStart) {
        for (CheckableStar ckb :
                mCheckBoxes) {
            ckb.setChecked(Arrays.asList(selectedStart).contains(ckb.getNumber()));
        }
    }

    public int getNumberOfStar() {
        return mNumberOfStar;
    }

    public void setNumberOfStar(int numberOfStar) {
        this.mNumberOfStar = numberOfStar;
    }

    public Integer[] getSelectedStars() {
        ArrayList<Integer> selectedStars = new ArrayList<>();
        for (CheckableStar ckb :
                mCheckBoxes) {
            if(ckb.isChecked())
                selectedStars.add(ckb.getNumber());
        }

        return selectedStars.toArray(new Integer[selectedStars.size()]);
    }

    public void setSelectedStars(Integer[] stars) {
        if (stars == null)
            return;
        updateCheckedState(stars);
    }
}
