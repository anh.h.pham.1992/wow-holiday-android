package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import vcc.com.wowholiday.R;

public class RatingView extends LinearLayout {

    private int mRating = 3;
    private int mActiveColor;
    private int mInactiveColor;

    public RatingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        compound();
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        update();
    }

    public void setRating(int mRating) {
        this.mRating = mRating;
        update();
    }

    private void update() {
        ImageView ivStar1 = findViewById(R.id.rating_layout_iv_star_1);
        ImageView ivStar2 = findViewById(R.id.rating_layout_iv_star_2);
        ImageView ivStar3 = findViewById(R.id.rating_layout_iv_star_3);
        ImageView ivStar4 = findViewById(R.id.rating_layout_iv_star_4);
        ImageView ivStar5 = findViewById(R.id.rating_layout_iv_star_5);
        ImageView[] ivStars = new ImageView[]{ivStar1, ivStar2, ivStar3, ivStar4, ivStar5};
        for (int i = 0; i < ivStars.length; i++) {
            if (i < mRating) {
                ivStars[i].setColorFilter(mActiveColor, PorterDuff.Mode.SRC_IN);
            } else {
                ivStars[i].setColorFilter(mInactiveColor,PorterDuff.Mode.SRC_IN);
            }
        }
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.RatingView, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.RatingView_rating_view_active_tint)) {
                mActiveColor = ta.getColor(R.styleable.RatingView_rating_view_active_tint, Color.YELLOW);
            }
            if (ta.hasValue(R.styleable.RatingView_rating_view_inactive_tint)) {
                mInactiveColor = ta.getColor(R.styleable.RatingView_rating_view_inactive_tint, Color.LTGRAY);
            }
            ta.recycle();
        }
    }

    private void compound() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_rating_view, this, true);
    }
}
