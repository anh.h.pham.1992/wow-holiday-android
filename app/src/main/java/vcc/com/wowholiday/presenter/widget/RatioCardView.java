package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/24/2019.
 */
public class RatioCardView extends CardView {

    private float mWidthRatio;
    private float mHeightRatio;

    public RatioCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int originalWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        int originalHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        if (originalHeight == 0) {
            int newH = (int) ((float)originalWidth * mHeightRatio/ mWidthRatio);
            super.onMeasure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(newH, View.MeasureSpec.EXACTLY));
        } else if (originalWidth == 0){
            int newW = (int) ((mWidthRatio / mHeightRatio) * originalHeight);
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(newW, View.MeasureSpec.EXACTLY), heightMeasureSpec);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs,
                R.styleable.RatioCardView);
        String ratioDes = typedArray.getString(R.styleable.RatioCardView_card_ratio);
        if (ratioDes != null) {
            String[] splits = ratioDes.split(":");
            mWidthRatio = Float.valueOf(splits[0]);
            mHeightRatio = Float.valueOf(splits[1]);
        }

        typedArray.recycle();
    }
}
