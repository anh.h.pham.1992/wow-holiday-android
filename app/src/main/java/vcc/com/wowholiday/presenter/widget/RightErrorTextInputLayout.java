package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputLayout;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class RightErrorTextInputLayout extends TextInputLayout {

    public RightErrorTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setErrorEnabled(boolean enabled) {
        super.setErrorEnabled(enabled);
        if (!enabled) {
            return;
        }

        TextView errorView = this.findViewById(com.google.android.material.R.id.textinput_error);
        errorView.setTextSize(12);
        FrameLayout errorViewParent = (FrameLayout) errorView.getParent();
        errorViewParent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        errorView.setGravity(Gravity.RIGHT);
    }
}
