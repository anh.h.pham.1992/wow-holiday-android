package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 11/18/2019.
 */
public class RightLeftTextView extends LinearLayout {

    private TextView mTvLeft;
    private TextView mTvRight;

    private String mLeft;
    private String mRight;
    private int mLeftColor;
    private int mRightColor;
    private float mRightSize;
    private float mLeftSize;

    public RightLeftTextView(Context context) {
        super(context);
        compound();
    }

    public RightLeftTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
        compound();
    }

    public void setRightText(String text) {
        mRight = text;
        mTvRight.setText(text);
    }

    public void setLeftTextSize(float size) {
        mLeftSize = size;
        mTvLeft.setTextSize(TypedValue.COMPLEX_UNIT_PX, mLeftSize);
    }

    public void setRightTextSize(float size) {
        mRightSize = size;
        mTvRight.setTextSize(TypedValue.COMPLEX_UNIT_PX, mRightSize);
    }

    public void setLeftText(String text) {
        mLeft = text;
        mTvLeft.setText(text);
    }

    public void setLeftColor(int leftColor) {
        mLeftColor = leftColor;
        mTvLeft.setTextColor(mLeftColor);
    }

    public void setRightColor(int rightColor) {
        this.mRightColor = rightColor;
        mTvRight.setTextColor(mRightColor);
    }

    private void compound() {
        setOrientation(HORIZONTAL);
        LayoutInflater.from(getContext()).inflate(R.layout.layout_right_left_textview, this, true);
        mTvLeft = findViewById(R.id.right_left_textview_layout_tv_left);
        mTvRight = findViewById(R.id.right_left_textview_layout_tv_right);
        mTvLeft.setTextColor(mLeftColor);
        mTvRight.setTextColor(mRightColor);
        mTvLeft.setText(mLeft);
        mTvRight.setText(mRight);
        if (mLeftSize > 0) {
            mTvLeft.setTextSize(TypedValue.COMPLEX_UNIT_PX, mLeftSize);
        }

        if (mRightSize > 0) {
            mTvRight.setTextSize(TypedValue.COMPLEX_UNIT_PX, mRightSize);
        }
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.RightLeftTextView, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.RightLeftTextView_right_left_tv_left_label)) {
                mLeft = ta.getString(R.styleable.RightLeftTextView_right_left_tv_left_label);
            }

            if (ta.hasValue(R.styleable.RightLeftTextView_right_left_tv_right_label)) {
                mRight = ta.getString(R.styleable.RightLeftTextView_right_left_tv_right_label);
            }

            if (ta.hasValue(R.styleable.RightLeftTextView_right_left_tv_left_color)) {
                mLeftColor = ta.getColor(R.styleable.RightLeftTextView_right_left_tv_left_color,
                        ContextCompat.getColor(getContext(), R.color.blue_dark));
            }

            if (ta.hasValue(R.styleable.RightLeftTextView_right_left_tv_right_color)) {
                mRightColor = ta.getColor(R.styleable.RightLeftTextView_right_left_tv_right_color,
                        ContextCompat.getColor(getContext(), R.color.blue_dark));
            }

            if (ta.hasValue(R.styleable.RightLeftTextView_right_left_tv_left_size)) {
                mLeftSize = ta.getDimension(R.styleable.RightLeftTextView_right_left_tv_left_size, -1);
            }

            if (ta.hasValue(R.styleable.RightLeftTextView_right_left_tv_right_size)) {
                mRightSize = ta.getDimension(R.styleable.RightLeftTextView_right_left_tv_right_size, -1);
            }

            ta.recycle();
        }
    }
}
