package vcc.com.wowholiday.presenter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 2/13/2019.
 */

public class TextViewWithIcon extends RelativeLayout {

    private ImageView mIvLeftIcon;
    private ImageView mIvRightIcon;
    private TextView mTvLabel;

    private String mTitle;
    private int mTitleColor;
    private Drawable mImgLeftDrawable;
    private Drawable mImgRightDrawable;
    private int mLeftIconTint;
    private int mRightIconTint;
    private String mHint;
    private float mTextSize;
    private int mIconSize;
    private int mLabelStyle;

    public TextViewWithIcon(Context context) {
        super(context);
    }

    public TextViewWithIcon(Context context, AttributeSet attrs) {
        super(context, attrs);
        compoundView();
        init(attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mIvLeftIcon = findViewById(R.id.tv_with_icon_layout_iv_left_icon);
        mIvRightIcon = findViewById(R.id.tv_with_icon_layout_iv_right_icon);
        mTvLabel = findViewById(R.id.tv_with_icon_layout_tv_label);

        if (mImgLeftDrawable == null) {
            mIvLeftIcon.setVisibility(GONE);
        } else {
            mIvLeftIcon.setImageDrawable(mImgLeftDrawable);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mIconSize, mIconSize);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            mIvLeftIcon.setLayoutParams(params);
            if (mLeftIconTint != 0) {
                mIvLeftIcon.setColorFilter(mLeftIconTint);
            }
        }

        if (mImgRightDrawable == null) {
            mIvRightIcon.setVisibility(GONE);
        } else {
            mIvRightIcon.setImageDrawable(mImgRightDrawable);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mIconSize, mIconSize);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
            mIvRightIcon.setLayoutParams(params);
            if (mRightIconTint != 0) {
                mIvRightIcon.setColorFilter(mRightIconTint);
            }
        }

        mTvLabel.setPadding(mImgLeftDrawable == null ? 0 : mIconSize,
                0, mImgRightDrawable == null ? 0 : mIconSize, 0);

        if (mImgLeftDrawable == null) {
            RelativeLayout.LayoutParams params = (LayoutParams) mTvLabel.getLayoutParams();
            params.leftMargin = 0;
            if (mImgRightDrawable == null) {
                params.rightMargin = 0;
            }
            mTvLabel.setLayoutParams(params);
        }

        if (mHint != null) {
            mTvLabel.setHint(mHint);
        } else {
            mTvLabel.setText(mTitle);
        }

        if (mTitleColor != 0) {
            mTvLabel.setTextColor(mTitleColor);
        }

        if (mTextSize > 0) {
            mTvLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        }

        if (mLabelStyle > 0) {
            if (mLabelStyle == 1) {
                mTvLabel.setTypeface(mTvLabel.getTypeface(), Typeface.BOLD);
            } else if (mLabelStyle == 2) {
                mTvLabel.setTypeface(mTvLabel.getTypeface(), Typeface.NORMAL);
            } else if (mLabelStyle == 3) {
                mTvLabel.setTypeface(mTvLabel.getTypeface(), Typeface.ITALIC);
            } else if (mLabelStyle == 4) {
                mTvLabel.setTypeface(mTvLabel.getTypeface(), Typeface.BOLD_ITALIC);
            }
        }
    }

    public void setText(String text) {
        mTvLabel.setText(text);
    }

    public String getText() {
        return mTvLabel.getText().toString();
    }

    public void setTypeface(Typeface typeface) {
        mTvLabel.setTypeface(typeface);
    }

    public void setTextColor(int color) {
        mTvLabel.setTextColor(color);
    }

    public void setLeftIconTint(int color) {
        mIvLeftIcon.setColorFilter(color);
    }

    private void compoundView() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_textview_with_icon, this, true);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.TextViewWithIcon, 0, 0);
        if (ta != null) {
            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_text_label)) {
                mTitle = ta.getString(R.styleable.TextViewWithIcon_tv_with_icon_text_label);
            }

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_left_res)) {
                mImgLeftDrawable = ta.getDrawable(R.styleable.TextViewWithIcon_tv_with_icon_left_res);
            }

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_right_res)) {
                mImgRightDrawable = ta.getDrawable(R.styleable.TextViewWithIcon_tv_with_icon_right_res);
            }

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_text_label_color)) {
                mTitleColor = ta.getColor(R.styleable.TextViewWithIcon_tv_with_icon_text_label_color, 0);
            }

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_left_ic_tint)) {
                mLeftIconTint = ta.getColor(R.styleable.TextViewWithIcon_tv_with_icon_left_ic_tint, 0);
            }

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_right_ic_tint)) {
                mRightIconTint = ta.getColor(R.styleable.TextViewWithIcon_tv_with_icon_right_ic_tint, 0);
            }

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_text_label_hint)) {
                mHint = ta.getString(R.styleable.TextViewWithIcon_tv_with_icon_text_label_hint);
            }

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_label_text_size)) {
                mTextSize = ta.getDimension(R.styleable.TextViewWithIcon_tv_with_icon_label_text_size, 0);
            }

            mIconSize = ta.getDimensionPixelSize(R.styleable.TextViewWithIcon_tv_with_icon_ic_size,
                    getResources().getDimensionPixelSize(R.dimen.textview_icon_size));

            if (ta.hasValue(R.styleable.TextViewWithIcon_tv_with_icon_label_style)) {
                mLabelStyle = ta.getInt(R.styleable.TextViewWithIcon_tv_with_icon_label_style, 0);
            }
            ta.recycle();
        }
    }
}
