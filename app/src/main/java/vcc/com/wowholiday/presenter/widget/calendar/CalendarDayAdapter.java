package vcc.com.wowholiday.presenter.widget.calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.util.DateUtil;

/**
 * Created by Pham Hai Quang on 10/12/2019.
 */
public class CalendarDayAdapter extends ArrayAdapter<DateInfo> {

    private LayoutInflater mLayoutInflater;
   // private Calendar mCalendar = new GregorianCalendar();
    private CalendarProperty mCalendarProperty;

    public CalendarDayAdapter(@NonNull Context context, CalendarProperty calendarProperty, List<DateInfo> dateList) {
        super(context, R.layout.item_calendar_view_day, dateList);
        mCalendarProperty = calendarProperty;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_calendar_view_day, parent, false);
        }

        TextView dayLabel = view.findViewById(R.id.calendar_itm_tv_day);
        setTextColor(dayLabel, view.findViewById(R.id.calendar_itm_v_today), getItem(position));
        return view;
    }

    private void setTextColor(TextView tvDayLabel, View todayIndicator, DateInfo info) {
        if (!info.isShow) {
            tvDayLabel.setText("");
        } else {
            final Calendar calendar = new GregorianCalendar();
            calendar.setTime(info.date);
            tvDayLabel.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            if (DateUtil.isDayAfter(calendar, mCalendarProperty.getMaximumDate())
                    || DateUtil.isDayBefore(calendar, mCalendarProperty.getMinimumDate())) {
                tvDayLabel.setEnabled(false);
                tvDayLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.light_grey));
            } else {
                tvDayLabel.setEnabled(true);
                tvDayLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.dark));
                tvDayLabel.setSelected(DateUtil.isSameDay(calendar, mCalendarProperty.getSelectedDate()));
            }

            if (DateUtil.isSameDay(calendar, DateUtil.getCalendar())) {
                todayIndicator.setVisibility(View.VISIBLE);
            } else {
                todayIndicator.setVisibility(View.INVISIBLE);
            }
        }

    }
}
