package vcc.com.wowholiday.presenter.widget.calendar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.util.DateUtil;

/**
 * Created by Pham Hai Quang on 10/12/2019.
 */
public class CalendarPageAdapter extends PagerAdapter {

    private CalendarProperty mCalendarProperty;

    public CalendarPageAdapter(CalendarProperty calendarProperty) {
        mCalendarProperty = calendarProperty;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    /**
     * 2401 months means 1200 months (100 years) before and 1200 months after the current month
     * @return
     */
    @Override
    public int getCount() {
        return 2401;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NotNull
    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        CalendarGridView calendarGridView =
                (CalendarGridView) inflater.inflate(R.layout.layout_calendar_view_grid, null);
        loadMonth(position, calendarGridView);
        calendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DateInfo itemAtPosition = (DateInfo) parent.getItemAtPosition(position);
                if (itemAtPosition != null && itemAtPosition.isShow) {
                    Calendar day = new GregorianCalendar();
                    day.setTime(itemAtPosition.date);
                    selectOneDay(container, day);
                }
            }
        });

        container.addView(calendarGridView);
        return calendarGridView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private void loadMonth(int position, CalendarGridView calendarGridView) {
        List<DateInfo> days = new ArrayList<>();
        Calendar calendar = (Calendar) mCalendarProperty.getFirstPageCalendarDate().clone();
        calendar.add(Calendar.MONTH, position);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        int currMonth = calendar.get(Calendar.MONTH);

        /* some days of previous month */
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int firstDayOfWeek = calendar.getFirstDayOfWeek() + 1; // Set first day of week = monday, 1 = sunday, 2 = monday
        int monthBeginningCell = (dayOfWeek < firstDayOfWeek ? 7 : 0) + dayOfWeek - firstDayOfWeek;
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        /*
        Get all days of one page (42 is a number of all possible cells in one page
        (a part of previous month, current month and a part of next month))
         */
        while (days.size() < 42) {
            DateInfo info = new DateInfo();
            info.date = calendar.getTime();
            if (calendar.get(Calendar.MONTH) != currMonth) {
                info.isShow = false;
            }
            days.add(info);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        int month = calendar.get(Calendar.MONTH) - 1;
        CalendarDayAdapter calendarDayAdapter = new CalendarDayAdapter(calendarGridView.getContext(),
                mCalendarProperty, days);
        calendarGridView.setAdapter(calendarDayAdapter);
    }

    private void selectOneDay(View container, Calendar day) {
        if (!DateUtil.isSameDay(mCalendarProperty.getSelectedDate(), day)) {
            mCalendarProperty.fireSelectedListener(day);
            mCalendarProperty.setSelectedDate(day);
            notifyDataSetChanged();
        }
    }
}
