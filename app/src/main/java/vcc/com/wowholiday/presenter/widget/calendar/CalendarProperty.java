package vcc.com.wowholiday.presenter.widget.calendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vcc.com.wowholiday.util.DateUtil;

/**
 * Created by Pham Hai Quang on 10/12/2019.
 */
public class CalendarProperty {
    public static final int CALENDAR_SIZE = 2401;
    public static final int FIRST_VISIBLE_PAGE = CALENDAR_SIZE / 2;

    private Calendar mFirstPageCalendarDate = DateUtil.getCalendar();
    private Calendar mSelectedDate, mMinimumDate, mMaximumDate;

    private List<IOnDaySelectedListener> mSelectedListenerSet = new ArrayList<>();

    public Calendar getFirstPageCalendarDate() {
        return mFirstPageCalendarDate;
    }

    public Calendar getMinimumDate() {
        return mMinimumDate;
    }

    public void setMinimumDate(Calendar minimumDate) {
        mMinimumDate = minimumDate;
    }

    public Calendar getMaximumDate() {
        return mMaximumDate;
    }

    public void setMaximumDate(Calendar maximumDate) {
        mMaximumDate = maximumDate;
    }

    public void setSelectedDate(Calendar selectedDate) {
        mSelectedDate = selectedDate;
    }

    public Calendar getSelectedDate() {
        return mSelectedDate;
    }

    public void addSelectedListener(IOnDaySelectedListener listener) {
        mSelectedListenerSet.add(listener);
    }

    public void fireSelectedListener(Calendar selected) {
        for (IOnDaySelectedListener listener : mSelectedListenerSet) {
            listener.onSelected(mSelectedDate, selected);
        }
    }
}
