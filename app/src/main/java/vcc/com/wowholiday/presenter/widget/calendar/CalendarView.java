package vcc.com.wowholiday.presenter.widget.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import java.util.Calendar;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.presenter.widget.WrapContentViewPager;
import vcc.com.wowholiday.util.DateUtil;

/**
 * Created by Pham Hai Quang on 10/12/2019.
 */
public class CalendarView extends LinearLayout {

    private WrapContentViewPager mViewPager;
    private CalendarPageAdapter mCalendarPageAdapter;

    private CalendarProperty mCalendarProperty = new CalendarProperty();
    private TextView mCurrentMonthLabel;

    public CalendarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        compound();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mCurrentMonthLabel = findViewById(R.id.calendar_layout_tv_month);
        mViewPager = findViewById(R.id.calendar_layout_calvp);
        mCalendarPageAdapter = new CalendarPageAdapter(mCalendarProperty);
        mViewPager.setAdapter(mCalendarPageAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Calendar calendar = (Calendar) mCalendarProperty.getFirstPageCalendarDate().clone();
                calendar.add(Calendar.MONTH, position);

                if (!isScrollingLimited(calendar, position)) {
                    setHeaderName(calendar);
                }
            }
        });

        findViewById(R.id.calendar_layout_iv_prev).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
            }
        });

        findViewById(R.id.calendar_layout_iv_next).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
            }
        });

        initPage(DateUtil.getCalendar());
    }

    public void setDate(Calendar date) {
        if (DateUtil.isDayBefore(date, mCalendarProperty.getMinimumDate())
                || DateUtil.isDayAfter(date, mCalendarProperty.getMaximumDate())) {
            throw new IllegalArgumentException("Out of valid date");
        }
        mCalendarProperty.setSelectedDate(date);
        initPage(date);
        setHeaderName(date);
        mCalendarPageAdapter.notifyDataSetChanged();
    }

    public void setMinDate(Calendar date) {
        mCalendarProperty.setMinimumDate(date);
    }

    public void setMaxDate(Calendar date) {
        mCalendarProperty.setMaximumDate(date);
    }

    public void addSelectedListener(IOnDaySelectedListener listener) {
        mCalendarProperty.addSelectedListener(listener);
    }

    private void compound() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_calendar_view, this, true);
    }

    private void initPage(Calendar cal) {
        mCalendarProperty.getFirstPageCalendarDate().setTime(cal.getTime());
        mCalendarProperty.getFirstPageCalendarDate().add(Calendar.MONTH, -CalendarProperty.FIRST_VISIBLE_PAGE);
        mViewPager.setCurrentItem(CalendarProperty.FIRST_VISIBLE_PAGE);
    }

    private boolean isScrollingLimited(Calendar calendar, int position) {
        if (DateUtil.isMonthBefore(mCalendarProperty.getMinimumDate(), calendar)) {
            mViewPager.setCurrentItem(position + 1);
            return true;
        }

        if (DateUtil.isMonthAfter(mCalendarProperty.getMaximumDate(), calendar)) {
            mViewPager.setCurrentItem(position - 1);
            return true;
        }

        return false;
    }

    private void setHeaderName(Calendar calendar) {
        mCurrentMonthLabel.setText(DateUtil.getMonthAndYearDate(getContext(), calendar));
        //callOnPageChangeListeners(position);
    }
}
