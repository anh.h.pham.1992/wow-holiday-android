package vcc.com.wowholiday.presenter.widget.calendar;

import java.util.Calendar;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public interface IOnDaySelectedListener {
    void onSelected(Calendar oldSelected, Calendar selectedDay);
}
