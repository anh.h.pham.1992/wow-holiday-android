package vcc.com.wowholiday.presenter.widget.checklist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */
public class CheckItemListView extends LinearLayout {

    private LayoutInflater mInflater = LayoutInflater.from(getContext());
    private IChecklistItem[] allItems;
    private List<IChecklistItem> mSelectedList = new ArrayList<>();
    private ISelectedListener mListener;

    public CheckItemListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void show(IChecklistItem...items) {
        allItems = items;
        for (IChecklistItem item : items) {
            addItemView(item);
        }
    }

    public void setSelectedListener(ISelectedListener listener) {
        mListener = listener;
    }

    public void notifyItemChanged(IChecklistItem item) {
        if (!hasItem(item)) return;
        View itemView = findViewWithTag(item.getId());
        if (itemView != null) {
            CheckBox cb = itemView.findViewById(R.id.checklist_item_layout_cb);
            cb.setChecked(item.isSelected());
            TextView tvTitle = itemView.findViewById(R.id.checklist_item_layout_tv_title);
            tvTitle.setText(item.getTitle());
        }
    }

    public boolean hasItem(IChecklistItem item) {
        if (allItems == null) return false;
        for (IChecklistItem itm : allItems) {
            if (item.getId().equals(itm.getId())) {
                return true;
            }
        }
        return false;
    }

    private void addItemView(final IChecklistItem item) {
        View itemView = mInflater.inflate(R.layout.layout_checklist_item, this, false);
        final CheckBox cb = itemView.findViewById(R.id.checklist_item_layout_cb);
        cb.setChecked(item.isSelected());
        cb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setSelected(!item.isSelected());
                cb.setChecked(item.isSelected());

                if (item.isSelected()) {
                    mSelectedList.add(item);
                    if (mListener != null) {
                        mListener.onSelected(item);
                    }
                } else {
                    mSelectedList.remove(item);
                    if (mListener != null) {
                        mListener.onUnselected(item);
                    }
                }
            }
        });

        TextView tvTitle = itemView.findViewById(R.id.checklist_item_layout_tv_title);
        tvTitle.setText(item.getTitle());
        itemView.setTag(item.getId());
        addView(itemView);
    }


    public interface ISelectedListener {
        void onSelected(IChecklistItem item);
        void onUnselected(IChecklistItem item);
    }
}
