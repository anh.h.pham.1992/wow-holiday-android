package vcc.com.wowholiday.presenter.widget.checklist;

/**
 * Created by Pham Hai Quang on 10/30/2019.
 */
public interface IChecklistItem {
    String getTitle();
    String getId();
    void setSelected(boolean isSelected);
    boolean isSelected();
}
