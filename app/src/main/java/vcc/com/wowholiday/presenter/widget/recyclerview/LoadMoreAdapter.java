package vcc.com.wowholiday.presenter.widget.recyclerview;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

import common.presenter.adapter.BaseRclvAdapter;
import common.presenter.adapter.BaseRclvHolder;
import common.presenter.adapter.BaseVHData;
import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */
public class LoadMoreAdapter extends BaseRclvAdapter {

    public static final int LOADING_TYPE = -1;

    private boolean isShowLoadMore = false;

    public LoadMoreAdapter() {}

    public LoadMoreAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutResource(int viewType) {
        if (viewType == LOADING_TYPE) {
            return R.layout.item_loading;
        }
        return 0;
    }

    @Override
    public BaseRclvHolder onCreateVH(View itemView, int viewType) {
        if (viewType == LOADING_TYPE) {
            return new LoadingVH(itemView);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public void reset(List newItems) {
        mDataSet.clear();
        addItems(newItems);
        if (isShowLoadMore) {
            addItem(new LoadingVHData(null));
        }
        notifyDataSetChanged();
    }

    public void showLoadMore(boolean isShown) {
        if (isShown) {
            if (!isShowLoadMore) {
                isShowLoadMore = true;
                addItemAndNotify(new LoadingVHData(null));
            }
        } else {
            if (isShowLoadMore) {
                isShowLoadMore = false;
                removeItemAndNotify(mDataSet.size() - 1);
            }
        }
    }

    public boolean isLoadMoreSupport() {
        return isShowLoadMore;
    }

    public void enableLoadMore(boolean isLoadMore) {
        isShowLoadMore = isLoadMore;
    }

    public boolean isFooterPosition(int position) {
        return isShowLoadMore && position == mDataSet.size() - 1;
    }


    /**********************************************************************************************/
    public static class LoadingVHData extends BaseVHData<Void> {
        public LoadingVHData(Void data) {
            super(data);
            type = LOADING_TYPE;
        }
    }


    class LoadingVH extends BaseRclvHolder<LoadingVHData> {
        public LoadingVH(@NonNull View itemView) {
            super(itemView);
        }
    }
}
