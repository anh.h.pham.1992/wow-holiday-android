package vcc.com.wowholiday.presenter.widget.recyclerview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import vcc.com.wowholiday.R;

/**
 * Created by Pham Hai Quang on 10/17/2019.
 */

public class WrapRecyclerView extends RelativeLayout {

    private RecyclerView mRclvContent;
    private TextView mTvNoItem;
    private ImageView mIvNoItem;
    private ProgressBar mPrgbLoading;
    private LinearLayout mLlNoItem;

    private boolean isLoading = false;
    private boolean hasNoItemImage = false;
    private LoadMoreAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private IOnLoadMoreDataListener mLoadDataListener;

    public WrapRecyclerView(Context context) {
        super(context);
    }

    public WrapRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        compound();
    }

    public WrapRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        compound();
    }

    public WrapRecyclerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        compound();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mRclvContent = findViewById(R.id.wrap_rclv_layout_rclv_content);
        mTvNoItem = findViewById(R.id.wrap_rclv_layout_tv_no_item);
        mIvNoItem = findViewById(R.id.wrap_rclv_layout_iv_no_item);
        mPrgbLoading = findViewById(R.id.wrap_rclv_layout_prgb_loading);
        mLlNoItem = findViewById(R.id.wrap_rclv_layout_ll_no_item_container);

        if (mLayoutManager == null) {
            mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        }
        mRclvContent.setLayoutManager(mLayoutManager);
        mRclvContent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!mAdapter.isLoadMoreSupport()) {
                    return;
                }

                if (mLoadDataListener == null) {
                    return;
                }

                if (dy > 0 && !isLoading) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                        isLoading = true;
                        mLoadDataListener.onLoadData();
                    }
                }
            }
        });

        mRclvContent.setVisibility(INVISIBLE);
        mTvNoItem.setVisibility(INVISIBLE);
        showLoading(true);
    }

    public void setLoadDataListener(IOnLoadMoreDataListener listener) {
        mLoadDataListener = listener;
    }

    public void setLayoutManager(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;
    }

    public void setAdapter(LoadMoreAdapter adapter) {
        mAdapter = adapter;
        mRclvContent.setAdapter(adapter);
    }

    public void addItemDecoration(RecyclerView.ItemDecoration decor) {
        mRclvContent.addItemDecoration(decor);
    }

    public void addOnScrollListener(RecyclerView.OnScrollListener listener) {
        mRclvContent.addOnScrollListener(listener);
    }

    public void enableLoadMore(boolean enable) {
        mAdapter.showLoadMore(enable);
    }

    public void addItemsAndNotify(List itemList, boolean isStillLoadMore) {
        isLoading = false;
        int startIndex = mAdapter.isLoadMoreSupport() ? mAdapter.getItemCount() - 1 : mAdapter.getItemCount();
        mAdapter.addItemsAtIndex(itemList, startIndex);
        if (mAdapter.getItemCount() == 0) {
            showNoItem(true);
        } else {
            showNoItem(false);
            mAdapter.showLoadMore(isStillLoadMore);
        }
        showLoading(false);

        if (!itemList.isEmpty()) {
            mAdapter.notifyItemRangeInserted(startIndex, itemList.size());
        }
    }

    public void reset(List itemList) {
        mAdapter.reset(itemList);
        showLoading(false);
        showNoItem(itemList.isEmpty());
    }

    public void addItem(@NonNull Object item) {
        mAdapter.addItem(item);
    }

    public void addItems(@NonNull List items) {
        mAdapter.addItems(items);
    }

    public void notifyDataChanged() {
        mAdapter.notifyDataSetChanged();
        if (mAdapter.getItemCount() > 0) {
            showLoading(false);
            showNoItem(false);
        }
    }

    public void clearData() {
        mRclvContent.scrollToPosition(0);
        mAdapter.clearData();
        mAdapter.enableLoadMore(false);
    }

    public void showLoading(boolean isShown) {
        mPrgbLoading.setVisibility(isShown ? VISIBLE : INVISIBLE);
    }

    public void showNoItem(boolean isShown) {
        mTvNoItem.setVisibility(isShown ? VISIBLE : INVISIBLE);
        if (hasNoItemImage) {
            mIvNoItem.setVisibility(isShown ? VISIBLE : GONE);
        } else {
            mIvNoItem.setVisibility(GONE);
        }
        mRclvContent.setVisibility(isShown ? INVISIBLE : VISIBLE);
    }

    public void setNoItemText(int resId) {
        mTvNoItem.setText(resId);
    }

    public void setFontNoItemText(Typeface typeface) {
        mTvNoItem.setTypeface(typeface);
    }

    public void setNoItemImage(int resId) {
        hasNoItemImage = true;
        mIvNoItem.setImageResource(resId);
    }

    public void setNoItemImageTint(int color) {
        mIvNoItem.setColorFilter(color);
    }

    public void setNoItemImageBackground(int resId) {
        mIvNoItem.setBackgroundResource(resId);
    }

    public void setNoItemImagePadding(int padding) {
        mIvNoItem.setPadding(padding, padding, padding, padding);
    }

    public void setPaddingTopLlNoItem(int paddingTop) {
        mLlNoItem.setPadding(0, paddingTop, 0, 0);
    }

    private void compound() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_wrap_recyclerview, this, true);
    }


    public interface IOnLoadMoreDataListener {
        void onLoadData();
    }
}
