package vcc.com.wowholiday.repository;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.domain.ICallback;
import quangph.com.mvp.mvp.repo.IConverter;
import retrofit2.Response;
import vcc.com.wowholiday.domain.repo.IActivitiesRepo;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.dto.ActivitiesLocationDto;
import vcc.com.wowholiday.repository.api.APIException;
import vcc.com.wowholiday.repository.api.WHApiCaller;
import vcc.com.wowholiday.repository.api.WHRetrofitCallback;
import vcc.com.wowholiday.repository.api.activitiesdetail.ActivitiesDetailRequest;
import vcc.com.wowholiday.repository.api.activitiesdetail.ActivitiesDetailResponse;
import vcc.com.wowholiday.repository.api.activitieslocation.ActivitiesLocationRequest;
import vcc.com.wowholiday.repository.api.activitieslocation.ActivitiesLocationResponse;
import vcc.com.wowholiday.repository.api.activitiessearch.ActivitiesSearchResponse;
import vcc.com.wowholiday.repository.api.converter.ActivitiesDetailDataToActivitiesDetail;
import vcc.com.wowholiday.repository.api.converter.ActivitiesInfoDataToActivitiesInfo;
import vcc.com.wowholiday.repository.api.converter.LocationDataToLocation;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.activities.ActivitiesDetailData;
import vcc.com.wowholiday.repository.api.model.activities.ActivitiesInfoData;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */
public class ActivitiesRepo implements IActivitiesRepo {
    @Override
    public void searchLocation(String authen, String locationName, final ICallback<ActivitiesLocationDto> callback) {
        ActivitiesLocationRequest request = new ActivitiesLocationRequest();
        request.locationName = locationName;
        WHApiCaller.searchActivitiesLocation(authen, request).enqueue(new WHRetrofitCallback<ActivitiesLocationResponse>() {
            @Override
            public void onSuccess(Response<ActivitiesLocationResponse> response) {
                if (response.body() != null && response.isSuccessful()) {
                    ActivitiesLocationDto result = new ActivitiesLocationDto();
                    IConverter<LocationData, Location> converter = new LocationDataToLocation();
                    for (ActivitiesLocationResponse.ActivitiesLocationData data : response.body().data) {
                        if ("location".equals(data.type)) {
                            addLocation(result.searchData, data.item, converter);
                        } else if ("recentlocations".equals(data.type)) {
                            addLocation(result.recentData, data.item, converter);
                        }
                    }
                    callback.onSuccess(result);
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void searchActivities(final ICallback<List<ActivitiesInfo>> callback) {
        WHApiCaller.activitiesSearch().enqueue(new WHRetrofitCallback<ActivitiesSearchResponse>() {
            @Override
            public void onSuccess(Response<ActivitiesSearchResponse> response) {
                if (response.body() != null && response.body().isResponseAvailable()) {
                    List<ActivitiesInfo> results = new ArrayList<>();
                    IConverter<ActivitiesInfoData, ActivitiesInfo> converter = new ActivitiesInfoDataToActivitiesInfo();
                    String searchToken = response.body().firstPage.response.token;
                    for (ActivitiesSearchResponse.FirstPageResponseData res : response.body().firstPage.response.data) {
                        results.addAll(extractData(res, searchToken,  converter));
                    }
                    callback.onSuccess(results);
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void getActivitiesDetail(String authen, String token, String activitiesID,
                                    final ICallback<ActivitiesDetail> callback) {
        ActivitiesDetailRequest request = new ActivitiesDetailRequest();
        request.activitiesID = activitiesID;
        request.token = token;
        WHApiCaller.getActivitiesDetail(authen, request).enqueue(new WHRetrofitCallback<ActivitiesDetailResponse>() {
            @Override
            public void onSuccess(Response<ActivitiesDetailResponse> response) {
                if (response.body() != null && response.body().isResponseAvailable()) {
                    ActivitiesDetailData data = response.body().data[0].item[0];
                    IConverter<ActivitiesDetailData, ActivitiesDetail> converter
                            = new ActivitiesDetailDataToActivitiesDetail();
                    callback.onSuccess(converter.convert(data));
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    private List<ActivitiesInfo> extractData(ActivitiesSearchResponse.FirstPageResponseData data,
                                             String token,
                                             IConverter<ActivitiesInfoData, ActivitiesInfo> converter) {
        List<ActivitiesInfo> results = new ArrayList<>();
        for (ActivitiesInfoData itm : data.item) {
            ActivitiesInfo info = converter.convert(itm);
            info.setToken(token);
            results.add(info);
        }
        return results;
    }

    private void addLocation(List<Location> result, LocationData[] data, IConverter<LocationData, Location> converter) {
        if (data == null) {
            return;
        }

        for (LocationData locData : data) {
            result.add(converter.convert(locData));
        }
    }
}
