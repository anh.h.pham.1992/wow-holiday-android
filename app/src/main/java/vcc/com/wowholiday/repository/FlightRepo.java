package vcc.com.wowholiday.repository;

import java.util.List;

import quangph.com.mvp.mvp.domain.ICallback;
import quangph.com.mvp.mvp.repo.IConverter;
import retrofit2.Response;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.domain.repo.IFlightRepo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Sort;
import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.model.air.FlightDataDTO;
import vcc.com.wowholiday.model.air.FlightFilter;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.repository.api.APIException;
import vcc.com.wowholiday.repository.api.WHApiCaller;
import vcc.com.wowholiday.repository.api.WHRetrofitCallback;
import vcc.com.wowholiday.repository.api.airlocation.AirLocationRequest;
import vcc.com.wowholiday.repository.api.airlocation.AirLocationResponse;
import vcc.com.wowholiday.repository.api.converter.ArrayConverter;
import vcc.com.wowholiday.repository.api.converter.FlightFilterDataToFlightFilter;
import vcc.com.wowholiday.repository.api.converter.LocationDataToLocation;
import vcc.com.wowholiday.repository.api.converter.TripDataSummaryToFlightTrip;
import vcc.com.wowholiday.repository.api.flightsearch.FlightSearchRequest;
import vcc.com.wowholiday.repository.api.flightsearch.FlightSearchResponse;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.SortData;
import vcc.com.wowholiday.repository.api.model.flight.FlightFilterData;
import vcc.com.wowholiday.repository.api.model.flight.TripDataSummary;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class FlightRepo implements IFlightRepo {
    @Override
    public void getLocationSuggestion(String authen, String city, final ICallback<List<Location>> callback) {
        AirLocationRequest request = new AirLocationRequest();
        request.city = city;
        WHApiCaller.getAirLocSearch(authen, request).enqueue(new WHRetrofitCallback<AirLocationResponse>() {
            @Override
            public void onSuccess(Response<AirLocationResponse> response) {
                if (response.body() != null) {
                    if (response.body().isResponseAvailable()) {
                        callback.onSuccess(new ArrayConverter<LocationData, Location>(
                                new LocationDataToLocation()).convert(response.body().data[0].item));
                    }
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void searchAir(String authen, FlightBookingProperty property, final ICallback<FlightDataDTO> callback) {
        FlightSearchRequest request = new FlightSearchRequest();
        request.criterialInfo = property;

        boolean isCombo = property.isRoundTrip()
                && (!property.getStartLoc().getCountryID().equals("VN") || !property.getEndLoc().getCountryID().equals("VN"));

        WHApiCaller.flightSearch(authen, request, isCombo).enqueue(new WHRetrofitCallback<FlightSearchResponse>() {
            @Override
            public void onSuccess(Response<FlightSearchResponse> response) {
                if (response.body() != null && response.body().isResponseAvailable()) {
                    FlightDataDTO dataDTO = new FlightDataDTO();

                    if (response.body().firstPage.response.data != null) {
                        IConverter<TripDataSummary, FlightTrip> converter = new TripDataSummaryToFlightTrip();
                        ArrayConverter<TripDataSummary, FlightTrip> arrayConverter = new ArrayConverter<>(converter);
                        dataDTO.flightTripList = arrayConverter.convert(response.body().firstPage.response.data[0].item);
                    }

                    if (response.body().firstPage.response.availableFilters != null) {
                        IConverter<FlightFilterData, FlightFilter> converter = new FlightFilterDataToFlightFilter();
                        for (FlightFilterData f : response.body().firstPage.response.availableFilters) {
                            FlightFilter filter = converter.convert(f);
                            if (filter != null) {
                                dataDTO.flightFilterList.add(filter);
                            }
                        }
                    }

                    if (response.body().firstPage.response.availableSorting != null) {
                        for (SortData data : response.body().firstPage.response.availableSorting) {
                            Sort s = new Sort();
                            if ("departure".equals(data.name)) {
                                s.setName(AppConfig.AirSortName.START_TIME);
                                s.setOrder(data.order);
                            } else if ("arrival".equals(data.name)) {
                                s.setName(AppConfig.AirSortName.END_TIME);
                                s.setOrder(data.order);
                            } else if ("duration".equals(data.name)) {
                                s.setName(AppConfig.AirSortName.DURATION);
                                s.setOrder(data.order);
                            } else if ("rate".equals(data.name)) {
                                s.setName(AppConfig.AirSortName.PRICE);
                                s.setOrder(data.order);
                            }
                            if (s.getName() != null) {
                                dataDTO.flightSortList.add(s);
                            }
                        }
                    }
                    callback.onSuccess(dataDTO);
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }
}
