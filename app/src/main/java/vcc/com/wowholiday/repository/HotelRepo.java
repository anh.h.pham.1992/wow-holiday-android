package vcc.com.wowholiday.repository;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.domain.ICallback;
import quangph.com.mvp.mvp.repo.IConverter;
import retrofit2.Response;
import vcc.com.wowholiday.domain.repo.IHotelRepo;
import vcc.com.wowholiday.model.Sort;
import vcc.com.wowholiday.model.air.FlightFilter;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.model.hotel.HotelFilter;
import vcc.com.wowholiday.model.hotel.HotelInfo;
import vcc.com.wowholiday.model.hotel.HotelSearchDTO;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.model.hotel.HotelSuggestionList;
import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.repository.api.APIException;
import vcc.com.wowholiday.repository.api.WHApiCaller;
import vcc.com.wowholiday.repository.api.WHRetrofitCallback;
import vcc.com.wowholiday.repository.api.converter.ArrayConverter;
import vcc.com.wowholiday.repository.api.converter.FlightFilterDataToFlightFilter;
import vcc.com.wowholiday.repository.api.converter.HotelDetailDataToHotelDetail;
import vcc.com.wowholiday.repository.api.converter.HotelFilterDataToHotelFilter;
import vcc.com.wowholiday.repository.api.converter.HotelSearchDataToHotelInfo;
import vcc.com.wowholiday.repository.api.converter.HotelSuggestionDataToHotelSuggestion;
import vcc.com.wowholiday.repository.api.converter.SortDataToHotelSort;
import vcc.com.wowholiday.repository.api.hoteldetail.HotelDetailRequest;
import vcc.com.wowholiday.repository.api.hoteldetail.HotelDetailResponse;
import vcc.com.wowholiday.repository.api.hotelsearch.HotelSearchRequest;
import vcc.com.wowholiday.repository.api.hotelsearch.HotelSearchResponse;
import vcc.com.wowholiday.repository.api.hotelsuggestion.HotelSuggestionRequest;
import vcc.com.wowholiday.repository.api.hotelsuggestion.HotelSuggestionResponse;
import vcc.com.wowholiday.repository.api.model.SortData;
import vcc.com.wowholiday.repository.api.model.flight.FlightFilterData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelDetailData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelFilterData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSearchData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSuggestionData;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class HotelRepo implements IHotelRepo {
    @Override
    public void getHotelSuggestion(String authen, String keyword, final ICallback<List<HotelSuggestionList>> callback) {
        HotelSuggestionRequest request = new HotelSuggestionRequest();
        request.keyword = keyword;
        WHApiCaller.getHotelSuggestionSearch(authen, request).enqueue(new WHRetrofitCallback<HotelSuggestionResponse>() {
            @Override
            public void onSuccess(Response<HotelSuggestionResponse> response) {
                if (response.body() != null) {
                    if (response.body().isResponseAvailable()) {
                        HotelSuggestionResponse.HotelSuggestionResponseData[] data = response.body().data;
                        List<HotelSuggestionList> hotelSuggestionLists = new ArrayList<>();
                        for (HotelSuggestionResponse.HotelSuggestionResponseData d :
                                data) {
                            if (d.item != null && d.item.length > 0) {
                                HotelSuggestionList list = new HotelSuggestionList();
                                list.setType(d.type);
                                list.setList(new ArrayConverter<HotelSuggestionData, HotelSuggestion>(
                                        new HotelSuggestionDataToHotelSuggestion()).convert(d.item));
                                hotelSuggestionLists.add(list);
                            }
                        }
                        callback.onSuccess(hotelSuggestionLists);
                    }
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void searchHotel(String authen, HotelSearchProperty property, final ICallback<HotelSearchDTO> callback) {
        HotelSearchRequest request = new HotelSearchRequest();
        request.hotelSearchProperty = property;

        WHApiCaller.hotelSearch(authen, request).enqueue(new WHRetrofitCallback<HotelSearchResponse>() {
            @Override
            public void onSuccess(Response<HotelSearchResponse> response) {
                HotelSearchDTO dataDTO = new HotelSearchDTO();
                if (response.body() != null && response.body().isResponseAvailable()) {

                    if (response.body().firstPage.response.data != null) {
                        IConverter<HotelSearchData, HotelInfo> converter = new HotelSearchDataToHotelInfo(response.body().firstPage.response.token);
                        ArrayConverter<HotelSearchData, HotelInfo> arrayConverter = new ArrayConverter<>(converter);
                        dataDTO.hotelInfoList = arrayConverter.convert(response.body().firstPage.response.data[0].item);
                    }

                    if (response.body().firstPage.response.availableFilters != null) {
                        IConverter<HotelFilterData, HotelFilter> converter = new HotelFilterDataToHotelFilter();
                        ArrayConverter<HotelFilterData, HotelFilter> arrayConverter = new ArrayConverter<>(converter);
                        dataDTO.hotelFilterList = arrayConverter.convert(response.body().firstPage.response.availableFilters);
                    }
                    if (response.body().firstPage.response.availableSorting != null) {
                        IConverter<SortData, Sort> converter = new SortDataToHotelSort();
                        ArrayConverter<SortData, Sort> arrayConverter = new ArrayConverter<>(converter);
                        dataDTO.hotelSortList = arrayConverter.convert(response.body().firstPage.response.availableSorting);
                    }

                    dataDTO.token = response.body().firstPage.response.token;
                }
                callback.onSuccess(dataDTO);
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void getHotelDetail(String authen, String token, String data, final ICallback<HotelDetail> callback) {
        final HotelDetailRequest request = new HotelDetailRequest();
        request.token = token;
        request.data = data;
        WHApiCaller.getHotelDetails(authen, request).enqueue(new WHRetrofitCallback<HotelDetailResponse>() {

            @Override
            public void onSuccess(Response<HotelDetailResponse> response) {
                if (response.body() != null && response.body().isResponseAvailable()) {
                    if (response.body().data != null
                            && response.body().data.size() > 0
                            && response.body().data.get(0).item != null
                            && response.body().data.get(0).item.length > 0) {
                        IConverter<HotelDetailData, HotelDetail> converter = new HotelDetailDataToHotelDetail();
                        HotelDetail hotelDetail = converter.convert(response.body().data.get(0).item[0]);
                        callback.onSuccess(hotelDetail);
                    }
                }
                callback.onSuccess(null);
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }
}
