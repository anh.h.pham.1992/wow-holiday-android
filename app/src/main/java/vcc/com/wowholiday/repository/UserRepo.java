package vcc.com.wowholiday.repository;

import quangph.com.mvp.mvp.domain.ICallback;
import quangph.com.mvp.mvp.repo.IConverter;
import retrofit2.Response;
import vcc.com.wowholiday.domain.repo.IUserRepo;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.repository.api.APIException;
import vcc.com.wowholiday.repository.api.WHApiCaller;
import vcc.com.wowholiday.repository.api.WHRetrofitCallback;
import vcc.com.wowholiday.repository.api.converter.UserDataToUser;
import vcc.com.wowholiday.repository.api.converter.UserToUserData;
import vcc.com.wowholiday.repository.api.login.LoginRequest;
import vcc.com.wowholiday.repository.api.login.LoginResponse;
import vcc.com.wowholiday.repository.api.model.UserData;
import vcc.com.wowholiday.repository.api.register.RegisterRequest;
import vcc.com.wowholiday.repository.api.register.RegisterResponse;
import vcc.com.wowholiday.repository.api.updateprofile.UpdateProfileRequest;
import vcc.com.wowholiday.repository.api.updateprofile.UpdateProfileResponse;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class UserRepo implements IUserRepo {
    @Override
    public void registerEmail(String authen, String email, String firstName, String lastName, String password, String gender,
                              final ICallback<User> callback) {
        RegisterRequest request = new RegisterRequest();
        request.authen = authen;
        request.email = email;
        request.password = password;
        request.firstName = firstName;
        request.lastName = lastName;
        if (User.MALE.equals(gender)) {
            request.gender = "M";
        } else {
            request.gender = "FM";
        }

        WHApiCaller.register(request).enqueue(new WHRetrofitCallback<RegisterResponse>() {
            @Override
            public void onSuccess(Response<RegisterResponse> response) {
                if (response.body() != null) {
                    if (response.body().isResponseAvailable()) {
                        IConverter<UserData, User> converter = new UserDataToUser();
                        callback.onSuccess(converter.convert(response.body().data));
                    } else {
                        //TODO hard code
                        if (response.body().status != null) {
                            callback.onError(new APIException(response.body().status.code));
                        } else {
                            callback.onError(new APIException("Có lỗi xảy ra!", 500));
                        }
                    }
                }
            }

            @Override
            public void onError(APIException e) {
                e.printStackTrace();
                callback.onError(e);
            }
        });
    }

    @Override
    public void registerPhone(String authen, String phone, String firstName, String lastName, String password, String gender, final ICallback<User> callback) {
        RegisterRequest request = new RegisterRequest();
        request.authen = authen;
        request.phoneNumber = phone;
        request.password = password;
        request.firstName = firstName;
        request.lastName = lastName;
        if (User.MALE.equals(gender)) {
            request.gender = "M";
        } else {
            request.gender = "FM";
        }

        WHApiCaller.register(request).enqueue(new WHRetrofitCallback<RegisterResponse>() {
            @Override
            public void onSuccess(Response<RegisterResponse> response) {
                if (response.body() != null) {
                    if (response.body().isResponseAvailable()) {
                        IConverter<UserData, User> converter = new UserDataToUser();
                        callback.onSuccess(converter.convert(response.body().data));
                    } else {
                        //TODO hard code
                        //callback.onError(new APIException("Có lỗi xảy ra!", 500));
                        if (response.body().status != null) {
                            callback.onError(new APIException(response.body().status.code));
                        } else {
                            callback.onError(new APIException("Có lỗi xảy ra!", 500));
                        }
                    }
                }
            }

            @Override
            public void onError(APIException e) {
                e.printStackTrace();
                callback.onError(e);
            }
        });
    }

    @Override
    public void loginEmail(String authen, String email, String password, final ICallback<User> callback) {
        LoginRequest request = new LoginRequest();
        request.email = email;
        request.password = password;
        request.authen = authen;
        WHApiCaller.loginEmail(request).enqueue(new WHRetrofitCallback<LoginResponse>() {
            @Override
            public void onSuccess(Response<LoginResponse> response) {
                if (response.body() != null) {
                    if (response.body().isResponseAvailable()) {
                        IConverter<UserData, User> converter = new UserDataToUser();
                        callback.onSuccess(converter.convert(response.body().data));
                    } else {
                        //TODO hard code
                        callback.onError(new APIException("Có lỗi xảy ra!", 500));
                    }
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void loginPhone(String authen, String phone, String countryCode, String password, final ICallback<User> callback) {
        LoginRequest request = new LoginRequest();
        request.phoneNumber = phone;
        request.phoneNumberCountryCode = countryCode;
        request.password = password;
        request.authen = authen;
        WHApiCaller.loginEmail(request).enqueue(new WHRetrofitCallback<LoginResponse>() {
            @Override
            public void onSuccess(Response<LoginResponse> response) {
                if (response.body() != null) {
                    if (response.body().isResponseAvailable()) {
                        IConverter<UserData, User> converter = new UserDataToUser();
                        callback.onSuccess(converter.convert(response.body().data));
                    } else {
                        //TODO hard code
                        callback.onError(new APIException("Có lỗi xảy ra!", 500));
                    }
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void update(String authen, User user, final ICallback<Boolean> callback) {
        UpdateProfileRequest request = new UpdateProfileRequest();
        request.authen = authen;
        request.user = new UserToUserData().convert(user);
        WHApiCaller.updateProfile(request).enqueue(new WHRetrofitCallback<UpdateProfileResponse>() {
            @Override
            public void onSuccess(Response<UpdateProfileResponse> response) {
                if (response.body() != null && response.body().isResponseAvailable()) {
                    callback.onSuccess(response.body().data);
                }
            }

            @Override
            public void onError(APIException e) {
                callback.onError(e);
            }
        });
    }

    @Override
    public void logout() {

    }
}
