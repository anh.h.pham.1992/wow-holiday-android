package vcc.com.wowholiday.repository.api;

/**
 * Created by Pham Hai Quang on 10/08/2019.
 */
public class APIException extends Exception {

    public static final int NETWORK_ERROR = 0;
    public static final int AUTHEN_ERROR = 401;

    private int mCode;
    private Object mPayLoad;

    public APIException(int code) {
        super();
        mCode = code;
    }

    public APIException(String message) {
        super(message);
    }

    public APIException(String message, int code) {
        super(message);
        mCode = code;
    }

    public APIException(String message, int code, Object payload) {
        super(message);
        mCode = code;
        mPayLoad = payload;
    }

    public APIException(String message, Throwable t) {
        super(message, t);
    }

    public int getCode() {
        return mCode;
    }

    public Object getPayload() {
        return mPayLoad;
    }
}
