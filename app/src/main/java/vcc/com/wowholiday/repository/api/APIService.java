package vcc.com.wowholiday.repository.api;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.repository.api.activitiesdetail.ActivitiesDetailResponse;
import vcc.com.wowholiday.repository.api.activitieslocation.ActivitiesLocationResponse;
import vcc.com.wowholiday.repository.api.activitiessearch.ActivitiesSearchResponse;
import vcc.com.wowholiday.repository.api.airlocation.AirLocationResponse;
import vcc.com.wowholiday.repository.api.flightsearch.FlightSearchResponse;
import vcc.com.wowholiday.repository.api.hoteldetail.HotelDetailResponse;
import vcc.com.wowholiday.repository.api.hotelsearch.HotelSearchResponse;
import vcc.com.wowholiday.repository.api.hotelsuggestion.HotelSuggestionResponse;
import vcc.com.wowholiday.repository.api.login.LoginResponse;
import vcc.com.wowholiday.repository.api.register.RegisterResponse;
import vcc.com.wowholiday.repository.api.updateprofile.UpdateProfileResponse;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public interface APIService {

    @POST(AppConfig.APIConfig.REGISTER)
    Call<RegisterResponse> register(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.LOGIN)
    Call<LoginResponse> login(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.UPDATE_PROFILE)
    Call<UpdateProfileResponse> updateProfile(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.AIR_LOCATION_SEARCH)
    Call<AirLocationResponse> getAirLocationSuggestion(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.AIR_SEARCH)
    Call<FlightSearchResponse> searchFlight(@Body JsonObject body);

    //For test oneway
    @POST("/api/v1/air/search/air_search_mock_oneway")
    Call<FlightSearchResponse> searchFlightTestOneway(@Body JsonObject body);

    @POST(AppConfig.APIConfig.ACTIVITIES_LOCATION_SEARCH)
    Call<ActivitiesLocationResponse> searchActivitiesLocation(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.ACTIVITIES_SEARCH)
    Call<ActivitiesSearchResponse> searchActivities();

    @POST(AppConfig.APIConfig.ACTIVITIES_DETAIL)
    Call<ActivitiesDetailResponse> getActivitiesDetail(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.HOTEL_LOCATION_SEARCH)
    Call<HotelSuggestionResponse> getHotelSuggestion(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.HOTEL_SEARCH)
    Call<HotelSearchResponse> searchHotel(@Header ("Authorization") String authorization, @Body JsonObject body);

    @POST(AppConfig.APIConfig.HOTEL_DETAILS)
    Call<HotelDetailResponse> getHotelDetails(@Header ("Authorization") String authorization, @Body JsonObject body);
}
