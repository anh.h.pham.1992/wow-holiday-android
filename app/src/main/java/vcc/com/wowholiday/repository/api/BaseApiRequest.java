package vcc.com.wowholiday.repository.api;

import com.google.gson.JsonObject;

import java.lang.reflect.Field;

import vcc.com.wowholiday.config.AppConfig;

/**
 * Created by Pham Hai Quang on 10/8/2019.
 */
public class BaseApiRequest {

    public String apiKey = AppConfig.APIConfig.API_KEY;
    public String device = AppConfig.APIConfig.OS;

    public JsonObject toJsonBody() {
        JsonObject body = new JsonObject();
        Field[] declaredFields = getClass().getDeclaredFields();
        try {
            for (Field f : declaredFields) {
                body.addProperty(f.getName(), f.get(this).toString());
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        onJsonBody(body);
        return body;
    }

    public void onJsonBody(JsonObject body) {
    }
}
