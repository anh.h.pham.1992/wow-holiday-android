package vcc.com.wowholiday.repository.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pham Hai Quang on 10/8/2019.
 */
public class BaseApiResponse<T> {
    @SerializedName("status")
    @Expose
    public Status status;

    @SerializedName("response")
    @Expose
    public T data;

    public boolean isResponseAvailable() {
        return data != null;
    }

    public static class Status {
        @SerializedName("code")
        public int code;

        @SerializedName("message")
        public String message;

    }
}
