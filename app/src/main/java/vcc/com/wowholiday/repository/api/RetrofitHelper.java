package vcc.com.wowholiday.repository.api;


import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vcc.com.wowholiday.BuildConfig;
import vcc.com.wowholiday.WHApplication;
import vcc.com.wowholiday.config.AppConfig;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class RetrofitHelper {

    private static final String TAG = RetrofitHelper.class.getSimpleName();

    public static <T> T createService(Class<T> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.APIConfig.BASE_URL)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(provideOkHttpClient())
                .build();
        return retrofit.create(service);
    }

    private static Gson getGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    private static OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new ContentTypeJson())
                .addInterceptor(new MockAirSearch())
                .addInterceptor(new AuthenticationInterceptor())
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();
    }

    /**
     * For URL http://preprod-coreapi.wowholiday.vn
     */
    private static class AuthenticationInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            String authToken = Credentials.basic("bcd15f925ee8adcf90205e03356c0837f3512f01dbbfaa27821b93fe35ef38b2",
                    "041d8cc4f0346fa440f458e6aadfe2a7fd1be4caf81f28f7eebe40ff026684b5");
            Request.Builder builder = original.newBuilder()
                    .header("Authorization", authToken);

            Request request = builder.build();
            return chain.proceed(request);
        }
    }


    private static class NullOnEmptyConverterFactory extends Converter.Factory {
        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type,
                                                                Annotation[] annotations,
                                                                Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate =
                    retrofit.nextResponseBodyConverter(this, type, annotations);
            return new Converter<ResponseBody, Object>() {
                @Override
                public Object convert(ResponseBody value) throws IOException {
                    if (value.contentLength() == 0) {
                        return null;
                    }
                    return delegate.convert(value);
                }
            };
        }
    }

    private static class ContentTypeJson implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        }
    }

    public static class EncryptionInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            RequestBody oldBody = request.body();
            Buffer buffer = new Buffer();
            oldBody.writeTo(buffer);
            String strOldBody = buffer.readUtf8();
            MediaType mediaType = MediaType.parse("text/plain; charset=utf-8");
            String strNewBody = encrypt(strOldBody);
            RequestBody body = RequestBody.create(mediaType, strNewBody);
            request = request.newBuilder().header("apisign", strNewBody).build();
            return chain.proceed(request);
        }

        private String encrypt(String body) {
            String encryptBody = "";
            try {
                String secret = AppConfig.APIConfig.API_SECRET_KEY;
                Mac HMACsha = Mac.getInstance("HmacSHA512");

                SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA512");
                HMACsha.init(secretKey);
                char[] chars = Hex.encodeHex(HMACsha.doFinal(body.getBytes("UTF-8")));
                encryptBody = new String(chars);
                //encryptBody = Base64.encodeToString(HMACsha.doFinal(body.getBytes()), Base64.NO_WRAP);
                Log.e("ENCRYPTO", encryptBody);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return encryptBody;
        }
    }


    static class MockAirSearch implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            String uri = chain.request().url().uri().toString();
            String response = "";
            if (uri.endsWith("air_search_mock")) {
                response = dummyData("arisearch.txt");
                return chain.proceed(chain.request())
                        .newBuilder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message("success")
                        .body(ResponseBody.create(MediaType.parse("application/json"), response))
                        .addHeader("content-type", "application/json")
                        .build();
            } else if (uri.endsWith("air_search_mock_oneway")) {
                response = dummyData("airsearch_oneway.txt");
                return chain.proceed(chain.request())
                        .newBuilder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message("success")
                        .body(ResponseBody.create(MediaType.parse("application/json"), response))
                        .addHeader("content-type", "application/json")
                        .build();
            } else if (uri.endsWith("activity_search")) {
                response = dummyData("activitysearch.txt");
                return chain.proceed(chain.request())
                        .newBuilder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message("success")
                        .body(ResponseBody.create(MediaType.parse("application/json"), response))
                        .addHeader("content-type", "application/json")
                        .build();
            } else if (uri.endsWith("act_detail")) {
                response = dummyData("activitiesdetail.txt");
                return chain.proceed(chain.request())
                        .newBuilder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message("success")
                        .body(ResponseBody.create(MediaType.parse("application/json"), response))
                        .addHeader("content-type", "application/json")
                        .build();
            }

            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        }

        private String dummyData(String fileName) {
            String content = null;
            try {
                InputStream is = WHApplication.getInstance().getAssets().open(fileName);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                content = new String(buffer, "UTF-8");
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
            return content;
        }
    }
}
