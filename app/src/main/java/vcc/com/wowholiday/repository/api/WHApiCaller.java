package vcc.com.wowholiday.repository.api;

import android.util.Log;

import com.google.gson.JsonObject;

import retrofit2.Call;
import vcc.com.wowholiday.repository.api.activitiesdetail.ActivitiesDetailRequest;
import vcc.com.wowholiday.repository.api.activitiesdetail.ActivitiesDetailResponse;
import vcc.com.wowholiday.repository.api.activitieslocation.ActivitiesLocationRequest;
import vcc.com.wowholiday.repository.api.activitieslocation.ActivitiesLocationResponse;
import vcc.com.wowholiday.repository.api.activitiessearch.ActivitiesSearchResponse;
import vcc.com.wowholiday.repository.api.airlocation.AirLocationRequest;
import vcc.com.wowholiday.repository.api.airlocation.AirLocationResponse;
import vcc.com.wowholiday.repository.api.flightsearch.FlightSearchRequest;
import vcc.com.wowholiday.repository.api.flightsearch.FlightSearchResponse;
import vcc.com.wowholiday.repository.api.hoteldetail.HotelDetailRequest;
import vcc.com.wowholiday.repository.api.hoteldetail.HotelDetailResponse;
import vcc.com.wowholiday.repository.api.hotelsearch.HotelSearchRequest;
import vcc.com.wowholiday.repository.api.hotelsearch.HotelSearchResponse;
import vcc.com.wowholiday.repository.api.hotelsuggestion.HotelSuggestionRequest;
import vcc.com.wowholiday.repository.api.hotelsuggestion.HotelSuggestionResponse;
import vcc.com.wowholiday.repository.api.login.LoginRequest;
import vcc.com.wowholiday.repository.api.login.LoginResponse;
import vcc.com.wowholiday.repository.api.register.RegisterRequest;
import vcc.com.wowholiday.repository.api.register.RegisterResponse;
import vcc.com.wowholiday.repository.api.updateprofile.UpdateProfileRequest;
import vcc.com.wowholiday.repository.api.updateprofile.UpdateProfileResponse;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class WHApiCaller {

    /*public static Call<RegisterResponse> registerEmail(RegisterRequest request) {
        *//*JsonObject json = createBaseBody(request);
        json.addProperty("email", request.emailOrPhone);
        json.addProperty("password", request.password);*//*
        APIService service = RetrofitHelper.createService(APIService.class);
        return service.register(json);
    }

    public static Call<RegisterResponse> registerPhone(RegisterRequest request) {
        JsonObject json = createBaseBody(request);
        json.addProperty("phone", request.emailOrPhone);
        if (request.password != null) {
            json.addProperty("password", request.password);
        }
        Log.e("BODY", json.toString());
        APIService service = RetrofitHelper.createService(APIService.class);
        return service.register(json);
    }*/

    public static Call<RegisterResponse> register(RegisterRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        JsonObject body = request.toJsonBody();
        return service.register(request.authen, body);
    }

    public static Call<LoginResponse> loginEmail(LoginRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        JsonObject body = request.toJsonBody();
        return service.login(request.authen, body);
    }

    public static Call<UpdateProfileResponse> updateProfile(UpdateProfileRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        JsonObject body = request.toJsonBody();
        return service.updateProfile(request.authen, body);
    }

    public static Call<AirLocationResponse> getAirLocSearch(String authen, AirLocationRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        JsonObject body = request.toJsonBody();
        return service.getAirLocationSuggestion(authen, body);
    }

    public static Call<FlightSearchResponse> flightSearch(String authen, FlightSearchRequest request, boolean isCombo) {
        APIService service = RetrofitHelper.createService(APIService.class);
        if (isCombo) {
            return service.searchFlight(request.toJsonBody());
        } else {
            return service.searchFlightTestOneway(request.toJsonBody());
        }

    }

    public static Call<ActivitiesLocationResponse> searchActivitiesLocation(String authen,
                                                                            ActivitiesLocationRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        return service.searchActivitiesLocation(authen, request.toJsonBody());
    }

    public static Call<ActivitiesSearchResponse> activitiesSearch() {
        APIService service = RetrofitHelper.createService(APIService.class);
        return service.searchActivities();
    }

    public static Call<ActivitiesDetailResponse> getActivitiesDetail(String authen, ActivitiesDetailRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        return service.getActivitiesDetail(authen, request.toJsonBody());
    }

    public static Call<HotelSuggestionResponse> getHotelSuggestionSearch(String authen, HotelSuggestionRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        JsonObject body = request.toJsonBody();
        return service.getHotelSuggestion(authen, body);
    }

    public static Call<HotelSearchResponse> hotelSearch(String authen, HotelSearchRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        JsonObject body = request.toJsonBody();
        return service.searchHotel(authen,body);
    }

    public static Call<HotelDetailResponse> getHotelDetails(String authen, HotelDetailRequest request) {
        APIService service = RetrofitHelper.createService(APIService.class);
        JsonObject body = request.toJsonBody();
        return service.getHotelDetails(authen,body);
    }

    private static JsonObject createBaseBody(BaseApiRequest request) {
        JsonObject base = new JsonObject();
        base.addProperty("apikey", request.apiKey);
        //base.addProperty("device_type", request.device);
        return base;
    }
}
