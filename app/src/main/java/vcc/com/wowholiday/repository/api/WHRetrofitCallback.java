package vcc.com.wowholiday.repository.api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public abstract class WHRetrofitCallback<T extends BaseApiResponse> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.code() == 200) {
            onSuccess(response);
        } else {
            onError(new APIException(response.message(), response.code()));
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        t.printStackTrace();
        onError(new APIException(t.getMessage(), t));
    }

    public abstract void onSuccess(Response<T> response);
    public abstract void onError(APIException e);
}
