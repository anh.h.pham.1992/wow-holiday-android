package vcc.com.wowholiday.repository.api.activitiesdetail;

import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class ActivitiesDetailRequest extends BaseApiRequest {
    public String token;
    public String activitiesID;

    @Override
    public JsonObject toJsonBody() {
        JsonObject object = new JsonObject();
        JsonObject request = new JsonObject();
        request.addProperty("Token", token);
        request.addProperty("Data", activitiesID);
        object.add("Request", request);
        return object;
    }
}
