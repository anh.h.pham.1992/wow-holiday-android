package vcc.com.wowholiday.repository.api.activitiesdetail;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.activities.ActivitiesDetailData;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class ActivitiesDetailResponse extends BaseApiResponse<ActivitiesDetailResponse.DetailResponse[]> {

    @Override
    public boolean isResponseAvailable() {
        return data != null && data.length > 0
                && data[0].item != null && data[0].item.length > 0;
    }

    public static class DetailResponse {
        public ActivitiesDetailData[] item;
    }
}
