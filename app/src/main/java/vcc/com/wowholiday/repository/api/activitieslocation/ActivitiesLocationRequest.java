package vcc.com.wowholiday.repository.api.activitieslocation;

import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */
public class ActivitiesLocationRequest extends BaseApiRequest {
    public String locationName;

    @Override
    public JsonObject toJsonBody() {
        JsonObject body = new JsonObject();
        body.addProperty("Request", locationName);
        return body;
    }
}
