package vcc.com.wowholiday.repository.api.activitieslocation;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.LocationData;

/**
 * Created by Pham Hai Quang on 11/26/2019.
 */
public class ActivitiesLocationResponse
        extends BaseApiResponse<ActivitiesLocationResponse.ActivitiesLocationData[]> {

    public static class ActivitiesLocationData {
        public String type;
        public LocationData[] item;
    }
}
