package vcc.com.wowholiday.repository.api.activitiessearch;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.activities.ActivitiesInfoData;

/**
 * Created by Pham Hai Quang on 12/2/2019.
 */
public class ActivitiesSearchResponse extends BaseApiResponse<Void> {
    public FirstPage firstPage;

    @Override
    public boolean isResponseAvailable() {
        return firstPage != null && firstPage.response != null && firstPage.response.data != null;
    }

    public static class FirstPage {
        public FirstPageResponse response;
    }

    public static class FirstPageResponse {
        public String token;
        public FirstPageResponseData[] data;
    }

    public static class FirstPageResponseData {
        public ActivitiesInfoData[] item;
    }
}
