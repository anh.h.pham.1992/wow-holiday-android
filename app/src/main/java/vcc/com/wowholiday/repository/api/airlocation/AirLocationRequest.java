package vcc.com.wowholiday.repository.api.airlocation;

import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class AirLocationRequest extends BaseApiRequest {
    public String city;

    @Override
    public JsonObject toJsonBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("Request", city);
        return jsonObject;
    }
}
