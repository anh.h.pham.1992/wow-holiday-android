package vcc.com.wowholiday.repository.api.airlocation;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.LocationData;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class AirLocationResponse extends BaseApiResponse<AirLocationResponse.AirLocData[]> {

    @Override
    public boolean isResponseAvailable() {
        if (super.isResponseAvailable()) {
            return data.length > 0 && data[0].item != null;
        }
        return false;
    }

    public static class AirLocData {
        public LocationData[] item;
    }
}
