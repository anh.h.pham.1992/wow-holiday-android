package vcc.com.wowholiday.repository.api.converter;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.ActivitiesDetail;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Facility;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Policy;
import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.repository.api.model.AmenityData;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.FacilityData;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.PaxInfoData;
import vcc.com.wowholiday.repository.api.model.PolicyData;
import vcc.com.wowholiday.repository.api.model.activities.ActivitiesDetailData;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class ActivitiesDetailDataToActivitiesDetail implements IConverter<ActivitiesDetailData, ActivitiesDetail> {
    @Override
    public ActivitiesDetail convert(ActivitiesDetailData data) {
        ActivitiesDetail detail = new ActivitiesDetail();
        detail.setId(data.id);
        detail.setName(data.name);
        detail.setRating(data.rating);
        detail.setRaterCount(data.ratersCount);

        IConverter<DateInfoData, DateInfo> dateConverter = new DateInfoDataToDateInfo();
        detail.setDateInfo(dateConverter.convert(data.dateInfo));
        detail.setDescription(data.description);

        IConverter<LocationData, Location> locConverter = new LocationDataToLocation();
        detail.setLocation(locConverter.convert(data.locationInfo.fromLocation));

        if (data.images != null) {
            detail.setImageList(extractImage(data.images));
        }

        detail.setPrice(data.amount);
        detail.setOriginPrice(data.originalAmount);
        detail.setCurrencyUnit(getCurrencyUnit(data));

        Vendor vendor = new Vendor();
        vendor.setCode(data.vendors[0].item.code);
        vendor.setName(data.vendors[0].item.name);
        detail.setVendor(vendor);

        if (data.policies != null) {
            detail.setPolicyList(extractPolicy(data.policies));
        }

        if (data.amenities != null) {
            detail.setAmenityList(extractAmenity(data.amenities));
        }

        if (data.facilities != null) {
            detail.setFacilityList(extractFacility(data.facilities));
        }

        if (data.paxInfo != null) {
            detail.setPaxInfoList(extractPaxInfo(data.paxInfo));
        }
        return detail;
    }

    private List<String> extractImage(DxImageData[] images) {
        List<String> results = new ArrayList<>();
        for (DxImageData img : images) {
            results.add(img.url);
        }
        return results;
    }

    private String getCurrencyUnit(ActivitiesDetailData data) {
        if (TextUtils.isEmpty(data.displayAmount)) {
            return "";
        }
        String[] splits = data.displayAmount.split(" ");
        if (splits.length > 1) {
            return splits[0].trim();
        }
        return "";
    }

    private List<Policy> extractPolicy(PolicyData[] datas) {
        List<Policy> results = new ArrayList<>();
        IConverter<PolicyData, Policy> converter = new PolicyDataToPolicy();
        for (PolicyData d : datas) {
            results.add(converter.convert(d));
        }
        return results;
    }

    private List<Amenity> extractAmenity(AmenityData[] amenities) {
        List<Amenity> results = new ArrayList<>();
        for (AmenityData a : amenities) {
            Amenity amenity = new Amenity();
            amenity.setID(a.id);
            amenity.setName(a.name);
            results.add(amenity);
        }
        return results;
    }

    private List<Facility> extractFacility(FacilityData[] facilities) {
        List<Facility> results = new ArrayList<>();
        for (FacilityData f : facilities) {
            Facility fal = new Facility();
            fal.setId(f.id);
            fal.setDescription(f.description);
            results.add(fal);
        }
        return results;
    }

    private List<PaxInfo> extractPaxInfo(PaxInfoData[] data) {
        List<PaxInfo> results = new ArrayList<>();
        for (PaxInfoData p : data) {
            PaxInfo fal = new PaxInfo();
            fal.setQuantity(p.quantity);
            if ("adult".equals(p.typeString)) {
                fal.setType(PaxInfo.ADULT);
            } else if ("children".equals(p.typeString)) {
                fal.setType(PaxInfo.CHILD);
            }
            fal.setAmount(p.displayRateInfo[0].amount);
            results.add(fal);
        }
        return results;
    }
}
