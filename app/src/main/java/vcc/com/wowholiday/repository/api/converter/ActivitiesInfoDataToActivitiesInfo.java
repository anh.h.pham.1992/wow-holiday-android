package vcc.com.wowholiday.repository.api.converter;

import android.text.TextUtils;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.ActivitiesInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.activities.ActivitiesInfoData;

/**
 * Created by Pham Hai Quang on 11/30/2019.
 */
public class ActivitiesInfoDataToActivitiesInfo implements IConverter<ActivitiesInfoData, ActivitiesInfo> {
    @Override
    public ActivitiesInfo convert(ActivitiesInfoData activitiesInfoData) {
        ActivitiesInfo activitiesInfo = new ActivitiesInfo();
        activitiesInfo.setID(activitiesInfoData.id);
        activitiesInfo.setName(activitiesInfoData.name);
        activitiesInfo.setAvatarUrl(activitiesInfoData.url);
        activitiesInfo.setRating(activitiesInfoData.rating);
        activitiesInfo.setRateCount(activitiesInfoData.ratersCount);
        activitiesInfo.setPrice(activitiesInfoData.amount);
        activitiesInfo.setOriginalPrice(activitiesInfoData.originalAmount);
        activitiesInfo.setCurrencyUnit(getCurrencyUnit(activitiesInfoData));
        activitiesInfo.setLocation(getLocation(activitiesInfoData));
        Vendor vendor = new Vendor();
        vendor.setCode(activitiesInfoData.vendors[0].item.code);
        vendor.setName(activitiesInfoData.vendors[0].item.name);
        activitiesInfo.setVendor(vendor);

        IConverter<LocationData, Location> locConverter = new LocationDataToLocation();
        activitiesInfo.setLocation(locConverter.convert(activitiesInfoData.locationInfo.fromLocation));
        return activitiesInfo;
    }

    private String getCurrencyUnit(ActivitiesInfoData data) {
        if (TextUtils.isEmpty(data.displayAmount)) {
            return "";
        }
        String[] splits = data.displayAmount.split(" ");
        if (splits.length > 1) {
            return splits[0].trim();
        }
        return "";
    }

    private Location getLocation(ActivitiesInfoData data) {
        if (data.locationInfo.fromLocation != null) {
            Location loc = new Location();
            loc.setCountryName(data.locationInfo.fromLocation.country);
            loc.setCity(data.locationInfo.fromLocation.city);
            loc.setLat(data.locationInfo.fromLocation.latitude);
            loc.setLong(data.locationInfo.fromLocation.longitude);
            return loc;
        }
        return null;
    }
}
