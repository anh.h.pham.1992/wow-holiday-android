package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import quangph.com.mvp.mvp.repo.mapper.IValidFieldName;
import quangph.com.mvp.mvp.repo.mapper.ObjectMapper;
import vcc.com.wowholiday.model.ContactInfo;
import vcc.com.wowholiday.repository.api.model.ContactInfoData;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class ContactInfoDataToContactInfo implements IConverter<ContactInfoData, ContactInfo> {
    @Override
    public ContactInfo convert(ContactInfoData contactInfoData) {
        ObjectMapper<ContactInfoData> mapper = ObjectMapper.from(contactInfoData);
        mapper.setValidFieldName(new IValidFieldName() {
            @Override
            public boolean valid(String s, String destName) {
                String dest = "m" + s;
                return dest.toLowerCase().equals(destName.toLowerCase());
            }
        });
        return mapper.to(ContactInfo.class);
    }
}
