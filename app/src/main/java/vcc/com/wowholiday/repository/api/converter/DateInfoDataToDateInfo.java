package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.repository.api.model.DateInfoData;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class DateInfoDataToDateInfo implements IConverter<DateInfoData, DateInfo> {
    @Override
    public DateInfo convert(DateInfoData dateInfoData) {
        DateInfo info = new DateInfo();
        info.setStartDate(dateInfoData.startDate);
        info.setEndDate(dateInfoData.endDate);
        info.setStartTime(dateInfoData.startTime);
        info.setEndTime(dateInfoData.endTime);
        return info;
    }
}
