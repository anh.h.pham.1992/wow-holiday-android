package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Image;
import vcc.com.wowholiday.repository.api.model.DxImageData;

public class DxImageDataToImage implements IConverter<DxImageData, Image> {

    @Override
    public Image convert(DxImageData dxImageData) {
        Image image = new Image();
        image.setId(dxImageData.id);
        image.setTitle(dxImageData.title);
        image.setUrl(dxImageData.url);
        image.setCategory(dxImageData.category);
        image.setDescription(dxImageData.description);
        image.setUpdatedDate(dxImageData.updatedDate);
        image.setType(dxImageData.type);
        image.setIsDefault(dxImageData.isDefault);
        return image;
    }
}
