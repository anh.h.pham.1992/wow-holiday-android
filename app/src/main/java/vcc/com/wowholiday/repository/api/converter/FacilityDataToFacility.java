package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Facility;
import vcc.com.wowholiday.repository.api.model.FacilityData;

/**
 * Created by QuangPH on 2/8/2020.
 */
public class FacilityDataToFacility implements IConverter<FacilityData, Facility> {
    @Override
    public Facility convert(FacilityData facilityData) {
        Facility fal = new Facility();
        fal.setId(facilityData.id);
        fal.setDescription(facilityData.description);
        return fal;
    }
}
