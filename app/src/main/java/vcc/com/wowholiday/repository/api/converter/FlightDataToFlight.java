package vcc.com.wowholiday.repository.api.converter;

import android.text.TextUtils;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.air.Flight;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.Touple;
import vcc.com.wowholiday.repository.api.model.flight.FlightData;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.VendorData;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class FlightDataToFlight implements IConverter<FlightData, Flight> {
    @Override
    public Flight convert(FlightData flightData) {
        Flight flight = new Flight();
        setConfig(flight, flightData);
        flight.setToken(flightData.token);

        IConverter<DateInfoData, DateInfo> dateConveter = new DateInfoDataToDateInfo();
        flight.setDateInfo(dateConveter.convert(flightData.dateInfo));

        flight.setLayOverDuration(flightData.layOverDuration * 60 * 1000);
        flight.setJourneyDuration(flightData.journeyDuration * 60 * 1000);

        IConverter<LocationData, Location> locConvert = new LocationDataToLocation();
        flight.setStartPoint(locConvert.convert(flightData.locationInfo.fromLocation));
        flight.setEndPoint(locConvert.convert(flightData.locationInfo.toLocation));
        flight.setPrice(flightData.amount);

        if (!TextUtils.isEmpty(flightData.displayAmount)) {
            String[] display = flightData.displayAmount.split(" ");
            if (display.length > 1) {
                flight.setCurrencyUnit(display[0].trim());
            }
        }

        if (flightData.vendors != null && flightData.vendors.length > 0) {
            for (VendorData vendor : flightData.vendors) {
                if ("airline".equals(vendor.type)) {
                    Vendor av = new Vendor();
                    av.setCode(vendor.item.code);
                    av.setName(vendor.item.name);
                    flight.setVendors(av);
                    break;
                }
            }
        }

        return flight;
    }

    private void setConfig(Flight flight, FlightData flightData) {
        if (flightData.config != null && flightData.config.length > 0) {
            for (Touple t : flightData.config) {
                if ("FlightType".equals(t.key)) {
                    flight.setFlightType(t.value);
                } else if ("FlightID".equals(t.key)) {
                    flight.setID(t.value);
                }
            }
        }
    }
}
