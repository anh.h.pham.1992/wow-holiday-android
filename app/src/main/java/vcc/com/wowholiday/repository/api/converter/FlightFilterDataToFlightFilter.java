package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.model.RangeValue;
import vcc.com.wowholiday.model.air.FlightFilter;
import vcc.com.wowholiday.repository.api.model.flight.FlightFilterData;

/**
 * Created by Pham Hai Quang on 11/15/2019.
 */
public class FlightFilterDataToFlightFilter implements IConverter<FlightFilterData, FlightFilter> {
    @Override
    public FlightFilter convert(FlightFilterData data) {
        String name = null;
        if ("stops".equals(data.name)) {
            name = AppConfig.AirFilterName.STOP;
        } else if ("departurestarttime".equals(data.name)) {
            name = AppConfig.AirFilterName.START_TIME;
        } else if ("departureendtime".equals(data.name)) {
            name = AppConfig.AirFilterName.END_TIME;
        } else if ("airline".equals(data.name)) {
            name = AppConfig.AirFilterName.AIR;
        } else if ("pricerange".equals(data.name)) {
            name = AppConfig.AirFilterName.PRICE;
        }
        if (name == null) {
            return null;
        }

        FlightFilter filter = new FlightFilter();
        filter.setName(name);
        if ("radio".equals(data.type)) {
            filter.setType(FlightFilter.RADIO_TYPE);
            filter.setValues(data.values);
        } else if ("checkBox".equals(data.type)) {
            filter.setType(FlightFilter.CHECKBOX_TYPE);
            filter.setValues(data.values);
        } else if ("rangeList".equals(data.type)) {
            filter.setType(FlightFilter.RANGE_LIST_TYPE);
            RangeValue[] rangeValues = new RangeValue[data.minMaxList.length];
            for (int i = 0; i < rangeValues.length; i++) {
                RangeValue val = new RangeValue();
                val.setMinValue(data.minMaxList[i].minValue);
                val.setMaxValue(data.minMaxList[i].maxValue);
                rangeValues[i] = val;
            }
            filter.setRangeValues(rangeValues);
        } else if ("range".equals(data.type)) {
            filter.setType(FlightFilter.RANGE_TYPE);
            RangeValue[] rangeValues = new RangeValue[1];
            RangeValue val = new RangeValue();
            val.setMinValue(data.minValue);
            val.setMaxValue(data.maxValue);
            rangeValues[0] = val;
            filter.setRangeValues(rangeValues);
        }
        return filter;
    }
}
