package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.repository.api.model.hotel.HotelAmenityData;

public class HotelAmenityDataToAmenity implements IConverter<HotelAmenityData, Amenity> {

    @Override
    public Amenity convert(HotelAmenityData amenityData) {
        Amenity amenity = new Amenity();
        amenity.setID(amenityData.id);
        amenity.setName(amenityData.name);
        amenity.setType(amenityData.type);
        return amenity;
    }
}
