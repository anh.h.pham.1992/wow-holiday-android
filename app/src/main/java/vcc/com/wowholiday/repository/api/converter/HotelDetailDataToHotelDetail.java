package vcc.com.wowholiday.repository.api.converter;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.ContactInfo;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Image;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.hotel.HotelDetail;
import vcc.com.wowholiday.model.hotel.RoomInfo;
import vcc.com.wowholiday.repository.api.model.ContactInfoData;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelAmenityData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelDetailData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSearchData;
import vcc.com.wowholiday.repository.api.model.hotel.RoomGroupData;
import vcc.com.wowholiday.repository.api.model.hotel.RoomInfoData;


public class HotelDetailDataToHotelDetail implements IConverter<HotelDetailData, HotelDetail> {

    @Override
    public HotelDetail convert(HotelDetailData hotelDetailData) {
        HotelDetail hotelDetail = new HotelDetail();
        hotelDetail.setID(hotelDetailData.id);
        hotelDetail.setName(hotelDetailData.name);
        hotelDetail.setRating(hotelDetailData.rating);
        hotelDetail.setRatingCount(hotelDetailData.ratingCount);
        hotelDetail.setCategory(hotelDetailData.category);


        List<RoomInfoData> roomInfoDataList = new ArrayList<>();
        for (RoomGroupData roomGroupData :
                hotelDetailData.items) {
            if (roomGroupData != null && roomGroupData.item != null)
                for (RoomInfoData roomInfoData :
                        roomGroupData.item) {
                    roomInfoDataList.add(roomInfoData);
                }
        }
        IConverter<RoomInfoData, RoomInfo> roomConverter = new RoomInfoDataToRoomInfo();
        ArrayConverter<RoomInfoData, RoomInfo> roomArrayConverter = new ArrayConverter<>(roomConverter);
        hotelDetail.setRooms(roomArrayConverter.convert(roomInfoDataList.toArray(new RoomInfoData[roomInfoDataList.size()])));

        hotelDetail.setBusiness(hotelDetailData.business);
        hotelDetail.setDescription(hotelDetailData.description);
        IConverter<DateInfoData, DateInfo> dateConverter = new DateInfoDataToDateInfo();
        hotelDetail.setDateInfo(dateConverter.convert(hotelDetailData.dateInfo));

        IConverter<LocationData, Location> locConverter = new LocationDataToLocation();
        hotelDetail.setLocation(locConverter.convert(hotelDetailData.locationInfo.fromLocation));

        DxImageDataToImage imageConverter = new DxImageDataToImage();
        ArrayConverter<DxImageData, Image> imageArrayConverter = new ArrayConverter<>(imageConverter);
        hotelDetail.setImages(imageArrayConverter.convert(hotelDetailData.images));

        IConverter<ContactInfoData, ContactInfo> contactConverter = new ContactInfoDataToContactInfo();
        hotelDetail.setContactInfo(contactConverter.convert(hotelDetailData.contactInformation));

        Vendor vendor = new Vendor();
        vendor.setCode(hotelDetailData.vendors[0].item.code);
        vendor.setName(hotelDetailData.vendors[0].item.name);
        hotelDetail.setVendor(vendor);

        HotelAmenityDataToAmenity amenityConverter = new HotelAmenityDataToAmenity();
        ArrayConverter<HotelAmenityData, Amenity> amenityArrayConverter = new ArrayConverter<>(amenityConverter);
        hotelDetail.setAmenities(amenityArrayConverter.convert(hotelDetailData.amenities));

        hotelDetail.setObjectIdentifier(hotelDetailData.objectIdentifier);
        hotelDetail.setStatus(hotelDetailData.status);

        hotelDetail.setPrice(hotelDetailData.amount);
        hotelDetail.setOriginalPrice(hotelDetailData.originalAmount);
        hotelDetail.setCurrencyUnit(getCurrencyUnit(hotelDetailData));
        return hotelDetail;
    }

    private String getCurrencyUnit(HotelDetailData data) {
        if (TextUtils.isEmpty(data.displayAmount)) {
            return "";
        }
        String[] splits = data.displayAmount.split(" ");
        if (splits.length > 1) {
            return splits[0].trim();
        }
        return "";
    }

    private String getAvatarUrl(HotelSearchData data) {
        if (data == null || data.images == null || data.images.length == 0)
            return null;
        return data.images[0].url;
    }
}
