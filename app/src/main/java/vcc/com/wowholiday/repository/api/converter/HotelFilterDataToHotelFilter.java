package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.model.RangeValue;
import vcc.com.wowholiday.model.hotel.HotelFilter;
import vcc.com.wowholiday.repository.api.model.hotel.HotelFilterData;

public class HotelFilterDataToHotelFilter implements IConverter<HotelFilterData, HotelFilter> {
    @Override
    public HotelFilter convert(HotelFilterData data) {
        String name = null;
        if ("name".equals(data.name)) {
            name = AppConfig.HotelFilterName.NAME;
        } else if ("currency".equals(data.name)) {
            name = AppConfig.HotelFilterName.CURRENCY;
        } else if ("pricerange".equals(data.name)) {
            name = AppConfig.HotelFilterName.PRICE_RANGE;
        } else if ("starrating".equals(data.name)) {
            name = AppConfig.HotelFilterName.STAR_RATING;
        } else if ("category".equals(data.name)) {
            name = AppConfig.HotelFilterName.CATEGORY;
        }
        if (name == null) {
            return null;
        }

        HotelFilter filter = new HotelFilter();
        filter.setName(name);
        if ("input".equals(data.type)) {
            filter.setType(HotelFilter.INPUT_TYPE);
            filter.setValues(data.values);
        } else if ("checkBox".equals(data.type)) {
            filter.setType(HotelFilter.CHECKBOX_TYPE);
            filter.setValues(data.values);
        }  else if ("range".equals(data.type)) {
            filter.setType(HotelFilter.RANGE_TYPE);
            RangeValue[] rangeValues = new RangeValue[1];
            RangeValue val = new RangeValue();
            val.setMinValue(data.minValue);
            val.setMaxValue(data.maxValue);
            rangeValues[0] = val;
            filter.setRangeValues(rangeValues);
        }
        return filter;
    }
}
