package vcc.com.wowholiday.repository.api.converter;

import android.text.TextUtils;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.hotel.HotelInfo;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSearchData;


public class HotelSearchDataToHotelInfo implements IConverter<HotelSearchData, HotelInfo> {
    private String mSearchToken;

    public HotelSearchDataToHotelInfo(String searchToken) {
        mSearchToken = searchToken;
    }

    @Override
    public HotelInfo convert(HotelSearchData hotelSearchData) {
        HotelInfo hotelInfo = new HotelInfo();
        hotelInfo.setID(hotelSearchData.id);
        hotelInfo.setName(hotelSearchData.name);
        hotelInfo.setAvatarUrl(getAvatarUrl(hotelSearchData));
        hotelInfo.setRating(hotelSearchData.rating);
        hotelInfo.setRatingCount(hotelSearchData.ratingCount);
        hotelInfo.setObjectIdentifier(hotelSearchData.objectIdentifier);
        hotelInfo.setToken(hotelSearchData.token);
        hotelInfo.setStatus(hotelSearchData.status);

        IConverter<DateInfoData, DateInfo> dateConverter = new DateInfoDataToDateInfo();
        hotelInfo.setDateInfo(dateConverter.convert(hotelSearchData.dateInfo));

        hotelInfo.setDescription(hotelSearchData.description);

        IConverter<LocationData, Location> locConverter = new LocationDataToLocation();
        hotelInfo.setLocation(locConverter.convert(hotelSearchData.locationInfo.fromLocation));

        hotelInfo.setPrice(hotelSearchData.amount);
        hotelInfo.setOriginalPrice(hotelSearchData.originalAmount);
        hotelInfo.setCurrencyUnit(getCurrencyUnit(hotelSearchData));
        Vendor vendor = new Vendor();
        vendor.setCode(hotelSearchData.vendors[0].item.code);
        vendor.setName(hotelSearchData.vendors[0].item.name);
        hotelInfo.setVendor(vendor);
        hotelInfo.setSearchToken(mSearchToken);

        return hotelInfo;
    }

    private String getCurrencyUnit(HotelSearchData data) {
        if (TextUtils.isEmpty(data.displayAmount)) {
            return "";
        }
        String[] splits = data.displayAmount.split(" ");
        if (splits.length > 1) {
            return splits[0].trim();
        }
        return "";
    }

    private String getAvatarUrl(HotelSearchData data) {
        if(data ==null || data.images == null || data.images.length ==0)
            return null;
        return data.images[0].url;
    }
}
