package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.hotel.HotelSuggestion;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSuggestionData;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class HotelSuggestionDataToHotelSuggestion implements IConverter<HotelSuggestionData, HotelSuggestion> {
    @Override
    public HotelSuggestion convert(HotelSuggestionData hotelSuggestionData) {
        HotelSuggestion hotelSuggestion = new HotelSuggestion();
        hotelSuggestion.setID(hotelSuggestionData.id);
        hotelSuggestion.setName(hotelSuggestionData.name);
        hotelSuggestion.setCountryID(hotelSuggestionData.countryID);
        hotelSuggestion.setType(hotelSuggestionData.type);
        hotelSuggestion.setPriority(hotelSuggestionData.priority);
        return hotelSuggestion;
    }
}
