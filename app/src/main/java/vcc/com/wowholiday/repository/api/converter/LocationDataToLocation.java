package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.repository.api.model.LocationData;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class LocationDataToLocation implements IConverter<LocationData, Location> {
    @Override
    public Location convert(LocationData locationData) {
        Location loc = new Location();
        loc.setID(locationData.id);
        loc.setZipCode(locationData.zipCode);
        loc.setName(locationData.name);
        loc.setCultureIrrelevantName(locationData.cultureIrrelevantName);
        loc.setCountryID(locationData.countryID);
        loc.setCountryName(locationData.country);
        loc.setType(locationData.type);
        loc.setLat(locationData.latitude);
        loc.setLong(locationData.longitude);
        loc.setAddress(locationData.address);
        loc.setCity(locationData.city);
        loc.setState(locationData.state);
        loc.setDistrict(locationData.district);
        return loc;
    }
}
