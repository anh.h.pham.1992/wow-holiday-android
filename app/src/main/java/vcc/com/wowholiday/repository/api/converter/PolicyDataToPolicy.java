package vcc.com.wowholiday.repository.api.converter;

import java.util.ArrayList;
import java.util.List;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Criteria;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Policy;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.PolicyData;
import vcc.com.wowholiday.repository.api.model.Touple;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class PolicyDataToPolicy implements IConverter<PolicyData, Policy> {
    @Override
    public Policy convert(PolicyData data) {
        Policy policy = new Policy();
        policy.setType(data.type);
        policy.setDescription(data.description);

        IConverter<DateInfoData, DateInfo> dateConverter = new DateInfoDataToDateInfo();
        if (data.dateInfo != null) {
            policy.setDateInfo(dateConverter.convert(data.dateInfo));
        }

        policy.setPortalPolicy(data.isPortalPolicy);

        if (data.criteria != null) {
            policy.setCriteriaList(extractCriteria(data));
        }

        return policy;
    }

    private List<Criteria> extractCriteria(PolicyData data) {
        List<Criteria> results = new ArrayList<>();
        for (Touple t : data.criteria) {
            Criteria criteria = new Criteria();
            criteria.setKey(t.key);
            criteria.setValue(t.value);
            results.add(criteria);
        }
        return results;
    }
}
