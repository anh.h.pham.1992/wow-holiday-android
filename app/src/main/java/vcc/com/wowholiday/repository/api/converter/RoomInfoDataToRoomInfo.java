package vcc.com.wowholiday.repository.api.converter;

import android.text.TextUtils;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Amenity;
import vcc.com.wowholiday.model.Image;
import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.hotel.RoomInfo;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelAmenityData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSearchData;
import vcc.com.wowholiday.repository.api.model.hotel.RoomInfoData;


public class RoomInfoDataToRoomInfo implements IConverter<RoomInfoData, RoomInfo> {

    @Override
    public RoomInfo convert(RoomInfoData roomInfoData) {
        RoomInfo room = new RoomInfo();
        room.setName(roomInfoData.name);
        room.setBusiness(roomInfoData.business);
        room.setObjectIdentifier(roomInfoData.objectIdentifier);
        room.setToken(roomInfoData.token);
        room.setCode(roomInfoData.code);
        room.setStatus(roomInfoData.status);
        room.setBoardingStatus(roomInfoData.boardingStatus);
        DxImageDataToImage imageConverter = new DxImageDataToImage();
        ArrayConverter<DxImageData, Image> arrayConverter = new ArrayConverter<>(imageConverter);
        room.setImages(arrayConverter.convert(roomInfoData.images));

        HotelAmenityDataToAmenity amenityConverter = new HotelAmenityDataToAmenity();
        ArrayConverter<HotelAmenityData, Amenity> amenityArrayConverter = new ArrayConverter<>(amenityConverter);
        room.setAmenities(amenityArrayConverter.convert(roomInfoData.amenities));

        room.setPrice(roomInfoData.amount);
        room.setOriginalPrice(roomInfoData.originalAmount);
        room.setStrikeThroughPrice(roomInfoData.strikeThroughAmount);
        room.setCurrencyUnit(getCurrencyUnit(roomInfoData));
        room.setAvatar(getAvatar(roomInfoData));
        return room;
    }

    private String getCurrencyUnit(RoomInfoData data) {
        if (TextUtils.isEmpty(data.displayAmount)) {
            return "";
        }
        String[] splits = data.displayAmount.split(" ");
        if (splits.length > 1) {
            return splits[0].trim();
        }
        return "";
    }

    private Image getAvatar(RoomInfoData data) {
        if (data == null || data.images == null || data.images.length == 0)
            return null;
        DxImageDataToImage imageConverter = new DxImageDataToImage();
        return imageConverter.convert(data.images[0]);
    }
}
