package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.config.AppConfig;
import vcc.com.wowholiday.model.Sort;
import vcc.com.wowholiday.repository.api.model.SortData;

public class SortDataToHotelSort implements IConverter<SortData, Sort> {
    @Override
    public Sort convert(SortData data) {
        Sort s = new Sort();
        if ("name".equals(data.name)) {
            s.setName(AppConfig.HotelSortName.NAME);
            s.setOrder(data.order);
        }  else if ("rate".equals(data.name)) {
            s.setName(AppConfig.HotelSortName.RATE);
            s.setOrder(data.order);
        }  else if ("starrating".equals(data.name)) {
            s.setName(AppConfig.HotelSortName.STAR_RATING);
            s.setOrder(data.order);
        }  else if ("promotions".equals(data.name)) {
            s.setName(AppConfig.HotelSortName.PROMOTIONS);
            s.setOrder(data.order);
        }
        return s;
    }
}
