package vcc.com.wowholiday.repository.api.converter;

import android.text.TextUtils;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.Vendor;
import vcc.com.wowholiday.model.air.Flight;
import vcc.com.wowholiday.model.air.FlightTrip;
import vcc.com.wowholiday.model.air.FlightWay;
import vcc.com.wowholiday.model.air.PaxInfo;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.DisplayRateInfo;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.VendorData;
import vcc.com.wowholiday.repository.api.model.PaxInfoData;
import vcc.com.wowholiday.repository.api.model.flight.TripData;
import vcc.com.wowholiday.repository.api.model.flight.TripDataSummary;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class TripDataSummaryToFlightTrip implements IConverter<TripDataSummary, FlightTrip> {
    @Override
    public FlightTrip convert(TripDataSummary tripData) {
        FlightTrip trip = new FlightTrip();
        trip.setPrice(tripData.amount);
        trip.setCurrencyUnit(extractCurrencyUnit(tripData.displayAmount));

        if ("roundtrip".equals(tripData.tripType.toLowerCase())) {
            trip.setType(FlightTrip.TYPE_ROUND_TRIP);
        } else {
            trip.setType(FlightTrip.TYPE_ONE_WAY);
        }

        trip.setTotalDur(tripData.totalDuration * 60 * 1000);
        trip.setJourneyDuration(tripData.journeyDuration * 60 * 1000);
        trip.setDomestic(tripData.flags.isDomestic);
        IConverter<DateInfoData, DateInfo> dateConveter = new DateInfoDataToDateInfo();
        trip.setDateInfo(dateConveter.convert(tripData.dateInfo));
        buildDepartureFlight(trip, tripData);
        buildArrivalFlight(trip, tripData);
        buildVendor(trip, tripData);
        buildPaxInfo(trip, tripData);
        return trip;
    }

    private String extractCurrencyUnit(String displayAmount) {
        if (!TextUtils.isEmpty(displayAmount)) {
            String[] display = displayAmount.split(" ");
            if (display.length > 1) {
                return display[0].trim();
            }
        }
        return "";
    }

    private void buildDepartureFlight(FlightTrip trip, TripDataSummary tripData) {
        TripData departure = null;
        for (TripData t : tripData.items) {
            if (t.type == 1) {
                departure = t;
                break;
            }
        }
        if (departure != null && departure.item != null && departure.item.length > 0) {
            FlightWay way = buildWay(departure);
            way.setType(FlightWay.DEPARTURE);
            trip.setDepartureWay(way);
        }
    }

    private void buildArrivalFlight(FlightTrip trip, TripDataSummary tripData) {
        TripData arrival = null;
        for (TripData t : tripData.items) {
            if (t.type == 2) {
                arrival = t;
                break;
            }
        }
        if (arrival != null && arrival.item != null && arrival.item.length > 0) {
            FlightWay way = buildWay(arrival);
            way.setType(FlightWay.ARRIVAL);
            trip.setArrivalWay(way);
        }
    }

    private FlightWay buildWay(TripData data) {
        FlightWay way = new FlightWay();
        way.setPrice(data.amount);
        way.setCurrencyUnit(extractCurrencyUnit(data.displayAmount));
        IConverter<DateInfoData, DateInfo> dateConveter = new DateInfoDataToDateInfo();
        way.setDateInfo(dateConveter.convert(data.dateInfo));

        FlightDataToFlight flightConverter = new FlightDataToFlight();
        Flight[] flights = new Flight[data.item.length];
        for (int i = 0; i < data.item.length; i++) {
            flights[i] = flightConverter.convert(data.item[i]);
        }
        way.setTransits(flights);
        setLocationForWay(way, data);
        way.setDuration(new int[]{data.properties.duration_Hour, data.properties.duration_Minute});
        return way;
    }

    private void setLocationForWay(FlightWay way, TripData data) {
        IConverter<LocationData, Location> locConvert = new LocationDataToLocation();
        way.setStartLocation(locConvert.convert(data.properties.tripLocationDetails.fromLocation));
        way.setEndLocation(locConvert.convert(data.properties.tripLocationDetails.toLocation));
    }

    private void buildVendor(FlightTrip trip, TripDataSummary tripData) {
        VendorData vendor = null;
        if (tripData.vendors != null) {
            for (VendorData v : tripData.vendors) {
                if ("default".equals(v.type) || "airline".equals(v.type)) {
                    vendor = v;
                    break;
                }
            }
        }
        if (vendor != null) {
            Vendor air = new Vendor();
            air.setName(vendor.item.name);
            air.setCode(vendor.item.code);
            trip.setVendor(air);
        }
    }

    private void buildPaxInfo(FlightTrip trip, TripDataSummary tripData) {
        if (tripData.paxInfo != null) {
            PaxInfo[] paxInfos = new PaxInfo[tripData.paxInfo.length];
            for (int i = 0; i < tripData.paxInfo.length; i++) {
                PaxInfoData data = tripData.paxInfo[i];
                PaxInfo itm = new PaxInfo();
                if ("ADT".equals(data.typeString)) {
                    itm.setType(PaxInfo.ADULT);
                } else if ("CHD".equals(data.typeString)) {
                    itm.setType(PaxInfo.CHILD);
                } else if ("INF".equals(data.typeString)) {
                    itm.setType(PaxInfo.NEW_BORN);
                }
                itm.setQuantity(data.quantity);

                DisplayRateInfo total = findTotalDisplayRate(data);
                if (total != null) {
                    itm.setAmount(total.amount);
                    itm.setCurrencyCode(total.currencyCode);
                }
                paxInfos[i] = itm;
            }
            trip.setPaxInfos(paxInfos);
        }
    }

    private DisplayRateInfo findTotalDisplayRate(PaxInfoData paxInfo) {
        if (paxInfo.displayRateInfo != null) {
            for (DisplayRateInfo d : paxInfo.displayRateInfo) {
                if (d.purpose == 10) {
                    return d;
                }
            }
        }
        return null;
    }
}
