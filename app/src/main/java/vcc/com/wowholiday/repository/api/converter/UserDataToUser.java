package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.ContactInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.repository.api.model.ContactInfoData;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.UserData;

/**
 * Created by Pham Hai Quang on 11/12/2019.
 */
public class UserDataToUser implements IConverter<UserData, User> {
    @Override
    public User convert(UserData userData) {
        User user = new User();
        user.setID(userData.userID);
        user.setEntityID(userData.entityID);
        user.setAgentID(userData.agentID);
        user.setCustomerID(userData.customerID);
        user.setFirstName(userData.firstName);
        user.setLastName(userData.lastName);

        IConverter<LocationData, Location> locConverter = new LocationDataToLocation();
        user.setLocation(locConverter.convert(userData.location));

        IConverter<ContactInfoData, ContactInfo> contactConverter = new ContactInfoDataToContactInfo();
        user.setContactInfo(contactConverter.convert(userData.contactInformation));

        if (userData.gender.toLowerCase().equals("m")) {
            user.setGender(User.MALE);
        } else {
            user.setGender(User.FEMALE);
        }
        user.setBirthday(userData.birthDate);
        user.setPassportExpirationDate(userData.passportExpirationDate);
        user.setAvatarUrl(userData.profilePicture.url);
        return user;
    }
}
