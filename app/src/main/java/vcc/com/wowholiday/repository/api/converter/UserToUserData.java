package vcc.com.wowholiday.repository.api.converter;

import quangph.com.mvp.mvp.repo.IConverter;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.model.User;
import vcc.com.wowholiday.repository.api.model.ContactInfoData;
import vcc.com.wowholiday.repository.api.model.LocationData;
import vcc.com.wowholiday.repository.api.model.ProfilePictureData;
import vcc.com.wowholiday.repository.api.model.UserData;

/**
 * Created by Pham Hai Quang on 11/16/2019.
 */
public class UserToUserData implements IConverter<User, UserData> {
    @Override
    public UserData convert(User user) {
        UserData data = new UserData();
        data.userID = user.getID();
        data.entityID = user.getEntityID();
        data.agentID = user.getAgentID();
        data.customerID = user.getCustomerID();
        data.firstName = user.getFirstName();
        data.lastName = user.getLastName();
        if (user.getGender().equals(User.MALE)) {
            data.gender = "m";
        } else {
            data.gender = "f";
        }
        data.birthDate = user.getBirthday();
        data.passportExpirationDate = user.getPassportExpirationDate();
        data.profilePicture = new ProfilePictureData();
        data.profilePicture.url = user.getAvatarUrl();

        data.contactInformation = new ContactInfoData();
        data.contactInformation.phoneNumber = user.getContactInfo().getPhoneNumber();
        data.contactInformation.actlFormatHomePhoneNumber = user.getContactInfo().getActlFormatHomePhoneNumber();
        data.contactInformation.homePhoneNumber = user.getContactInfo().getHomePhoneNumber();
        data.contactInformation.phoneNumberCountryCode = user.getContactInfo().getPhoneNumberCountryCode();
        data.contactInformation.homePhoneNumberCountryCode = user.getContactInfo().getHomePhoneNumberCountryCode();
        data.contactInformation.actlFormatHomePhoneNumber = user.getContactInfo().getActlFormatHomePhoneNumber();
        data.contactInformation.fax = user.getContactInfo().getFax();
        data.contactInformation.email = user.getContactInfo().getEmail();

        if (user.getLocation() != null) {
            Location loc = user.getLocation();
            LocationData locData = new LocationData();
            locData.id = loc.getID();
            locData.cultureIrrelevantName = loc.getCultureIrrelevantName();
            locData.name = loc.getName();
            locData.countryID = loc.getCountryID();
            locData.country = loc.getCountryName();
            locData.type = loc.getType();
            locData.latitude = loc.getLat();
            locData.longitude = loc.getLong();
            locData.address = loc.getAddress();
            locData.city = loc.getCity();
            locData.state = loc.getState();
            locData.zipCode = loc.getZipCode();
            locData.district = loc.getDistrict();
            data.location = locData;
        }

        return data;
    }
}
