package vcc.com.wowholiday.repository.api.flightsearch;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import vcc.com.wowholiday.model.air.FlightBookingProperty;
import vcc.com.wowholiday.repository.api.BaseApiRequest;
import vcc.com.wowholiday.util.TimeFormatUtil;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class FlightSearchRequest extends BaseApiRequest {
    public FlightBookingProperty criterialInfo;

    @Override
    public JsonObject toJsonBody() {
        JsonObject request = new JsonObject();

        JsonObject fromLoc = new JsonObject();
        fromLoc.addProperty("ID", criterialInfo.getStartLoc().getID());
        JsonObject toLoc = new JsonObject();
        toLoc.addProperty("ID", criterialInfo.getEndLoc().getID());
        JsonObject locInfo = new JsonObject();
        locInfo.add("FromLocation", fromLoc);
        locInfo.add("ToLocation", toLoc);

        JsonObject criterialInfoItm = new JsonObject();
        criterialInfoItm.add("LocationInfo", locInfo);

        JsonObject dateInfo = new JsonObject();
        dateInfo.addProperty("StartDate", "2019-12-26T00:00:00+0700");
        if (criterialInfo.isRoundTrip()) {
            dateInfo.addProperty("EndDate", TimeFormatUtil.toISO8601UTC(criterialInfo.getEndDate().getTime()));
        }
        criterialInfoItm.add("DateInfo", dateInfo);
        criterialInfoItm.addProperty("SequenceNumber", criterialInfo.isRoundTrip() ? 2 : 1);

        JsonArray array = new JsonArray();
        array.add(criterialInfoItm);
        request.add("CriteriaInfo", array);

        JsonArray peopleItem = new JsonArray();
        JsonObject adlItm = new JsonObject();
        adlItm.addProperty("TypeString", "ADT");
        adlItm.addProperty("Quantity", criterialInfo.getAdultCount());
        peopleItem.add(adlItm);

        if (criterialInfo.getChildrenCount() > 0) {
            JsonObject childItm = new JsonObject();
            childItm.addProperty("TypeString", "CHD");
            childItm.addProperty("Quantity", criterialInfo.getChildrenCount());
            peopleItem.add(childItm);
        }

        if (criterialInfo.getNewbornCount() > 0) {
            JsonObject newBornItm = new JsonObject();
            newBornItm.addProperty("TypeString", "INF");
            newBornItm.addProperty("Quantity", criterialInfo.getNewbornCount());
            peopleItem.add(newBornItm);
        }

        JsonObject paxInfoItem = new JsonObject();
        paxInfoItem.add("Item", peopleItem);

        JsonArray paxInfo = new JsonArray();
        paxInfo.add(paxInfoItem);

        request.add("PaxInfoData", paxInfo);
        request.addProperty("TripType", criterialInfo.getEndDate() == null ? "oneway" : "roundtrip");
        request.addProperty("Business", "Air");

        JsonObject flag = new JsonObject();
        flag.addProperty("feature:disableseparateroundtrip", true);

        JsonObject body = new JsonObject();
        body.add("Request", request);
        body.add("Flags", flag);
        return body;
    }
}
