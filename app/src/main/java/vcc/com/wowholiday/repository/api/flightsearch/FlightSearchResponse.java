package vcc.com.wowholiday.repository.api.flightsearch;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.SortData;
import vcc.com.wowholiday.repository.api.model.flight.FlightFilterData;
import vcc.com.wowholiday.repository.api.model.flight.TripDataSummary;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class FlightSearchResponse extends BaseApiResponse<Void> {

    public FirstPage firstPage;

    @Override
    public boolean isResponseAvailable() {
        return firstPage != null;
    }

    public static class FirstPage {
        public FirstPageResponse response;
    }

    public static class FirstPageResponse {
        public FlightFilterData[] availableFilters;
        public SortData[] availableSorting;
        public FirstPageResponseData[] data;
    }

    public static class FirstPageResponseData {
        public TripDataSummary[] item;
    }
}
