package vcc.com.wowholiday.repository.api.hoteldetail;

import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;

public class HotelDetailRequest extends BaseApiRequest {
    public String token;
    public String data;

    @Override
    public JsonObject toJsonBody() {
        JsonObject request = new JsonObject();
        request.addProperty("Token", token);
        request.addProperty("Data", data);
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("Request", request);
        return jsonObject;
    }
}
