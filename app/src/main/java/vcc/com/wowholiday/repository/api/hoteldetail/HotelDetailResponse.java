package vcc.com.wowholiday.repository.api.hoteldetail;

import java.util.List;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.hotel.HotelDetailData;


public class HotelDetailResponse extends BaseApiResponse<List<HotelDetailResponse.ResponseData>> {

    public static class ResponseData {
        public HotelDetailData[] item;
    }

}
