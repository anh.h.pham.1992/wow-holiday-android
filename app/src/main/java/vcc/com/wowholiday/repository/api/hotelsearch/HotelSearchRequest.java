package vcc.com.wowholiday.repository.api.hotelsearch;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import vcc.com.wowholiday.presenter.hotelsearch.HotelSearchProperty;
import vcc.com.wowholiday.repository.api.BaseApiRequest;
import vcc.com.wowholiday.util.TimeFormatUtil;

public class HotelSearchRequest extends BaseApiRequest {
    public HotelSearchProperty hotelSearchProperty;

    @Override
    public JsonObject toJsonBody() {
        JsonObject request = new JsonObject();
        JsonObject fromLoc = new JsonObject();
        fromLoc.addProperty("ID", hotelSearchProperty.getLocation().getID());
        fromLoc.addProperty("Name", hotelSearchProperty.getLocation().getName());
        fromLoc.addProperty("CountryID", hotelSearchProperty.getLocation().getCountryID());
        fromLoc.addProperty("Type", hotelSearchProperty.getLocation().getType());
        fromLoc.addProperty("Priority", hotelSearchProperty.getLocation().getPriority());

        JsonObject locInfo = new JsonObject();
        locInfo.add("FromLocation", fromLoc);

        JsonObject dateInfo = new JsonObject();
        dateInfo.addProperty("StartDate", TimeFormatUtil.toISO8601UTC(hotelSearchProperty.getStartDate().getTime()));
        dateInfo.addProperty("EndDate", TimeFormatUtil.toISO8601UTC(hotelSearchProperty.getEndDate().getTime()));

        JsonObject criteriaInfoItm = new JsonObject();
        criteriaInfoItm.add("LocationInfo", locInfo);
        criteriaInfoItm.add("DateInfo", dateInfo);

        JsonArray array = new JsonArray();
        array.add(criteriaInfoItm);
        request.add("CriteriaInfo", array);

        JsonArray peopleItem = new JsonArray();
        JsonObject adlItm = new JsonObject();
        adlItm.addProperty("TypeString", "ADT");
        adlItm.addProperty("Quantity", hotelSearchProperty.getPersonCount());
        peopleItem.add(adlItm);

        JsonObject paxInfoItem = new JsonObject();
        paxInfoItem.add("Item", peopleItem);

        JsonArray paxInfo = new JsonArray();
        paxInfo.add(paxInfoItem);

        request.add("PaxInfo", paxInfo);

        request.addProperty("Status", "Available");
        request.addProperty("Business", "hotel");

        JsonObject body = new JsonObject();
        body.add("Request", request);

        return body;
    }
}
