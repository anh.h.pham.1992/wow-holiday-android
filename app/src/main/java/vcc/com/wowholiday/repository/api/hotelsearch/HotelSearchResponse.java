package vcc.com.wowholiday.repository.api.hotelsearch;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.SortData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelFilterData;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSearchData;

public class HotelSearchResponse extends BaseApiResponse<Void> {
    public FirstPage firstPage;

    @Override
    public boolean isResponseAvailable() {
        return firstPage != null;
    }

    public static class FirstPage {
        public FirstPageResponse response;
    }

    public static class FirstPageResponse {
        public String token;
        public HotelFilterData[] availableFilters;
        public SortData[] availableSorting;
        public FirstPageResponseData[] data;
    }

    public static class FirstPageResponseData {
        public HotelSearchData[] item;
    }
}
