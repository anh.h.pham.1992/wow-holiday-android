package vcc.com.wowholiday.repository.api.hotelsuggestion;

import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class HotelSuggestionRequest extends BaseApiRequest {
    public String keyword;

    @Override
    public JsonObject toJsonBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("Request", keyword);
        return jsonObject;
    }
}
