package vcc.com.wowholiday.repository.api.hotelsuggestion;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.hotel.HotelSuggestionData;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class HotelSuggestionResponse extends BaseApiResponse<HotelSuggestionResponse.HotelSuggestionResponseData[]> {

    @Override
    public boolean isResponseAvailable() {
        if (super.isResponseAvailable()) {
            if (data.length == 0)
                return false;
            for (HotelSuggestionResponseData d :
                    data) {
                if (d.item != null)
                    return true;
            }
        }
        return false;
    }

    public static class HotelSuggestionResponseData {
        public HotelSuggestionData[] item;
        public String type;
    }
}
