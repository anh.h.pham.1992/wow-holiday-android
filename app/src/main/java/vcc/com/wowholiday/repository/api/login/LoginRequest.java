package vcc.com.wowholiday.repository.api.login;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;

/**
 * Created by Pham Hai Quang on 11/12/2019.
 */
public class LoginRequest extends BaseApiRequest {
    public String authen;
    public String email;
    public String phoneNumber;
    public String phoneNumberCountryCode;
    public String password;

    @Override
    public JsonObject toJsonBody() {
        JsonObject body = new JsonObject();
        body.addProperty("Password", password);

        JsonObject contact = new JsonObject();
        if (phoneNumber == null) {
            contact.addProperty("Email", email);
        } else {
            contact.addProperty("phoneNumber", phoneNumber);
            contact.addProperty("phoneNumberCountryCode", phoneNumberCountryCode);
        }

        body.add("ContactInformation", contact);

        JsonObject request = new JsonObject();
        request.add("Request", body);
        Log.e("LoginRq", request.toString());
        return request;
    }
}
