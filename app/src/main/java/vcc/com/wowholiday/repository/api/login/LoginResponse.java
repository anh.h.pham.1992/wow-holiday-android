package vcc.com.wowholiday.repository.api.login;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.UserData;

/**
 * Created by Pham Hai Quang on 11/12/2019.
 */
public class LoginResponse extends BaseApiResponse<UserData> {
}
