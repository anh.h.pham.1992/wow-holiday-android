package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/12/2019.
 */
public class ContactInfoData {
    public String phoneNumber;
    public String actlFormatPhoneNumber;
    public String homePhoneNumber;
    public String phoneNumberCountryCode;
    public String homePhoneNumberCountryCode;
    public String actlFormatHomePhoneNumber;
    public String fax;
    public String email;
}
