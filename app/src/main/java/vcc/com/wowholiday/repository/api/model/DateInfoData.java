package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class DateInfoData {
    public String startDate;
    public String endDate;
    public String startTime;
    public String endTime;
}
