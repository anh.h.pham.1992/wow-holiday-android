package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class DisplayRateInfo {
    public String amount;
    public String description;
    public String currencyCode;
    public int purpose;
}
