package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */
public class DxImageData {
    public String title;
    public String url;
    public String category;
    public String description;
    public String updatedDate;
    public String[] tags;
    public String type;
    public String id;
    public boolean isDefault;
    public String rawData;
}
