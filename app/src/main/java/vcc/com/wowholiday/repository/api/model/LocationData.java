package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */

public class LocationData {
    public String id;
    public String commonCode;
    public String cultureIrrelevantName;
    public String name;
    public String countryID;
    public String country ;
    public String type;
    public float latitude;
    public float longitude;
    public int priority;
    public String address;
    public String city;
    public String state;
    public String zipCode;
    public String district;
    public DxImageData image;
    //public String flags;

}
