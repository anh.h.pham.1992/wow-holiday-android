package vcc.com.wowholiday.repository.api.model;

import vcc.com.wowholiday.repository.api.model.LocationData;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class LocationInfoData {
    public LocationData fromLocation;
    public LocationData toLocation;
}
