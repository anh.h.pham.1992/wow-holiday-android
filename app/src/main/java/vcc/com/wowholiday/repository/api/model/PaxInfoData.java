package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class PaxInfoData {
    public int type;
    public String typeString;
    public int max;
    public int min;
    public int quantity;
    public DisplayRateInfo[] displayRateInfo;
}
