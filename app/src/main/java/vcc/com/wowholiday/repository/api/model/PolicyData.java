package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class PolicyData {
    public String type;
    public String description;
    public DateInfoData dateInfo;
    public boolean isPortalPolicy;
    public Touple[] criteria;
}
