package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/12/2019.
 */
public class ProfilePictureData {
    public String url;
    public String updatedDate;
    public String isDefault;
}
