package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class UserData {

    public String entityID;
    public String userID;
    public String agentID;
    public String customerID;
    public boolean isNewsLetterSubscription;
    public boolean isActive;
    public ProfilePictureData profilePicture;
    public String memberNumber;
    public String firstName;
    public String middleName;
    public String lastName;
    public int ratePercentage;
    public int discountChangePercentage;
    public int cancelWaiveOffPercentage;
    public LocationData location;
    public ContactInfoData contactInformation;
    public String issuingCountryCode;
    public boolean isSelfSubscribed;
    public int gmtTimeDifference;
    public boolean isHeadOffice;
    public int agentBalance;
    public String milesCard;
    public String milesCardNumber;
    public String crmid;
    public boolean canSendEmail;
    public int age;
    public String nationalityCode;
    public String genderDesc;
    public String gender;
    public String actlGender;
    public String birthDate;
    public int userType;
    public boolean isPortalAdmin;
    public String anniversaryDate;
    public String passportExpirationDate;
    public boolean isCoPAX;
    public int seqNo;
    public int profilePercentage;
    public String createdDate;
    public int loginCount;
    public int totalBookingAmount;
}
