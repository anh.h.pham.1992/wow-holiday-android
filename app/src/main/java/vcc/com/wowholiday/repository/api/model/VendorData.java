package vcc.com.wowholiday.repository.api.model;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class VendorData {
    public String type;
    public Vendor item;

    public static class Vendor {
        public String code;
        public String name;
        public String provider;
    }
}
