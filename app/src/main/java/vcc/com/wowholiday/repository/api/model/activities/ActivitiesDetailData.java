package vcc.com.wowholiday.repository.api.model.activities;

import vcc.com.wowholiday.model.DateInfo;
import vcc.com.wowholiday.model.Location;
import vcc.com.wowholiday.repository.api.model.AmenityData;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.FacilityData;
import vcc.com.wowholiday.repository.api.model.LocationInfoData;
import vcc.com.wowholiday.repository.api.model.PaxInfoData;
import vcc.com.wowholiday.repository.api.model.PolicyData;
import vcc.com.wowholiday.repository.api.model.VendorData;

/**
 * Created by Pham Hai Quang on 12/3/2019.
 */
public class ActivitiesDetailData {
    public String id;
    public String name;
    public float rating;
    public int ratersCount;
    public String status;
    public DateInfoData dateInfo;
    public String description;
    public LocationInfoData locationInfo;
    public DxImageData[] images;
    public String amount;
    public String displayAmount;
    public String originalAmount;
    public String displayOriginalAmount;
    public VendorData[] vendors;
    public AmenityData[] amenities;
    public PolicyData[] policies;
    public FacilityData[] facilities;
    public PaxInfoData[] paxInfo;
}
