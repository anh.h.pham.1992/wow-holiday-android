package vcc.com.wowholiday.repository.api.model.activities;

import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.LocationInfoData;
import vcc.com.wowholiday.repository.api.model.Touple;
import vcc.com.wowholiday.repository.api.model.VendorData;

/**
 * Created by Pham Hai Quang on 11/30/2019.
 */
public class ActivitiesInfoData {
    public String id;
    public String name;
    public int rating;
    public int ratersCount;
    public String url;
    public Touple[] tpExtension;
    public String status;
    public DateInfoData dateInfo;
    public String description;
    public LocationInfoData locationInfo;
    public String amount;
    public String displayAmount;
    public String originalAmount;
    public VendorData[] vendors;
}
