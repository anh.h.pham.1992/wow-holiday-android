package vcc.com.wowholiday.repository.api.model.flight;

import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.LocationInfoData;
import vcc.com.wowholiday.repository.api.model.PaxInfoData;
import vcc.com.wowholiday.repository.api.model.VendorData;
import vcc.com.wowholiday.repository.api.model.Touple;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class FlightData {
    public boolean boardingStatus;
    public int age;
    public String business;
    public String objectIdentifier;
    public int code;
    public String token;
    public Touple[] tpExtension;
    public DateInfoData dateInfo;
    public LocationInfoData locationInfo;
    public DxImageData[] images;
    public PaxInfoData[] paxInfo;
    public float amount;
    public String displayAmount;
    public String strikeThroughAmount;
    public String originalAmount;
    public Touple[] config;
    public VendorData[] vendors;
    public long layOverDuration;
    public long journeyDuration;
}
