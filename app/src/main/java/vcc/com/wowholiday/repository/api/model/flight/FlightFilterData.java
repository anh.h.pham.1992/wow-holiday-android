package vcc.com.wowholiday.repository.api.model.flight;

/**
 * Created by Pham Hai Quang on 11/15/2019.
 */
public class FlightFilterData {
    public String type;
    public RangeList[] minMaxList;
    public String minValue;
    public String maxValue;
    public String name;
    public String[] values;

    public static class RangeList {
        public String minValue;
        public String maxValue;
    }
}
