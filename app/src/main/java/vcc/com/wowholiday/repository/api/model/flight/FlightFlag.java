package vcc.com.wowholiday.repository.api.model.flight;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class FlightFlag {
    public boolean isDomestic;
    public boolean instantBooking;
    public boolean isBookAndPayLater;
    public boolean isPassportMandatory;
}
