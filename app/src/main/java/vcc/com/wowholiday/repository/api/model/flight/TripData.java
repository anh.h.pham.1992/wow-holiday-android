package vcc.com.wowholiday.repository.api.model.flight;

import vcc.com.wowholiday.repository.api.model.DateInfoData;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class TripData {
    public float amount;
    public String displayAmount;
    public float strikeThroughAmount;
    public float originalAmount;
    public DateInfoData dateInfo;
    public boolean hasSpecialDeal;
    public int type;
    public TripProperty properties;
    public FlightData[] item;
}
