package vcc.com.wowholiday.repository.api.model.flight;

import java.io.Serializable;

import vcc.com.wowholiday.repository.api.model.DisplayRateInfo;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.PaxInfoData;
import vcc.com.wowholiday.repository.api.model.VendorData;
import vcc.com.wowholiday.repository.api.model.Touple;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class TripDataSummary implements Serializable {
    public int rating;
    public int ratersCount;
    public TripData[] items;
    public long totalDuration;
    public String ticketTimeLimit;
    public String fareType;
    public String token;
    public String status;
    public String tripType;
    public DateInfoData dateInfo;
    public DxImageData[] images;
    public PaxInfoData[] paxInfo;
    public VendorData[] vendors;
    public String amount;
    public String displayAmount;
    public float strikeThroughAmount;
    public float originalAmount;
    public String displayOriginalAmount;
    public DisplayRateInfo[] displayRateInfo;
    public Touple[] config;
    public FlightFlag flags;
    public int stops;
    public long layOverDuration;
    public long journeyDuration;
}
