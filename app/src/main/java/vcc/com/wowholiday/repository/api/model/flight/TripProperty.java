package vcc.com.wowholiday.repository.api.model.flight;

import vcc.com.wowholiday.repository.api.model.LocationInfoData;

/**
 * Created by Pham Hai Quang on 11/14/2019.
 */
public class TripProperty {
    public LocationInfoData tripLocationDetails;
    public int duration_Hour;
    public int duration_Minute;
}
