package vcc.com.wowholiday.repository.api.model.hotel;

public class BoardTypeData {
    public String id;
    public String type;
    public int priority;
}
