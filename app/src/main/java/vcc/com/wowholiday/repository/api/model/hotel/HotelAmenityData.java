package vcc.com.wowholiday.repository.api.model.hotel;

public class HotelAmenityData {
    public String id;
    public String name;
    public String type;
    public int priority;
    public HotelAmenityFlag flag;
}
