package vcc.com.wowholiday.repository.api.model.hotel;

import vcc.com.wowholiday.repository.api.model.ContactInfoData;
import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.LocationInfoData;
import vcc.com.wowholiday.repository.api.model.Touple;
import vcc.com.wowholiday.repository.api.model.VendorData;

public class HotelDetailData {
    public String id;
    public String name;
    public int rating;
    public int ratingCount;
    public String category;
    public String url;
    public RoomGroupData[] items;
    public int currencyRefCode;
    public DateInfoData dateInfo;
    public String description;
    public LocationInfoData locationInfo;
    public DxImageData[] images;
    public float amount;
    public float originalAmount;
    public float strikeThroughAmount;
    public String displayAmount;
    public String displayOriginalAmount;
    public ContactInfoData contactInformation;
    public VendorData[] vendors;
    public HotelAmenityData[] amenities;
    public int displayPriority;
    public boolean hasSpecialDeal;
    public int viewCount;
    public int quantity;
    public String business;
    public String objectIdentifier;
    public String token;
    public Touple[] tpExtension;
    public String status;






}
