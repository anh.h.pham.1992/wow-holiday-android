package vcc.com.wowholiday.repository.api.model.hotel;


public class HotelFilterData {
    public String type;
    public RangeList[] minMaxList;
    public String minValue;
    public String maxValue;
    public String name;
    public String[] values;
    public String defaultValue;

    public static class RangeList {
        public String minValue;
        public String maxValue;
    }
}
