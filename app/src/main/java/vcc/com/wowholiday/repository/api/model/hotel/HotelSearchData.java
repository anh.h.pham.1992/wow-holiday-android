package vcc.com.wowholiday.repository.api.model.hotel;

import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.LocationInfoData;
import vcc.com.wowholiday.repository.api.model.Touple;
import vcc.com.wowholiday.repository.api.model.VendorData;

public class HotelSearchData {
    public String id;
    public String name;
    public int rating;
    public int ratingCount;
    public String category;
    public String business;
    public String objectIdentifier;
    public String token;
    public Touple[] tpExtension;
    public String status;
    public DateInfoData dateInfo;
    public String description;
    public LocationInfoData locationInfo;
    public DxImageData[] images;
    public float amount;
    public float originalAmount;
    public String displayAmount;
    public VendorData[] vendors;
}
