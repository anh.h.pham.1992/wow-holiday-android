package vcc.com.wowholiday.repository.api.model.hotel;

/**
 * Created by Pham Hai Quang on 11/11/2019.
 */

public class HotelSuggestionData {
    public String id;
    public String name;
    public String countryID;
    public String type;
    public int priority;
}
