package vcc.com.wowholiday.repository.api.model.hotel;

import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.Touple;

public class RoomGroupData {
    public String id;
    public int amount;
    public String displayAmount;
    public int strikeThroughAmount;
    public int originalAmount;
    public DateInfoData dateInfo;
    public boolean hasSpecialDeal;
    public String business;
    public Touple[] tpExtension;
    public String code;
    public RoomInfoData[] item;
}
