package vcc.com.wowholiday.repository.api.model.hotel;

import vcc.com.wowholiday.repository.api.model.DateInfoData;
import vcc.com.wowholiday.repository.api.model.DisplayRateInfo;
import vcc.com.wowholiday.repository.api.model.DxImageData;
import vcc.com.wowholiday.repository.api.model.Touple;
import vcc.com.wowholiday.repository.api.model.VendorData;

public class RoomInfoData {
    public boolean boardingStatus;
    public int age;
    public BoardTypeData[] boardTypes;
    public String name;
    public String business;
    public String objectIdentifier;
    public String code;
    public String token;
    public Touple[] tpExtension;
    public Touple[] secureTPExtension;
    public String status;
    public DateInfoData dateInfo;
    public DxImageData[] images;
    public float amount;
    public float originalAmount;
    public float strikeThroughAmount;
    public String displayAmount;
    public VendorData[] vendors;
    public DisplayRateInfo[] displayRateInfo;
    public Touple[] config;
    public RoomFlag flags;
    public int availabilityCount;
    public HotelAmenityData[] amenities;
    public int displayPriority;
    public boolean hasSpecialDeal;
    public String specialDealID;
    public String specialDealDescription;
    public int viewCount;
    public int quantity;
}
