package vcc.com.wowholiday.repository.api.register;

import android.util.Log;

import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class RegisterRequest extends BaseApiRequest {
    public String authen;
    public String phoneNumber;
    public String email;
    public String password;
    public String firstName;
    public String lastName;
    public String gender;

    @Override
    public JsonObject toJsonBody() {
        JsonObject body = new JsonObject();
        body.addProperty("LoginName", lastName + " " + firstName);
        body.addProperty("FirstName", firstName);
        body.addProperty("LastName", lastName);
        body.addProperty("Password", password);

        JsonObject contact = new JsonObject();
        contact.addProperty("Name", lastName + " " + firstName);
        if (phoneNumber == null) {
            contact.addProperty("Email", email);
        } else {
            contact.addProperty("PhoneNumber", phoneNumber);
        }

        body.add("ContactInformation", contact);
        body.addProperty("Gender", gender);

        JsonObject loc = new JsonObject();
        loc.addProperty("CountryID", "VN");
        loc.addProperty("Country", "Vietnam");
        body.add("Location", loc);

        JsonObject request = new JsonObject();
        request.add("Request", body);
        Log.e("LoginRq", request.toString());
        return request;
    }
}
