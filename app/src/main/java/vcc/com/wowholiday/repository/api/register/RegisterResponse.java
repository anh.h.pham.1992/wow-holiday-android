package vcc.com.wowholiday.repository.api.register;

import vcc.com.wowholiday.repository.api.BaseApiResponse;
import vcc.com.wowholiday.repository.api.model.UserData;

/**
 * Created by Pham Hai Quang on 10/16/2019.
 */
public class RegisterResponse extends BaseApiResponse<UserData> {
}
