package vcc.com.wowholiday.repository.api.updateprofile;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import vcc.com.wowholiday.repository.api.BaseApiRequest;
import vcc.com.wowholiday.repository.api.model.UserData;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class UpdateProfileRequest extends BaseApiRequest {
    public UserData user;
    public String authen;

    @Override
    public JsonObject toJsonBody() {
        JsonObject body = new JsonObject();
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(user);
        body.add("Request", element);
        return body;
    }
}
