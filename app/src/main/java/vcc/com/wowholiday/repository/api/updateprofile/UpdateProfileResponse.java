package vcc.com.wowholiday.repository.api.updateprofile;

import vcc.com.wowholiday.repository.api.BaseApiResponse;

/**
 * Created by Pham Hai Quang on 11/13/2019.
 */
public class UpdateProfileResponse extends BaseApiResponse<Boolean> {
}
