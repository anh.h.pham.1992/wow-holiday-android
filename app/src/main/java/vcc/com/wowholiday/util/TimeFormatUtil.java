package vcc.com.wowholiday.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import vcc.com.wowholiday.R;
import vcc.com.wowholiday.WHApplication;

/**
 * Created by Pham Hai Quang on 10/13/2019.
 */
public class TimeFormatUtil {
    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    public static final String FULL_DEFAULT_DATE_FORMAT = "dd/MM/yyyy hh:mm";
    public static final String DEFAULT_TIME_FORMAT = "hh:mm";
    public static final String DEFAULT_DAY_FORMAT = "dd/MM";

    public static String getDefaultDateFormat(Calendar calendar) {
        SimpleDateFormat format = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        return format.format(calendar.getTime());
    }

    public static String getDefaultDateFormat(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        return format.format(date);
    }

    public static String getDefaultDateFormat(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        return getDefaultDateFormat(cal);
    }

    public static long getMillisFromDefaultDateFormat(String date) {
        DateFormat df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        try {
            return df.parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getDayOfWeekDateFormat(Calendar calendar) {
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return getNameDayOfWeek(day) + ", " + getDefaultDateFormat(calendar);
    }

    public static String getNameDayOfWeek(int dayOfWeek) {
        WHApplication app = WHApplication.getInstance();
        switch (dayOfWeek) {
            case 1:
                return app.getString(R.string.sunday);
            case 2:
                return app.getString(R.string.monday);
            case 3:
                return app.getString(R.string.tuesday);
            case 4:
                return app.getString(R.string.wednesday);
            case 5:
                return app.getString(R.string.thursday);
            case 6:
                return app.getString(R.string.friday);
            case 7:
                return app.getString(R.string.saturday);
        }
        return  "";
    }

    public static String toISO8601UTC(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public static Date fromISO8601UTC(String dateStr) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(tz);

        try {
            return df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static long fromISO8601UTCToMillis(String dateStr) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(tz);

        try {
            return df.parse(dateStr).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static String getDefaultDateFormatFromISO8601UTC(String dateStr) {
        long millis = fromISO8601UTCToMillis(dateStr);
        Date date = new Date();
        date.setTime(millis);
        SimpleDateFormat format = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }

    public static String getFullDateFormatFromISO8601UTC(String dateStr) {
        long millis = fromISO8601UTCToMillis(dateStr);
        Date date = new Date();
        date.setTime(millis);
        SimpleDateFormat format = new SimpleDateFormat(FULL_DEFAULT_DATE_FORMAT);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }

    public static int[] getHourDuration(long millis) {
        int min = (int) (((millis/1000) / 60) % 60);
        int hour = (int) (((millis/1000) / 60) / 60);
        return new int[]{hour, min};
    }

    public static String getDayOfWeekDateFromISO8601UTC(String dateStr) {
        long millis = fromISO8601UTCToMillis(dateStr);
        Date date = new Date();
        date.setTime(millis);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_WEEK);
        return getNameDayOfWeek(day) + ", " + getDefaultDateFormat(cal);
    }

    public static String getDefaultTimeFormatFromISO8601UTC(String dateStr) {
        long millis = fromISO8601UTCToMillis(dateStr);
        Date date = new Date();
        date.setTime(millis);
        SimpleDateFormat format = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }

    public static String getDefaultDayFormatFromISO8601UTC(String dateStr) {
        long millis = fromISO8601UTCToMillis(dateStr);
        Date date = new Date();
        date.setTime(millis);
        SimpleDateFormat format = new SimpleDateFormat(DEFAULT_DAY_FORMAT);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }
}
